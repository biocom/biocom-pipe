#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function dedicated to clean the file name given as input and returon only the sample name important
# for the user as output. This name will be added to Summary files to clarify user reading.
sub ExtractSampleFromFileName
{
	#Needed variables
	my $input_filename = $_[0];
	#Cleaning all prefixes added to the filename to keep only the sample name
	$input_filename =~ s/FilterAlign_//;
	$input_filename =~ s/Derep_//g;		$input_filename =~ s/Rdm_//g;
	$input_filename =~ s/Hpreprocess_//;	$input_filename =~ s/Mpreprocess_//;
	$input_filename =~ s/Lpreprocess_//;	#$input_filename =~ s/Align_//;
	$input_filename =~ s/PrinSeq_//;		$input_filename =~ s/FLASH_//;
	$input_filename =~ s/Clust_//;		$input_filename =~ s/Clean_//;
	$input_filename =~ s/Hunt_//;			$input_filename =~ s/Qual_//;
	$input_filename =~ s/Taxo_//;			$input_filename =~ s/SILVA_//;
	$input_filename =~ s/RDP_//;
	#Cleaning also the suffixes from file types
	$input_filename =~ s/\.fasta//;		$input_filename =~ s/\.clust//;
	return($input_filename);
}
# End of the function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#=======================================================================================================
#         FILE:  Globanalysis_tree_mapping_computation_1.6.pl
#        USAGE:  perl Globanalysis_tree_mapping_computation_1.6.pl
#	ARGUMENTS:  - Clustering choice [Raw or Clean],
#			- Clustering input folder [FOLDER_NAME],
#			- Dissimilarity percentage of clustering [0-100],
#			- Alignment folder input [FOLDER_NAME],
#			- Choice [yes or no] of the selection of one representative read of each OTU.
#
#  DESCRIPTION:  This PERL program was developed to treat the global alignement realized with the
#  concatenated reads file to realize a phylogenetic tree with the FastTree (v2.1.10)
#  program implemented by Price M.N. et al. To do this, we store all the alignment, we then
#  delete the not needed parts (at the beginning and at the end), and we launch the
#  FastTree program.
#  We also define an ID mapping file to link each read to each sample and also to the
#  number of corresponding unique reads.
#
#  1.5 Modifications (30/04/2013): We add some parts and arguments to let the user chose if
#  we use all reads to define the phyogenetic tree, or the most abundant read (ie the seed)
#  of all defined OTUs. This method fast the analysis, as a lower number of reads are used
#  to define the phylogenetic tree. See details in the program.
#
#  2.0 Modifications (03/05/2018): We integrate some modifications to fast the treatment of the files
#  using the esl-alimerge program (see below). We also integrate a new version of FastTree (v2.1.10) to
#  fast the analysis and the realization of the phylogenetic tree
#
# REQUIREMENTS:  Perl language only
#       AUTHOR:  Sebastien TERRAT <sebastien.terrat@inra.fr>
#      COMPANY:  UMR 1347 INRA/uB/AgroSup - BIOCOM Team
#      VERSION:  2.0
#    REALEASED:  21/04/2013
#     REVISION:  03/05/2018
#=======================================================================================================
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#=======================================================================================================

#!/usr/bin/perl

#Print errors and warnings during the implementation of the perl program
use strict;
use warnings;
use FindBin;

#Defined variables
my $file_info = "Concatenated_files.txt";
my $file_derep = "Derep_details_Concatenated_reads.fasta.fasta";
my $file_clust = "";
my $file_name = "";
my $file = "";
my $read = "";
my $ID = "";
my $FastTree_result = "";
my $key = "";
my $part_deleted_start = -1;
my $part_deleted_end = -1;
my @tmp = ();
my @tmp2 = ();
my @aligned_reads = ();
my @file_info_clean = ();
my @file_IDs = ();
my %Aligned_reads = ();
my %Picked_read_OTU = ();
my %Derep_info = ();
my $verif = 0;
my $count = 0;
my $i = 0;

my $launch = "";

#1.6: Here, we define the absolute path to find the FastTree program stored efficiently to launch
#needed analyses.
my $folder_Tools = "$FindBin::RealBin/../Data/Tools";

#Needed variables to store all parameters given as input by the main program
my $user_clustering_choice = $ARGV[0];
my $folder_clustering = $ARGV[1];
my $clustering_cutoff = $ARGV[2];
my $folder_alignment = $ARGV[3];
my $unifrac_otu_picking_choice = $ARGV[4];

#=======================================================================================================
#
#=======================================================================================================
#1.5: We treat the clustering OTU picking only when the user chose to do it
if (uc($unifrac_otu_picking_choice) eq "YES")
{
	#1.5: Here, we first search for the clustering file for the concatenated reads
	opendir (DIR, "Result_files/$folder_clustering/") || die "can't opendir Result_files/$folder_clustering/: $!";
	#1.5: To do this, we search for each file name and if we found it, we store it in $file_clust
	LOOP00:while (defined($file = readdir(DIR))) { if ($file =~ m/Concatenated\_reads\.fasta\.fasta/) { $file_clust = $file; } }
	#1.5: We finally close the directory
	closedir (DIR);

	#1.5: The next step is to open the clustering file containing needed data to treat them
	open(F_CLUST,"Result_files/$folder_clustering/$file_clust") || die "==> File Not Found: $file_clust in $folder_clustering directory <==";
	#1.5: We read each line of the file
	LOOP00:while(<F_CLUST>)
	{
		#1.5: We search for the good clustering step based on the given cutoff
		if ($_ =~ m/distance cutoff\:\t(.*)\n/) { if ($1 == $clustering_cutoff) { $verif = 1; } }
		#1.5: If we read the good clustering step
		if ($verif == 1)
		{
			#1.5: We search for all clusters (starting by a number)
			if ($_ =~ m/^\d/)
			{
				#1.5: We catch the descriptive line of each cluster
				$_ =~ m/\n/g;
				@tmp = split("\t", $_);
				#1.5: Based on this descriptive line, we catch the seed of the OTU (the most abundant read)
				$tmp[3] =~ m/([A-Za-z0-9\_\|=]*)\s/;
				#1.5: We used this seed as key for our hash table, and as value the descriptive line
				$Picked_read_OTU{$1} = $tmp[3];
			}
			#1.5: We found the end of the clustering step, we stop the analysis here and increase the verif value
			#1.5: to block further readings
			elsif ($_ eq "\n") { $verif++; last LOOP00; }
		}
	}
	#1.5: We close the clustering file
	close (F_CLUST);
}

#=======================================================================================================
# First step, we open and catch the concatenated alignment sequences to treat them efficiently. To do
# this, we first search the alignement file, treat it using the esl-alimerge program to obtain the
# alignment in afa format and then store the alignment in the @aligned_reads array. Moreover, we catch
# and store also the descriptive lines of all reads in the %Aligned_reads hash table.
#=======================================================================================================

#Then, we open the defined directory to find the file dedicated to the Concatenated reads
opendir (DIR, "Result_files/$folder_alignment/") || die "can't opendir Result_files/$folder_alignment/: $!";
#We search and store the name of the Concatenated reads file
while (defined($file = readdir(DIR))) { if ($file =~ m/Concatenated\_reads\.fasta\.fasta/) { $file_name = $file; last; } }
closedir (DIR);

#2.0: We create a temporay list file with only one file to launch the esl-alimerge program
open(LIST, ">Temporary_files/List_file") || die "Can't create the temporary list file to define efficiently a global alignment\n";
printf(LIST "Result_files/$folder_alignment/$file_name\n");
close(LIST);
#2.0: We then launch the program itself to format the alignment in afa format
$launch = `esl-alimerge --outformat afa -o Temporary_files/ALIGN_TMP --list Temporary_files/List_file`;
#2.0: We the delete the list file, as we not need it anymore
unlink("Temporary_files/List_file");
#1.5: Needed empty variable as we will use it in the next loop treatment
$verif = 0;
#2.0: Here, we will then open and read the temporary alignment file in afa format
open (F1,"Temporary_files/ALIGN_TMP") || die "==> File Not Found: Temporary_files/ALIGN_TMP<==\n";
#1.5:  We treat the alignment and select only the seeds when the user chose to do it
if (uc($unifrac_otu_picking_choice) eq "YES")
{
	#For each line in the alignment file
	while(<F1>)
	{
		#If the line is the descriptive line
		if($_ =~ m/^>/)
		{
			#1.5: We first delete all not needed characters
			$_ =~ s/\n//;		$_ =~ s/^>//;
			#1.5: We verify the existence of the seed (based on keys existence)
			if (exists($Picked_read_OTU{$_}))
			{
				#We keep the other descriptive lines in the temporary file
				push(@aligned_reads, ">$_\n");
				#We finally reinitialize the $verif variable to avoid the consensus,
				#the structure lines from Infernal and not wanted reads (as we take only
				#the seed of each OTU).
				$verif = 0;
			}
			#1.5 : We need to put the $verif to 1 for each not needed read and fast the analysis
			else { $verif = 1; }
		}
		#We print all the sequences lines, without the consensus and structure lines from Infernal
		else { if($verif == 0) { chomp($_); $aligned_reads[$#aligned_reads] .= $_; } }
	}
}
#1.5: We treat all reads from the multiple alignment
else
{
	while(<F1>)
	{
		#If the line is the descriptive line
		if($_ =~ m/^>/)
		{
			#We keep the other descriptive lines in the temporary file
			push(@aligned_reads, $_);
			#We also treat the descriptive line to store it in the hash table %Aligned_reads
			#to create in further steps the ID mapping file accompanying the phylogenetic tree.
			$_ =~ s/\n//g;		$_ =~ s/^>//;
			$Aligned_reads{$_}++;
			#We finally reinitialize the $verif variable to avoid the the consensus and structure
			#lines from Infernal
			$verif = 0;
		}
		#We print all the sequences lines, without the consensus and structure lines from Infernal
		else { if($verif == 0) { chomp($_); $aligned_reads[$#aligned_reads] .= $_; } }
	}
}
#We close and delete the temporary alignment file
close F1;
unlink("Temporary_files/ALIGN_TMP");

#=======================================================================================================
# Second step, we treat each aligned read stored in the @aligned_reads array to define what part of the
# alignment we can delete (the beginning and the end, as these parts are generally identical between all
# analyzed reads.
#=======================================================================================================

#Now, for each read found in the multiple alignment (ID and sequence itself)
LOOP02:foreach $read (@aligned_reads)
{
	#We first split the ID from the sequence itself
	@tmp = split("\n", $read);
	#We then split and store in @tmp2 each character of the aligned read
	@tmp2 = split("", $tmp[1]);
	#Here, we count the number of "-" characters found at the beginning of each aligned read
	#We keep the lowest value found, corresponding to the part of the alignement that can be deleted
	while($tmp2[$i] eq "-") { $i++; }
	if ($part_deleted_start == -1) { $part_deleted_start = $i; next; }
	else { if($part_deleted_start > $i) { $part_deleted_start = $i; next; } }
	$i = 0;
	#We do the same thing for the end of each read as described just before
	while($tmp2[$#tmp2 - $i] eq "-") { $i++; }
	if ($part_deleted_end == -1) { $part_deleted_end = $i; next; }
	else { if($part_deleted_end > $i) { $part_deleted_end = $i; next; } }
	$i = 0;
}

#Then, again for each aligned read, we will keep only the multiple alignment part between the
#$part_deleted_start and $part_deleted_end
LOOP03:foreach $read (@aligned_reads)
{
	#We first split the line to obtain the descriptive line and the alignment sequence in @tmp
	@tmp = split("\n", $read);
	#If we did not delete the end of the alignment, we just avoid one problem with this test and keep the
	#good part of the alignment.
	if ($part_deleted_end == 0) { $read = $tmp[0]."\n".substr($tmp[1], $part_deleted_start)."\n"; $read =~ s/\./\-/g; }
	else { $read = $tmp[0]."\n".substr($tmp[1], $part_deleted_start, -$part_deleted_end)."\n"; $read =~ s/\./\-/g; }
}
#We also create the temporary file to store the cleaned alignment file without specificities from the
#Infernal alignment (e.g. the #=GC_SS_cons and the #=GC_RF lines).
open (F_OUT, ">Temporary_files/TMP_$file_name") || die "==> Can't create file : Temporary_files/TMP_$file_name<==";
printf(F_OUT join("", @aligned_reads));
close F_OUT;

#=======================================================================================================
# Third step, we launch the FastTree program itself to realize the phylogenetic tree with needed
# arguments and defined parameters already evaluated and optimized.
#=======================================================================================================

#Then, we launch the FastTree program (stored in the Prog_Perl folder), with these options:
#	-nt (indicate that we give as input a multiple nucleotide alignment,
#	-spr 4 (to define the number of rounds of SPR (higher phylogenetic tree quality),
#	-mlacc 2 (mlacc 2 or -mlacc 3 to always optimize all 5 branches at each NNI, and to optimize all 5 branches in 2 or 3 rounds), higher phylogenetic tree quality,
#	-slownni (to turn off heuristics to avoid constant subtrees (affects both ML and ME NNIs)), higher phylogenetic tree quality,
#	-quiet (do not write to standard error during normal operation (no progress indicator, no options summary, no likelihood values, etc.).
$file = $user_clustering_choice."_Tree_Concatenated_reads.fasta.fasta";
$FastTree_result = `$folder_Tools/FastTree/FastTreeMP-2.1.10 -nt -spr 4 -mlacc 2 -slownni -quiet Temporary_files/TMP_$file_name > Summary_files/Global_Analysis/Unifrac_data/$file`;

#Needed empty variables
@aligned_reads = ();
#We finally delete the temporary file as we did not need it anlymore.
unlink("Temporary_files/TMP_$file_name");

#=======================================================================================================
# Fourth step, we catch and treat all sample names to design efficiently the ID mapping file. To do
# this, we use the elements in the Concatenated_files.txt stored previously
#=======================================================================================================

#We first open the Concatenated_files.txt in the Global_Analysis directory
open(F1,"Summary_files/Global_Analysis/$file_info") || die "==> File Not Found: $file_info<==";
#For each line found in the file
LOOP04:while(<F1>)
{
	#We delete the "\n" character
	$_ =~ s/\n//g;
	#We then split the elements in the file (number ID and file name).
	@tmp = split("\t", $_);
	#We treat the file name
	$tmp[1] = &ExtractSampleFromFileName($tmp[1]);
	$tmp[1] =~ s/\_/\#/g;
	#We keep in two different arrays the file names and the IDs.
	push(@file_info_clean, $tmp[1]);
	push(@file_IDs, $tmp[0]);
}
#We finally close the Concatenated_files.txt
close F1;

#=======================================================================================================
# Fifth step, we define the ID mapping file using needed information to launch efficiently the
# Unifrac analysis.
#=======================================================================================================

#Here, we open the dereplication details file for the concatenated reads
open(F_DEREP,"Summary_files/Global_Analysis/$file_derep") || die "==> File Not Found: $file_derep<==";
#For each line found in the file
LOOP04:while(<F_DEREP>)
{
	#We first delete the "\n" character
	$_ =~ s/\n//;
	#We then split the line to separate the dereplicated read from all unique corresponding reads
	@tmp = split("\t", $_);
	$Derep_info{$tmp[0]} = $tmp[1];
}
#We finally close the dereplication file
close F_DEREP;

#We first create the ID mapping file needed from the Unifrac analysis
$file = $user_clustering_choice."_ID_mapping_file.txt";
open(F_MAPPING, ">Summary_files/Global_Analysis/Unifrac_data/$file")|| die "==> Can't create file ID_mapping_file.txt <==";
if (uc($unifrac_otu_picking_choice) eq "YES")
{
	foreach $key (keys(%Picked_read_OTU))
	{
		@tmp = split(" ", $Picked_read_OTU{$key});
		foreach $ID (@tmp) {if(exists($Derep_info{$ID})) { $ID = $Derep_info{$ID}; } }

		$ID = join(" ", @tmp);
		$ID =~ s/  / /;
		#We will treat the dereplicated reads based on each file pooled
		for($i=0;$i<=$#file_IDs;$i++)
		{
			#Goatse system to count the number of occurrences in the line (PERL system)
			#The idea is that the expression at the right in the line to be evaluated by
			#the regular expression in array context. Assigning a regex to an empty list
			#forces the list context. Then, the list is assigned to a scalar, to force
			#the scalar context again.
			#The key thing to remember is that a list assignment in scalar context returns
			#the number of elements on the right-hand side of the list assignment (here,
			#the number of elements corresponding to the $file_IDs[$i] ^^
			$count = () = $ID =~ m/\Q$file_IDs[$i]|/g;
			#If the $cunt is higher than 0, we write the result in the ID mapping file
			if ($count > 0) { printf(F_MAPPING "$key\t$file_info_clean[$i]\t$count\n"); }
		}
	}
}
else
{
	foreach $key (keys(%Aligned_reads))
	{
		if(exists($Derep_info{$key})) { $ID = $Derep_info{$key}; }
		else { $ID = $key; }
		$ID =~ s/  / /;
		#We will treat the dereplicated reads based on each file pooled
		for($i=0;$i<=$#file_IDs;$i++)
		{
			#Goatse system to count the number of occurrences in the line (PERL system)
			#The idea is that the expression at the right in the line to be evaluated by
			#the regular expression in array context. Assigning a regex to an empty list
			#forces the list context. Then, the list is assigned to a scalar, to force
			#the scalar context again.
			#The key thing to remember is that a list assignment in scalar context returns
			#the number of elements on the right-hand side of the list assignment (here,
			#the number of elements corresponding to the $file_IDs[$i] ^^
			$count = () = $ID =~ m/\Q$file_IDs[$i]|/g;
			#If the $cunt is higher than 0, we write the result in the ID mapping file
			if ($count > 0) { printf(F_MAPPING "$key\t$file_info_clean[$i]\t$count\n"); }
		}
	}
}
#To be sure, we close again the mapping file
close F_MAPPING;
#We exit properly the program
exit(0);
