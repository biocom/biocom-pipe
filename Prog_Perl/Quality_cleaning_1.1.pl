#=======================================================================================================
#         FILE:  Quality_cleaning_1.1.pl
#        USAGE:  perl Quality_cleaning_1.1.pl Arguments
#  DESCRIPTION:  This program was defined to clean obtained reads based on their quality scores,
#	particularly based on two user parameters: the lowest quality score tolerated and also
#	the lowest average quality score chosen. This program will first determine the quality
#	of the dataset, and then, based on chosen parameters, will clean the sequences. Then,
#	it will compute the quality of the cleaned dataset in a second summary file.
#
#		1.1 Modifs (07/08/2018): We modified the program to manage the fact that a sample can be
#	empty due to the absence of reads with the good MID. To manage that, the file name will
#	be empty and will be not treated anymore if the number of reads is equal to 0.
#
# REQUIREMENTS:  Perl language only
#       AUTHOR:  Sebastien TERRAT <sebastien.terrat@inra.fr>
#      COMPANY:  UMR 1347 Agroecologie - BIOCOM Team
#      VERSION:  1.1
#    REALEASED:  20/12/2013
#     REVISION:  07/08/2018
#=======================================================================================================
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#=======================================================================================================

#Print errors and warnings during the implementation of the perl program
use strict;
use warnings;
use List::Util qw(min sum);

#Defined variables
my $nbseq_tot = 0;
my ($min,$avg,$i,$j,$val) = (0,0,0,0,0);
my $tmp_value = 0;
my $nb_raw_reads = 0;
my $nb_del_reads_lowest = 0;
my $nb_del_reads_avg = 0;

my $line = "";
my $descr_line = "";
my $key = "";
my $range = "";

my %Qual = ();	my %Fasta = ();
my @qual_rawdata = ();
my @val20 = ();	my @val25 = ();	my @val30 = ();
my @val35 = ();	my @val40 = ();	my @tot = ();
my @tmp = ();

#Modifications to catch needed information given by the workflow
my $file = $ARGV[0];
my $lowestQ = $ARGV[1];
my $lowest_avgQ = $ARGV[2];
my $folder_input = $ARGV[3];
my $folder_output = $ARGV[4];

#=======================================================================================================
# Defined functions needed for the main program
#=======================================================================================================

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function used to define, based on the quality average of 25 bases the group of the sequence, to count
# the number of perfect, good, medium and bad sequences
sub EvalQualZone
{
	#Needed variables (empty, or given by the main program)
	my @tmp = ();
	my $avg = 0;
	my $i = $_[1];
	
	#We split the given string based on spaces
	@tmp = split(" ", $_[0]);
	#We compute the quality average
	$avg = sum(@tmp)/($#tmp+1);
	#We evaluate the quality of the read
	if ($avg < 20) { $val20[$i]++; }
	elsif (($avg >= 20)&&($avg < 25)) { $val25[$i]++; }
	elsif (($avg >= 25)&&($avg < 30)) { $val30[$i]++; }
	elsif (($avg >= 30)&&($avg < 35)) { $val35[$i]++; }
	else { $val40[$i]++; }
}
# End of the function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function used to format the obtained results in a summary file for the user
sub FormatResults
{
	#Needed variables (empty, or given by the main program)
	my @tmp = split(" ", $_[0]);
	my $i = $_[1];
	my $tmp_value = 0;
	my ($avg, $Q1, $Q3) = (0,0,0);
	
	#Before the computation of the output string, we have to test if the array is defined or not
	if (not defined($val20[$i])) { $val20[$i] = 0;}
	if (not defined($val25[$i])) { $val25[$i] = 0;}
	if (not defined($val30[$i])) { $val30[$i] = 0;}
	if (not defined($val35[$i])) { $val35[$i] = 0;}
	if (not defined($val40[$i])) { $val40[$i] = 0;}
	
	#We compute the quality average
	$avg = sum(@tmp) / ($#tmp+1);
	#We also organize the array score of the sequence 
	@tmp = sort { $a <=> $b } @tmp;
	#We define the position of the Q1 value
	$tmp_value = int(($#tmp+1)/4);
	$Q1 = $tmp[$tmp_value];
	#We define the position of the Q3 value
	$tmp_value = 3*$tmp_value;
	$Q3 = $tmp[$tmp_value];
	#We return the computed result in one string
	return("$avg\t$tmp[0]\t$Q1\t$Q3\t$tmp[$#tmp]\t$val40[$j]\t$val35[$j]\t$val30[$j]\t$val25[$j]\t$val20[$j]\t$tot[$j]\n");	
}
# End of the function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#=======================================================================================================
# First step: we open and store all needed data form the two files (.qual and .fasta or .fna)
#=======================================================================================================

#Test to know if the input folder is the "Raw_data" or a folder previously created by the main 
#program and a previous treatment step
if ($folder_input eq "None")
{
	open(F_FASTA, "Raw_data/$file")|| die "==> File Not Found: $file<==";
	#We then modify the name of the file to open the sequence file
	$file =~ s/\.fna//;
	$file =~ s/\.fasta//;
	#Open file one given by the user and containing quality sequences
	open(F_QUAL, "Raw_data/$file".".qual")|| die "==> File Not Found: $file<==";
}
else
{
	open(F_FASTA, "Result_files/$folder_input/$file")|| die "==> File Not Found: $folder_input/$file<==";
	#We then modify the name of the file to open the sequence file
	$file =~ s/\.fna//;
	$file =~ s/\.fasta//;
	#Open file one given by the user and containing quality sequences
	open(F_QUAL, "Result_files/$folder_input/$file".".qual")|| die "==> File Not Found: Result_files/$folder_input/$file".".qual<==";
	#We then modify the name of the file to open the sequence file
}

#We first read the quality file in FASTA format
LOOP01:while(<F_QUAL>)
{
	#If we found the descriptive line, we create the $descr_line
	if ($_ =~ m/>/) { $descr_line = $_; }
	#If the line is not a descr one, we add the info to the hash table %Qual
	else
	{
		chomp($_);
		$_ = $_." ";
		$Qual{$descr_line} .= $_;
	}
}
#We close the quality file
close F_QUAL;

#We then read the fasta file in FASTA format
LOOP02:while(<F_FASTA>)
{
	#If we found the descriptive line, we create the $descr_line and store the name of the read
	if ($_ =~ m/>/)
	{
		$descr_line = $_;
		$Fasta{$_} = $_;
	}
	#If the line is the sequence itself, we add the info to the hash table %Fasta
	else { $Fasta{$descr_line} .= $_; }
}
#We finally close the F_FASTA file.
close F_FASTA;

#=======================================================================================================
# Second step, we will evaluate the quality of the raw dataset using given reads and quality.
#=======================================================================================================

#For each quality sequence found in the file
LOOP03:foreach $key (keys(%Qual))
{
	#Needed empty variables
	$i = 0;	$j = 0;	$range = "";
	#We split the quality sequence
	@tmp = split(" ", $Qual{$key});
	#For each quality score in the sequence
	LOOP04:foreach $val (@tmp)
	{
		#We analyze each range of 25 bases in the complete sequence
		if (($i % 25 == 0)&&($i != 0))
		{
			#We count the number of analyzed reads
			$tot[$j]++;
			#We launch the analysis using the defined function EvalQualZone
			&EvalQualZone($range, $j);
			#We store scores to compute min, max and quartiles in the next step
			$qual_rawdata[$j] .= $range." ";
			#We start the new range of 25 bases
			$range = $val;
			$j++;
		}
		#If we are into the 25 bases, we just store quality values into needed tables
		elsif ($i == 0) { $range = $val; }
		else { $range .= " ".$val; }
		$i++;
	}
	#We count the number of analyzed reads
	$tot[$j]++;
	#We launch the analysis using the defined function EvalQualZone
	&EvalQualZone($range, $j);
	#We store scores to compute min, max and quartiles in the next step
	$qual_rawdata[$j] .= $range." ";
	
}

#We found the number of analyzed reads
$nbseq_tot = keys(%Qual);
$nb_raw_reads = keys(%Qual);
#We open the result file and write the information (in the defined directory)
open(F_OUT, ">Summary_files/Quality_Cleaning/Raw_Qual_"."$file.txt")|| die "==> Can't create: Raw_Qual_$file.txt in /Summary_files directory<==";
#We print for the user needed information
printf (F_OUT "Total number of reads analyzed:\t$nbseq_tot\n");
printf (F_OUT "Std\tAverage\tMin\tMax\tQ1\tQ3\t40-35\t35-30\t30-25\t25-0\tTOTAL\n");

$i = 0;

#Loop to treat all obtained results
LOOP05:for ($j = 0; $j <= $#qual_rawdata; $j++)
{
	#We first compute and organize data using the function FormatResults
	$line = &FormatResults($qual_rawdata[$j], $j);
	#We then print in the result file needed results
	printf(F_OUT "$i-");
	$i = $i + 25;
	printf(F_OUT "$i\t$line");
}
#We close the output file
close(F_OUT);

#Needed empty variables to realize the next steps
@val20 = ();	@val25 = ();	@val30 = ();
@val35 = ();	@val40 = ();	@tot = ();
$range = 0;	$val = 0;
@qual_rawdata = ();

#=======================================================================================================
# Third step, we then treat data based on user chosen parameters (lowestQ and lowest_avgQ).
#=======================================================================================================

#Needed empty variable
$nbseq_tot = 0;
#We open output result files
open(F_OUT_FNA, ">Result_files/$folder_output/Qual_"."$file.fasta")|| die "==> Can't create: Qual_$file.fasta in /Result_files directory<==";
open(F_OUT_QUAL, ">Result_files/$folder_output/Qual_"."$file.qual")|| die "==> Can't create: Qual_$file.qual in /Result_files directory<==";

#For each quality sequence found in the file
LOOP06:foreach $key (keys(%Qual))
{
	#Needed empty variables
	$i = 0;	$j = 0;
	$range = "";
	#We split the quality sequence
	@tmp = split(" ", $Qual{$key});
	#We compute the average and the min of the sequence
	$avg = sum(@tmp)/($#tmp+1);
	$min = min(@tmp);
	
	#Test to define if we keep the sequence based on its quality or not
	
	if ($min < $lowestQ) { $nb_del_reads_lowest++; next LOOP06; }
	elsif ($avg < $lowest_avgQ) { $nb_del_reads_avg++; next LOOP06; }
	#If the sequence is kept
	else
	{
		#We first store in result files the sequence and its quality
		printf(F_OUT_QUAL "$key$Qual{$key}\n");
		printf(F_OUT_FNA $Fasta{$key});
		#We count the number of kept reads
		$nbseq_tot++;
		#For each quality score in the sequence
		LOOP07:foreach $val (@tmp)
		{
			#We determine for each range of 25 bases some details
			if (($i % 25 == 0)&&($i != 0))
			{
				#We count the number of analyzed reads
				$tot[$j]++;
				#We launch the analysis using the defined function EvalQualZone
				&EvalQualZone($range, $j);
				#We store scores to compute min, max and quartiles in the next step
				$qual_rawdata[$j] .= $range." ";
				#We start the new range of 25 bases
				$range = $val;
				$j++;
			}
			#If we are into the 25 bases, we just store quality values into needed tables
			elsif ($i == 0) { $range = $val; }
			else { $range .= " ".$val; }
			$i++;
		}
		#We count the number of analyzed reads
		$tot[$j]++;
		#We launch the analysis using the defined function EvalQualZone
		&EvalQualZone($range, $j);
		#We store scores to compute min, max and quartiles in the next step
		$qual_rawdata[$j] .= $range." ";
	}
}
#We close result files
close(F_OUT_FNA);
close(F_OUT_QUAL);

#We open the result file and write the information (in the defined directory)
open(F_OUT2, ">Summary_files/Quality_Cleaning/Clean_Qual_"."$file.txt")|| die "==> Can't create: Clean_Qual_$file.txt in /Summary_files directory<==";
#We print it for the user
printf (F_OUT2 "Total number of reads analyzed:\t$nbseq_tot\n");
printf (F_OUT2 "Std\tAverage\tMin\tMax\tQ1\tQ3\t40-35\t35-30\t30-25\t25-0\tTOTAL\n");

#We then print in the summary file the obtained results
open (F_Summary, ">>Summary_files/Quality_Cleaning.txt")|| die "==> Can't open summary file of the Quality_Cleaning step <==";
flock(F_Summary, 2);
printf(F_Summary "$file\t$nb_raw_reads\t$nb_del_reads_lowest\t$nb_del_reads_avg\t$nbseq_tot\n");
flock(F_Summary, 8);
close (F_Summary);

#Needed empty variable
$i = 0;
#Loop to format the results and give a good summary file for the user
LOOP08:for ($j = 0; $j <= $#qual_rawdata; $j++)
{
	#We first compute and organize data using the function FormatResults
	$line = &FormatResults($qual_rawdata[$j], $j);
	#We then print in the result file needed results
	printf(F_OUT2 "$i-");
	$i = $i + 25;
	printf(F_OUT2 "$i\t$line");
}
#We finally close the summary file
close (F_OUT2);
#1.1: We then print the file name for the main program, taking into account that
#the file can be empty (if it's the case, the file name will be empty)
if($nbseq_tot == 0) { print ""; }
else { print "Qual_"."$file.fasta"; }
#We finally exit the program
exit(0);
