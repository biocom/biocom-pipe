#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function dedicated to clean the file name given as input and returon only the sample name important
# for the user as output. This name will be added to Summary files to clarify user reading.
sub ExtractSampleFromFileName
{
	#Needed variables
	my $input_filename = $_[0];
	#Cleaning all prefixes added to the filename to keep only the sample name
	$input_filename =~ s/FilterAlign_//;
	$input_filename =~ s/Derep_//g;		$input_filename =~ s/Rdm_//g;
	$input_filename =~ s/Hpreprocess_//;	$input_filename =~ s/Mpreprocess_//;
	$input_filename =~ s/Lpreprocess_//;	#$input_filename =~ s/Align_//;
	$input_filename =~ s/PrinSeq_//;		$input_filename =~ s/FLASH_//;
	$input_filename =~ s/Clust_//;		$input_filename =~ s/Clean_//;
	$input_filename =~ s/Hunt_//;			$input_filename =~ s/Qual_//;
	$input_filename =~ s/Taxo_//;			$input_filename =~ s/SILVA_//;
	$input_filename =~ s/RDP_//;
	#Cleaning also the suffixes from file types
	$input_filename =~ s/\.fasta//;		$input_filename =~ s/\.clust//;
	return($input_filename);
}
# End of the function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


#=======================================================================================================
#         FILE:  Globanalysis_matrix_computation_3.0.pl
#        USAGE:  perl Globanalysis_matrix_computation_3.0.pl arguments
#	ARGUMENTS:  - Clustering choice [Raw or Clean],
#			- Clustering folder input [FOLDER_NAME],
#			- Dissimilarity percentage of clustering [0-100],
#			- Taxonomy folder input [FOLDER_NAME],
#			- Choice [yes or no] of the taxonomic assignment of all OTUs in the global matrix,
#			- Chosen taxonomy threshold (confidence or percentage) [0-100],
#			- Choice [yes or no] for the phylogeneteic tree computation,
#			- Alignment folder input [FOLDER_NAME],
#			- Choice [yes or no] of the selection of a representative read for each OTU.
#  DESCRIPTION:  This PERL program was developed to treat the global clustering realized with analyzed
#			  samples chosen by the user. To do this, this program use different files to join and
#			  analyze the global clustering. Two particular files, defined by the previous program,
#			  are necessary : "Concatenated_files.txt", lists the concatenated files and their IDs ;
#			  and the "Derep_details_Concatenated_reads.fasta.fasta" file, listing all unique reads
#			  corresponding for each dereplicated read found in the clustering.
#
#			  This program needs also three elements as input : the "user_choice", corresponding to
#			  the raw or clean clustering used, the percentage of dissimilarity chosen, named
#			  "cutoff", and finally the folder name where the program can found the clustering file.
#
#			  The program first read and store the IDs for each file that can be found in the global
#			  clustering. Then, the PERL program read and store each cluster (the list of reads) and
#			  also each dereplicated information. The next step is the joining of these two
#			  information to store only unique reads in the clustering. Finally, the PERL program
#			  determine the number of reads for each sample in each global OTU, and create two
#			  different OTU matrices (one vertical, and one horizontal).
#
#			  1.5 Modifications (16/01/2013): This main modification and improvement was, for each
#			  global OTU, the definition of all taxonomic assignment, using previous taxonomic
#			  analysis of independent samples. Briefly, using all unique reads for all OTUs, we do a
#			  research of taxonomic results for each unique OTU. And we finally create a file to
#			  summarize all results for each OTU.
#
#			  2.0 Modifications (25/03/2013): Main modification to introduce the possibility to
#			  define a phylogenetic tree using the multiple alignment realized before (Raw or Clean)
#			  and an ID mapping file. These two files will be stored in the Unifrac_data/ folder and
#			  can be useful for the user to realize an Unifrac analysis. Briefly, the alignment file
#			  will be cleaned, and used to define a phylogenetic tree with the FastTree program. Then,
#			  during the treatment and the definition of the OTU table, we will create the ID mapping
#			  file.
#
#			  2.5 Modifications (17/04/2013): We simplify the program tranferring all parts of the
#			  program dedicated to the Unifrac part in a proper PERL program to simplify and
#			  facilitate the maintenance of the program.
#			  Some errors are also corrected for the global analysis treatment based on the random
#			  selection and it position based on the user choice.
#
#			  2.6 Modifications (02/07/2013): We correct one step of this program, due to an absent
#			  test to read a particular folder (the TAXOOMY one) to find and keep info.
#			  See details in the program.
#
#			  2.7 Modifications (04/02/2014): We correct some minor errors (see details above).
#
#			  2.8 Modifications (21/10/2015): We modify the LOOP07 and LOOP08 to gain time during the
#			  analysis and avoid not needed treatments and also the treatment of big files and clusters.
#
#			  3.0 Modifications (23/04/2018); We modify the program to redefine a global clustering file,
#			  organized as the OTU Taxonomic table. We modified also the version of the perl program
#			  for the realization of the phylogenetic tree.
#
# REQUIREMENTS:  Perl language only
#       AUTHOR:  Sebastien TERRAT <sebastien.terrat@dijon.inra.fr>
#      COMPANY:  GenoSol platform (UMR 1347 INRA/uB/AgroSup)
#      VERSION:  3.0
#    REALEASED:  11/01/2013
#     REVISION:  23/04/2018
#=======================================================================================================
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#=======================================================================================================

#!/usr/bin/perl

#Print errors and warnings during the implementation of the perl program
use strict;
use warnings;
use FindBin;

#Defined variables
my $file_info = "Concatenated_files.txt";
my $file_derep = "Derep_details_Concatenated_reads.fasta.fasta";
my $file_name = "";
my $tmp = "";
my $file = "";
my $file_tmp = "";
my $element = "";
my $read = "";
my $ID = "";
my $group = "";
my $defined_group = "";
my @tmp = ();
my @tmp2 = ();
my @tmp3 = ();
my @file_taxonomy = ();
my @file_info = ();
my @file_info_OTU = ();
my @file_IDs = ();
my @clust_input = ();
my @OTU_result = ();
my @temporary_derep_files = ();
my %Init_OTUs = ();
my %New_OTUs = ();
my %OTU = ();
my %Derep_info = ();
my $verif = 0;
my ($i,$j) = (0,1);
my $tree_id_production = "";
my $comp_init = "";
my $data = "";

#Needed variables to store all parameters given as input by the main program
my $user_clustering_choice = $ARGV[0];
my $folder_clustering = $ARGV[1];
my $clustering_cutoff = $ARGV[2]/100;
my $folder_taxonomy = $ARGV[3];
my $user_taxonomy_choice = $ARGV[4];
my $taxo_threshold = sprintf("%.2f",$ARGV[5]);
my $unifrac_choice = $ARGV[6];
my $folder_alignment = $ARGV[7];
my $unifrac_otu_picking_choice = $ARGV[8];

#=======================================================================================================
# 2.0: First step, we launch the realization of the phylogenetic tree if the user chose this option. To
# do this phylogenetic tree, we use the FastTree program developed by Price, M.N. et al. And we give to
# this program the multiple alignment from the INFERNAL program in FASTA format without lines
# corresponding to the structure and the consensus sequence.
#=======================================================================================================

#2.0: We first test for the user choice if the UNIFRAC data will be created or not.
if (uc($unifrac_choice) eq "YES")
{
	$tree_id_production = $user_clustering_choice." ".$folder_clustering." ".$clustering_cutoff." ".$folder_alignment." ".$unifrac_otu_picking_choice;
	$tree_id_production = `perl $FindBin::RealBin/Globanalysis_tree_mapping_computation_2.0.pl $tree_id_production`;
}

#=======================================================================================================
# Second step, we store the information of all concatenated files for subsequent analyses.
#=======================================================================================================

#Here, we catch and store information of the association file_name-corresponding number in two
#different arrays (@file_IDs and @file_info)
open(F1,"Summary_files/Global_Analysis/$file_info") || die "==> File Not Found: $file_info<==";
#To do this, we read each line of the file, and we store them first in a temporary array
LOOP01:while(<F1>)
{
	$_ =~ s/\n//g;
	@tmp = split("\t", $_);
	#We need to inverse the information to sort it in alphabetical order efficiently
	push(@tmp2, $tmp[1]."\t".$tmp[0]);
	#2.5: We treat the name of the files to simplify their utilization
	$tmp[1] =~ s/[LMH]preprocess_//g;	$tmp[1] =~ s/Rdm_//g;	$tmp[1] =~ s/FilterAlign_//g;
	$tmp[1] =~ s/Clust_//g;			$tmp[1] =~ s/Derep_//g;	$tmp[1] =~ s/Clean_//g;
	$tmp[1] =~ s/Hunt_//g; $tmp[1] =~s/Qual_//g;

	#1.5: We also store the information based on their number to efficiently retrieve them
	$file_info_OTU[$tmp[0]] = $tmp[1];
}
close F1;

#We organize the array based on the alphabetical order of file names
@tmp2 = sort(@tmp2);
#Needed empty variable
@tmp = ();
#Then, the information was splitted in two different tables: the chosen ID of the file, and the name of
#the file itself.
LOOP02:foreach $element (@tmp2)
{
	@tmp = split("\t", $element);
	push(@file_IDs, $tmp[1]);
	push(@file_info, $tmp[0]);
}

#=======================================================================================================
# 1.5: Third step, we read the chosen clustering of the concatenated reads to treat them in the next
# step and also the taxonomic results folder for further treatments
#=======================================================================================================

#Then, we open the defined directory to find the file dedicated to the Concatenated reads
opendir (DIR, "Result_files/$folder_clustering/") || die "can't opendir Result_files/$folder_clustering/: $!";
#We search and store the name of the Concatenated reads file
LOOP03:while (defined($file = readdir(DIR)))
{
	if ($file =~ m/Concatenated\_reads\.fasta\.fasta/) { $file_name = $file; }
}
closedir (DIR);

#2.6: If the user choose also the analysis of the OTUs based on the taxonomy
if (uc($user_taxonomy_choice) eq "YES")
{
	#1.5: Here, we open the taxonomy results folder (if existing) to find all file names of taxonomic results
	opendir (DIR_TAXO, "Result_files/$folder_taxonomy/") || die "can't opendir Result_files/$folder_taxonomy/: $!";
	#1.5: We search and store in the @file_taxonomy array all file names
	LOOP04:while (defined($file = readdir(DIR_TAXO)))
	{
		if ($file =~ /^\.\.?$/) { next LOOP04; }
		else { push(@file_taxonomy, $file); }
	}
	closedir (DIR_TAXO);
}

#2.5: We open the temporary directory to store all file names corresponding to the dereplication step
opendir (DIR, "Temporary_files/") || die "can't opendir Temporary_files/: $!";
#2.5: We search and store the names of the Derep_details files
LOOP05:while (defined($file = readdir(DIR)))
{
	if ($file =~ m/Derep\_details/) { push(@temporary_derep_files, $file); }
}
closedir (DIR);

#=======================================================================================================
# Fourth step, we read the chosen clustering of the concatenated reads to treat them in the next step
#=======================================================================================================

#The next step is to open the clustering file containing needed data to treat them
open(F_CLUST,"Result_files/$folder_clustering/$file_name") || die "==> File Not Found: $file_name<==";
#We read each line of the file
LOOP06:while(<F_CLUST>)
{
	#We search for the good clustering step based on the given cutoff
	if ($_ =~ m/distance cutoff\:\t(.*)\n/) { if ($1 == $clustering_cutoff) { $verif = 1; } }
	#If we read the good clustering step
	if ($verif == 1)
	{
		#We search for all clusters (starting by a number)
		if ($_ =~ m/^\d/)
		{
			#We catch the descriptive line of each cluster and store them in the array @clust_input
			$_ =~ m/\n/g;
			@tmp = split("\t", $_);
			push(@clust_input, $tmp[3]);
			#3.0: We store needed data to redefine and reorganize the OTU result file
			$Init_OTUs{$tmp[3]} = "$tmp[1]\t$tmp[2]\t$tmp[3]";
		}
		#We found the end of the clustering step, we stop the analysis here and increase the verif value
		#to block further readings
		elsif ($_ eq "\n") { $verif++; last LOOP06; }
	}
}
#We close the clustering file
close (F_CLUST);

#=======================================================================================================
# Fifth step, we modify each clustering line to add the dereplication information (to replace all
# dereplicated reads by all unique corresponding reads).
#=======================================================================================================

#2.8: Here, we catch information from the dereplication step to add these info to the descriptive line of each OTU
open(F_DEREP,"Summary_files/Global_Analysis/$file_derep") || die "==> File Not Found: $file_derep<==";
LOOP07:while(<F_DEREP>)
{
	#2.8: For each line of the dereplication step
	$_ =~ s/\n//;
	@tmp = split("\t", $_);
	#2.8: We store data from the derep file using IDs as keys
	$Derep_info{$tmp[0]} = $tmp[1];
}
close F_DEREP;

#2.8: Needed empty value
$i = 0;
#2.8: loop to read the complete clustering file
LOOP08:foreach $element (@clust_input)
{
	chomp($element);
	#We store needed data to redefine and reorganize the OTU result file
	$comp_init = $element;
	#2.8: Here, we check for the number of elements to treat, if 0, we stop
	#and gain time
	$i = keys(%Derep_info);
	if($i == 0) { $New_OTUs{$element} = $comp_init; next LOOP08; }
	#Loop to read all keys from the %Derep_info hash table
	LOOP08b:foreach $ID (keys(%Derep_info))
	{
		#WARNING: Use of \Q in the regex to avoid the problem with the variable from an array with "[]"
		#recognized in the regex as not as it does: [0] search for the character 0, and create errors !!!
		if ($element =~ s/\Q$ID//)
		{
			#2.8: We store needed data of all corresponding unique reads in $tmp.
			$tmp .= $Derep_info{$ID}." ";
			#2.8: We delete the treated $ID, as we already found it
			delete($Derep_info{$ID});
			#2.8: We go for the next $ID
			next LOOP08b;
		}
	}
	#2.8: We replace the dereplicated read by all corresponding unique reads
	$element .= " ".$tmp;
	$tmp = "";
	$element =~ s/  / /g;
	#3.0: We store needed data (initial and dereplicated data) to redefine and reorganize the OTU result file
	$New_OTUs{$element} = $comp_init;
}
#2.8: Needed empty value
$i = 0;

#=======================================================================================================
# Next step, based on all OTUs with all unique reads, we search for the taxonomic information of all
# reads, specifically in each OTU, and for all taxonomic levels, to define the taxonomic assignment of
# each global OTU.
#=======================================================================================================

#1.5: Here, we sort the clusters firstly based on their abundance (the most abundant first),
#and then based on their composition (the 01 first, then 02, etc... for OTU with the same abundance).
@clust_input = sort { length($b) <=> length($a) || $a cmp $b } @clust_input;

#3.0: Then, we open the initial clustering file to read it and catch some data
open(F_CLUST,"Result_files/$folder_clustering/$file_name") || die "==> File Not Found: $file_name<==";
while(<F_CLUST>) { if($_ =~ m/^\d/) { close(F_CLUST); last; } else {$data .= $_; }}
close(F_CLUST);
#3.0: We then delete it as we will recreate it efficiently
unlink("Result_files/$folder_clustering/$file_name");
#3.0: Here, we redefine a new clustering file efficiently organized with the same name oas the deleted
#clustering file
open(F_CLUST_NEW,">Result_files/$folder_clustering/$file_name") || die "==> File Not Found: $file_name<==";
printf(F_CLUST_NEW $data);
$i = 1;
foreach $element (@clust_input) { printf(F_CLUST_NEW $i."\t".$Init_OTUs{$New_OTUs{$element}."\n"}); $i++; }
close(F_CLUST_NEW);
#3.0: We finally empty both hash tables as we did not need them anymore
%Init_OTUs = ();
%New_OTUs = ();

#1.5: If the user choose also the analysis of the OTUs based on the taxonomy
if (uc($user_taxonomy_choice) eq "YES")
{
	#1.5: We open the result file to create them and put results for each cluster analyzed
	open(F_OUT2,">Summary_files/Global_Analysis/$user_clustering_choice"."_OTU_taxo_assignment_".$taxo_threshold.".txt") || die "==> File Not Found: $user_clustering_choice"."_OTU_taxo_assignment_".$taxo_threshold.".txt<==";
	#1.5: We determine the taxonomy threshold chosen by the user
	$taxo_threshold = $taxo_threshold/100;
	#1.5: We print also in this file column names
	printf (F_OUT2 "DEFINED OTU\tPHYLUM\tCLASS\tORDER\tFAMILY\tGENUS\n");
	#1.5: For each organized OTU defined previously in the global analysis
	LOOP09:foreach $element (@clust_input)
	{
		#1.5: Needed empty variables for each cluster analyzed
		$ID = "";
		@OTU_result = ();
		@tmp = ();
		@tmp2 = ();
		%OTU = ();
		#1.5: We split the OTU based on the space to analyze each unique read
		@tmp = split(" ", $element);
		#1.5: For each unique read in the analyzed OTU
		LOOP10:foreach $read (@tmp)
		{
			#1.5: We catch in the read the number of the file of origin ($1) and the ID of the read itself
			$read =~ m/(.*)\|(.*)/;
			#1.5: We store the read ID
			$ID = $2;
			#1.5: We also catch the name of the origin file to open the details dereplicated file
			$file_name = $file_info_OTU[$1];
			#2.5: We serach efficiently the name of the file based only on the sample name given at the beginning
			foreach $file_tmp (@temporary_derep_files) { if ($file_tmp =~ m/$file_name/) { $file_name = $file_tmp; last; } }
			$file_name =~ s/Derep\_details\_//g;
			open(F_DEREP_DETAILS, "Temporary_files/Derep_details_$file_name") || die "==> File Not Found: Derep_details_$file_name<==";
			#1.5: We read the file line by line
			LOOP11:while(<F_DEREP_DETAILS>)
			{
				#1.5: If the ID is found (so that this unique reads was dereplicated in the independant analysis)
				if ($_ =~ m/$ID/)
				{
					#1.5: We catch and store the name of the dereplicated read to retrieve the taxonomic assignment
					$_ =~ m/(.*)\t/;
					#1.5: To store it efficiently, we use a hash table with keys as file_name##dereplicated_read and values
					# as number of occurrences
					$OTU{"$file_name##$1"}++;
					#1.5: #We finally close the file
					close (F_DEREP_DETAILS);
					#1.5: We then treat the next unique read
					next LOOP10;
				}
			}
			#1.5: We finally close the file
			close (F_DEREP_DETAILS);
			#1.5: Finally, if the unique read was not found (and was not dereplicated then in the independant analysis, we keep its
			# name and the file also, as described before).
			$OTU{"$file_name##$ID"}++;
		}

		#1.5: Then, we will analyze each unique OTU ID defined previously
		LOOP12:foreach $ID (keys(%OTU))
		{
			#1.5: We first split each ID based on "##" to split the file name and the ID itself
			@tmp2 = split('##',$ID);
			#1.5: We then search the complete file name based on the file name kept before
			LOOP13:foreach $file (@file_taxonomy)
			{
				#1.5: If we found it, we then open the taxonomy assignment file in the taxonomic folder
				if ($file =~ m/$tmp2[0]/)
				{
					open(F_TAXO, "Result_files/$folder_taxonomy/$file") || die "==> File Not Found: $file <==";
					last;
				}
			}
			@tmp3 = split("_", $tmp2[1]);
			#1.5: We read each line of the taxonomic file
			LOOP14:while(<F_TAXO>)
			{
				#1.5: If we found the ID of the read, we will treat the taxonomic information for this particular read
				if (($_ =~ m/$tmp2[1]/)||($_ =~ m/$tmp3[0]/))
				{
					#1.5: We first delete all """ characters in the line
					$_ =~ s/"//g;
					#1.5: We then search the confidence or similarity percentage and the name of the taxonomic level
					# (here the phylum) to treat it
					$_ =~ m/\t([\d\.]*)\t([\w\_\s\-\/]*)\tphylum\t/;
					#1.5: We then verify if the percentage is higher than the defined threshold by the user. If it
					# is the case, we add it in the hash table stored into the array @OTU_result and the value added
					# is the number of reads found for this particular read (as defined previously, using the second
					# hash table %OTU and the ID of the read $ID.
					if($1 >= $taxo_threshold) { $OTU_result[0]{$2} += $OTU{$ID}; }
					else { $OTU_result[0]{"Unknown"} += $OTU{$ID}; }
					#1.5: Same analysis as before for the class level.
					$_ =~ m/\t([\d\.]*)\t([\w\_\s\-\/]*)\tclass\t/;
					if($1 >= $taxo_threshold) { $OTU_result[1]{$2} += $OTU{$ID}; }
					else { $OTU_result[1]{"Unknown"} += $OTU{$ID}; }
					#1.5: Same analysis as before for the order level.
					$_ =~ m/\t([\d\.]*)\t([\w\_\s\-\/]*)\torder\t/;
					if($1 >= $taxo_threshold) { $OTU_result[2]{$2} += $OTU{$ID}; }
					else { $OTU_result[2]{"Unknown"} += $OTU{$ID}; }
					#1.5: Same analysis as before for the family level.
					$_ =~ m/\t([\d\.]*)\t([\w\_\s\-\/]*)\tfamily\t/;
					if($1 >= $taxo_threshold) { $OTU_result[3]{$2} += $OTU{$ID}; }
					else { $OTU_result[3]{"Unknown"} += $OTU{$ID}; }
					#1.5: Same analysis as before for the genus level.
					$_ =~ m/\t([\d\.]*)\t([\w\_\s\-\/]*)\tgenus\t/;
					if($1 >= $taxo_threshold) { $OTU_result[4]{$2} += $OTU{$ID}; }
					else { $OTU_result[4]{"Unknown"} += $OTU{$ID}; }
					#1.5: We finally close the taxonomic file and we treat the next defined read
					close (F_TAXO);
					next LOOP12;
				}
			}
			#1.5: Just in case, we also close here the taxonomic file
			close (F_TAXO);
		}
		#1.5: We first name the OTU based on its abundance in the result file
		if (scalar(@OTU_result) == 0 ) { printf(F_OUT2 "OTU$j\tNo taxonomic information, deleted by the hunting step"); }
		else { printf (F_OUT2 "OTU$j"); }

		#1.5: Finally, we read all hash tables (phylum, class, order, family and genus) stored in the array
		# @OTU result
		LOOP15:foreach $defined_group (@OTU_result)
		{
			#1.5: We add form in the result file
			printf(F_OUT2 "\t");
			#1.5: We then read all keys in the defined hash table (here, named as %$defined_group), the
			# reference $defined_group of the hash table %
			LOOP16:foreach $group (keys(%$defined_group))
			{
				#1.5: We finally print the key itself (here, the name of the taxonomic level) and the
				# corresponding value $$defined_group{$group} using the reference $defined_group
				if (keys(%$defined_group) == 1) { printf(F_OUT2 "$group($$defined_group{$group})"); }
				else { printf(F_OUT2 "$group($$defined_group{$group}) "); }
			}
		}
		printf(F_OUT2 "\n");
		#1.5: We count the number of OTU defined
		$j++;
	}
	#1.5: We finally close the result file.
	close(F_OUT2);
}

#1.5: Needed empty or defined variables
@tmp2 = ();
@tmp = ();
$ID = "";
$j = 1;

#=======================================================================================================
# Final step, we finally analyze each clustering to define the number of total reads in each global OTU
# and also the number of reads for each sample for each OTU defined.
#=======================================================================================================

#We modify the cutoff to integrate it in the filename
$clustering_cutoff = sprintf("%.2f", $clustering_cutoff*100);

#We open the result file (vertical, for an easy reading).
open(F_OUT,">Summary_files/Global_Analysis/$user_clustering_choice"."_OTU_matrix_".$clustering_cutoff."_vertical.txt") || die "==> File Not Found: $user_clustering_choice"."_OTU_matrix_".$clustering_cutoff."_vertical.txt<==";
#We print all treated file names
LOOP17:foreach $element (@file_info)
{
	$element = &ExtractSampleFromFileName($element);
	printf(F_OUT "\t$element");
}
printf(F_OUT "\n");
#For each organized cluster
LOOP18:foreach $element (@clust_input)
{
	#We named it OTU + number
	printf (F_OUT "OTU$j");
	#We also store all IDs in a dedicated variable for the second result file
	$ID=$ID."\tOTU$j";
	#We print all results for each cluster
	LOOP19:for($i=0;$i<=$#file_IDs;$i++)
	{
		#We count the number of reads for the particular sample
		@tmp = split(/$file_IDs[$i]\|/, $element);
		#We then print the number of reads for the dedicated sample
		printf (F_OUT "\t$#tmp");
		#We also store this value to the array @file_info
		$file_info[$i]=$file_info[$i]."\t$#tmp";
	}
	printf (F_OUT "\n");
	$j++;
}
close (F_OUT);
#We open the second result file (horizontal, for R) and write all needed information from the previous analysis
open(F_OUT2,">Summary_files/Global_Analysis/$user_clustering_choice"."_OTU_matrix_".$clustering_cutoff."_horizontal.txt") || die "==> File Not Found: $user_clustering_choice"."_OTU_matrix_".$clustering_cutoff."_horizontal.txt<==";
printf(F_OUT2 "$ID\n");
printf (F_OUT2 join("\n",@file_info));
close (F_OUT2);

#We finally stop the program efficiently to avoid zombis and problems
exit(0);
