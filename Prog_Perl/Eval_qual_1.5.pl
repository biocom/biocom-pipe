#=======================================================================================================
#         FILE:  Eval_qual.pl
#        USAGE:  perl Eval_qual.pl
#  DESCRIPTION:  This PERL program was developed to have a better understanding of the quality of
#			  obtained pyrosequences for each RUN. To do this, the program takes all quality files
#			  found in the IN/ directory and compute several values for each position of the
#			  sequence. Briefly, this program determines the standard deviation, the median value, the
#			  minimum, the maximum, the Q1 and Q3 values for each 25 bases of reads. It also computes
#			  the total number of sequences treated and the number of sequences with quality values
#			  high and low (between 40 and 35, 35 and 30, 30 and 25, and less than 25).
#			  The result is finally computed and stored in a tabulated file that can be directly
#			  imported in Excel or other tables.
#
#			  1.2 Modifications: First, all the program have been completely annotated to clearly
#			  indicate all steps. And, a new step have been added for the user to ask for the
#			  minimum length tolerated for reads analysis. See details and modifications in the
#			  PERL program as indicated.
#
#			  1.5 Modifications: Transformation of the program to be launched automatically with
#			  another PERL program to develop an automatic workflow of analysis (e.g., we delete the
#			  the loop to analyze and search each file needed treatment).
#
# REQUIREMENTS:  Perl language only
#       AUTHOR:  Sebastien TERRAT <sebastien.terrat@gmail.com>
#      COMPANY:  GENOSOL UMR-MSE Team5
#      VERSION:  1.5
#    REALEASED:  27/07/2011
#     REVISION:  20/03/2012
#=======================================================================================================
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#=======================================================================================================

#Print errors and warnings during the implementation of the perl program
use strict;
#use warnings;

#Defined variables
my $length = 0;
my $file = "";
my $seq = "";
my $nbseq = 0;
my $nbseq_tot = 0;
my @tab_qual = ();
my @tmp = ();
my @tmp2 = ();
my @matrix;
my @tab_num_qual = ();
my $pos_matrix = 0;
my $moy = 0;
my $i = 0;
my $j = 0;

my $sum = 0;
my $average = 0;
my $min = 0;
my $max = 0;
my $Q1 = 0;
my $Q3 = 0;
my $tmp_value = 0;
my $val = 0;

#1.5 : Modifications to catch needed information given by the workflow
$length = $ARGV[0];
$file = $ARGV[1];

#1.5: Open file one given by the user and containing cleaned sequences
open(F1, "Raw_data/$file")|| die "==> File Not Found: $file<==";

$nbseq_tot = $nbseq_tot + $nbseq;
$nbseq = 0;
@tab_qual = ();

#1.5: For every line of the file given by the user
BOUCLE1:while (<F1>)
{
	#As the file have a FASTA format-like, we search for the name and increment $nbseq
	#for each new read
	if ($_ =~ m/>/) { $nbseq++; }
	#If the line contain only the PHRED values for the read
	else
	{
		#We delete the last character (\n)
		chomp ($_);
		#We check if we have already data for this read, if not, we created a new value in @tab_qual
		#Or, we concatenate all data already stored with the new line.
		if (!defined($tab_qual[$nbseq-1])) {$tab_qual[$nbseq-1] = $_;}
		else {$tab_qual[$nbseq-1] = $tab_qual[$nbseq-1]." ".$_;}
	}
}
#We close the readed file
close F1;

#For each read found in the file and stored in @tab_qual
BOUCLE2:foreach $seq (@tab_qual)
{
	#We split all PHRED values based on \s found
	@tmp = split(" ", $seq);
	#1.2: If the value is below the defined threshold, we avoid this read
	if (scalar (@tmp) < $length) {next;}
	#If the read length id OK
	else
	{
		#Needed empty variables
		$pos_matrix = 0;
		$moy = 0;
		#For each PHRED value
		for ($i = 0; $i <= $#tmp; $i++)
		{
			#We determine for each range of 25 bases some details
			if (($i % 25 == 0)&&($i != 0))
			{
				#The average value for the 25 bases analyzed
				$moy = $moy/25;
				#We store it in the matrix
				push @{$matrix[$pos_matrix]}, $moy;
				#We move to the next matrix value
				$pos_matrix++;
				#Needed empty variable
				$moy = 0;
			}
			else
			{
				#We add to the average the PHRED value of the base
				$moy = $moy + $tmp[$i];
			}
		}
	}
}
#We store the defined number of reads (complete, not analyzed)
$nbseq_tot = $nbseq_tot + $nbseq;

#1.5: We open the result file and write the information (in the defined directory)
open(F2, ">Summary_files/Eval_qual_$length"."_"."$file.txt")|| die "==> Can't create: Eval_qual_$file.txt in /Summary_files directory<==";
#1.5: We print it for the user
printf (F2 "Defined length threshold to analyze the read:\t$length\n");
printf (F2 "Total number of reads (with those eliminated by the threshold):\t$nbseq_tot\n");
printf (F2 "Std\tAverage\tMin\tMax\tQ1\tQ3\t40-35\t35-30\t30-25\t25-0\tTOTAL\n");

#For each element of the matrix
BOUCLE3:foreach $i (@matrix)
{
	#We organize data based on the values stored
	@tmp = sort { $a <=> $b } @$i;
	#For each element in @tmp
	BOUCLE4:foreach $val (@tmp)
	{
		#We do the sum and the number of values in each defined range
		$sum = $sum + $val;
		if ($val > 35) {$tab_num_qual[0]++;next BOUCLE4;}
		elsif($val > 30) {$tab_num_qual[1]++;next BOUCLE4;}
		elsif($val > 25) {$tab_num_qual[2]++;next BOUCLE4;}
		else {$tab_num_qual[3]++;next BOUCLE4;}
	}
	#We defined also the minimum and the maximum
	$min = $tmp[0];
	$max = $tmp[$#tmp];
	#The average value is also defined
	$average = $sum / ($#tmp+1);
	$tmp_value = int(($#tmp+1)/4);
	#And the Q1 and the Q3 values
	$Q1 = $tmp[$tmp_value];
	$tmp_value = 3*int(($#tmp+1)/4);
	$Q3 = $tmp[$tmp_value];
	$val = $#tmp +1;
	#We print data in the result file
	printf (F2 "$j-");$j = $j+25;
	printf (F2 "$j\t");
	printf (F2 "$average\t$min\t$max\t$Q1\t$Q3\t$tab_num_qual[0]\t$tab_num_qual[1]\t$tab_num_qual[2]\t$tab_num_qual[3]\t$val\n");
	#We need empty variables
	$sum = 0;
	@tmp = ();
	@tab_num_qual = ();
}
#1.5: Close the summary_file
close F2;

print "Eval_qual_$length"."_"."$file.txt";
