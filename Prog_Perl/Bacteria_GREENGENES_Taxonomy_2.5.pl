#!/usr/bin/perl

#Print errors and warnings during the implementation of the perl program
use strict;
use warnings;
use FindBin;

#=======================================================================================================
###SUMMARY: 	Function developed to launch with defined parameters the USEARCH analysis efficiently
#with defined and validated parameters.
#
###INPUT: 	the directory path where we can find the input folder ($_[0]), the input file with sequences
#needed treatments ($_[1]), the database folder ($_[2]), the database file name ($_[3]), the name of the
#output file ($_[4]), the defined threshold ($_[5]) and the number of cores used ($_[6]), and finally the
#folder where the useacrh tool can be found.
###OUPUT:		NONE
sub LaunchUsearchAnalysis
{
	my $taxo_exec = "";
	$taxo_exec = `$_[7]/usearch -usearch_local $_[0]/$_[1] -db $_[2]/$_[3] -id $_[5] -strand plus -blast6out Temporary_files/Tmp_$_[1] -maxhits 15 -maxaccepts 0 -maxrejects 0 -threads $_[6] -quiet`;
	open(F_TMP, "Temporary_files/Tmp_$_[1]")||die "==> File Not Found: Tmp_Complete_$_[1]<==";
	open(F_OUT, ">>Temporary_files/$_[4]")||die "==> File Not Found: $_[4]<==";
	while(<F_TMP>) { printf(F_OUT "$_"); }
	close (F_TMP);
	close (F_OUT);
	unlink("Temporary_files/Tmp_$_[1]");
}
# End of the function
#=======================================================================================================

#=======================================================================================================
###SUMMARY: 	Function needed to treat efficiently and fastly the USEARCH results obtained after
#analysis with the dedicated base ([C] for complete or [EI] for environmental-incomplete bases. This
#function is dedicated to put in form the information given by USEARCH.
#
###INPUT: 	the name of the file where USEARCH data have been stored $_[0] and the hash table where
#the results will be stored $_[1], the defined threshold ($_[2])
###OUPUT:		the hash table treated, as it is used as a reference, has not to be returned.
sub TreatUsearchRawResults
{
	#Defined local variables
	my $length = 0;	my @tmp = ();		my $line = "";
	my $threshold = $_[2]; my $identity = 0;
	#Opening the needed files (the temporary file and the out file)

	open (F, "Temporary_files/$_[0]")|| die "==> File Not Found: Temporary_files/$_[0]<==";
	#For each line of the file
	LOOP:while(<F>)
	{
		#We first catch the length of the treated read
		$_ =~ m/length=(\d+)/;		$length = $1;
		#We delete the \n character
		$_ =~ s/\n//;
		#We then split the line based on the \t character
		@tmp = split("\t", $_);
		#We determine the identity of the read with the given result by USEARCH
		#2.1 Modification of the formula (here, in commentary the bad one, and the good one just after)
		#$identity = ($tmp[7]-$tmp[6]+1-$tmp[4])/$tmp[3];
		$identity = ($tmp[3]-$tmp[4]-1)/$length;
		#We verify that the threshold is lower than the determined identity to keep the line
		if($threshold <= $identity)
		{
			#We recreate the line to treat it
			$line = "$tmp[0]\t$length"."\t$tmp[6]\t$tmp[7]\t$tmp[10]\t$tmp[3]\t$tmp[4]\t$tmp[1]\n";
			#We finally store the information in the hash table
			push(@{$_[1]{$tmp[0]}}, $line);
		}
	}
	#We finally close the file
	close (F);
}
# End of the function
#=======================================================================================================

#=======================================================================================================
###SUMMARY:	Function defined to treat USEARCH results efficiently. This function takes the results
#given by the main program, treat them and then give back the information (% od identity and name of the
#taxonomic result found).
###INPUT: 	the reference of the array containing results ($_[0]), the taxonomic level chosen for the
# analysis ($_[1]).
###OUPUT:		A string composed of : the found taxonomic name, the taxonomic level and the identity (%).
sub SelectViableResultGG
{
	my $line = "";
	my $chosen_taxon = "";
	my $name = "";
	my $identity = 0;
	my $highest_id = 0;
	my $kept_name = "";
	my %Data = ();
	my @tmp = ();


	if($_[1] eq "superkingdom") { open(TEST, ">>Temporary_files/Link_ID_read_ID_GG_".$_[3]) || die "Can't create file link ID GG for $_[3] file\n"; }
	#For each line found in the USEARCH result
	foreach $line (@{$_[0]})
	{
		#We first split the results line to catch useful information
		@tmp = split("\t", $line);
		#We determine the percentage of identity of each result
		$identity = ($tmp[5]-$tmp[6]-1)/$tmp[1];
		#We also catch the name of the taxonomic rank studied
		if ($line =~ m/\;([\w\-\_]+)\($_[1]\)\;/) { $name = $1; }
		else { $name = "Unknown"; }
		#We search for the higher result for each found name
		if (defined($Data{$name}))
		{
			if ($Data{$name} < $identity) { $Data{$name} = $identity; }
		}
		else { $Data{$name} = $identity; }
		#Here, we catch the highest value with the analyzed read
		if($highest_id < $identity) { $kept_name = $tmp[7]; }
	}
	#We then sort our results based on their highest identities
	@tmp = (reverse sort { ($a cmp $b) } values %Data);
	#If, for two names, we have the same two highest identites, we return that we have a problem
	if (($#tmp > 0) && ($tmp[0] == $tmp[1]))
	{
		if(($_[1] eq "superkingdom")||($_[1] eq "domain")) { return ("Unknown\tdomain\t0.00\t"); }
		else { return ("Unknown\t$_[1]\t0.00\t"); }
		#
		if($_[1] eq "superkingdom") { printf(TEST "$_[2]\tUnknown_twoIDs_same_results"); }
	}
	else
	{
		if($_[1] eq "superkingdom") { printf(TEST "$_[2]\t$kept_name"); }
		$highest_id = sprintf("%.2f", $tmp[0]);
		#We then sort the names based on the values
		@tmp = (reverse sort { ($Data{$a} cmp $Data{$b}) } keys %Data);
		#We catch the highest one
		$chosen_taxon = $tmp[0];
		#
		$chosen_taxon =~ s/unknown/Unknown/;
	}
	#If the chosen taxon is not "Unknown"
	if ($chosen_taxon ne "Unknown")
	{
		@tmp = ();
		#We then select into the complete array only results with chosen taxonomy, to simplify further
		#analysis.
		foreach $line (@{$_[0]})
		{
			if ($line =~ m/\;($chosen_taxon)\($_[1]\)\;/) { push(@tmp, $line); }
		}
		@{$_[0]} = @tmp;
	}

	#
	if($_[1] eq "superkingdom")  { close(TEST); }

	#We then return the result to the main program.
	if(($_[1] eq "superkingdom")||($_[1] eq "domain")) { return ("$chosen_taxon\tdomain\t$highest_id\t"); }
	else { return ("$chosen_taxon\t$_[1]\t$highest_id\t"); }
}
# End of the function
#=======================================================================================================



#=======================================================================================================
# MAIN PROGRAM
#=======================================================================================================

#=======================================================================================================
#         FILE:   	perl Bacteria_GREENGENES_Taxonomy_2.5.pl
#        USAGE:   	perl Bacteria_GREENGENES_Taxonomy_2.5.pl
#  DESCRIPTION:   	This PERL program was developed to launch in parallel a USEARCH analysis of each read
#				against the GREENGENES database of bacterial 16S. The program was developed to treat efficiently
#				and fastly the results given by the USEARCH program.
#
#				Modifs 1.5 (16/06/2013) : The complete program was rewritten due to some errors found.
#				The logic based on BLAST results realized before was modified, as with USEARCH, the
#				number of results is not a good info. Moreover, the databases of sequences were
#				splitted to realize the analysis without consuming all the memory available (see
#				details in the program).
#
#				Modifs 2.0 (18/06/2013) : We modify the program to eliminate duplicates of databases
#				and structure files. TO do this, we store the databases and structures of the
#				bacterial SILVA analysis somewhere else, and we indicate its absolute position using
#				the $folder_DB string. We also modify the program to seacrh the tool itself (usearch)
#				in another folder (the same storage folder). See details in the program.
#
#				Modifs 2.1 (20/12/2013) : Due to a deprecation in the PERL language: "Use of defined
#				on aggregates (hashes and arrays) is deprecated. It used to report whether memory for
#				that aggregate had ever been allocated. This behavior may disappear in future versions
#				of Perl." We then modify some particular tests to avoid warnings in the program.
#
#				Modifs 2.2 (08/08/2018): The formula to compute the identity percent was corrected
#				in the TreatUsearchRawResults function. Indeed, an error was corrected regarding the
#				raw percentage of identity for the TAXONOMY step. Due to the modification of the
#				structure of the USEARCH output, bad values were used to compute the identity percent.
#				Consequently, biased results were computed in previous versions (more precisely, too
#				high identities were obtained). Finally, a new version of the SILVA database (R132) was
#				added, and the previous one stay available. To manage that, some minor modifications
#				have been made.
#
#				Modifs 2.5 (10/05/2019): As this program is dedicated to the use of the GreenGenes database,
#				it was simplified. More precisely, the part concerning the assignment using Incomplete and
#				Environmental sequences was deleted, as this kind of sequences are not present into this
#				specific database. This is why this kind of program was developed and why it is more simple
#				than the Bacteria_SILVA_Taxonomy program.
#
#       AUTHOR:   	Sebastien TERRAT <sebastien.terrat@inra.fr>
#      COMPANY:   	UMR 1347 Agroecologie, BIOCOM Team
#      VERSION:   	2.5
#    REALEASED:   	27/05/2013
#     REVISION:   	10/05/2019
#=======================================================================================================
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#=======================================================================================================

#Defined main variables
my $i = 0;
my $identity = 0;
my $hits = 0;
my $line = "";
my $result_final = "";
my $result_tmp = "";
my $file = "";
my @line = ();
my @Taxo_id_OK = ();
my @Taxo_id_NOT = ();
my @Files_DB = ();

my $result_test = 0;
my $nbreads_EI = 0;
my %Taxo_C_results = ();
my %Taxo_EI_results = ();
my %Sequences = ();
my $key = "";
my @tmp = ();
my @tmp2 = ();
my $folder_Input = $ARGV[0];
my $folder_Output = $ARGV[1];
my $file_input = $ARGV[2];
my $cores = $ARGV[3];
my $threshold = $ARGV[4]/100;
#2.0: We create here the two needed directories: the Databases directory, and the Tools directory
my $folder_DB = "$FindBin::RealBin/../Data/Databases/BACTERIA/GREENGENES/";
my $folder_Tools = "$FindBin::RealBin/../Data/Tools/USEARCH";


#=======================================================================================================
#First step, we launch the USEARCH_LOCAL step using the information given by the Taxo_assignment_2.7.pl
#program (folders, file name, number of cores and defined threshold used for the analysis).
#=======================================================================================================

#1.5: We open the folder containing all sub databases
opendir (DIR_DB, "$folder_DB"."Complete/") || die "can't opendir Complete: $!";
#1.5: Loop to read each file name
LOOP00:while (defined($file = readdir(DIR_DB)))
{
	#No treatment for files '.' and '..'
  	if ($file =~ m/^\.\.?$/){next LOOP00;}
  	else
  	{
  		#1.5: We catch all sub database names
  		if ($file =~ m/.udb/) { push(@Files_DB, $file); }
  	}
}
#1.5: We then close the folder
closedir DIR_DB;

#1.5: For each sub database file found
foreach $file (@Files_DB)
{
	#2.0: Launch the program with all needed parameters ($folder_Input, $file_input, other parameters)
	#Note: Here, we used a defined threshold of 0.8 to fast the analysis, as with 0.9, is clearly longer (hours against minutes).
	#Several tests have been realized to find the best parameters (longer of words, indexing, etc...).
	&LaunchUsearchAnalysis("Result_files/$folder_Input", $file_input, "$folder_DB"."Complete", $file, "USEARCH_C_$file_input", 0.8, $cores, $folder_Tools);
}

#=======================================================================================================
#Second step, we read the input file as we will need some data to treat efficiently taxonomic results
#=======================================================================================================

#Loop to treat the reults given by the database
open (F_IN, "Result_files/$folder_Input/$file_input")|| die "==> File Not Found: Result_files/$folder_Input/$file_input<==";
LOOP01:while(<F_IN>)
{
	#We search for the descriptive line of each read
	if($_ =~ s/^>//)
	{
		#We delete the \n character
		$_ =~ s/\n//;
		#We store in two hash tables the IDs as keys. For %Taxo_C_results, we create the hash of arrays,
		#and for %Sequences, we store also the sequence itself to use it if needed
		$Taxo_C_results{$_} = ();
		$Sequences{$_} = <F_IN>;
	}
}
#W then close the input file
close F_IN;

#=======================================================================================================
#Fourth step, we obtain and treat the [EI] database results, to concatenate them with the [C] results
#and obtain the best information for all treated reads.
#=======================================================================================================

#Then we treat the USEARCH results to obtain a format easily available for other programs
#giving two information, the filename and the reference of the array
&TreatUsearchRawResults("USEARCH_C_$file_input", \%Taxo_C_results, $threshold);

#We open the two result files needed (the real Taxonomic result, and the temporary file to treat the
#sequences with the [EI] database
#Here, small modification to take into account the fact that the user can choose that files are not kept
if((exists($ARGV[6]))&&(uc($ARGV[6]) eq "YES")) { open (F_RESULTS, ">Result_files/$folder_Output/Taxo_GREENGENES_$file_input")|| die "==> Can't create file: Taxo_GREENGENES_$file_input<=="; }
elsif((exists($ARGV[6]))&&(uc($ARGV[6]) eq "NO")) { open (F_RESULTS, ">Temporary_files/Taxo_GREENGENES_$file_input")|| die "==> Can't create file: Taxo_GREENGENES_$file_input<=="; }
else { open (F_RESULTS, ">Result_files/$folder_Output/Taxo_GREENGENES_$file_input")|| die "==> Can't create file: Taxo_GREENGENES_$file_input<=="; }


open(TEST2, ">>Temporary_files/Link_ID_read_ID_GG_".$file_input) || die "Can't open the $file_input with link to GG IDs\n";
#2.1: For each read treated and stored in %Taxo_EI_results
LOOP04:foreach $key (keys(%Taxo_C_results))
{
	#We first look if we find the information after USEARCH with the [C] database, if not
	if (not defined ($Taxo_C_results{$key}))
	{
		#We just store the information if we need it again
		printf(F_RESULTS "$key\t\tRoot\tnorank\t1.0\tUnknown\tdomain\t0.00\tUnknown\tphylum\t0.00\tUnknown\tclass\t0.00\tUnknown\torder\t0.00\tUnknown\tfamily\t0.00\tUnknown\tgenus\t0.00\t\n");
		printf(TEST2 "$key\tUnknown_nomatch\n");
		next LOOP04;
	}
	#Or, we will treat them
	else { @tmp = @{$Taxo_C_results{$key}}; }
	#Needed information
	$result_final = $result_final."$key\t\tRoot\tnorank\t1.0\t";
	#We then treat all results, and delete only those who gave not good results
	$result_tmp = &SelectViableResultGG(\@tmp, "superkingdom", $key, $file_input);
	$result_final .= $result_tmp;
	$result_tmp = &SelectViableResultGG(\@tmp, "phylum", $key, $file_input);
	$result_final .= $result_tmp;
	$result_tmp = &SelectViableResultGG(\@tmp, "class", $key, $file_input);
	$result_final .= $result_tmp;
	$result_tmp = &SelectViableResultGG(\@tmp, "order", $key, $file_input);
	$result_final .= $result_tmp;
	$result_tmp = &SelectViableResultGG(\@tmp, "family", $key, $file_input);
	$result_final .= $result_tmp;
	$result_tmp = &SelectViableResultGG(\@tmp, "genus", $key, $file_input);
	$result_final .= $result_tmp."\n";
	#Finally, we print results to the output file
	printf (F_RESULTS $result_final);
	$result_final = ""; $result_tmp = "";
}
close F_RESULTS;
close(TEST2);
#We finally delete the Temporary files not needed anymore.
unlink("Temporary_files/USEARCH_C_$file_input");
exit(0);
