#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function dedicated to clean the file name given as input and returon only the sample name important
# for the user as output. This name will be added to Summary files to clarify user reading.
sub ExtractSampleFromFileName
{
	#Needed variables
	my $input_filename = $_[0];
	#Cleaning all prefixes added to the filename to keep only the sample name
	#2.8: Addition of FilterAlign_ to manage this new step
	$input_filename =~ s/Derep_//g;		$input_filename =~ s/Rdm_//g;
	$input_filename =~ s/Hpreprocess_//;	$input_filename =~ s/Mpreprocess_//;
	$input_filename =~ s/Lpreprocess_//;	$input_filename =~ s/FilterAlign_//;
	$input_filename =~ s/Align_//;
	$input_filename =~ s/PrinSeq_//;		$input_filename =~ s/FLASH_//;
	$input_filename =~ s/Clust_//;		$input_filename =~ s/Clean_//;
	$input_filename =~ s/Hunt_//;			$input_filename =~ s/Qual_//;
	$input_filename =~ s/Taxo_//;			$input_filename =~ s/SILVA_//;
	$input_filename =~ s/RDP_//;		$input_filename =~ s/GREENGENES_//;
	#Cleaning also the suffixes from file types
	$input_filename =~ s/\.fasta//;		$input_filename =~ s/\.clust//;
	return($input_filename);
}
# End of the function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#=======================================================================================================
#     FILE:  Taxo_treatment_2.8.pl
#     USAGE:  perl Taxo_treatment_2.8.pl
#  		DESCRIPTION:  This program was developed to treat taxonomic results from various programs and databases.
#		  First, it search for all taxonomic groups at the defined level and count for
#		  each treated file in the given directory the number of each found groups (based on the
#		  previously step: the dereplication). Then, it creates a file containing a matrix with
#		  for each file the result, classed based on the taxonomic groups).
#
#		  1.5 Modifications :  This program was modified to be included in the workflow to fast
#		  the analysis. All modified steps are described in the program (e.g.: using the
#		  name of the input and a different location for the output file ans summary file,
#		  deletion of the parallelization, as it was done by the main PERL program).
#
#		  2.0 Modifications :  These major modifications have been realized to take into account
#		  the treatment of fungal sequences with the SILVA database. To easily do this we add the
#		  possibility, modify some parts of the program and add particular new groups specific to
#		  the fungal analysis.
#
#		  2.1 Modifs (05/11/2012): We add also a test to avoid the analysis of the concatenated
#		  file created for the Global_Analysis step (this modification was resolved by another
#		  modification in a previous program).
#
#		  2.2 Modifs (27/05/2013): We add the SILVA bacteria database treatment. To do this, we
#		  add some possibilities to take into account the treatment of microbial sequences with
#		  the SILVA database.
#
#		  2.5 Modifs (18/06/2013): Here, we modify and simplify the program itself to find the
#		  structure folder and also the databases itselves. Indeed, we delete all test loops
#		  based on organisms and database names, and used them to define efficiently the absolute
#		  path of all needed data.
#
#		  2.6 Modifs (08/08/2018): Algae addition (15/09/2016) and integration of the FindBin
#		  package to manage easily the treatment of the results from the new database. We also
#		  modified the structure of the output file to delete non-wanted tabulations at the end of
#		  lines. We also add a line to clean the file name and keep only the sample name using a
#		  function added to the program. As two versions of the database BACTERIA SILVA are
#		  available, the program now takes also that into account to manage the versions.
#
#			2.7 Modifs (03/05/2019): Addition of the Greengene database of 16S sequences. To manage
#			its use, we created some new possibilities. This program now use alos the Structures
#			defined specifically for this database.
#
#			2.8 Modifs (08/08/2019): Minor modification of ExtractSampleFromFileName to manage
#			the fact that a new step (and consequently a new prefix) was present in the samples
#			names.
#
# REQUIREMENTS:  Perl language only and also POSIX module for trigonometric steps
#                included with the Perl basic module
#       AUTHOR:  Sebastien TERRAT <sebastien.Terrat@inra.fr>
#      COMPANY:  UMR 1347 Agrécologie - BIOCOM Team
#      VERSION:  2.7
#    REALEASED:  24/05/2011
#     REVISION:  03/05/2019
#=======================================================================================================
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#=======================================================================================================

#Print errors and warnings during the implementation of the perl program
use strict;
use warnings;
use FindBin;

#1.5: Defined variables
my ($i, $j, $k) = (0,0,0);
my @taxo_high = ();
my @taxo_possibilities = ();
my @taxo_others = ();
my @results = ();
my $file = "";
my $sample = "";
my @files = ();
my @files_struct = ();
my @tmp = ();
my $tmp = "";
my $level = ();
my $pid = 0;
my @childs = ();

#2.0: New defined variables
my $organism = uc($ARGV[0]);
my $database = uc($ARGV[1]);
my $version = uc($ARGV[2]);
my $chosen_percentage = $ARGV[3]/100;
my $folder = $ARGV[4];

#2.5 and 2.6: Here, we define the absolute path to find the taxonomic databases for all taxonomic steps
#(Bacteria with RDP or SILVA, and Fungi with SILVA, and Algae wih SILVA).
my $folder_struct = "$FindBin::RealBin/../Data/Databases/".$organism."/".$database."/";


#=======================================================================================================
#1.5: First step, we count the number of files needed treatment in the given folder by the main program
#and we catch all files used as structures found in the dedicated directory.
#=======================================================================================================

#1.5: Open the directory containing all files needed to be treated
opendir (DIR, "Result_files/$folder/") || die "can't opendir Result_files/$folder/: $!";

#1.5: For each file found in the directory given by the main program
LOOP01:while (defined($file = readdir(DIR)))
{
	#1.5: No treatment for files '.' and '..'
  	if ($file =~ m/^\.\.?$/){next LOOP01;}
  	elsif (-d $file){next LOOP01;}
  	else {push(@files, $file);}
}
closedir (DIR);
@files = sort(@files);

#2.5: Here, we deleted all the test loops, as we use the $organism and the $database to define
#efficiently the folder path containing structures dedicated to all taxonomic analyses.

#2.5: Open the directory containing all files with defined structures
#2.6 and 2.7: We check if the chosen organism is BACTERIA or FUNGI (several versions for BACTERIA and FUNGI)
#to add the version folder.
if(($organism ne "ALGAE")&&($database eq "SILVA"))
{
	#2.6: As the user can give a bad version of the database, we chose to check the given version,
	#and, if not recognized, by defult, the R132 version is used (the newest one).
	if(-d $folder_struct.$version."/") { $folder_struct .= "$version/"; }
	else { $folder_struct .= "R132/"; }
}
$folder_struct .= "Structures/";
#2.6: We open the needed directory containing structures data
opendir (DIR2, $folder_struct) || die "can't opendir Structures folder $folder_struct: $!";

#2.0: For each file found in the directory where we can find structures of names used by the chosen database
LOOP02:while (defined($file = readdir(DIR2)))
{
	#2.0: No treatment for files '.' and '..'
  	if ($file =~ m/^\.\.?$/){next LOOP02;}
  	else {push(@files_struct, $file);}
}
closedir (DIR2);

#=======================================================================================================
#Second step, treatments of each file found in the given directory and for all taxonomic levels.
#=======================================================================================================

#Loop to treat all taxonomic levels for all files
foreach $file (@files_struct)
{
	#1.5: Creation of a parent and a child process. One child for each found file needed treatment
	$pid = fork();
	#1.5: If the program is the parent! (only one)
	if ($pid) {push(@childs, $pid);}
	#1.5: If the program is a child (number as number of files)
	elsif ($pid == 0)
	{
		#1.5: Needed empty variables to treat several files and levels
		@taxo_high = (); @taxo_possibilities = (); @taxo_others = (); @results = ();
		#We open the needed structure_file and store data in two arrays: @taxo_high, @taxo_possibilities
		open(F_STRUCT, "$folder_struct"."$file") || die "can't open $folder_struct"."$file";
		#Loop to treat every element found in the structure file
		while(<F_STRUCT>)
		{
			#We delete the "\n" of all lines
			$_ =~ s/\n//g;
			#We split the line based on "\t"
			@tmp = split("\t", $_);
			#We store needed data on two different arrays
			push (@taxo_high, $tmp[0]);
			push (@taxo_possibilities, $tmp[$#tmp]);
			#2.0: We also store all data not essential but informative in the @taxo_others table for the
			#creation of the file results.
			for($i=0;$i<$#tmp;$i++)
			{
				if (defined($taxo_others[$i])) {$taxo_others[$i] = $taxo_others[$i]."\t".$tmp[$i];}
				else {$taxo_others[$i] = $tmp[$i];}
			}

		}
		#We close the file F_STRUCT
		close (F_STRUCT);

		#Test for the addition of particular groups based on the chosen organism
		if ($organism eq "FUNGI")
		{
			#2.0: We add three more columns cells Unknown, Unclassified and Environmental
			push (@taxo_high, "Unknown");
			push (@taxo_possibilities, "Unknown");
			push (@taxo_high, "Unclassified");
			push (@taxo_possibilities, "Unclassified");
			push (@taxo_high, "Environmental");
			push (@taxo_possibilities, "Environmental");
		}
		elsif ($organism eq "BACTERIA")
		{
			if ($database eq "RDP")
			{
				#2.2: We add two more columns cells Unknown and Undefined
				push (@taxo_high, "Unknown");
				push (@taxo_possibilities, "Unknown");
				push (@taxo_high, "Undefined");
				push (@taxo_possibilities, "Undefined");
			}
			elsif ($database eq "SILVA")
			{
				#2.2: We add two more columns cells Unknown and Unclassified
				push (@taxo_high, "Unknown");
				push (@taxo_possibilities, "Unknown");
				push (@taxo_high, "Unclassified");
				push (@taxo_possibilities, "Unclassified");
				push (@taxo_high, "Environmental");
				push (@taxo_possibilities, "Environmental");
			}
			#2.7: Addition of this possibility for this new database
			elsif ($database eq "GREENGENES")
			{
				#2.7: We add two more columns cells Unknown and Unclassified
				push (@taxo_high, "Unknown");
				push (@taxo_possibilities, "Unknown");
				push (@taxo_high, "Unclassified");
				push (@taxo_possibilities, "Unclassified");
			}
		}
		elsif ($organism eq "ALGAE")
		{
			#2.6: We add two more columns cells Unknown and Unclassified
			push (@taxo_high, "Unknown");
			push (@taxo_possibilities, "Unknown");
			push (@taxo_high, "Unclassified");
			push (@taxo_possibilities, "Unclassified");
			push (@taxo_high, "Environmental");
			push (@taxo_possibilities, "Environmental");
		}

		#1.5: We catch also the name of the studied level (phylum, class, etc...)
		$file =~ s/Struct_//;	$file =~ s/\.txt//;		$level = $file;

		#1.5: Loop to read each file needed treatment and found in the given directory
		LOOP03:for($i=0;$i<=$#files;$i++)
		{
			#1.5: First of all, we add for all @taxo_possibilities the value 0
			for($j=0;$j<=$#taxo_high;$j++) {$results[$i][$j] = 0;}
			#1.5: Open the file in the given directory
			open (F, "Result_files/$folder/$files[$i]") || die "==> Can't open Result_files/$folder/$files[$i]\n";
			#1.5: For each line in the file
			LOOP04:while (<F>)
			{
				#Delete some unwanted characters if they are present
				$_ =~ s/\n//g;  $_ =~ s/"//g; $_ =~ s/\t-\t/\t\t/g;
				#We then search for known and defined taxonomic groups
				LOOP05:for($j=0;$j<=$#taxo_high;$j++)
				{
					#We look for lines containing the name of the group and the level
					if ($_ =~ m/$taxo_possibilities[$j]\t$level\t(\d\.\d*)/)
					{
						#Using the catched confidence value, we verify if it's higher than the chosen threshold
						if($1>=$chosen_percentage)
						{
							#We catch the number of occurrences for dereplicated reads
							$_ =~ m/_(\d*)_length=/;
							#We add the number for this group to the array @results
							$results[$i][$j]+=$1;
							#We go for the next read
							next LOOP04;
						}
						else
						{
							#We catch the number of occurrences for dereplicated reads
							$_ =~ m/_(\d*)_length=/;
							#We add the number for the Unknown group to the array @results
							#2.1 Modifs: We add a test to avoid the problem of Unknown reads for different organisms
							if ($organism eq "BACTERIA") {$results[$i][$#taxo_high-1]+=$1;}
							elsif ($organism eq "FUNGI") {$results[$i][$#taxo_high-2]+=$1;}
							elsif ($organism eq "ALGAE") {$results[$i][$#taxo_high-1]+=$1;}
							#We go for the next read
							next LOOP04;
						}
					}
				}
				#Problem here, we need to add another step to manage reads affiliated to organisms
				#without subclasses, orders, suborders or families.
				#We catch the number of occurrences for dereplicated reads
				$_ =~ m/_(\d*)_length=/;
				#We add the number for the Undefined group to the array @results
				#2.1 Modifs: We add a test to avoid the problem of Undefined or Unclassified reads for different organisms
				if ($organism eq "BACTERIA") {$results[$i][$#taxo_high]+=$1;}
				elsif ($organism eq "FUNGI") {$results[$i][$#taxo_high-1]+=$1;}
				elsif ($organism eq "ALGAE") {$results[$i][$#taxo_high]+=$1;}
			}
		}
		#We open the result file
		open (F_R, ">Summary_files/Taxonomy_results_$chosen_percentage"."_"."$level.txt");
		#We write needed data in it
		printf (F_R "Taxonomy_highest");
		#Such as highest taxonomy information
		foreach $tmp (@taxo_high) {printf (F_R "\t$tmp");}
		#2.0: We also add intermediate data to give some more indication to the user
		for($i=1;$i<=$#taxo_others;$i++) {printf(F_R "\nTaxonomy_intermediate\t$taxo_others[$i]");}
		#Such as taxonomy information at the defined level
		printf (F_R "\nTaxonomy_$level");
		foreach $tmp (@taxo_possibilities) {printf (F_R "\t$tmp");}
		printf (F_R "\n");
		#Loop to print all values found for each file treated
		for($i=0;$i<=$#files;$i++)
		{
			#2.6: Here, we modified the file name to keep only the specific sample name
			$sample = &ExtractSampleFromFileName($files[$i]);
			printf(F_R "$sample");
			for($j=0;$j<=$#taxo_high;$j++) {printf (F_R "\t$results[$i][$j]");}
			printf(F_R "\n");
		}
		close F_R;
		#Necessary for parallelized programs
		exit(0);
	}
	#If we can't fork (create parent and child processes, die the process)
 	else {die "couldn’t fork: $!\n";}
}
#The parent process wait for all child results
foreach (@childs) {waitpid($_, 0);}
