#=======================================================================================================
#         FILE:  Taxo_recovery_3.3.pl
#        USAGE:  perl Taxo_recovery_3.3.pl
#  DESCRIPTION:  This program have been developed to serve as a second filter after deletion of unique
#		  sequences using the Hunt_singleton_opt.pl. This PERL program needs 3 files to do the
#		  treatment. One with the taxonomic results for the deleted sequences (after assignment
#		  with the RDP multiclassifier), the second with the deleted sequences in FASTA format
#		  and the third with the cleaned sequences in FASTA format (these two last files are
#	  	  given by the Hunt_singleton_opt.pl).
#		  This program is dedicated to catch the sequences with good taxonomy assignments and
#		  reinject them in the file with cleaned sequences to be treated further. This program
#		  is based on the idea that sequences that can be affiliated are potentially "true"
#		  sequences.
#
#		  1.5 Modifications :  This program was modified to be included in the workflow to fast
#		  the analysis. All modified steps are described in the program (e.g.: using the
#		  name of the input and a different location for the output file ans summary file,
#		  deletion of the parallelization, as it was done by the main PERL program).
#		  We also add the taxonomy
#
#		  2.0 Modifications :  This optimisation was developed to include in the analysis and in
#		  the complete workflow the fungal analysis (based on the 18S database given by SILVA).
#		  To do this, we modify the acquisition of data, the treatement, etc... Details are
#		  described in the program itself.
#
#		  2.1 Modifs (31/10/2012): We add a step for the test concerning the file dedicated to the
#		  global_analysis step. This step is needed to summarize results in a dedicated file in a
#		  particular folder (global_analysis), and not the classical summary file.
#
#		  2.2 Modifs (05/06/13): We modify the program to add the new possibility to realize the
#		  taxonomic assignation with the SILVA database using the USEARCH program. To do this, we
#		  add a new possibility to launch and treat information and data. (See details in the
#		  program itself).
#
#		  2.8 Modifs (18/06/2013): The main modifications of this PERL program were realized to
#		  define and use a common directory for the databases, but also for all programs. So, we
#		  create a new variable ($folder_Tools). See details in the PERL program.
#
#		  2.9 Modifs (09/09/2014): Minor modifications of the summary file data given for the user.
#
#		  3.0 Modifs (08/08/2018): Addition of the ALGAE database and the FindBin package to manage
#		  easily such samples. Moreover, a small modification was also added to manage the specific
#		  case where user decide to delete taxonomy files after the RECOVERING step. We modified
#		  the program to manage the fact that a sample can be empty due to the absence of reads
#		  with the good MID. To manage that, the file name will be empty and will be not treated
#		  anymore if the number of reads is equal to 0.
#
#     3.1 Modifs (18/07/2019): Little modification to avoid the treatment of empty files from
#			the Hunting step. To do this, we catch the info from the main program with a new argument
#			given as input ($ARGV[11] => EMPTY or NOT).
#
#			3.2 Modifs (08/08/2019): Modifications to integrate the R132 fungal database and also the
#			USEARCH format results, in this particular step of RECOVERING. See details in the program.
#
#			3.3 Modifs (24/10/2019): A minor modification was done to manage the specific fact that
#			this step was critical. More precisely, here, each sample is checked based on its number
#			of reads, and let passed, or stopped as a too low number of reads was available (less than
#			9000). For cases with higher number of reads (between 9000 and 10000), a warning was given
#			to the user. To do this, the Recovering program send back to the main program the number
#			of reads with the sample name.
#
#       AUTHOR:  Sebastien TERRAT <Sebastien.Terrat@inra.fr>
#      COMPANY:  UMR Agroecologie - BIOCOM Team
#      VERSION:  3.3
#    REALEASED:  07/06/2011
#     REVISION:  24/10/2019
#=======================================================================================================
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#=======================================================================================================

#Print errors and warnings during the implementation of the perl program
use strict;
use warnings;
use File::Copy;
use FindBin;

#Defined variables
my @line = ();
my $j = 0;
my $tmp = "";
my $seq = "";
my $taxo_results = "";
my $verif = 0;
my $nb_save_seqs = 0;
my $nb_tot_reads = 0;
my $nb_hunt_reads = 0;
my $nb_hqual_reads = 0;
my @kept_seqs = ();
my @seqs = ();
my @tmp = ();

#3.1: Defined variables
my $organism = $ARGV[1];
my $file_hdel = $ARGV[0];
my $file_taxo = "";
my $file_hunt = $ARGV[6];
my $database = $ARGV[2];
my $taxo_level = $ARGV[3];
my $threshold = $ARGV[4];
my $cores = $ARGV[9];
my $version = $ARGV[10];
my $emptied = $ARGV[11];


#2.3: Here, we define the absolute path to find the RDP Multiclassifier program, but also all PERL
#programs essentail to launch taxonomical analyses.
my $folder_Tools = "$FindBin::RealBin/Data/Tools";
#3.1: Test to avoid the treatment of emptied hunting files (no singleton OTUs)
if($emptied eq "NOT")
{
	#2.0: Test to verify the type of microorganism studied to know what program will be launched
	if (uc($organism) eq "BACTERIA")
	{
		#1.5: Using the chosen database to do the taxonomic assignment (here RDP)
		if (uc($database) eq "RDP")
		{
			#1.5: User choose if the taxonomic files of the deleted reads are kept or not.
			if(uc($ARGV[5]) eq "YES")
			{
				#1.5: We launch the multilassifier of RDP (V1.4) to assign the deleted reads by the hunt program
				$taxo_results = `java -jar $folder_Tools/RDP_Multiclassifier/MultiClassifier.jar Result_files/$ARGV[7]/$file_hdel --assign_outfile=Result_files/$ARGV[8]/Taxo_RDP_$file_hdel`;
				#1.5: We kept the location where the files are saved
				$file_taxo = "Result_files/$ARGV[8]/Taxo_RDP_$file_hdel";
			}
			#1.5: User choose if the taxonomic files of the deleted reads are kept or not.
			else
			{
				#1.5: We launch the multilassifier of RDP (V1.4) to assign the deleted reads by the hunt program
				$taxo_results = `java -jar $folder_Tools/RDP_Multiclassifier/MultiClassifier.jar Result_files/$ARGV[7]/$file_hdel --assign_outfile=Temporary_files/Taxo_RDP_$file_hdel`;
				#1.5: We kept the location where the files are saved
				$file_taxo = "Temporary_files/Taxo_RDP_$file_hdel";
			}
		}
		#2.2: Using the chosen database to do the taxonomic assignment (here Silva)
		elsif(uc($database) eq "SILVA")
		{
			#2.2: We launch the USEARCH step against the SILVA database to assign the deleted reads by the hunt program
			#2.2: Two arguments were added ($version to manage the database version) and the $ARGV[5] to check if the file
			#with taxonomic results are kept or not
			$taxo_results = `perl $FindBin::RealBin/Bacteria_SILVA_Taxonomy_2.2.pl $ARGV[7] $ARGV[8] $file_hdel $cores $threshold $version $ARGV[5]`;
			#2.2: User choose if the taxonomic files of the deleted reads are kept or not.
			#2.2: We kept the location where the files are saved
			if(uc($ARGV[5]) eq "YES") { $file_taxo = "Result_files/$ARGV[8]/Taxo_SILVA_$file_hdel"; }
			else { $file_taxo = "Temporary_files/Taxo_SILVA_$file_hdel"; }
		}
	}
	#2.0: Test to verify the type of microorganism studied to know what program will be launched
	elsif (uc($organism) eq "FUNGI")
	{
		#2.0: Using the chosen database to do the taxonomic assignment (here RDP)
		if (uc($database) eq "RDP")
		{
		}
		#2.0: Using the chosen database to do the taxonomic assignment (here SILVA)
		elsif (uc($database) eq "SILVA")
		{
			#3.2: Temporary test to keep both methods (USEARCH and BLAST) for both versions of the FUNGI database (R114 and R132)
			if($version eq "R114")
			{
				#2.0: We launch the BLAST step against the SILVA database to assign the deleted reads by the hunt program
				$taxo_results = `perl $FindBin::RealBin/Fungi_SILVA_Taxonomy_3.5.pl $ARGV[7] $ARGV[8] $file_hdel $cores $threshold $ARGV[5]`;
				#1.5: User choose if the taxonomic files of the deleted reads are kept or not.
				#2.0: We kept the location where the files are saved
				if(uc($ARGV[5]) eq "YES") { $file_taxo = "Result_files/$ARGV[8]/Taxo_SILVA_$file_hdel"; }
				else { $file_taxo = "Temporary_files/Taxo_SILVA_$file_hdel"; }
			}
			else
			{
				#3.2: We launch the USEARCH step against the SILVA database to assign the deleted reads by the hunt program
				$taxo_results = `perl $FindBin::RealBin/Fungi_SILVA_Taxonomy_4.0.pl $ARGV[7] $ARGV[8] $file_hdel $cores $threshold $version $ARGV[5]`;
				#1.5 and 3.2: User choose if the taxonomic files of the deleted reads are kept or not.
				#2.0 and 3.2: We kept the location where the files are saved
				if(uc($ARGV[5]) eq "YES") { $file_taxo = "Result_files/$ARGV[8]/Taxo_SILVA_$file_hdel"; }
				else { $file_taxo = "Temporary_files/Taxo_SILVA_$file_hdel"; }
			}
		}
	}

	#3.0: Test to verify the type of microorganism studied to know what program will be launched
	if (uc($organism) eq "ALGAE")
	{
		#3.0: Using the chosen database to do the taxonomic assignment (here Silva)
		if(uc($database) eq "SILVA")
		{
			#3.0: We launch the USEARCH step against the SILVA database to assign the deleted reads by the hunt program
			$taxo_results = `perl $FindBin::RealBin/Algae_SILVA_Taxonomy_1.1.pl $ARGV[7] $ARGV[8] $file_hdel $cores $threshold $ARGV[5]`;
			#3.0: User choose if the taxonomic files of the deleted reads are kept or not.
			#3.0: We kept the location where the files are saved
			if(uc($ARGV[5]) eq "YES") { $file_taxo = "Result_files/$ARGV[8]/Taxo_SILVA_$file_hdel"; }
			else { $file_taxo = "Temporary_files/Taxo_SILVA_$file_hdel"; }
		}
	}
}

$threshold = $threshold/100;
#3.1: Test to avoid the treatment of emptied hunting files (no singleton OTUs)
if($emptied eq "NOT")
{
	#1.5: Open the file in the IN/ directory
	open (F_taxo, "$file_taxo") || die "==> Can't open $file_taxo\n";
	#For each line in the taxonomic result file
	LOOP01:while (<F_taxo>)
	{
		#Needed empty variable
		@line = ();
	 	#1.5: Delete some unwanted characters if they are present
	 	$_ =~ s/\n//g;  $_ =~ s/"//g;   $_ =~ s/\t-\t/\t\t/g;
	 	#Split each treated line based on the character 'tabulation'
	 	@line = split("\t",$_);
	 	#For each element in the line
	 	for($j=0;$j<=$#line;$j++)
	 	{
	 		#1.5: If we found the position of the taxonomic level chosen
	 		if ($line[$j] eq $taxo_level)
	 		{
	 			#1.5: If the percentage of similarity is higher or equal than the chosen threshold, we
	 			#kept this sequence in @kept_seqs (only the ID)
	 			if ($line[$j+1]>=$threshold) {push(@kept_seqs, $line[0]);}
	 		}
	 	}
	}
	#Close the taxonomic result file
	close F_taxo;
}

#3.0: If the taxonomy files are not kept based on user choices, we need
#to delete them into the temporary directory, if kept, they have already been
#produced into the Result_files sub directory
if(uc($ARGV[5]) eq "NO") { unlink($file_taxo); }
#3.1: Test to avoid the treatment of emptied hunting files (no singleton OTUs)
if($emptied eq "NOT")
{
	#1.5: Open the file containing deleted reads
	open (F_del, "Result_files/$ARGV[7]/$file_hdel") || die "==> Can't open $file_hdel\n";
	#For each line of the file (containing deleted sequences)
	LOOP04:while (<F_del>)
	{
		#If the line is the ID one
		if ($_ =~ s/>//)
		{
			#We delete the last character (\n)
			chomp $_;
			#For each kept sequence
			foreach $seq (@kept_seqs)
			{
				#If the ID in the file is equal to one in the array, we kept the sequence
				#and go for the next line in the file
				if ($seq eq $_) {$tmp = ">".$seq."\n"; $verif = 1; next LOOP04;}
			}
		}
		#If $verif is equal to one (only if we found a good sequence), we concatenate the ID
		#and the sequence itself
		if ($verif == 1){$tmp = $tmp.$_; push (@seqs,$tmp); $verif = 0;}
	}
}
$nb_save_seqs = $#kept_seqs+1;

#1.5 : Copy the cleaned file in a new file to then add kept sequences
copy("Result_files/$ARGV[7]/$file_hunt","Result_files/$ARGV[8]/Clean_$file_hunt");

#1.5: Create a new file in the OUT/ directory with good sequences
open (F_result, ">>Result_files/$ARGV[8]/Clean_$file_hunt") || die "==> Can't open Clean_$file_hunt\n";
foreach $seq (@seqs) {printf (F_result "$seq");}
close F_result;

#2.1: Test for the file dedicated to the global_analysis step
if($file_hunt =~ m/Concatenated\_reads\.fasta\.fasta/)
{
	#2.9: We open the needed summary file
	open (F_HUNT, "Summary_files/Global_Analysis/Hunting_ss_summary_results.txt") || die "==> Can't open Summary of HUNTING step\n";
	#2.9: We delete the first line of the hunting summary file
	$tmp = <F_HUNT>;
	#2.9: We then search for the needed information of the analyzed file
	while (<F_HUNT>)
	{
		@tmp = split("\t", $_);
		#2.9: We keep needed data for the summary file
		if ($file_hunt =~ m/$tmp[0]/)
		{
			$nb_tot_reads = $tmp[1];
			$nb_hunt_reads = $tmp[2];
			$nb_hqual_reads = $nb_tot_reads - $nb_hunt_reads + $nb_save_seqs;
		}
	}
	close (F_HUNT);
	#1.5: Adding data to the summary file
	open (F_summary, ">>Summary_files/Global_Analysis/RECOVERING_step_summary.txt") || die "==> Can't open Summary.txt\n";
}
else
{
	#2.9: We open the needed summary file
	open (F_HUNT, "Summary_files/Hunting_ss_summary_results.txt") || die "==> Can't open Summary of HUNTING step\n";
	#2.9: We delete the first line of the hunting summary file
	$tmp = <F_HUNT>;
	#2.9: We then search for the needed information of the analyzed file
	while (<F_HUNT>)
	{
		@tmp = split("\t", $_);
		#2.9: We keep needed data for the summary file
		if ($file_hunt =~ m/$tmp[0]/)
		{
			$nb_tot_reads = $tmp[1];
			$nb_hunt_reads = $tmp[2];
			$nb_hqual_reads = $nb_tot_reads - $nb_hunt_reads + $nb_save_seqs;
		}
	}
	close (F_HUNT);
	#1.5: Adding data to the summary file
	open (F_summary, ">>Summary_files/RECOVERING_step_summary.txt") || die "==> Can't open Summary.txt\n";
}
flock (F_summary, 2);
printf(F_summary "Clean_$file_hunt\t$nb_tot_reads\t$nb_hunt_reads\t$nb_save_seqs\t$nb_hqual_reads\n");
flock (F_summary, 8);
close (F_summary);

#1.5: Step to delete taxonomy files if the user chose to not store them
if(uc($ARGV[5]) eq "NO") {unlink("$file_taxo");}

#3.3: We pipe the name of the created file (the result file) for the main program,
#taking into account that the file can be empty (if it's the case, the file
#name will be kept, but the number of reads is equal to 0)
print "Clean_$file_hunt $nb_hqual_reads";
