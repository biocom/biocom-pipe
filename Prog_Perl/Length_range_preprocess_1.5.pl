#=======================================================================================================
#         FILE:  Length_range_preprocess_1.5.pl
#        USAGE:  perl Length_range_preprocess_1.5.pl
#  DESCRIPTION:  This PERL program is a simple one, developed to count the number of sequences for each
#			  length found to know if we are homogen or not.
#
#			  1.5 Modifications: Transformation of the program to be launched automatically with
#			  another PERL program to develop an automatic workflow of analysis (e.g., we delete the
#			  the loop to analyze and search each file needed treatment).
#
# REQUIREMENTS:  Perl language only
#       AUTHOR:  Sebastien TERRAT <sebastien.terrat@dijon.inra.fr>
#      COMPANY:  UMR-MSE GenoSol platform, Team5
#      VERSION:  1.5
#    REALEASED:  21/12/2011
#     REVISION:  11/04/2012
#=======================================================================================================
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#=======================================================================================================

#Print errors and warnings during the implementation of the perl program
use strict;
use warnings;

#Defined variables
my $i, my $j = 0,0;
my $nbseq_tot = 0;
my $nbseq_treated = 0;
my @length_range = ();
my $sub_somme = 0;
my $file = "";
my $folder = "";
my $length = 0;

#=======================================================================================================
#First step, store all sequences in the @tab_seq for further analysis
#=======================================================================================================

#1.5 : Modifications to catch needed information given by the workflow
$length = $ARGV[0];
$file = $ARGV[1];
$folder = $ARGV[2];

LOOP01:for ($i=0; $i<5000; $i++) { $length_range[$i] = 0; }

#1.5: Open file one given by the user and containing cleaned sequences
open(F1, "Result_files/$folder/$file")|| die "==> File Not Found: $file<==";

BOUCLE1:while (<F1>)
{
	#If the line contains the character : "length="
	if ($_ =~ m/length=(\d*)/)
	{
		#We verify the length of each read
		if ($1 < $length)
		{
			#We count the total number of reads
			$nbseq_tot++;
		}
		else
		{
			#We store the read length
			$length_range[$1]++;
			#We count the number of treated reads
			$nbseq_treated++;
			#We count also the total number of reads
			$nbseq_tot++;
		}
	}
}

#Close the treated file
close F1;

#1.5: File to store results
open(F2, ">Summary_files/Length_range_$length"."_"."$file.txt")|| die "==> Can't create file of the Length_range step of analysis<==";
#1.5: We print it for the user
printf (F2 "Defined length threshold to analyze the read:\t$length\n");
printf (F2 "Total number of reads (with those eliminated by the threshold):\t$nbseq_tot\n");
printf (F2 "Number of reads analyzed based on the threshold defined:\t$nbseq_treated\n");
printf (F2 "Length range\tNumber of reads in the range\n");
#Loop to analyze all length data
for($i=0; $i<5000; $i++)
{
	#For each step of five bases
	if (($i%5) == 0)
	{
		#We count the number of reads comprised in the given range
		$sub_somme = $sub_somme + $length_range[$i];
		#If we have more than 0 reads
		if($sub_somme !=0)
		{
			#We print in the result file
			printf(F2 "]$j-$i]\t$sub_somme\n");
			#We catch the needed information and empty variable
			$sub_somme = 0;
			$j = $i;
		}
		else { $j = $i; }
	}
	else
	{
		#We just count the number of reads in the studied range
		$sub_somme = $sub_somme + $length_range[$i];
	}
}
close F2;
#We send information to the main program
print "Length_range_$length"."_"."$file.txt";
