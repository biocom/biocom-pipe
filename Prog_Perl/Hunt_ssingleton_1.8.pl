#=======================================================================================================
#         FILE:  Hunt_ssingleton_1.8.pl
#        USAGE:  perl Hunt_ssingleton_1.8.pl
#  DESCRIPTION:  This program was defined firstly to delete all single-singletons found based on the
#		cluster level defined by the user. All files needs to be stored in the IN/ directory
#		and defined in the Input text file. The program will analyze first the clustering, and
#		then delete from the .fasta file all reads below the threshold defined. This threshold
#		is calculated based on the total number of reads (threshold = 0.01*nbreads/100). For
#		example, for 30000 reads, we will delete all clusters composed by less than 1, 2 or 3
#		reads, excepted for clusters with dereplicated reads (i.e. reads with more than one
#		occurence). In the OUT/ directory, two files will be then created, one with the
#		deleted reads, and one with kept reads for further analysis.
#		All steps have been parallelized, to fast the analysis.
#
#		1.5 Modifs (09/05/2012): This program was modified to be included in the workflow to fast
#		the analysis. All modified steps are described in the program (e.g.: using the
#		name of the input and a different location for the output file ans summary file,
#		deletion of the parallelization, as it was done by the main PERL program).
#
#		1.6 Modifs (31/10/2012): We add a step for the test concerning the file dedicated to the
#		global_analysis step. This step is needed to summarize resultas in a dedicated file in a
#		particular folder (Global_Analysis), and not the classical summary file.
#
#		1.7 Modifs (07/08/2018): We modified the program to manage the fact that a sample can be
#		empty due to the absence of reads with the good MID. To manage that, the file name will
#		be empty and will be not treated anymore if the number of reads is equal to 0.
#
#   1.8 Modifs (15/08/2019): We modified some lines of the program to manage the fact that a
#		new step was added to the pipeline: FILTERING_RAW_ALIGNMENT. Indeed, this step added
#		prefixes to filenames, and these prefixes must have been taken into account.
#
# REQUIREMENTS:  Perl language only and also POSIX module for trigonometric steps
#                included with the Perl basic module
#       AUTHOR:  Sebastien TERRAT <sebastien.terrat@inra.fr>
#      COMPANY:  UMR Agroecologie - BIOCOM Team
#      VERSION:  1.8
#    REALEASED:  18/05/2011
#     REVISION:  15/08/2019
#=======================================================================================================
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#=======================================================================================================

#!/usr/bin/perl

#Print errors and warnings during the implementation of the perl program
use strict; use warnings;
use POSIX;

#Defined variables
my $pid = 0; my @childs = ();
my @file_fasta = ();
my @deleted_seqs = ();
my @file_clust = ();
my $seq = "";
my @clust = ();
my @tmp = ();
my @tmp2 = ();
my ($i,$j,$k) = (0,0,0);
my $good_cluster = 0;
my $chosen_clust = 0;
my $dist_cutoff= 0;
my $threshold = 0;
my $verif = 0;
my $nb_seqs_tot = 0;
my $nb_clusters_tot = 0;
my $nb_ssingletons = 0;
my $nb_clust_ssingletons = 0;

#Needed variables
$chosen_clust = $ARGV[0] / 100;
my $file = "";
my $file_clust = $ARGV[1];
my $file_fasta = $ARGV[1];
my $folder_input_clust = $ARGV[2];
my $folder_input_fasta = $ARGV[3];
my $folder_output = $ARGV[4];

#1.8: Treatment to define the name of the treated file using the clustering file name
$file_fasta =~ s/\.clust//;
$file_fasta =~ s/^Clust_Align_//;
$file_fasta =~ s/^Clust_FilterAlign_Align_/FilterAlign_/;

#1.5: Opening clustering file obtained
open(F1C,"Result_files/$folder_input_clust/$file_clust")|| die "==> File Not Found: $file_clust<==";
#1.5: Loop to read the clustering file
LOOP01:while (<F1C>)
{
	#Split each line based on found tabulations
	@tmp = split('\t', $_);
	#If we found the line giving the number of sequences
	if ($tmp[0] =~ /Sequences/)
	{
		#We catch the number of sequences to determine the used threshold
		$tmp[1] =~ s/\s\n//g;
		$nb_seqs_tot = $tmp[1];
		$threshold = ceil(0.01*$nb_seqs_tot/100);
	}
	#if the line is the one giving the distance cutoff used for the cluster
	if ($tmp[0] =~ /distance/)
	{
		#We catch this number and compare to the one given by the user
		$tmp[1] =~ s/\s\n//g;
		$dist_cutoff = $tmp[1]+0;
		#If we found the good cluster to analyze, we increase the value $good_cluster
		if ($dist_cutoff == $chosen_clust) {$good_cluster += 1;}
		#If we found a cluster higher than the one searched, we also increase $good_cluster
		#to avoid another analysis, as for one lower after we have found the one researched.
		if (($dist_cutoff > $chosen_clust) && ($good_cluster == 1)) {$good_cluster += 1;}
		if (($dist_cutoff < $chosen_clust) && ($good_cluster == 1)) {$good_cluster += 1;}
	}
	#If the number of elements in the line is higher than 2 and it's the good cluster
	if (($#tmp > 2) && ($good_cluster == 1))
	{
		#Test to verify if it's a single-singleton
		if ($tmp[2] == 1)
		{
			#We deleted the last character '\n'
			chomp ($tmp[3]);
			#We add the sequence name in the table @deleted_seqs
			push (@deleted_seqs, $tmp[3]);
			#Analysis of the next sequence
			next LOOP01;
		}
		#If the number of sequences in the cluster is below the threshold we analyze the good cluster
		elsif ($tmp[2] < ($threshold+1))
		{
			#We split the element based on spaces found
			@clust = split(" ",$tmp[3]);
			#Needed 0 value
			$verif = 0;
			#For each sequence in the cluster, if we found two identical sequences, we keep the
			#cluster and go for the next
			foreach $seq (@clust){@tmp2 = split("_",$seq);if ($tmp2[1]>=2) {next LOOP01;}}
			#If we don't found any identical sequences
			if ($verif==0)
			{
				#We delete this cluster, adding it in the table @deleted_seqs and go for the next one
				foreach $seq (@clust){$seq =~ s/\n//g;push(@deleted_seqs, $seq);}
				next LOOP01;
			}
		}
	}
}
#We close the analyzed file
close F1C;

#1.5: We open the sequence file to delete the sequences and create a cleaned file without singletons
open(F1F,"Result_files/$folder_input_fasta/$file_fasta")|| die "==> File Not Found: $file_fasta<==";
open(F1_CLEAN, ">Result_files/$folder_output/Hunt_$file_fasta")|| die "==> Can't create: Hunt_$file_fasta<==";
open(F1_DEL, ">Result_files/$folder_output/HDel_$file_fasta")|| die "==> Can't create: HDel_$file_fasta<==";

#1.6: Test for the file dedicated to the Global_Analysis step
if($file_fasta =~ m/Concatenated\_reads\.fasta\.fasta/)
{
	open(SUMMARY, ">>Summary_files/Global_Analysis/Hunting_ss_summary_results.txt")|| die "Can't access to summary file";
}
else
{
	open(SUMMARY, ">>Summary_files/Hunting_ss_summary_results.txt")|| die "Can't access to summary file";
}
flock(SUMMARY, 2);
$j = $#deleted_seqs+1;
$k = $nb_seqs_tot-$j;
printf(SUMMARY "$file_fasta\t$nb_seqs_tot\t$j\t$k\n");
flock(SUMMARY, 8);
close SUMMARY;

#For each line of the treated file
LOOP02:while (<F1F>)
{
	#If it's a descriptive line
	if ($_ =~ s/>//)
	{
		$_ =~ s/\s//g;
		foreach $seq (@deleted_seqs) {if($_ eq $seq){printf(F1_DEL ">$_\n");$verif=1;}}
		if ($verif == 0) {printf(F1_CLEAN ">$_\n");}
	}
	#If it's the sequence itself
	else
	{
		if ($verif == 0) {printf(F1_CLEAN "$_");next LOOP02;}
		if ($verif == 1) {printf(F1_DEL "$_");$verif=0;next LOOP02;}
	}

}
#We close the sequence file
close F1F; close F1_CLEAN; close F1_DEL;

#1.7: Print the filenames created by the hunt program, taking into account that
#the file can be empty (if it's the case, the file name will be followed by EMPTY)
if($k == 0) { print "Hunt_$file_fasta HDel_$file_fasta EMPTY"; }
else { print "Hunt_$file_fasta HDel_$file_fasta NOT"; }
