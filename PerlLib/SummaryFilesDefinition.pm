#=======================================================================================================
# PACKAGE NAME: SummaryFilesDefinition
#    FUNCTIONS:	 - SummaryFilesDefinition (Creation of all summary files using user choices),
#
# REQUIREMENTS:  Perl language, and Math library to compute binary files
#       AUTHOR:  Sebastien TERRAT <sebastien.terrat@dijon.inra.fr>
#      COMPANY:  UMR 1347 Agroecology - Platform GenoSol
#      VERSION:  1.1
#    REALEASED:  16/04/2014
#     REVISION:  XXX
#=======================================================================================================

# Name of the package defined for the main program
package  PerlLib::SummaryFilesDefinition;
# Needed libraries
use strict;
use warnings;

#=======================================================================================================
#=======================================================================================================
# MAIN FUNCTION CALLED BY THE MAIN PROGRAM
#=======================================================================================================
#=======================================================================================================
# FILE:  		SummaryFilesDefinition
# USAGE:  		PerlLib::SummaryFilesDefinition::SummaryFilesDefinition(ARGUMENTS)
# ARGUMENTS:  	- $_[0]: Name of the step given by the main program needed treatment,
#			- $_[1]: Integer value indicating when the merging is done in the main analysis.
#
#	  DETAILS:	1.1 (09/09/2014): Minor modification of the definition of the RECOVERING summary file
#				(name and some additions in the columns to simplify the information for the user).
#
#	  		1.2 (19/07/2018): A minor modification was made to the SummaryFilesDefinition to
#			manage the copy of the COMPUTATION dedicated file
#
# DESCRIPTION:	This function was defined to create and fill all summary files needed for the analysis
#			based on the user choices. The function takes two arguments as input, the name of the
#			step given by the main program, and the integer $concatenation_step indicating when the
#			merging of the sequences will be done for the global analysis. Based on that information
#			the needed summary file will be created using other parameters dedicated for the step.
#=======================================================================================================
sub SummaryFilesDefinition
{
	# Needed variables
	my $ref_array = "";
	my $folder_DB = "";
	my @parameters = ();
	my @param_demultiplexing = ();
	my @steps = ();
	my ($i, $h) = (0,0);
	# Input variables
	my $step = $_[0];
	my $concatenation_step = $_[1];

	#We catch the Parameters defined by the user in the Input.txt file
	$ref_array = PerlLib::Miscellaneous::SearchInputParameters($step);
	@parameters = @$ref_array;
	#As the dereplication step is automatically realized, we defined the choice as "YES"
	if($step eq "DEREPLICATION") { $parameters[0] = "YES"; }

	#Creation of the file for the homogenization step of the raw reads
	if ((uc($parameters[0]) eq "YES")&&($step eq "RANDOM_PREPROCESS_MIDS"))
	{
		open(F_Summary, ">Summary_files/Random_MIDs_$parameters[1].txt")|| die "==> Can't create file of the Random_MIDs step of analysis<==";
		close F_Summary;
	}
	#Creation of the file for the homogenization step of the reads after preprocessing
	elsif ((uc($parameters[0]) eq "YES")&&($step eq "RANDOM_PREPROCESS"))
	{
		open(F_Summary, ">Summary_files/Random_Preprocessed_$parameters[1].txt")|| die "==> Can't create file of the Random_Preprocessed step of analysis<==";
		close F_Summary;
	}
	#Creation of the file for the homogenization step of the reads after all filtering steps
	elsif ((uc($parameters[0]) eq "YES")&&($step eq "RANDOM_CLEANED"))
	{
		open(F_Summary, ">Summary_files/Random_Cleaned_$parameters[1].txt")|| die "==> Can't create file of the Random_Preprocessed step of analysis<==";
		close F_Summary;
	}
	#Creation of the file for the preprocessing step
	elsif ((uc($parameters[0]) eq "YES")&&($step eq "PREPROCESSING"))
	{
		open(F_Summary, ">Summary_files/Preprocessing_L$parameters[1]_N$parameters[2]_F_$parameters[3]_R_$parameters[4].txt")|| die "==> Can't create file of the Preprocessing step of analysis<==";
		printf (F_Summary "Minimum length threshold:\t$parameters[1]\n");
		printf (F_Summary "Number of ambiguities tolerated:\t$parameters[2]\n");
		printf (F_Summary "Forward primer:\t$parameters[3]\nReverse primer:\t$parameters[4]\n");
		printf (F_Summary "Number of tolerated differences in the forward primer sequence:\t$parameters[5]\n");
		printf (F_Summary "Number of tolerated differences in the reverse primer sequence:\t$parameters[6]\n");
		printf (F_Summary "Stringency of the distal primer:\t$parameters[7]\n\n");
		printf (F_Summary "File name\tRaw_reads\tAfter_length_filter\tAfter_ambiguities_filter\tAfter_primer_filter\tKept_reads\tAverage_length");
		#We catch the Parameters defined by the user in the Input.txt file
		$ref_array = PerlLib::Miscellaneous::SearchInputParameters("PREPROCESS_MIDS");
		@param_demultiplexing = @$ref_array;
		if(uc($param_demultiplexing[0]) eq "YES") { printf (F_Summary "\tMID_F_Name\tMID_F_Sequence\tMID_R_Name\tMID_R_Sequence\n"); }
		else { printf (F_Summary "\n"); }
		close F_Summary;
	}
	#Creation of the file for the quality cleaning step of the raw reads
	elsif ((uc($parameters[0]) eq "YES")&&($step eq "QUALITY_CLEANING"))
	{
		open(F_Summary, ">Summary_files/Quality_Cleaning.txt")|| die "==> Can't create file of the Quality_Cleaning step of analysis<==";
		printf (F_Summary "Chosen parameters:\nLowest quality score tolerated:\t$parameters[1]\n");
		printf (F_Summary "Lowest average quality score tolerated:\t$parameters[2]\n");
		printf (F_Summary "File name\tRaw reads\tReads deleted due to low score\tReads deleted due to low average score\tKept reads after cleaning step\n");
		close F_Summary;
	}
	#Creation of the file for the dereplication step of the reads
	elsif((uc($parameters[0]) eq "YES")&&($step eq "DEREPLICATION"))
	{
		open(F_Summary, ">Summary_files/Dereplication_summary_results.txt")|| die "==> Can't create file of the Dereplication step of analysis<==";
		#1.2 Modifications (addition of sample to summary file)
		printf (F_Summary "Sample\tFile name\tRaw reads\tUnique reads\tPercent of dereplication\n");
		close F_Summary;
		PerlLib::ManagingGlobalAnalysis::SummaryFilesDefinition("Dereplication_summary_results.txt", "DEREPLICATION");
	}
	#Creation of the file for the filtering of the alignment step
	elsif((uc($parameters[0]) eq "YES")&&($step eq "FILTERING_RAW_ALIGNMENT"))
	{
		open(F_Summary, ">Summary_files/Filtering_Raw_Infernal_Alignment.txt")|| die "==> Can't create file of the Filtering Alignment step of analysis<==";
		printf(F_Summary "Chosen parameter:\nQuality alignment threshold (percent):\t$parameters[1]\n");
		printf(F_Summary "Sample\tFilename\tNb Rawreads\tDeleted Nbreads\n");
		close F_Summary;
	}
	#Creation of the file for the hunting ssingleton step
	elsif((uc($parameters[0]) eq "YES")&&($step eq "HUNTING"))
	{
		open(F_Summary, ">Summary_files/Hunting_ss_summary_results.txt")|| die "==> Can't create file of the Hunting ssingleton step of analysis<==";
		printf(F_Summary "Filename\tTotal Nbreads\tNbreads deleted\tNbreads kept\n");
		close F_Summary;
		if ($concatenation_step == 1) { PerlLib::ManagingGlobalAnalysis::SummaryFilesDefinition("Hunting_ss_summary_results.txt", "HUNTING"); }
	}
	#Creation of the file for the taxo recovering step
	elsif((uc($parameters[0]) eq "YES")&&($step eq "RECOVERING"))
	{
		$parameters[1] = uc($parameters[1]);
		open(F_Summary, ">Summary_files/RECOVERING_step_summary.txt")|| die "==> Can't create file of the Recovering step of analysis<==";
		printf(F_Summary "Type of microorganisms studied:\t$parameters[1]\nDatabase used:\t$parameters[2]\nTaxonomic level chosen:\t$parameters[3]\nConfidence threshold chosen:\t$parameters[4]\n\n");
		printf(F_Summary "File name\tTotal Number of Reads\tReads deleted by the HUNTING step\tReads recovered\tTotal of high-quality reads\n");
		close F_Summary;
		if ($concatenation_step == 1) { PerlLib::ManagingGlobalAnalysis::SummaryFilesDefinition("RECOVERING_step_summary.txt", "RECOVERING"); }
	}
	#Creation of the file for the clustering step (RAW and/or CLEAN)
	elsif ((uc($parameters[0]) eq "YES")&&(($step eq "RAW_CLUSTERING")||($step eq "CLEAN_CLUSTERING")))
	{
		#We create here the needed @steps array to realize all clustering steps. Here, we catch the first
		#clustering step
		$h = $parameters[4];
		#Then, all others
		while($h<=$parameters[3] + $parameters[4])
		{
			push(@steps, $h);
			$h = $h + $parameters[4];
		}
		#We then define the last one to avoid some not wanted clustering steps.
		for($i=0;$i<=$#steps;$i++)
		{
			if ($steps[$i] >= $parameters[3])
			{
				$steps[$i] = $parameters[3];
				@steps = @steps[0..$i];
				last;
			}
		}
		#Loop to treat all summarized temporary files to give the user clear summary files
		foreach $i (@steps)
		{
			$i = sprintf("%.2f",$i);
			if ($step eq "RAW_CLUSTERING")
			{
				open(F_Summary, ">Summary_files/Raw_Clustering_summary_$i.txt")|| die "==> Can't create file for the Clustering step of analysis<==";
				if ($concatenation_step == 1) { PerlLib::ManagingGlobalAnalysis::SummaryFilesDefinition("Raw_Clustering_summary_$i.txt", "RAW_CLUSTERING"); }

			}
			elsif ($step eq "CLEAN_CLUSTERING")
			{
				open(F_Summary, ">Summary_files/Clean_Clustering_summary_$i.txt")|| die "==> Can't create file for the Clean_Clustering step of analysis<==";
				if ($concatenation_step == 2) { PerlLib::ManagingGlobalAnalysis::SummaryFilesDefinition("Clean_Clustering_summary_$i.txt", "CLEAN_CLUSTERING"); }
			}
			#1.2 Modifications (file name)
			printf(F_Summary "Treated clustering (percent of dissimilarity):\t$i\n");
			printf(F_Summary "Sample\tFilename\tNbreads\tNbOTUs\n");
			close F_Summary;
		}
		@steps = ();
	}
	#Creation of the files for the computation step
	elsif((uc($parameters[0]) eq "YES")&&($step eq "COMPUTATION"))
	{
		$parameters[2] = $parameters[2]/100;
		if((uc($parameters[1]) eq "RAW")||(uc($parameters[1]) eq "BOTH"))
		{
			#1.2 Modifications (file name and sample)
			open(F_Summary, ">Summary_files/Indices_Independent_Raw_$parameters[2].txt")|| die "==> Can't create file of the computation step of analysis<==";
			printf(F_Summary "Sample\tFilename\tNb_seqs\tNb_clusters\t");
			if (uc($parameters[5]) eq "YES") { printf(F_Summary "Chao1\tvar(Chao1)\tLCI95\tUCI95\t"); }
			if (uc($parameters[6]) eq "YES") { printf(F_Summary "ACE\tRare_ACE\tAbundant_ACE\t"); }
			if (uc($parameters[7]) eq "YES") { printf(F_Summary "Bootstrap\tShannon\tvar(Shannon)\tEvenness\tSimpson\t1/Simpson"); }
			printf(F_Summary "\n");
			close F_Summary;
			#1.2 Modifications (file name)
			PerlLib::ManagingGlobalAnalysis::SummaryFilesDefinition("Indices_Independent_Raw_$parameters[2].txt", "COMPUTATION");
		}
		if((uc($parameters[1]) eq "CLEAN")||(uc($parameters[1]) eq "BOTH"))
		{
			#1.2 Modifications (file name)
			open(F_Summary, ">Summary_files/Indices_Independent_Clean_$parameters[2].txt")|| die "==> Can't create file of the computation step of analysis<==";
			printf(F_Summary "Sample\tFilename\tNb_seqs\tNb_clusters\t");
			if (uc($parameters[5]) eq "YES") { printf(F_Summary "Chao1\tvar(Chao1)\tLCI95\tUCI95\t"); }
			if (uc($parameters[6]) eq "YES") { printf(F_Summary "ACE\tRare_ACE\tAbundant_ACE\t"); }
			if (uc($parameters[7]) eq "YES") { printf(F_Summary "Bootstrap\tShannon\tvar(Shannon)\tEvenness\tSimpson\t1/Simpson"); }
			printf(F_Summary "\n");
			close F_Summary;
			#1.2 Modifications (file name)
			PerlLib::ManagingGlobalAnalysis::SummaryFilesDefinition("Indices_Independent_Clean_$parameters[2].txt", "COMPUTATION");
		}
	}
	#Creation of the file for the global analysis step
	elsif((uc($parameters[0]) eq "YES")&&($step eq "GLOBAL_ANALYSIS"))
	{
		open(F_Summary, ">Summary_files/Global_Analysis/Concatenated_files.txt")|| die "==> Can't create file of the Global Analysis step of treatment<==";
		close(F_Summary);
	}

	#Creation of the file for the ReCLUSTOR step
	elsif ((uc($parameters[0]) eq "YES")&&($step eq "RECLUSTOR"))
	{
		$i = 100-$parameters[6];
		$folder_DB .= uc($parameters[4])."_".uc($parameters[5])."_".$i;
		open(F_Summary, ">Summary_files/ReClustOR/Clustering_summary_$i.txt")|| die "==> Can't create file of the ReClustOR summary file<==\n";
		printf(F_Summary "Treated clustering (percent of similarity):\t$i\n");
		printf(F_Summary "Name of the database used:\t$folder_DB\n");
		printf(F_Summary "Sample\tFilename\tTotalNbOTUs\tTotalNbreads\tNbOTUs_Affiliated\tNbreads_Affiliated\tNbOTUs_NotAffiliated\tNbreads_NotAffiliated\n");
		close(F_Summary);
	}

}
# End of the function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

1;
