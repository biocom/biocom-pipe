#=======================================================================================================
# PACKAGE NAME: Taxonomy
#    FUNCTIONS: - DefineInputFolderFile (Research of the needed folder and file for treatement),
# REQUIREMENTS: Perl language only
#       AUTHOR: Sebastien TERRAT <sebastien.terrat@inra.fr>
#      COMPANY: UMR 1347 Agroécologie
#      VERSION: 3.0
#=======================================================================================================

# Name of the package defined for the main program
package PerlLib::Taxonomy;

# Needed libraries
use strict;
use warnings;
use FindBin;

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function dedicated to the research and the definition of the good folder to select for the TAXONOMY
# step. Indeed, the main program can potentially give the previous folder, such as the RAW_ALIGNEMENT,
# that contains sequences, but not well suited for the TAXONOMY step. This function, based on the list
# of created folders, and the name of the file given by the main program, will define the best folder to
# use and the name of the corresponding file to treat. This function takes as input two elements : the
# reference array of the @folders array, and the name of the file given by the main program. As output,
# two elements are given : the name of the chosen folder, and the name of the good file.
sub DefineInputFolderFile
{
	#Defined variables
	my @folders = @{$_[0]};
	my $file = $_[1];
	my @files = ();
	my $folder = "";
	my $chosen_folder = "";
	my $ref_array = "";
	my $file_name = "";
	my $found = 0;

	#Here, we search for the good folder that will be used for the TAXONOMY step
	if($found == 0) { LOOP01:foreach $folder (@folders) { if($folder eq "RANDOM_CLEANED") { $found = 1; $chosen_folder = "RANDOM_CLEANED"; last LOOP01; } } }
	if($found == 0) { LOOP02:foreach $folder (@folders) { if($folder eq "RECOVERING") { $found = 1; $chosen_folder = "RECOVERING"; last LOOP02; } } }
	if($found == 0) { LOOP03:foreach $folder (@folders) { if($folder eq "HUNTING") { $found = 1; $chosen_folder = "HUNTING"; last LOOP03; } } }
	if($found == 0) { LOOP04:foreach $folder (@folders) { if($folder eq "DEREPLICATION") { $found = 1; $chosen_folder = "DEREPLICATION"; last LOOP04; } } }
	if($found == 0) { $chosen_folder = "Raw_data"; }

	#Then, we will search all files in this folder to seek the good one, corresponding to the last step
	if ($chosen_folder ne "Raw_data")
	{
		$ref_array = PerlLib::Miscellaneous::ListFilesInFolder($chosen_folder);
		@files = @{$ref_array};
	}
	else
	{
		#Open the directory containing all alignment files for all independant samples
		opendir (DIR, "Raw_data") || die "can't opendir Raw_data: $!";
		#For each file found in the directory Raw_data/
		LOOP:while (defined($file_name = readdir(DIR)))
		{
			#No treatment for files '.' and '..'
		  	if ($file_name =~ m/^\.\.?$/){ next LOOP; }
		  	#We store their names in the align_file_names array
		  	else { push(@files, $file_name); }
		}
		#We then close the directory
		closedir DIR;
	}

	#Finally, among all filenames, we will search the needed file that will be treated
	foreach $file_name (@files)
	{
		if($file =~ m/\Q$file_name/) { return ($chosen_folder, $file_name); }
	}
}
# End of the function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~



#=======================================================================================================
#=======================================================================================================
# MAIN FUNCTION CALLED BY THE MAIN PROGRAM
#=======================================================================================================
#=======================================================================================================
# FILE:		Taxonomy.pm (called by the main program)
# USAGE:		PerlLib::Taxonomy::Taxonomy(ARGUMENTS)
# ARGUMENTS:	- $_[0]: the name of input file that needed treatment,
#			- $_[1]: the user choice for the organism (bacteria, or fungi),
#			- $_[2]: the user choice for the wanted database (RDP, or SILVA),
#			- $_[3]: the user choice for the number of cores for the treatment,
#			- $_[4]: the output folder,
#			- $_[5]: the user choice for the defined threshold for the taxonomic assignation,
#			- $_[6]: a reference array giving access to all defined folders.
#
#=======================================================================================================
#  DESCRIPTION:     This function is dedicated to the taxonomical treatment of sequences (fungal or
#			  bacterial) based on user choices. Indeed, with the organism name and the database used,
#			  we can treat almost all possibilities (RDP Multiclassifier, SILVA databases with fungal
#			  or bacterial sequences, etc...).
#
#			  2.0 Modifications :  This program was modified to be included in the workflow to fast
#			  the analysis. All modified steps are described in the program (e.g.: using the
#			  name of the input and a different location for the output file ans summary file,
#			  deletion of the parallelization, as it was done by the main PERL program).
#
#			  2.5 Modifs (05/10/2012) :  Modifs were mainly the case of the fungi analysis, and the
#			  dedicated database to taxonomically assign all reads. We add two tests, one to specify
#			  the organism studied, the second the chosen database. Then, the dedicated program will
#			  be launched to do the analysis using the given parameters as input (e.g. the needed
#			  folders, the number of cores to do the analysis, etc...).
#
#			  2.6 Modifs (05/11/2012) : We add a test to avoid the analysis of the concatenated file
#			  for the global analysis step to fast the analysis. Moreover, we will use the results of
#			  all previous steps done on independent sequences of different samples to taxonomically
#			  assign all reads.
#
#			  2.7 Modifs (27/05/2013): As we can now assign bacterial sequences using the SILVA
#			  database, we add this possibility in this new program (see program details for more
#			  information).
#
#			  2.8 Modifs (18/06/2013): The main modifications of this PERL program were realized to
#			  define and use a common directory for the databases, but also for all programs. See
#			  details in the PERL program.
#
#			  2.9 Modifs (18/12/2015): The whole PERL program was rewritten to be integrated in a new
#			  library and new functions. Moreover, some functions have been modified and/or implemented
#			  (Taxonomy, DefineInputFolderFile, etc...) to manage all potential choices given by the
#			  user.
#
#			  3.0 Modifs (08/08/2018): Integration of the new ALGAE database previously developed, and
#			  verifications of the FindBin system to research the package positions, and the new
#			  version of the BACTERIA database.
#
#				3.1 Modifs (01/08/2019): Integration of the GreenGenes database, with the integration of a
#				new program to manage its use for the taxonomic definition based on USEARCH and common
#				approaches.
#
#				3.2 Modifs (10/05/2019): Integration of the new FUNGI R132 database previously developed, and
#			  we keep the old version (R114) available to test.
#
#       AUTHOR:  Sebastien TERRAT <Sebastien.Terrat@inra.fr>
#      COMPANY:  UMR 1747 Agroecologie - BIOCOM Team
#      VERSION:  3.2
#    REALEASED:  07/06/2011
#     REVISION:  10/05/2019
#=======================================================================================================
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#=======================================================================================================
sub Taxonomy
{
	#Defined variables
	my @line = ();
	my $j = 0;
	my $tmp = "";
	my $seq = "";
	my $taxo_results = "";
	my $folder_input = "";
	my $verif = 0;
	my $nb_save_seqs = 0;
	my @kept_seqs = ();
	my @seqs = ();

	#2.0: Defined variables
	my $file = $_[0];
	my $organism = $_[1];
	my $database = $_[2];
	my $version = $_[3];
	my $cores = $_[4];
	my $folder_output = $_[5];
	my $threshold = $_[6];
	my @folders = @{$_[7]};

	#2.8: Here, we define the absolute path to find the RDP Multiclassifier program, but also all PERL
	#programs essentail to launch taxonomical analyses.
	my $folder_Tools = "$FindBin::RealBin/Data/Tools";

	#2.6: Test to avoid the analysis of the dedicated file of the global analysis step.
	if ($file =~ m/Concatenated\_reads\.fasta\.fasta/) { return(1); }
	#Here, we search for the good taxonomy folder and file that will be used foe the TAXONOMY step
	($folder_input, $file) = &DefineInputFolderFile(\@folders, $file);

	#Then, we print for the user information that the program is started
	PerlLib::Miscellaneous::PrintScreen("TAXONOMY",$file,"started");
	#2.5: Here, we will first evaluate the organism studied to know what system will be applied (here bacteria)
	if (uc($organism) eq "BACTERIA")
	{
		#2.0: Using the chosen database to do the taxonomic assignment (here RDP)
		if (uc($database) eq "RDP")
		{
			#3.5: We launch the multilassifier of RDP (V1.4) to assign the deleted reads by the hunt program
			$taxo_results = `java -jar $folder_Tools/RDP_Multiclassifier/MultiClassifier.jar Result_files/$folder_input/$file --assign_outfile=Result_files/$folder_output/Taxo_RDP_$file`;
		}
		#2.7: Using the chosen database to do the taxonomic assignment (here Silva)
		elsif (uc($database) eq "SILVA")
		{
			#2.7: We launch the MEGABLAST step against the SILVA database to assign the reads given by the main program
			$taxo_results = `perl $FindBin::RealBin/Prog_Perl/Bacteria_SILVA_Taxonomy_2.2.pl $folder_input $folder_output $file $cores $threshold $version`;
		}
		#3.1: Using the chosen database to do the taxonomic assignment (here GREENGENES)
		elsif (uc($database) eq "GREENGENES")
		{
			#3.1: We launch the USEARCH step against the GREENGENES database to assign the reads given by the main program
			$taxo_results = `perl $FindBin::RealBin/Prog_Perl/Bacteria_GREENGENES_Taxonomy_2.5.pl $folder_input $folder_output $file $cores $threshold`;
		}
	}
	#2.5: We evaluate the organism studied to know what system will be applied (here fungi)
	elsif (uc($organism) eq "FUNGI")
	{
		#2.5: Using the chosen database to do the taxonomic assignment (here Silva)
		if (uc($database) eq "SILVA")
		{
			#3.2: Temporary test to keep old and new versions of the FUNGI DB to test, to modify to transfer the R114 version
			#to USEARCH to fast the analysis
			if($version eq "R114")
			{
				#3.2: We launch the MEGABLAST step against the SILVA database to assign the reads given by the main program
				$taxo_results = `perl $FindBin::RealBin/Prog_Perl/Fungi_SILVA_Taxonomy_3.5.pl $folder_input $folder_output $file $cores $threshold`;
			}
			else
			{
				#3.2: We launch the USEARCH step against the SILVA database to assign the reads given by the main program
				$taxo_results = `perl $FindBin::RealBin/Prog_Perl/Fungi_SILVA_Taxonomy_4.0.pl $folder_input $folder_output $file $cores $threshold $version`;
			}
		}
		#2.5: Using the chosen database to do the taxonomic assignment (here other)
		else
		{
		}
	}
	#3.0: Addition of the new system for ALGAE
	elsif (uc($organism) eq "ALGAE")
	{
		#3.0: Using the chosen database to do the taxonomic assignment (here Silva)
		if (uc($database) eq "SILVA")
		{
			#3.0: We launch the USEARCH step against the SILVA database to assign the reads given by the main program
			$taxo_results = `perl $FindBin::RealBin/Prog_Perl/Algae_SILVA_Taxonomy_1.1.pl $folder_input $folder_output $file $cores $threshold`;
		}
		#3.0: Using the chosen database to do the taxonomic assignment (here other)
		else
		{
		}
	}
	#Print for the user information that the program is terminated
	PerlLib::Miscellaneous::PrintScreen("TAXONOMY",$file,"terminated");
}
# End of the function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
1;
