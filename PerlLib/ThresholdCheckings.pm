#=======================================================================================================
# PACKAGE NAME: ThresholdCheckings
#	 FUNCTIONS:	- ThresholdCheckings (check the parameters (numbers and others..) of a chosen step).
#
#       AUTHOR:  Sebastien TERRAT <sebastien.terrat@inra.fr>
#      COMPANY:  BIOCOM TEAM (UMR 1347 INRA/uB/AgroSup)
#      VERSION:  1.0
#     RELEASED:  13/04/2020
#     REVISION:  XXX
#=======================================================================================================

# Name of the package defined for the main program
package PerlLib::ThresholdCheckings;

#Print errors and warnings during the implementation of the perl program
use strict;
use warnings;

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function developed to check the number of available reads for each treated sample, and indicate if the
# sample can be lower than the chosen threshold (10000 reads), or if the analysis for this sample should
# be stopped (lower than 9000 reads). It creates (if needed) summary files indicating potential problems
# with the sequencing depth of samples.
sub ThresholdCheckings
{
  #Defined variables
  my $filename = $_[0];
  my $nbreads = $_[1];
  my $checked_step = $_[2];

  if($nbreads < 10000)
  {
    if($nbreads < 9000)
    {
      open(F_RED, ">>Summary_files/Stopped_samples.txt") || die "\n==> Can't create the Stopped_samples.txt file <==\n";
      printf(F_RED "The sample $filename at the $checked_step was below than the defined threshold (9000): $nbreads found.\nThis sample analysis was stopped and this sample not taken into account for subsequent analyses.\n\n");
      close(F_RED);
      exit(0);
    }
    else
    {
      open(F_WARNING, ">>Summary_files/Warning_Samples.txt") || die "\n==> Can't create the Warning_Samples.txt file <==\n";
      printf(F_WARNING "The sample $filename at the $checked_step was lower than the defined warning threshold (10000): $nbreads found.\nPlease be careful with current results regarding this particular sample.\n\n");
      close(F_WARNING);
    }
  }
}
1;
