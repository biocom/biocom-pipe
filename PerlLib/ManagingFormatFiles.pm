#=======================================================================================================
# PACKAGE NAME: ManagingFormatFiles
#    FUNCTIONS:	- ExtractionFromSFF (Function treating SFF files, to produce .FASTA and .QUAL files),
#			- FormatStockholmRead (format one aligned read in Stockholm and give back it),
#			- FinishStockholmFiles (add some element to finalize alignments in Stockholm formats),
#			- StockholmToFasta (format an alignment in Stockholm format to Fasta format),
#			- FASTQEncodingVersion (define the verison of the fastq file, phred+33 or phred+64),
#			- ExtractionFromFASTQ (Function treating FASTQ files, to produce .FASTA and .QUAL files),
#			- PFAMToFasta (format an alignment in PFAM format to Fasta format).
#
#	   DETAILS: 1.1: Addition of new functions (FormatStockholmRead, FinishStockholmFiles and 
#			StockholmToFasta) from the GlobalAlignment part. Done the 23/07/2014.
#
#			1.2: Addition of a new function (FastQEncodingVersion, ExtractionFromFASTQ) to add
#			Illumina files treatment in the GnS-PIPE. Done the 09/09/2014.
#
#			1.3: Addition if a new function (PFAMToFasta) to easily manage new alignment files 
#			from INFERNAL 1.1.1. (06/02/18)
#
#			1.4: Modifications of the functions (ExtractionFromFASTQ and FASTQEncodingVersion) to
#			manage the cases of PRINSEQ and FLASH additions to the main program. (17/05/18)
#
# REQUIREMENTS: Perl language, and Math library to compute binary files
#       AUTHOR: Sebastien TERRAT <sebastien.terrat@inra.fr>
#      COMPANY: UMR 1347 Agroecology
#      VERSION: 1.4
#    REALEASED: 04/02/2014
#     REVISION: 17/05/2018
#=======================================================================================================

# Name of the package defined for the main program
package PerlLib::ManagingFormatFiles;

# Needed libraries
use strict;
use warnings;
# Libraries to analyze and treat binary files
use Math::Int64 qw(net_to_int64);
use Math::Int64 qw(int64_to_net);

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function developed to treat end extract from SFF files the dedicated .FNA and .QUAL files to
# efficiently and easily analyze data from the RUN(s). This function first verify the type of binary 
# file and stop the analysis if the file is not a .SFF file. Then, the function reads the header of the
# .SFF file, and use some data to read and extract the .FNA and .QUAL files from the .SFF file. Based on
# that, it produces and give these files directly in the Raw_data/ folder to easily analyze the data and
# do the treatment only once. It needs as input the name of the .SFF file, and gives back nothing.
sub ExtractionFromSFF
{
	# Needed input variables
	my $file = $_[0];
	# Needed empty variables
	my @common_header_section = ();
	my @tmp = ();
	my $magic_number = "0x";
	my $nb_reads = "";
	my $bit = "";
	my $name_length = "";
	my $seq = "";
	my $nb_flows_read = 0;
	my $nb_bases = 0;
	my ($i, $j) = (0,0);
	my $clip_qual_l = 0;
	my $clip_qual_r = 0;
	
	#==================================================================================================
	# #01# We open the .SFF file and verify its "magic_number" to check if its a real .SFF file. We
	# also open two output files (.FNA and .QUAL files)
	#==================================================================================================

	# We first open the .sff file given as input
	open(F_SFF, "Raw_data/$file")|| die "==> File Not Found: $file<==";	
	# We read the file as a binary file
	binmode (F_SFF);
	# Verifying the Magic number for the .sff file coding giving by the file analysis
	#Read the first 4 bytes, corresponding to .sff
	read(F_SFF, $bit, 4);
	# Convert these bytes to hexadecimal values and store it in $magic_number.
	foreach (split(//, $bit)) { $magic_number .= sprintf("%02x", ord($_)); }
	# We then check if the .sff file is in correct format.
	if($magic_number ne "0x2e736666")
	{
		die "\nThe $file is not a valid .sff file, the GnS-PIPE must be stopped !!
==> CHECK YOUR FILE FORMATS BEFORE RELAUNCHING GnS-PIPE PLEASE. <==\n";
	}
	
	#==================================================================================================
	# #02# We then read the header of the .SFF file to catch and store some needed data to efficiently
	# treat the rest of the file.
	#==================================================================================================
			
	# Print for the user information that the program is started
	PerlLib::Miscellaneous::PrintScreen("ExtractionFromSFF",$file,"started");
	
	# Read the file version and the index offset, but as we did not need them, we did
	# not store the data
	read(F_SFF, $bit, 12);
	# Catch all data needed in the read header section, and we keep the number of reads to analyze the
	# file
	read(F_SFF, $bit, 15);
	@common_header_section = unpack ('N2n3w',$bit);
	$nb_reads = $common_header_section[1];
	$nb_flows_read = $common_header_section[4];
	# Catch the flowchars and the key sequence done by the 454 pyrosequencing step, but as we did not
	# need them, we did not store the data
	read(F_SFF, $bit, $nb_flows_read + $common_header_section[3]);
	# Calculate the number of bytes to be a 8-byte divisible number based on found data in the common
	# header function.
	$i = 8 - (31 + $nb_flows_read + $common_header_section[3])%8;
	if ($i == 8) { $i = 0; }
	#Obtain a divisible number of read bytes by 8 using the computations done before (to be adapted 
	# for every .sff file)
	read(F_SFF, $bit, $i);
	
	#==================================================================================================
	# #03# We finally read the reads and their corresponding data to extract only needed information. 
	# To do this, we use the information extracted from the header of the .SFF file, and also the 
	# data from the header of each raw read stored in the .SFF file.
	#==================================================================================================
	
	# We modify the name of the file and open two output files to store needed data
	$file =~ s/\.sff//;
	open(F_FNA, ">Raw_data/$file.fna")|| die "==> Can't create file: $file.fna <==";
	open(F_QUAL, ">Raw_data/$file.qual")|| die "==> Can't create file: $file.qual <==";
	
	# Loop needed for each read analysis in the .sff file
	for ($i=1;$i<=$nb_reads;$i++)
	{
		# Catch the 16-bytes corresponding to the first part of the read header
		# section and store them, especially the name_length in bytes, the number
		# of bases or the clipping left and right quality
		read(F_SFF, $bit, 16);
		@tmp = unpack ('n2Nn4', $bit);
		
		$name_length = $tmp[1];
		$nb_bases = $tmp[2];
		$clip_qual_l = $tmp[3];
		$clip_qual_r = $tmp[4];
		
		# Read and store the name of the read
		read(F_SFF, $bit, $name_length);
		
		printf(F_FNA ">$bit\n");
		printf(F_QUAL ">$bit\n");
		
		# Calculate the number of bytes to be a 8-byte divisible number based 
		# on found data in the read header function.
		$j = 8-(16 + $name_length)%8;
		if ($j == 8){ $j = 0; }
		read(F_SFF, $bit, $j);
		# Read and catch the flowgram values
		read(F_SFF, $bit, 2*$nb_flows_read);
		@tmp = unpack ('n*', $bit);
		# Read and catch the flowgram index per base
		read(F_SFF, $bit, $nb_bases);
		@tmp = unpack ('U*', $bit);
		# Read and catch the sequence
		read(F_SFF, $bit, $nb_bases);
		$seq = substr($bit, $clip_qual_l-1, $clip_qual_r-$clip_qual_l+1);
		printf(F_FNA "$seq\n");
		# Read and catch the quality scores for the read
		read(F_SFF, $bit, $nb_bases);
		@tmp = unpack ('U*', $bit);
		@tmp = @tmp[$clip_qual_l-1..$clip_qual_r-1];
		printf(F_QUAL "@tmp\n");
		# Calculate the number of bytes to be a 8-byte divisible number based 
		# on found data in the read header function.
		$j = 8-((2*$nb_flows_read)+(3*$nb_bases))%8;
		if ($j == 8){ $j = 0; }
		read(F_SFF, $bit, $j);		
	}
	# Close all three files
    close(F_SFF);
    close(F_FNA);
    close(F_QUAL);
	# Print for the user information that the program is started
	PerlLib::Miscellaneous::PrintScreen("ExtractionFromSFF",$file.".sff","terminated");
}
# End of the function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function dedicated to format and print in an output file the sequence in Stockholm format. Needs as 
# input the ID of the file, the ID of the read, the max_length, and two filehandles (input and output). 
# This function returns no output.
sub FormatStockholmRead
{
	#Needed variables
	my $new_ID = "";
	my $line = "";
	my $ID_file = $_[0];
	my $ID = $_[1];
	my $max_length = $_[2];
	my $file_in = $_[3];
	my $file_out = $_[4];
	#We keep the one from the Concatenated reads
	$new_ID = $ID_file."|".$ID;
	#We add space for the Stockholm format (if needed, using the $max_length)
	while (length($new_ID) < $max_length + 1) { $new_ID = $new_ID." "; }
	#We catch then the aligned sequence itself
	$line = <$file_in>;
	#Finally, we print the sequence in the temporary file
	printf ($file_out $new_ID.$line);
}
# End of the function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function dedicated to add the two last needed lines (secondary structure and consensus sequence) to
# create the alignment in Stockholm format. This function takes as input an array of files, the name of 
# the folder and the max_length defined earlier.
sub FinishStockholmFiles
{
	#Needed variables
	my $file = "";
	my $line = "";
	my @files = @{$_[0]};
	my $folder_output = $_[1];
	my $max_length = $_[2];
	#Last loop to add and put in Stockholm format all alignment files before the merging of the files
	foreach $file (@files)
	{
		#Open needed files (input, output)
		open(F_IN, "Result_files/$folder_output/$file")|| die "==> File Not Found: Result_files/$folder_output/$file <==";
		open(F_OUT, ">>Temporary_files/Globanalysis_$file")|| die "==> Can't open file: Temporary_files/Globanalysis_$file <==";
		#For each line in the file
		while (<F_IN>)
		{
			#We search for needed lines (secondary structure and consensus sequence)
			if(($_ =~ m/#=GC_SS_cons/)||($_ =~ m/#=GC_RF/))
			{
				#We delete or transform data
				$_ =~ s/^>//; $_ =~ s/\n//; $_ =~ s/GC_/GC /;
				while (length($_) < $max_length + 1) { $_ = $_." "; }
				$line = <F_IN>;
				#We print formatted information				
				printf (F_OUT $_.$line);
			}
		}
		printf(F_OUT "//");
		#Finally, we close the files
		close (F_IN);
		close (F_OUT);
	}
}
# End of the function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function dedicated to format an alignment in Stockholm to an alignment in Fasta format. It uses as
# input the name of the file in Stockholm (input), and the file in Fasta (output). It gives back nothing.
sub StockholmToFasta
{
	#Needed variables
	my $line = "";
	my $file_stockholm = $_[0];
	my $file_fasta = $_[1];
	
	#Opening two files, the file in stockholm format (input), and the file in fasta format (output)
	open(F_IN, "$file_stockholm")|| die "==> Can't open the alignment file ($file_stockholm)";
	open(F_OUT, ">$file_fasta")|| die "==> Can't create the alignment file ($file_fasta))";
	#For each line of the file
	LOOP:while(<F_IN>)
	{
		#First, we store the line
		$line = $_;
		#If we found the # character, it's a descriptive line of the STOCKHOLM format
		if ($line =~ m/#/)
		{
			#If we found the consensus sequence or the profile sequence, we keep them
			if (($line =~ s/GC RF/GC_RF/)||($line =~ s/GC SS/GC_SS/))
			{
				#Modifying the line to obtain a FASTA format of the alignment
				$line =~ s/\s+/\n/;
				#Storying it the final result file
				printf(F_OUT ">$line");
			}
		}
		#If we found only a \n or a line with '//', we don't keep them
		elsif (($line eq "\n")||($line =~ /\/\//)) { next LOOP; }
		#Or, for alignment sequence lines, we modify them to obtain a FASTA format.
		else
		{
			$line =~ s/\s+/\n/;
			printf(F_OUT ">$line");
		}
	}
	#Close the two files
	close F_IN;
	close F_OUT;
}
# End of the function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function dedicated to define the used coding version of the quality data found in FastQ files. Indeed,
# several versions exists (phred+33 and phred+64). Thsi function was dedicated to the analysis of each
# dataset to define the coding version, based on the lowest and the highest value found.
sub FASTQEncodingVersion
{
	# Needed variables
	my $file = $_[0];
	my $folder_input = $_[1];
	my $qual = "";
	my @qual = ();
	my $val = 0;
	my $higher = 0;
	my $lower = 300;
	my $phred = 0;
	# We first open the .fastq file given as input
	if($folder_input eq "") { open(F_FASTQ, "Raw_data/$file")|| die "==> File Not Found: $file <=="; }
	elsif($folder_input eq "Temporary_files") { open(F_FASTQ, "Temporary_files/$file")|| die "==> File Not Found: $file <=="; }
	else { open(F_FASTQ, "Result_files/$folder_input/$file")|| die "==> File Not Found: $file <==";}
	
	# For each line of the fastQ file found
	while(<F_FASTQ>)
	{
		# We search for the descriptive line for quality data (third line starting by the character '+')
		if($_ =~ s/^\+//)
		{
			# We catch the next line containing scores in ASCII format
			$qual = <F_FASTQ>;
			$qual =~ s/\s//;
			# We split the line to analyze each character
			@qual = split("", $qual);
			foreach $qual (@qual)
			{
				# Function to find the corresponding ASCII value of the character
				$val = ord($qual);
				# Here, two tests to define the lowest and the highest value in the dataset to define
				# which coding version was used
				if ($val > $higher) { $higher = $val; }
				if ($val < $lower) { $lower = $val; }
			}
		}
		
	}
	# We close the input file
	close(F_FASTQ);
	# Here, we check the kept values (highest and lowest) to define which coding version was used
	if((33 <= $higher)&&($higher <= 73 )&&(33 <= $lower)&&($lower <= 73 )) { $phred = 33; }
	else { $phred = 64; }
	# We finally return the found version to convert data from FastQ to FASTA and QUAL
	return ($phred);
	
}
# End of the function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function dedicated to extract and format data from the FastQ file into two files, one in Fasta format
# with sequences, and another with quality data in Fasta format too. Each file will be produced in the
# Raw_data folder automatically if only fastQ files will be found.
sub ExtractionFromFASTQ
{
	# Needed input variables
	my $file = $_[0];
	my $folder_input = $_[1];
	my $folder_output = $_[2];
	my $ID = "";
	my $seq = "";
	my $qual = "";
	my $val = 0;
	my @qual = ();
	my $phred = 0;
	
	# Print for the user information that the program is started
	PerlLib::Miscellaneous::PrintScreen("ExtractionFromFastQ",$file,"started");
	# We launch the function developed to define the coding version of the FastQ data
	$phred = &FASTQEncodingVersion($file, $folder_input);
	# We first open the .fastq file given as input
	if($folder_input eq "") { open(F_FASTQ, "Raw_data/$file")|| die "==> File Not Found: $file <=="; }
	elsif($folder_input eq "Temporary_files") { open(F_FASTQ, "Temporary_files/$file")|| die "==> File Not Found: $file <=="; }
	else { open(F_FASTQ, "Result_files/$folder_input/$file")|| die "==> File Not Found: $file <==";}
	# We modify the name of the $file variable
	$file =~ s/\.fastq//;
	$file =~ s/\.fq//;
	# We then create the two output files
	if($folder_output eq "")
	{
		open(F_FNA, ">Raw_data/$file.fasta")|| die "==> Can't create file: $file.fasta <==";
		open(F_QUAL, ">Raw_data/$file.qual")|| die "==> Can't create file: $file.qual <==";
	}
	elsif($folder_output eq "Temporary_files")
	{
		open(F_FNA, ">Temporary_files/$file.fasta")|| die "==> Can't create file: $file.fasta <==";
		open(F_QUAL, ">Temporary_files/$file.qual")|| die "==> Can't create file: $file.qual <==";
	}
	else
	{
		open(F_FNA, ">Result_files/$folder_output/$file.fasta")|| die "==> Can't create file: $file.fasta <==";
		open(F_QUAL, ">Result_files/$folder_output/$file.qual")|| die "==> Can't create file: $file.qual <==";
	}
	
	# For each line in the input file
	while(<F_FASTQ>)
	{
		# Here, we search for the first descriptive line (starting by '@')
		if($_ =~ s/^\@//)
		{
			# We catch and format data
			$ID = $_;
			$ID =~ s/[\:\-\|\s\_]//g;
			# We then catch the second line, the sequence itself
			$seq = <F_FASTQ>;
			$seq =~ s/\s//g;
			# We then store needed data in the two output files
			printf(F_FNA ">$ID\n".$seq."\n");
			printf(F_QUAL ">$ID\n");
		}
		# Here, we search for the second descriptive line (starting by '+')
		elsif($_ =~ s/^\+//)
		{
			# We catch the fourht line, the quality data in ASCII format
			$qual = <F_FASTQ>;
			$qual =~ s/\s//;
			# We split the line to convert each character independently
			@qual = split("", $qual);
			foreach $qual (@qual)
			{
				# Here we do the conversion of the ASCII value using the ouput of the
				# FastQEncodingVersion function
				$val = ord($qual) - $phred;
				# We finally stroe data in the dedicated output file
				printf(F_QUAL "$val ");
			}
			printf(F_QUAL "\n");
		}
		
	}
	# We close files
	close(F_FNA);
	close(F_QUAL);
	close(F_FASTQ);
	# Print for the user information that the program is started
	PerlLib::Miscellaneous::PrintScreen("ExtractionFromFastQ",$file.".fastq","terminated");
}
# End of the function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function dedicated to format data from each alignment file in Fasta to easily treat and read it, 
# particularly for the clustering step of analysis. This function needs a filehandle from the targeted 
# file and return an array containing formatted data.
sub PFAMToFasta
{
	#Needed variables
	my $file = $_[0];
	my @data = ();
	my @tmp = ();
	my %seqs = ();
	my $key = "";
	#For each line of the PFAM file
	while(<$file>)
	{
		#We must avoid empty lines to avoid a problem during the analysis and also alignment lines 
		#without sequences
		if (($_ eq "\n")||($_ =~ m/^#/)||($_ =~ m/\/\//)) { next; }
		#For each sequence line, we catch needed data (ID and sequence using a hash table, as we can
		#have alignments with several lines
		else
		{
			#We clean the sequence
			chomp($_);
			$_ =~ s/\s+/\n/;
			#We split the line (ID and aligned sequence)
			@tmp = split("\n", $_);
			#We store data in %seqs
			$seqs{$tmp[0]} .= $tmp[1];
		}
		
	}
	close($file);
	#Then, using the %seqs, we create the @data array, for the main program
	foreach $key (keys(%seqs)) { push(@data, $key."\n".$seqs{$key}); }
	return (\@data);
}
# End of the function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
1;
