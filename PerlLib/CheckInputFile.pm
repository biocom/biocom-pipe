#=======================================================================================================
# PACKAGE NAME: CheckInputFile
#	 FUNCTIONS:	- VerifyParametersStep (check the parameters (numbers and others..) of a chosen step).
#
#       AUTHOR:  Sebastien TERRAT <sebastien.terrat@dijon.inra.fr>
#      COMPANY:  GenoSol platform (UMR 1347 INRA/uB/AgroSup)
#      VERSION:  1.0
#     RELEASED:  29/08/2014
#     REVISION:  XXX
#=======================================================================================================

# Name of the package defined for the main program
package PerlLib::CheckInputFile;

#Print errors and warnings during the implementation of the perl program
use strict;
use warnings;

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function developed to check the number of parameters and the first parameter of a given step. This
# function uses the function &SearchInputParameters to catch needed information and check parameters.
sub VerifyParametersStep
{
	#Needed variables
	my $step = $_[0];
	my $nb_parameters = $_[1];
	my $ref_array = "";
	my @parameters = ();

	# We first catch and store chosen parameters for the checked step
	$ref_array = PerlLib::Miscellaneous::SearchInputParameters($_[0]);
	@parameters = @$ref_array;
	#We then check the first parameter (must be a YES or a NO), nothing else.
	if((uc($parameters[0]) ne "YES")&&(uc($parameters[0]) ne "NO"))
	{
		die "==> The BIOCOM-PIPE was stopped after the verification of the Input.txt file.
In fact, an error was detected in the $step, in the question
Step done [yes-no]. Please correct this error in the Input.txt file
and relaunch BIOCOM-PIPE.\n";
	}
	#We avoid to test the number of parameters of the PREPROCESS_MIDS step, as it can be modified
	#by the user.
	if($step eq "PREPROCESS_MIDS") { return 1; }
	#We check the number of parameters that must be found in the Input.txt file
	if($#parameters != $nb_parameters)
	{
		$nb_parameters++;
		die "==> The BIOCOM-PIPE was stopped after the verification of the Input.txt file.
In fact, an error was detected in the $step, the number of parameters
is wrong ($nb_parameters lines). Please check carefully this step in
the Input.txt file and relaunch BIOCOM-PIPE.\n";
	}
	#We finally return something to the main program.
	return 1;
}
# End of the function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#=======================================================================================================
#=======================================================================================================
# MAIN FUNCTION CALLED BY THE MAIN PROGRAM
#=======================================================================================================
#=======================================================================================================
# FILE:		CheckInputFile.pm (called by the main program)
# USAGE:		PerlLib::CheckInputFile::CheckInputFile(ARGUMENTS)
# ARGUMENTS:	- $_[0]: a reference of an array containing all step names.
#
# REQUIREMENTS: - Perl language,
#			- PerlLib::Miscellaneous (SearchInputParameters function).
#
# DESCRIPTION:	This program was developed to check the Input.txt file due to errors of users after the
#			modification of the Input.txt file. To efficiently do this, each step is checked, based on
#			the number of given parameters (so the number of lines found), but also the program check
#			the first parameter as it is the same for all steps (Step done [yes-no]). If an error is
#			found, the program will stop GnS-PIPE and inform the user that a problem was found in the
#			Input.txt file.
#
#=======================================================================================================
sub CheckInputFile
{
	#Needed variables
	my $return = "";
	my $i = 0;
	my @nb_param = (6, 3, 1, 1, 1, 7, 2, 1, 1, 1, 0, 4, 1, 7, 1, 5, 1, 5, 7, 6, 8, 3);
	my $ref_array = $_[0];
	my @steps = @$ref_array;

	#Loop to test all steps given by the main program.
	for ($i=0;$i<=$#steps;$i++)
	{
		$return = &VerifyParametersStep($steps[$i], $nb_param[$i]);
	}
}
1;
