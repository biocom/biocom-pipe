#=======================================================================================================
# PACKAGE NAME: FastQTreatment
#    FUNCTIONS:	- PairedFilesAssociation (Define a hash table to associate both paired-end files),
#			- CleanTmpFiles (Delete some not needed files produced by PRINSEQ),
#			- PrinSeqTreatment (Manage and launch the PRINSEQ treatment of paired-end files),
#			- FLASHTreatment (Manage and launch the FLASH treatment of paired-end files).
#
# REQUIREMENTS:  Perl language, and Math library to compute binary files
#       AUTHOR:  Sebastien TERRAT <sebastien.terrat@inra.fr>
#      VERSION:  1.0
#    REALEASED:  17/05/2018
#     REVISION:  XXX
#=======================================================================================================

# Name of the package defined for the main program
package PerlLib::FastQTreatment;

# Needed libraries
use strict;
use warnings;
use File::Copy;

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function developed to define efficiently a hash table to associate paired-end files from the same
# sequencing. To do this, this program uses as input a list of all found files, search for good file
# pairs and associate them in a hash table given as output.
sub PairedFilesAssociation
{
	#Defined variables
	my @files = @{$_[0]};
	my %Paired_files = ();
	my $file = "";
	my $name1 = "";
	my $name2 = "";
	#For each found file in the list @files (so into Raw_data or a dedicated folder)
	foreach $file (@files)
	{
		#We first search for the more complex pattern (_1_1 and _1_2)
		if ($file =~ m/.*_1_1/)
		{
			$name1 = $file;
			($name2 = $file) =~ s/_1_1/_1_2/;
			$Paired_files{$name1} = $name2;
			next;
		}
		elsif ($file =~ m/.*_1_2/)
		{
			$name2 = $file;
			($name1 = $file) =~ s/_1_2/_1_1/;
			$Paired_files{$name1} = $name2;
			next;
		}
		#Then, for the potential existence of another pattern (R1 or R2)
		elsif ($file =~ m/.*_R1/)
		{
			$name1 = $file;
			($name2 = $file) =~ s/_R1/_R2/;
			$Paired_files{$name1} = $name2;
			next;
		}
		elsif ($file =~ m/.*_R2/)
		{
			$name2 = $file;
			($name1 = $file) =~ s/_R2/_R1/;
			$Paired_files{$name1} = $name2;
			next;
		}
		#And finally the classical one (_1 and _2)
		elsif ($file =~ m/.*_1/)
		{
			$name1 = $file;
			($name2 = $file) =~ s/_1/_2/;
			$Paired_files{$name1} = $name2;
			next;
		}
		elsif ($file =~ m/.*_2/)
		{
			$name2 = $file;
			($name1 = $file) =~ s/_2/_1/;
			$Paired_files{$name1} = $name2;
			next;
		}
	}
	return(\%Paired_files);
}
# End of the function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function developed to delete not needed files produced by some steps like the PRINSEQ one, copy also
# needed and kept files in good directories and finally, use the function FileInfoAppending to trace 
# data for the main program into the infos.info file.
sub CleanTmpFiles
{
	#Needed given input variables
	my $init_file = $_[0];
	my $output_file = $_[0];
	#First, we delete the not needed files (singleton files)
	unlink("$init_file"."_1_singletons.fastq");
	unlink("$init_file"."_2_singletons.fastq");
	#Then, using the copy function, we copy both files in the good folder
	$output_file =~ s/Temporary_files\//Result_files\/PRINSEQ\/PrinSeq_/;
	copy("$init_file"."_1.fastq","$output_file"."_R1.fastq");
	copy("$init_file"."_2.fastq","$output_file"."_R2.fastq");
	#Then, we delete the not needed files already copied
	unlink("$init_file"."_1.fastq");
	unlink("$init_file"."_2.fastq");
	$init_file =~ s/Temporary_files\///;
	#We create with this function a temp file (infos.info) storing everything needed to relaunch treatments
	PerlLib::Miscellaneous::FileInfoAppending("PrinSeq_$init_file"."_R1.fastq", "PRINSEQ");
	PerlLib::Miscellaneous::FileInfoAppending("PrinSeq_$init_file"."_R2.fastq", "PRINSEQ");
}
# End of the function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function developed to manage the PRINSEQ program treatment and outputs. To do this, this program takes
# as input the list fo treated files found, and the limit of cores for parallelization. This function 
# will then launch the PRINSEQ perl program using chosen parameters given by the user, and produces the
# needed output files and summary files.
sub PrinSeqTreatment
{
	#Needed given input variables
	my @files = @{$_[0]};
	my $limit_nb_cores = $_[1];
	my $folder = "Raw_data";
	#Needed variables
	my %file_names = ();
	my @parameters = ();
	my @childs = ();
	my $file = "";
	my ($name1,$name2) = ("","");
	my $log = "";
	my $ref_array = "";
	my $command = "";
	my $line = "";
	my $pid = "";
	my $nb_processes = 0;
	
	#First, we catch the parameters given as input by the user
	$ref_array = PerlLib::Miscellaneous::SearchInputParameters("PRINSEQ");
	@parameters = @$ref_array;
	#For each found file in @files (corresponding to Raw_data files)
	#We organize and create a hash table associating both paired-end files
	$ref_array = PerlLib::FastQTreatment::PairedFilesAssociation(\@files);
	%file_names = %{$ref_array};
	
	#Based on the hash table associating both paired-end files
	foreach $file (keys(%file_names))
	{
		#This test is defined to realize the treatment on a maximum of X different files defined by the
		#$limit_nb_cores : if the array @childs stored the good number of child preocesses pids.
		if($nb_processes >= $limit_nb_cores) { $pid = wait(); $nb_processes --; }
		#Creation of a parent and a child process. One child for each found file needed treatment
		$pid = fork();
		#If the program is the parent! (only one)
		if ($pid) { push(@childs, $pid); $nb_processes ++; }
		#If the program is a child (number as number of files)
		elsif ($pid == 0)
		{		
			#We catch the name of the file in $log
			$file =~ m/(.*)_1_1/;
			$log = $1;
			if(not defined $log)
			{
				$file =~ m/(.*)_R1/; $log = $1;
				$log = $1;
				if(not defined $log) { $file =~ m/(.*)_1/; $log = $1; }
			}
			# Print for the user information that the program is started
			PerlLib::Miscellaneous::PrintScreen("PrinSeqTrimming",$file." And ".$file_names{$file},"started");
			#We create the command line
			$command .= "perl $FindBin::RealBin/Data/Tools/PRINSEQ/prinseq-lite.pl -fastq $folder/$file -fastq2 $folder/$file_names{$file} -trim_qual_rule lt ";
			$command .= "-trim_qual_right $parameters[1] -trim_qual_left $parameters[2] -min_len $parameters[3] -ns_max_n $parameters[4] ";
			$command .= "-trim_qual_window $parameters[5] -trim_qual_step $parameters[6] -out_good Temporary_files/$log -out_bad null -log Summary_files/PrinSeq_$log.log";
			#We launch the command line, using a transfer of stdout (1) and stderr (2) in the log file.
			$line = `$command  >Temporary_files/Prinseq_file.log 2>&1`;
			#First, we delete the not needed log file
			unlink("Temporary_files/Prinseq_file.log");
			#Here, we used a function, to clean not needed files, and copy those needed in the good directory
			PerlLib::FastQTreatment::CleanTmpFiles("Temporary_files/$log");
			# Print for the user information that the program is terminated
			PerlLib::Miscellaneous::PrintScreen("PrinSeqTrimming",$file." And ".$file_names{$file},"terminated");
			exit(0);
		}
		#If we can't fork (create parent and child processes, die the process)
 		else {die "couldn’t fork: $!\n";}
	}
	#The parent process wait for all child results
	foreach (@childs) {waitpid($_, 0);}
}
# End of the function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function developed to manage the FLASH program treatment and outputs. To do this, this program takes
# as input the input folder were data willl be found (as PRINSEQ can be launched or not before), the 
# list of treated files found, and the limit of cores for parallelization. This function 
# will then launch the FLASH perl program using chosen parameters given by the user, and produces the
# needed output files and summary files.
sub FLASHTreatment
{
	#Needed given input variables
	my $folder_input = $_[0];
	my @files = @{$_[1]};
	my $limit_nb_cores = $_[2];
	#Needed variables
	my @parameters = ();
	my %file_names = ();
	my @childs = ();
	my $nb_files = 0;
	my $nb_processes = 0;
	my $ref_array = "";
	my $key = "";
	my $output_file = "";
	my $command = "$FindBin::RealBin/Data/Tools/FLASH/FLASH-1.2.11/flash ";
	my $result = "";
	my $pid = "";
	
	#First, we catch the parameters given as input by the user
	$ref_array = PerlLib::Miscellaneous::SearchInputParameters("FLASH");
	@parameters = @$ref_array;
	
	#For each found file in @files (corresponding to Raw_data files)
	#We organize and create a hash table associating both paired-end files
	$ref_array = PerlLib::FastQTreatment::PairedFilesAssociation(\@files);
	%file_names = %{$ref_array};
	
	#Based on the hash table associating both paired-end files
	foreach $key (keys(%file_names))
	{
		#This test is defined to realize the treatment on a maximum of X different files defined by the
		#$limit_nb_cores : if the array @childs stored the good number of child preocesses pids.
		if($nb_processes >= $limit_nb_cores) { $pid = wait(); $nb_processes --; }
		#Creation of a parent and a child process. One child for each found file needed treatment
		$pid = fork();
		#If the program is the parent! (only one)
		if ($pid) { push(@childs, $pid); $nb_processes ++; }
		#If the program is a child (number as number of files)
		elsif ($pid == 0)
		{
		
			#We search and simplify the name of the paired-end files
			$key =~ m/(.*)_1_1/;
			$output_file = $1;
			if(not defined $output_file)
			{
				$key =~ m/(.*)_R1/; $output_file = $1;
				$output_file = $1;
				if(not defined $output_file) { $key =~ m/(.*)_1/; $output_file = $1; }
			}
			#Print for the user information that the program is started
			PerlLib::Miscellaneous::PrintScreen("MergingWithFLASH",$key." With ".$file_names{$key},"started");	
			#Using given input by the main program, we define the input files and their locations
			if($folder_input eq "") { $command .= "Raw_data/$key Raw_data/$file_names{$key} "; }
			else { $command .= "Result_files/$folder_input/$key Result_files/$folder_input/$file_names{$key} "; }
			#We add needed parameters for the command line, from the parameters given by the user as input
			$command .= "-m $parameters[1] -M $parameters[2] -x $parameters[3] -t 1 ";
			$command .= "-c > Result_files/FLASH/FLASH_$output_file.fastq 2> Summary_files/FLASH_$output_file.log";
			#Here, we launch the command line to realize the file treatment
			$result = `$command`;
			# Print for the user information that the program is started
			PerlLib::Miscellaneous::PrintScreen("MergingWithFLASH",$key." With ".$file_names{$key},"terminated");	
			#Here, finally, we delete the not needed fastq file.
			unlink("Temporary_files/$output_file.fastq");
			#We create with this function a temp file (infos.info) storing everything needed to relaunch treatments
			PerlLib::Miscellaneous::FileInfoAppending("FLASH_$output_file".".fastq", "FLASH");
			exit(0);
		}
		#If we can't fork (create parent and child processes, die the process)
 		else {die "couldn’t fork: $!\n";}
	}
	#The parent process wait for all child results
	foreach (@childs) {waitpid($_, 0);}	
}
# End of the function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

1;
