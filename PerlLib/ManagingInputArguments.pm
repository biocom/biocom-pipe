#=======================================================================================================
# PACKAGE NAME: ManagingInputArguments
#    FUNCTIONS:	   - ManagingInputArguments (main function of this library)
#
# REQUIREMENTS:  Perl language only
#       AUTHOR:  Sebastien TERRAT <sebastien.terrat@inra.fr>
#      COMPANY:  UMR 1347 Agroecologie - BIOCOM TEAM
#      VERSION:  1.0
#    REALEASED:  18/09/2019
#     REVISION:  XXX
#=======================================================================================================

# Name of the package defined for the main program
package PerlLib::ManagingInputArguments;
# Needed libraries
use strict;
use warnings;


#=======================================================================================================
#=======================================================================================================
# MAIN FUNCTION CALLED BY THE MAIN PROGRAM
#=======================================================================================================
#=======================================================================================================
# FILE:		ManagingInputArguments.pm (called by the main program)
# USAGE: 		PerlLib::ManagingInputArguments::ManagingInputArguments(ARGUMENTS)
# ARGUMENTS:	None
#
#  DESCRIPTION:  This PERL program was developed to manage the given input arguments by the user and
#  chose what kind of analysis will be done by the main program.
#
#=======================================================================================================
sub ManagingInputArguments
{
  #Defined variables
	my %Input_args = ();
	my $i = 0;
	my $msg_cpu = "==> The given number of CPUs or cores was not an integer or below 0, the program will use the default value.\n";
  my $msg_unknown = "\n==> The main program did not know what launching step can be done (options: --prep, --launch, --graphs),
please chose one of them and try again.\n\n";

	#For all input arguments, we checked and organize the given data.
	for($i=0;$i<=$#ARGV;$i++)
	{
		if(($ARGV[$i] eq "--prep")||($ARGV[$i] eq "-p")) { $Input_args{"-p"} = 1; }
		if(($ARGV[$i] eq "--launch")||($ARGV[$i] eq "-l")) { $Input_args{"-l"} = 1; }
		if(($ARGV[$i] eq "--graphs")||($ARGV[$i] eq "-g")) { $Input_args{"-g"} = 1; }
		if(($ARGV[$i] eq "--cores")||($ARGV[$i] eq "-c"))
		{
			$ARGV[$i+1] = $ARGV[$i+1] + 0;
			if($ARGV[$i+1] == 0) { print "$msg_cpu"; $Input_args{"-c"} = 0; }
			else { $Input_args{"-c"} = $ARGV[$i+1]; }
      $i++;
		}
	}
	#Defining defaut values
	if(!exists($Input_args{"-c"})) { $Input_args{"-c"} = 1; }
  #Test to check the good number of input arguments
	if((!exists($Input_args{"-p"}))&&(!exists($Input_args{"-l"}))&&(!exists($Input_args{"-g"}))) { die "$msg_unknown"; }
  return(\%Input_args);
}
# End of the function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
1;
