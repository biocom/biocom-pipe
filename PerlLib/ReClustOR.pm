#=======================================================================================================
# PACKAGE NAME:	ReClustOR
#    FUNCTIONS: - DefineDatabaseDirectory (create the directory to store the database),
#               - ExtractClustDerepData (create database files from the global clustering),
#               - MergeAlignments (merge the alignment from the database with the samples alignment),
#               - ExtractDataFromGlobalAlignment (split info from the global alignment after merging),
#               - DefineGapsDiff (search for the number of common gaps between reads and consensus),
#               - OrganizeDataParallelization (manage the number of available cores to treat data),
#               - CompileClusteringResult (create the result files after ReClustOR),
#               - CleaningKeepingOuts (delete data that did not match with the database),
#               - CompileClusteredDataWithDerep (avoid miscounting datasets),
#               - DefineEnrichedSeqs (extract new reads that will enrich the database),
#               - TreatResults (trate the given results from previous functions).
#
# REQUIREMENTS:	Perl language only
#       AUTHOR:	Sebastien TERRAT <sebastien.terrat@inra.fr>
#      COMPANY:	UMR 1347 Agroécologie
#      VERSION:	1.0
#    REALEASED:	12/09/2019
#     REVISION:	XXX
#=======================================================================================================

# Name of the package defined for the main program
package PerlLib::ReClustOR;

# Needed libraries
use strict;
use warnings;
use File::Copy;

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function defined to create the directory to store the future database, where the database element
# will be stored automatically by the program and check the existence of previous versions. This
# function takes as input three strings: the database name, the targeted organism, and the clustering
# similarity percentage
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
sub DefineDatabaseDirectory
{
	#Defined variables
  my $choice_new = $_[0];
  my $database_name = $_[1];
  my $organism = uc($_[2]);
  my $threshold = $_[3];
	my $folder = "$FindBin::RealBin/Data/Databases/ReClustOR/";
  my $filename = "";

	#We define the database folder name
	$folder .= $database_name."_".$organism."_".$threshold;
  $filename = $database_name."_".$organism."_".$threshold;
  #Then, we create the database folder if needed
  if($choice_new eq "YES") { mkdir($folder); }
	#We finally return the name of the (created) folder to store and use it in further steps.
	return($folder, $filename);
}
#End of function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function defined to read and store data from two input files from the global analyis: the clustering
# file and the dereplication file. As output, this function returns the name of the database newly
# created. It needs four information as input: the folder names for the clustering step and where the
# pooled file was saved, the folder where the database will be saved, and the name of the database file.
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
sub ExtractClustDerepData
{
	#Input variables
  my $folder_clustering = $_[0];
  my $folder_seq_file = $_[1];
  my $folder_DB = $_[2];
  my $filename_DB = $_[3];
  #Defined variables
  my @tmp = ();
  my $clustering_file = "";
  my $file = "";
  my $seq_file = "";
  my $OTU_num = 1;
  my %seeds = ();

  #First, we open the defined directory to find the file dedicated to the Concatenated reads
  opendir (DIR, "Result_files/$folder_clustering/") || die "can't opendir Result_files/$folder_clustering/: $!";
  #We search and store the name of the Concatenated reads file
  while (defined($file = readdir(DIR))) { if ($file =~ m/Concatenated\_reads\.fasta\.fasta/) { $clustering_file = $file; } }
  closedir (DIR);
	#For each line found in the clustering file
	open(CLUST,"Result_files/$folder_clustering/$clustering_file")||die "==> Unable to open $folder_clustering/$clustering_file file\n.";
	LOOP01:while(<CLUST>)
	{
		if($_=~m/^\d/)
		{
      #We split the line of clustering data
			@tmp = split("\t",$_);
			#Then, we extract the ID of the seed
			$tmp[3] =~ m/(\d*\|.*?)\s/;
			#We store this seed ID in a hash table (the OTUnumber and the ID)
      $seeds{$1} = "DBOTU".sprintf("%08d", $OTU_num);
      $OTU_num++;
		}
	}
	close(CLUST);

  #Then, we open the defined directory to find the file dedicated to the Concatenated reads
  opendir (DIR, "Result_files/$folder_seq_file/") || die "can't opendir Result_files/$folder_seq_file/: $!";
  #We search and store the name of the Concatenated reads file
  LOOP03:while (defined($file = readdir(DIR))) { if ($file =~ m/Concatenated\_reads\.fasta\.fasta/) { $seq_file = $file; } }
  closedir (DIR);
	#For each line found in the dereplication file
	open(DEREP,"Result_files/$folder_seq_file/$seq_file")||die "==> Unable to open $folder_seq_file/$seq_file file\n.";
  open(DB, ">$folder_DB/DB_$filename_DB")||die "==> Unable to create DB_$filename_DB file into $folder_DB\n.";
	#For each line found in the dereplication file
	LOOP02:while(<DEREP>)
	{
    #We delete non wanted-characters
    $_ =~ s/\n//;
    #Then, we search for the seeds, and if found, we catch directly the sequence line, and then we store the OTU
    #number (as ID) and the sequence into the dedicated folder
    if($_ =~ s/^>//) { if(exists($seeds{$_})) { printf(DB ">".$seeds{$_}."\n".<DEREP>); } }
	}
	close(DEREP);
  close(DB);
	#We finally return the name of the database file created
	return("DB_$filename_DB");
}
#End of function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function defined to merge the global alignment from the GLOBAL_ANALYSIS step with the aligned file
# from the selected database created by the user for this analysis To manage that, the function needs
# four arguments: the folder of the created database, the name of the aligned database file, the
# folder where the global alignment is stored and two choices from the user (new database or not) and if
# the user wants to enrich or not the DB. As output, the program returns only the name of the merged
# global alignment stored.
sub MergeAlignments
{
  #Defined variables
  my $folder = $_[0];
  my $Align_DB_filename = $_[1];
  my $folder_align = $_[2];
  my $choice_new = $_[3];
  my $choice_enrich = $_[4];
  my $Align_file = "";
  my $file = "";
  my $stdout_tmp = "";

  if($choice_new eq "YES")
  {
    #We then store the esl-alimerge binary command with needed parameters (--dna: sort result as DNA
  	#format, -outformat pfam: PFAM format of alignment, -o: indicating the output file.
    my $command_esl_alimerge = "esl-alimerge --dna --outformat pfam -o Temporary_files/ReClustOR_Align_global_file ";
    #First, we open the defined directory to find the file dedicated to the Concatenated reads
    opendir (DIR, "Result_files/$folder_align/") || die "can't opendir Result_files/$folder_align/: $!";
    #We search and store the name of the Concatenated reads file
    while (defined($file = readdir(DIR))) { if ($file =~ m/Concatenated\_reads\.fasta\.fasta/) { $Align_file = $file; } }
    closedir (DIR);
    #We add to the command the two merged files used for the analysis (the database and the global file
  	$command_esl_alimerge .= "$folder/$Align_DB_filename Result_files/$folder_align/$Align_file";
    #We finally execute this command line
    $stdout_tmp = `$command_esl_alimerge`;
    #We finally return the name of the output aligned file
    return("ReClustOR_Align_global_file");
  }
  else
  {
    #We then store the esl-alimerge binary command with needed parameters (--dna: sort result as DNA
  	#format, -outformat pfam: PFAM format of alignment, -o: indicating the output file.
    my $command_esl_alimerge = "esl-alimerge --dna --outformat pfam -o Temporary_files/Added_Seed_Align_global_file ";
    #We add to the command the two merged files used for the analysis (the database and the seed aligned file)
  	$command_esl_alimerge .= "$folder/$Align_DB_filename Temporary_files/Align_Enriched";
    #We finally execute this command line
    $stdout_tmp = `$command_esl_alimerge`;
    #We finally return the name of the output aligned file
    return("Added_Seed_Align_global_file");
  }
}
#End of function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function developed to read and extract needed data from the global alignment of the database and the
# analyzed samples. Two hashes for the data from the DB or the treated sample were defined and filled
# and finally returned to the main program.
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
sub ExtractDataFromGlobalAlignment
{
	#Defined variables
	my $Align_global_filename = $_[0];
	my $ID = "";
	my $seq = "";
	my @tmp = ();
	my %otu = ();
	my %sample = ();
  my $test = 0;

	#We open the aligned file that will be treated
	open(ALIGN,"Temporary_files/$Align_global_filename") || die "Can't open the $Align_global_filename, please check this problem.\n";
	while(<ALIGN>)
	{
		#We search for needed data, separating the DB from the sample
		#and storing all data in three hashs tables (%otus, %sample)
		if(($_ =~ m/^#/)||($_ eq "\n")||($_ eq "//\n")) { next; }
		else
		{
      #We catch needed dat from the sequence ID
			$_ =~ m/([\|A-Za-z0-9\_\-\=]*)\s*(.*)\n/;
			$ID = $1;
			$seq = $2;
      #If the sequence is a sequence from the DB
			if($ID =~ m/^DBOTU/) { $otu{$ID} = $seq; }
      #if not, we store the sequence itself
			else { $sample{$ID} = $seq; }
		}
	}
	close(ALIGN);
	unlink("Temporary_files/$Align_global_filename");
	#We finally return the three filled hashes
	return(\%otu,\%sample);
}
#End of function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function developed to define easily the number of gaps and differences in each aligned read from the
# database and store the info in a hash to avoid multiple treatments as we have a global alignment. It
# takes as input the reference of the hash table containing the seeds and their aligned sequences and
# the given threshold.
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
sub DefineGapsDiff
{
	#Defined variables
	my $ref_otu = $_[0];
  my $given_threshold = $_[1];
	my $ID = "";
	my $seq = "";
  my $threshold = 0;
	my $count = 0;
  my $length = 0;
	my %diffs = ();
	#For each read and from the %otu hash table, we count the number of gaps
	while (($ID, $seq) = each(%{$ref_otu}))
	{
		$count = $seq =~ tr/\-\./\-\./;
    $length = length($seq)-$count;
    #We then define the threshold using this value
  	$threshold = int((((100-$given_threshold)/100)*$length) + 0.5);
		$diffs{$ID} = $threshold;
	}
	#We finally return the %diffs hash table
	return (\%diffs);
}
#End of function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function developed to organize and split the data treated to fast the analysis. Indeed, based on the
# number of cores and the global number of reads, we can divide the array in subarrays treated in
# parallel by the program. As output, the function gives a hash tables conatining as value each
# subarray with unique numbers as associated IDs.
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
sub OrganizeDataParallelization
{
  #Defined variables
  my ($i, $j) = (0,0);
  my %parallel_data = ();
  #We cattch the treated sequences by IDs and the limit of cores defined by the user
  my @sample_IDs = @{$_[0]};
  my $limit_nb_cores = $_[1];
  #Here, we split the data based on the number of cpus, to treat efficiently the sample
  for($i=1;$i<=$limit_nb_cores;$i++)
  {
  	$parallel_data{$i} = [@sample_IDs[$j..int($#sample_IDs/$limit_nb_cores*$i)]];
  	$j = int(($#sample_IDs/$limit_nb_cores)*$i) + 1;
  }
  #We finally return one reference
  return(\%parallel_data);
}
#End of function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function developed to print out the result needed for the definition of precise result files for the
# user. This function catch as input the header of the output file, the read ID, the ID of the seed
# from the database, the analyzed sequence itself, the number of differences found, and the out num
# position. The function prints out the organized information into the result file into the dedicated
# folder and return also if needed the name of the OUT name for the main program.
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
sub CompileClusteringResult
{
	#Defined variables
	my $output_header = $_[0];
	my $ID = $_[1];
	my $ID_DB = $_[2];
	my $seq = $_[3];
	my $nb_diff_found = $_[4];
  my $OUT_num = $_[5];
	my ($count,$length,$percent_id) = (0,0,0);

  #First, we define the number of gaps and the length of the read (in bases)
  $count = $seq =~ tr/\-\./\-\./;
  $length = length($seq)-$count;
  #We then define the result found in terms of identity
  $percent_id = ($length-$nb_diff_found)/$length;
	#We finally print this result in the output file
	flock($output_header, 2);
  #If the match is against the database, we need an empty return
  if($ID_DB =~ m/^DBOTU/)
  {
    printf($output_header $ID."\t".sprintf("%.2f",$percent_id*100)."\t".$ID_DB."\n");
    flock($output_header, 8);
    return("");
  }
  #If the match is against an already defined seed for the denovo step,, we also need
  #an empty return.
  elsif($ID_DB =~ m/^OUT/)
  {
    printf($output_header $ID."\t".sprintf("%.2f",$percent_id*100)."\t".$ID_DB."\n");
    flock($output_header, 8);
    return("");
  }
  #If it is a new OUT (seed sequence) for the denovo step, we define it and return its
  #name to the main program
  else
  {
    $OUT_num = $OUT_num + 2;
    $ID_DB = "OUT".sprintf("%08d", $OUT_num);
    printf($output_header $ID."\t".sprintf("%.2f",$percent_id*100)."\t".$ID_DB."\n");
    flock($output_header, 8);
    return($ID_DB);
  }
}
#End of function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function developed to delete not needed data stored in two hashes from the main program, to easily
# treat needed data that did not match in the database and used for a de novo clustering step. This
# function takes as input two hash tables (the sequences from all samples and the number of differences)
# defined on their corresponding lengths and return three hash tables, one containing the out reads,
#one with their corresponding abundances, and the final the differences for all out reads.
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
sub CleaningKeepingOuts
{
	#Defined variables
	my %reads = %{$_[0]};
  my %diffs_sample = %{$_[1]};
  my %abundance = ();
	my %IDs_out = ();
  my @tmp = ();
	my $key = "";

	#Here, we open and catch the needed IDs seq that did not match in the database, and we store them in %IDs_out
	open(TMP, "Temporary_files/Notmatched") || die "Unable to open and create the tamporary file Notmatched needed\n";
	while(<TMP>) { chomp($_); $IDs_out{$_} = ""; }
	close(TMP);
	unlink("Temporary_files/Notmatched");
	#Then, we read all IDs from the treated sample, and keep only the
	#IDs_out, deleting the others
	foreach $key (keys(%reads))
	{
		if(exists($IDs_out{$key}))
    {
      @tmp = split("_", $key);
      $abundance{$key} = $tmp[1];
    }
		else { delete($reads{$key}); delete($diffs_sample{$key}); }
	}
	#We then return to the main program the needed references of hashes
	return(\%reads, \%abundance, \%diffs_sample);
}
#End of function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function developed to define two hash tables based on data from the ReClustOR and DEREPLICATION steps.
# To do this, using as input the name of the file containing results from the ReClustOR file, the
# function catch data, and check if a treated read can be from various samples or from on sample, to
# avoid miscountigs after due to the dereplication step. It gaves as output three hashes, one with
# samples IDs, another with samples associated to OTU names from the DB, and the last with OTU names.
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
sub CompileClusteredDataWithDerep
{
  #Needed variables
  my $raw_result_file = $_[0];
  #Empty variables
  my $seq = "";
  my @tmp = ();
  my @tmp2 = ();
  my @seqs = ();
  my %derep_details = ();
  my %samples = ();
  my %results = ();
  my %otus = ();

  open(DEREP, "Summary_files/Global_Analysis/Derep_details_Concatenated_reads.fasta.fasta")|| die "Unable to open the derep details file for ReClustOR\n";
  while(<DEREP>)
  {
    chomp($_);
    @tmp = split("\t", $_);
    @tmp2 = split(" ", $tmp[1]);
    $derep_details{$tmp[0]} = [@tmp2];
  }
  close(DEREP);
  #We open and read the Raw data file obtained after ReClustOR treatment
  open(RAW, "Result_files/ReClustOR/$raw_result_file") || die "Unable to open the output file ReClustOR\n";
  #For each line found in the ReClustOR result file
  while(<RAW>)
  {
    chomp($_);
    #We split data to find the needed info (origin of the read, dereplication, etc.)
    @tmp = split("\t", $_);
    #Depending of the fact that the read corresponds to several different reads from different $samples
    #or one read
    if(exists($derep_details{$tmp[0]})) { @seqs = @{$derep_details{$tmp[0]}}; }
    else { push(@seqs, $tmp[0]); }
    #For each stored read
    foreach $seq (@seqs)
    {
      @tmp2 = split("_", $seq);
      $seq =~ m/(\d*)\|.*/;
      #We used this data to define efficiently three hash tables used after
      $samples{$1} = "";
      $results{$1}{$tmp[2]} += $tmp2[1];
      $otus{$tmp[2]} = "";
    }
    #We cleaned the @seqs array for the next read
    @seqs = ();
  }
  close(RAW);
  #We finally return needed data (three hash tables for the TreatResults function)
  return(\%samples, \%results, \%otus);
}
#End of function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function developed to define a temporary file containing new seed sequences from the denovo clustering
# approach to enrich the database. Automatically, it adds also seed sequences to the DB fasta file. As
# input, it takes the last DB otu name to define new names, the hash table of sequences, and the folder
# and name of the FASTA database.
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
sub DefineEnrichedSeqs
{
  #Defined and needed variables
  my $last_dbotu_name = $_[0];
  my %otu = %{$_[1]};
  my $folder = $_[2];
  my $filename = $_[3];
  my $OTU_num = 0;
  my $new_name = "";
  my $key = "";

  #First, we catch the corresponding value of the last DBOTU
  $last_dbotu_name =~ m/DBOTU(\d*)/;
  $OTU_num = $1;
  #We open the FASTA file of the Chosen Database
  open(RAW, ">>$folder/DB_$filename")|| die "Can't append the database filename $filename into the $folder of ReClustOR\n";
  open(TMP, ">Temporary_files/Enriched") || die "Can't create the Enriched fasta file dedicated to append the database\n";
  #Then, for each new OTU
  foreach $key (keys(%otu))
  {
    #We deinf the new number of seed
    $OTU_num++;
    $new_name = "DBOTU".sprintf("%08d", $OTU_num);
    #We delete non needed characters from the global alignment
    $otu{$key} =~ s/\-//g;
    $otu{$key} =~ s/\.//g;
    #We define the fasta seed sequences to the temp file and the DB file
    printf(TMP ">".$new_name."\n".uc($otu{$key})."\n");
    printf(RAW ">".$new_name."\n".uc($otu{$key})."\n");
  }
  #We close both files
  close(RAW);
  close(TMP);
}
#End of function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function developed to print clear and summarized results for the user. Indeed, using the name of the
# result file produced by ReClustOR, and the defined threshold, this function produce two summary files:
# one with summarized data for each sample, and one containing the global matrix defined by ReClustOR.
sub TreatResults
{
  #Defined variables
  my $raw_result_file = $_[0];
  my $threshold = $_[1];
  my @tmp = ();
  my @otus = ();
  my %otus = ();
  my %samples = ();
  my %results = ();
  my %derep_details = ();
  my ($sample, $key, $seq) = ("","","");
  my ($ref1, $ref2, $ref3) = ("","","");
  my $text = "";
  my %link_samples_IDs = ();
  my %link_samples_complete = ();
  my ($nbreads_tot, $nbreads_aff, $nbreads_out) = (0,0,0);
  my ($nbotus_tot, $nbotus_aff, $nbotus_out) = (0,0,0);

  #We open the summary file containing the links between filenames and IDs of thesefilenames
  open(SUM, "Summary_files/Global_Analysis/Concatenated_files.txt")|| die "Unable to open the concatenated listing file for the global analysis(ReClustOR)\n";
  #For each line in the summary file
  while(<SUM>)
  {
    #We first catch and split elements
    chomp($_);
    @tmp = split("\t", $_);
    #We store them with IDs as keys and filenames as values
    $link_samples_complete{$tmp[0]} = $tmp[1];
    #We cleaned filenames to store only the sample name
    $tmp[1] = PerlLib::Miscellaneous::ExtractSampleFromFileName($tmp[1]);
    #We store them with IDs as keys and sample names as values
    $link_samples_IDs{$tmp[0]} = $tmp[1];
  }
  close(SUM);

  #Using the raw file from the ReClustOR step and the dereplicated file, we
  #define three hash tables.
  ($ref1, $ref2, $ref3) = &CompileClusteredDataWithDerep($raw_result_file);
  %samples = %{$ref1};
  %results = %{$ref2};
  %otus = %{$ref3};
  #We first need the list of all found otus organized by name
  @otus = sort(keys(%otus));
  #We opened the result files produced for the user by the program
  open(OUT_MATRIX, ">Summary_files/ReClustOR/Reclustered_OTU_matrix_$threshold.txt") || die "==> Can't create the matrix file from the ReClustOR step <==\n";
  open(OUT_SUM, ">>Summary_files/ReClustOR/Clustering_summary_$threshold.txt")|| die "==> Can't append the summary result file from the ReClustOR step <==\n";
  #We first print the first line of the result matrice
  printf(OUT_MATRIX "Filename\tSample\t".join("\t", @otus)."\n");
  #Foreach sample treated by the ReClustOR step
  foreach $sample (keys(%samples))
  {
    #We print firts the filename and the sample name
    printf(OUT_MATRIX "$link_samples_complete{$sample}\t$link_samples_IDs{$sample}");
    #Needed empty variables to avoid miscountings
    ($nbreads_tot, $nbreads_aff, $nbreads_out) = (0,0,0);
    ($nbotus_tot, $nbotus_aff, $nbotus_out) = (0,0,0);
    #We count the number of otus for this sample
    $nbotus_tot = keys(%{$results{$sample}});
    #Foreach otu from the global list
    foreach $key (@otus)
    {
      #If the OTU was found in the sample
      if(exists($results{$sample}{$key}))
      {
        #We print the needed value into the matrix
        printf(OUT_MATRIX "\t$results{$sample}{$key}");
        #We count the number of global reads
        $nbreads_tot += $results{$sample}{$key};
        #If the OTU is one from the DB
        if($key =~ m/DBOTU/)
        {
          $nbreads_aff += $results{$sample}{$key};
          $nbotus_aff++;
        }
        #If the OTU is one defined by a de novo approach
        else
        {
          $nbreads_out += $results{$sample}{$key};
          $nbotus_out++;
        }
      }
      #If the OTU is not found into the sample
      else { printf(OUT_MATRIX "\t0"); }
    }
    #We also print the results into the summary files
    printf(OUT_SUM "$link_samples_IDs{$sample}\t$link_samples_complete{$sample}\t$nbotus_tot\t$nbreads_tot\t");
    printf(OUT_SUM "$nbotus_aff\t$nbreads_aff\t$nbotus_out\t$nbreads_out\n");
    printf(OUT_MATRIX "\n");
  }
  close(OUT_SUM);
}
#End of function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#=======================================================================================================
#=======================================================================================================
# MAIN FUNCTION CALLED BY THE MAIN PROGRAM
#=======================================================================================================
#=======================================================================================================
# FILE:		ReClustOR.pm (called by the main program)
# USAGE:	PerlLib::ReClustOR::ReClustOR(ARGUMENTS)
# ARGUMENTS:
#			- $_[1]: the user choice to create a new database (yes-no),
#			- $_[2]: the user choice to use a defined database (yes-no),
#			- $_[3]: the user choice to enrich a defined database (yes-no),
#			- $_[4]: the name of the database (created or used),
#			- $_[5]: the chosen model for the alignment (bacteria, archaea, fungi, algae),
#			- $_[6]: the global clustering threshold (0-100),
#			- $_[7]: the choice to ignore or not homopolymer errors (yes-no),
#			- $_[8]: the choice to avoid not common parts of reads durinf the clustering (yes-no),
#			- $_[9]: the clustering folder used,
#			- $_[10]: the alignment folder used,
#			- $_[11]: the folder used containg sequences,
#			- $_[12]: the number of allowed cores,
#
# DESCRIPTION:	This program was developed to improve the clustering definition and also avoid problems
# during the clustering (such as reads associated to false OTU seeds => first positive result). ReClustOR
# is a new post‐clustering method that overcomes problems (OTU stability and reliability) associated
# with classical clustering methods and thereby increases the quality and the congruence of the
# reconstructed OTUs. Moreover, the OTU database defined with ReClustOR can be used as a reference
# gradually enriched by merging new studies and samples. In this way, huge datasets (e.g. the Earth
# Microbiome Project or the Tara Oceans project) can be used as references for other projects within
# their range of application, and increase the quality of comparisons among studies and datasets.
#
# This new strategy combines two previously described clustering methods. Firstly, a classical
# clustering method is used to define OTU centroids and create a reference database. Secondly,
# a closed‐ or open‐reference method (depending on the user's choice) is computed for all reads which
# are not considered as OTU centroids. Each read is then compared to all centroids using a distance‐based
# greedy clustering method (Edgar, 2010; He et al., 2015), and assigned to the nearest one, thereby fixing
# the erroneous assignments of reads to OTUs. ReClustOR was engineered to refine the boundaries of OTUs,
# reduce the variance in the evaluation of microbial diversity and in turn improve the conclusions of
# ecological studies.
#
#=======================================================================================================
sub ReClustOR
{
  #Defined variables given as input by the program
  my $choice_new = uc($_[1]);
  my $choice_existing = uc($_[2]);
  my $choice_enrich = uc($_[3]);
  my $database_name = $_[4];
  my $organism = uc($_[5]);
  my $glob_threshold = 100-$_[6];
  my $choice_hm = uc($_[7]);
	my $choice_len = uc($_[8]);
  my $folder_clustering = $_[9];
  my $folder_align = $_[10];
  my $folder_seq_file = $_[11];
  my $limit_nb_cores = $_[12];
  #Needed variables
  my ($i,$j) = (0,0);
  my $nb_cores = 0;
  my $count = 0;
  my $threshold = 0;
  my $pid = "";
  my $folder = "";
  my $filename = "";
  my $DB_filename = "";
  my $Align_DB_filename = "";
  my $Align_seed_filename = "";
  my $Align_global_filename = "";
  my ($ref, $ref1, $ref2, $ref3) = ("","","","");
  my $key = "";
  my ($ID, $last_dbotu_name) = ("","");
  my ($myotu, $myout) = ("","");
  my $clust_function = "";
  my $output_header;
  my @childs = ();
  my @OUTs = ();
  my @keyotu = ();
  my @sample_IDs = ();
  my @IDs = ();
  my %otu = ();
  my %sample = ();
  my %abundance = ();
  my %diffs_db = ();
  my %diffs_sample = ();
  my %parallel_data = ();

  #Here, we create the database folder using the information given by the user
  #We also keep the information where the database is stored to compare data with it
  ($folder, $filename) = &DefineDatabaseDirectory($choice_new, $database_name, $organism, $glob_threshold);

  #If the user chose to define a new database using the GLOBAL_ANALYSIS data
  if($choice_new eq "YES")
  {
    #We launch the function dedicated to catch dereplicated reads corrresponding to seeds of each
    #defined cluster regarding the global clustering file.
    $DB_filename = &ExtractClustDerepData($folder_clustering, $folder_seq_file, $folder, $filename);
    #Then, another function was called to obtain the global alignment of seed sequences for further analysis
    $Align_DB_filename = PerlLib::InfernalAlignment::Alignment("", $organism, $DB_filename, $folder, $folder, 0, $limit_nb_cores);
    #As the new database is now defined, and to define it, it was essential that the GLOBAL_ANALYSIS was done,
    #we can use the Global_Alignment file to fast the analysis of samples.
    $Align_global_filename = &MergeAlignments($folder, $Align_DB_filename, $folder_align, "YES", "NO");
  }
  #If not, the user is considering using a defined database
  else
  {
    #As the global alignment of seed sequences is already available, we define its name
    $Align_DB_filename = "Align_DB_".$filename;
    #And we then merge the Global_Alignment file with the Align file of seed sequences to fast the analysis of samples,
    #but it was essential that the GLOBAL_ANALYSIS was done.
    $Align_global_filename = &MergeAlignments($folder, $Align_DB_filename, $folder_align, "YES", "NO");
  }
  #We then sort and organize data from the database and the treated file(s)
  ($ref1, $ref2) = &ExtractDataFromGlobalAlignment($Align_global_filename);
  %otu = %{$ref1};
  %sample = %{$ref2};
  #Here, we define for each representative sequence of the database and also for the sequences themselves the
  #number of gaps, to fast the analysis
	$ref = &DefineGapsDiff($ref1, $glob_threshold);
	%diffs_db = %{$ref};
  $ref = &DefineGapsDiff($ref2, $glob_threshold);
  %diffs_sample = %{$ref};
  #We then sort the DB sequences by OTU number
  @keyotu = sort(keys(%otu));
  $last_dbotu_name = $keyotu[$#keyotu];
  #We also sort the sample sequence IDs
  @sample_IDs = sort(keys(%sample));
  #Here, we split the data based on the number of cpus, to treat efficiently the sample
  ($ref) = &OrganizeDataParallelization(\@sample_IDs,$limit_nb_cores);
  %parallel_data = %{$ref};

  #Here we chose the clustering C function regarding the user choices given as input
  $clust_function = PerlLib::Clustering::ChoiceOfClustering($choice_hm, $choice_len);
  #Here, we launch the creation of the global output file
  open($output_header,">Result_files/ReClustOR/Clustering_results_$filename.txt") || die "Unable to open and create the output file ReClustOR needed\n";
  open(TMP, ">Temporary_files/Notmatched") || die "Unable to open and create the temporary file Notmatched needed\n";
  #Here, using the parallel information, we treat each read
  LOOP01:foreach $key (keys(%parallel_data))
  {
  	#We manage the limit of $cpu given by the user to treat data
  	if($nb_cores >= $limit_nb_cores) { $pid = wait(); $nb_cores --; }
  	#We define a new process
  	$pid = fork();
  	#If the process is parent
  	if($pid) { push(@childs, $pid); $nb_cores++; $i++; }
  	#If the process is a child
  	elsif ($pid == 0)
  	{
  		@IDs = @{$parallel_data{$key}};
  		#We take each sequence found in @IDs (from %parallel_data)
  		LOOP02:foreach $ID (@IDs)
  		{
  			#We then compare the sample sequence with all sequences from the database
  			LOOP03:foreach $ref (@keyotu)
  			{
  				#First we define the maximum number of differences tolerated and the length of the analyzed read
          #considering the maximum of tolerated differences based on both compared reads
  				if($myotu eq "")
          {
            if($diffs_db{$ref} > $diffs_sample{$ID}) { $threshold = $diffs_db{$ref};}
            else { $threshold = $diffs_sample{$ID}; }
          }
  				#Then, using the C function chosen based on the user choices, we launch the clustering itself.
  				$count = &$clust_function(uc($sample{$ID}),uc($otu{$ref}), $threshold);
  				#To fast the analysis, and keep only the best result, we redefine the threshold if a hit is found
  				if($count <= $threshold) { $threshold = $count; $myotu = $ref; }
  				#To fast the analysis, if a read have a 100% similarity, we stop the analysis for this result
  				if($threshold == 0)
  				{
            $myotu = &CompileClusteringResult($output_header, $ID, $myotu, $sample{$ID}, $threshold);
  					next LOOP02;
  				}
  			}
  			#If we found nothing higher than the chosen threshold of the user
  			if($myotu eq "") { flock(TMP, 2); printf(TMP "$ID\n"); flock(TMP, 8); next LOOP02; }
  			#If we found something, but not with 100% of identity but higher than the defined threshold
  			else { $myotu = &CompileClusteringResult($output_header, $ID, $myotu, $sample{$ID}, $threshold); }
  		}
  		exit(0);
  	}
  	#If we can't fork
  	else { die "coudn't fork !\n"; }
  }
  #We close the TMP file
  close(TMP);
  #We wait for the complete treatment of all children
  foreach (@childs) { waitpid($_, 0);	}

###USING A DEFINED DATABASE PART

  #If the user chose to define a new database using the GLOBAL_ANALYSIS data
  if($choice_existing eq "YES")
  {
    #Here, we cleaned not needed variables to avoid problems of memory and simplify the
    #treatment for further steps
    %parallel_data = ();    @sample_IDs = ();   %diffs_db = ();
    @IDs = ();              %otu = ();          @OUTs = ();   @keyotu = ();
    #Then, we launched the program dedicated to keep only the out reads compared to the database
    ($ref1, $ref2, $ref3) = &CleaningKeepingOuts(\%sample, \%diffs_sample);
    %sample = %{$ref1};
    %abundance = %{$ref2};
    %diffs_sample = %{$ref3};
    #Then, we sort, based on their abundance in the dataset, the OUT read IDs
    @IDs = keys(%sample);
    @IDs = sort { $abundance{$b} <=> $abundance{$a} || $a cmp $b } @IDs;
    #Not needed anymore
    %abundance = ();
    #For each sequence that needs treatment
    LOOP04:foreach $ID (@IDs)
    {
    	#If we don't have yet a seed OTU
    	if (keys(%otu) == 0)
    	{
    		#We create it using the first sequence from the @IDs data
        #We launch the function to print results into the ReClustOR result file
        $myout = &CompileClusteringResult($output_header, $ID, $ID, $sample{$ID}, 0, $#OUTs);
        #Using the given output of the function, we can define a new OUTname and store data
        #into the hash table, the number of differences for this new seed and the list of OUT
        #reads as needed
        $otu{$myout} = $sample{$ID};
        $diffs_db{$myout} = $diffs_sample{$ID};
        push(@OUTs, $myout);
    		next LOOP04;
    	}
    	#We then compare the sequence with all seed sequnces from the %otu enriched during the analysis
      #using the @OUTs list to keep the order of new defined OUTs
    	LOOP05:foreach $ref (@OUTs)
    	{
        #First we define the maximum number of differences tolerated and the length of the analyzed read
        #considering the maximum of tolerated differences based on both compared reads
        if($diffs_db{$ref} > $diffs_sample{$ID}) { $threshold = $diffs_db{$ref};}
        else { $threshold = $diffs_sample{$ID}; }
        #Then, using the C function chosen based on the user choices, we launch the clustering itself.
        $count = &$clust_function(uc($sample{$ID}),uc($otu{$ref}), $threshold);
    		#If we found a result higher than the defined threshold, we print the result and start to analyze a new sequence
    		if($count <= $threshold)
    		{
          #We create it using the first sequence from the @IDs data
          #We launch the function to print results into the ReClustOR result file
          $myout = &CompileClusteringResult($output_header, $ID, $ref, $sample{$ID}, $count, $#OUTs);
    			next LOOP04;
    		}
    	}
    	#If, at the end of the comparison with all seeds, the read did not match anything, it becomes a new seed
      #We launch the function to print results into the ReClustOR result file
      $myout = &CompileClusteringResult($output_header, $ID, $ID, $sample{$ID}, 0, $#OUTs);
      #Using the given output of the function, we can define a new OUTname and store data
      #into the hash table, the number of differences for this new seed and the list of OUT
      #reads as needed
      $otu{$myout} = $sample{$ID};
      $diffs_db{$myout} = $diffs_sample{$ID};
      push(@OUTs, $myout);
      next LOOP04;
    }
  }
  close($output_header);

##ENRICH A DEFINED DATABASE PART
  if(($choice_new eq "NO")&&($choice_enrich eq "YES"))
  {
    #First, we define the fasta file needed to add seed sequences
    &DefineEnrichedSeqs($last_dbotu_name, \%otu, $folder, $filename);
    #Then, another function was called to obtain the global alignment of new seed sequences
    $Align_seed_filename = PerlLib::InfernalAlignment::Alignment("", $organism, "Enriched", "Temporary_files", "Temporary_files", 0, $limit_nb_cores);
    #We delete not needed temporary file
    unlink("Temporary_files/Enriched");
    #Here, we use a function dedicated to merge two alignments (the global DB file and
    #the seed file defined previously based on the denovo step)
    $Align_seed_filename = &MergeAlignments($folder, $Align_DB_filename, $folder_align, "NO", "YES");
    #We delete not needed temporary file
    unlink("Temporary_files/Align_Enriched");
    #We finally delete the global file name to put the new one into the database folder
    unlink("$folder/$Align_DB_filename");
    #And we moved the new alignment to the dedicated folder
    move("Temporary_files/$Align_seed_filename","$folder/$Align_DB_filename");
  }

  #Finally, we print summarized data for users using this dedicated function
  &TreatResults("Clustering_results_$filename.txt", $glob_threshold);

}
1;
