#=======================================================================================================
# PACKAGE NAME: ProgramLaunchers
#    FUNCTIONS:	- ComputationLauncher (Launch the COMPUTATION step, rax and/or clean)
#			- GlobAnalysisLauncher (Launch the GLOBAL_ANALYSIS step).
#
#	   DETAILS: 1.1 (17/09/2014): We moved the DeMultiplexingLauncher function into the DeMultiplexing
#			library to regroup all needed functions for this particular step.
#
#			1.2 (11/12/2015): Deletion of Function called RandomLauncher and transfer this part
#			into the RandomSelection library to regroup all needed functions and programs for this
#			particular step.
#
#			1.3 (19/07/2018): Addition of a new step realization to compute indices on the Global
#			matrix data (see GlobAnalysisLauncher for more details) using the python dedicated
#			program
#
# REQUIREMENTS:  Perl language only
#       AUTHOR:  Sebastien TERRAT <sebastien.terrat@dijon.inra.fr>
#      COMPANY:  UMR 1347 Agroecology - Platform GenoSol
#      VERSION:  1.2
#    REALEASED:  17/04/2014
#     REVISION:  19/07/2018
#=======================================================================================================

# Name of the package defined for the main program
package PerlLib::ProgramLaunchers;
# Needed libraries
use strict;
use warnings;
use FindBin;
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function needed to launch the computation step chosen by the user and subsequent treatment steps.
sub ComputationLauncher
{
	#Needed empty variables
	my @files_Clust = ();
	my $file = "";
	my $file_name_pipe = "";
	my @parameters = @{$_[2]};
	my $sample = "";

	#Open the corresponding directory containing all files needed to be treated to launch one treatment by file
	opendir (DIR, "Result_files/$_[1]/") || die "can't opendir Result_files/$_[1]/: $!";
	#Loop to read all files found in the defined directory chosen by the user
	LOOP_COMPUT:while (defined($file = readdir(DIR)))
	{
		#No treatment for files '.' and '..'
	  	if ($file =~ m/^\.\.?$/){next LOOP_COMPUT;}
	  	elsif (-d $file){next LOOP_COMPUT;}
	  	else {push(@files_Clust, $file);}
	}
	closedir (DIR);
	@files_Clust = sort(@files_Clust);
	foreach $file (@files_Clust)
	{
		if($file =~ m/Concatenated_reads.fasta/) { next; }
		#Here, we launch the dedicated function to clean the filename to keep only the initial
		#sample name for further steps
  	$sample = PerlLib::Miscellaneous::ExtractSampleFromFileName($file);
		#Print for the user information that the program is started
		PerlLib::Miscellaneous::PrintScreen("Computation_rc_estimators_3.1.py",$file,"started");
		#Needed command line constructed. Launch the command line instruction using the "`". And we catch
		#the print in the $file_name_pipe variable to be used further
		$file_name_pipe = `python2.7 $FindBin::RealBin/Prog_Python/Computation_rc_estimators_3.1.py $_[0] $parameters[2] $parameters[3] $parameters[4] $parameters[5] $parameters[6] $parameters[7] $_[1] $file $sample`;
		#Print for the user information that the program is terminated
		PerlLib::Miscellaneous::PrintScreen("Computation_rc_estimators_3.1.py",$file,"terminated");
	}
	#Print for the user information that the program is started
	#PerlLib::Miscellaneous::PrintScreen("Curves_drawing_1.7.py","$_[0] files","started");
	#Needed command line constructed. Launch the command line instruction using the "`". And we catch
	#the print in the $file_name_pipe variable to be used further if needed
	#$file_name_pipe = `python2.7 $FindBin::RealBin/Prog_Python/Curves_drawing_1.7.py $_[0] $parameters[2] $parameters[3] $parameters[4]`;
	#Print for the user information that the program is terminated
	#PerlLib::Miscellaneous::PrintScreen("Curves_drawing_1.7.py","$_[0] files","terminated");
}
# End of the function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function needed to launch the global analysis of samples chosen by the user with subsequent
# treatment steps.
sub GlobAnalysisLauncher
{
	#Needed empty variable
	my $file_name_pipe = "";
	my @computation_params = ();
	my $ref_array = "";

	#Print for the user information that the program is started
	PerlLib::Miscellaneous::PrintScreen("Globanalysis_matrix_computation_3.0.pl","Concatenated_reads.fasta.fasta","started");
	#Needed command line constructed. Launch the command line instruction using the "`". And we catch
	#the print in the $file_name_pipe variable to be used further if needed
	$file_name_pipe = `perl $FindBin::RealBin/Prog_Perl/Globanalysis_matrix_computation_3.0.pl $_[0] $_[1] $_[2] $_[3] $_[4] $_[5] $_[6] $_[7] $_[8]`;
	#Here, we search for user choices for the computation step to manage the indices computation also
	#on the global matrix
	$ref_array = PerlLib::Miscellaneous::SearchInputParameters("COMPUTATION");
	@computation_params = @$ref_array;
	if (uc($computation_params[0]) eq "YES")
	{
		$file_name_pipe = `python2.7 $FindBin::RealBin/Prog_Python/Computation_rc_estimators_3.1.py $_[0] $computation_params[2] $computation_params[3] $computation_params[4] $computation_params[5] $computation_params[6] $computation_params[7] Global_Analysis Concatenated_reads.fasta.fasta None`;
	}
	#Print for the user information that the program is terminated
	PerlLib::Miscellaneous::PrintScreen("Globanalysis_matrix_computation_3.0.pl","Concatenated_reads.fasta.fasta","terminated");
}
# End of the function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
1;
