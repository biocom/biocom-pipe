#=======================================================================================================
# PACKAGE NAME: ManagingQuality
#    FUNCTIONS:	 - QualityEvaluation (Main function treating the quality files and giving a summary),
#				 - VerifyingValues (Check the definition of all values before printing).
#
# REQUIREMENTS:  Perl language only
#       AUTHOR:  Sebastien TERRAT <sebastien.terrat@dijon.inra.fr>
#      COMPANY:  UMR 1347 Agroecology - Platform GenoSol
#      VERSION:  1.0
#    REALEASED:  17/04/2014
#     REVISION:  XXX
#=======================================================================================================

# Name of the package defined for the main program
package PerlLib::ManagingQuality;
# Needed libraries
use strict;
use warnings;



#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function dedicated to the analysis of printed values to avoid undefined values
sub VerifyingValues
{
	# Needed variables
	my $element = "";
	my $printed_line = "";
	# For each argument given as input by the main function
	foreach $element (@_)
	{
		# We check if the variable is defined or not
		if (defined($element)) { $printed_line .= $element."\t"; }
		else { $printed_line .= "0\t"; }
	}
	# We delete the last character of the line
	chop($printed_line);
	$printed_line .= "\n";
	# We finally return the clean line that will be printed in the output summary file
	return ($printed_line);
}
# End of the function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#=======================================================================================================
#=======================================================================================================
# MAIN FUNCTION CALLED BY THE MAIN PROGRAM
#=======================================================================================================
#=======================================================================================================
# FILE:		ManagingQuality.pm (called by the main program)
# USAGE: 		PerlLib::ManagingQuality::QualityEvaluation(ARGUMENTS)
# ARGUMENTS:	- $_[0]: the length threshold,
#			- $_[1]: the name of the input file.
#
#  DESCRIPTION:  This PERL program was developed to have a better understanding of the quality of
#		obtained pyrosequences for each RUN. To do this, the program takes all quality files 
#		found in the IN/ directory and compute several values for each position of the 
#		sequence. Briefly, this program determines the standard deviation, the median value, the
#		minimum, the maximum, the Q1 and Q3 values for each 25 bases of reads. It also computes
#		the total number of sequences treated and the number of sequences with quality values 
#		high and low (between 40 and 35, 35 and 30, 30 and 25, and less than 25).
#		The result is finally computed and stored in a tabulated file that can be directly 
#		imported in Excel or other tables.
#
#		1.2 Modifications: First, all the program have been completely annotated to clearly 
#		indicate all steps. And, a new step have been added for the user to ask for the
#		minimum length tolerated for reads analysis. See details and modifications in the 
#		PERL program as indicated.
#
#		1.5 Modifications: Transformation of the program to be launched automatically with
#		another PERL program to develop an automatic workflow of analysis (e.g., we delete the
#		the loop to analyze and search each file needed treatment).
#
#=======================================================================================================
sub QualityEvaluation
{
	# Defined variables
	my $seq = "";
	my $ref_array = "";
	my $printed_line = "";
	my $nbseq = 0;
	my @tab_qual = ();
	my @tmp = ();
	my @tmp2 = ();
	my @matrix;
	my @tab_num_qual = ();
	my $pos_matrix = 0;
	my $moy = 0;
	my ($i,$j,$k) = (0,0,0);
	my $sum = 0;
	my $average = 0;
	my $min = 0;
	my $max = 0;
	my $Q1 = 0;
	my $Q3 = 0;
	my $tmp_value = 0;
	my $val = 0;
	
	# 1.5 : Modifications to catch needed information given by the workflow
	my $length = $_[0];
	my $file = $_[1];
	my $folder_input = $_[2];
	
	# Print for the user information that the program is started
	PerlLib::Miscellaneous::PrintScreen("EVAL_QUAL",$file,"started");
	
	# 1.5: Open file one given by the user and containing cleaned sequences
	if($folder_input eq "") { open(F1, "Raw_data/$file")|| die "==> File Not Found: $file<=="; }
	else { open(F1, "Result_files/$folder_input/$file")|| die "==> File Not Found: $file<=="; }
	
	# Here, we launch the function to catch and store data from an input file in Fasta format, and give back
	# all information stored in an array.
	$ref_array = PerlLib::Miscellaneous::ReadInputFastaFile(*F1, "QUAL");
	@tab_qual = @{$ref_array};
	# We count the number of total reads in the dataset
	$nbseq = $#tab_qual + 1;
	# We close the readed file
	close F1;

	# For each read found in the file and stored in @tab_qual
	LOOP01:foreach $seq (@tab_qual)
	{
		# We first split the @tab_qual to catch only the quality sequence
		@tmp = split("\n", $seq);
		# We split all PHRED values based on \s found
		@tmp2 = split(" ", $tmp[1]);
		# 1.2: If the value is below the defined threshold, we avoid this read
		if (scalar (@tmp2) < $length) { next; }
		# If the read length id OK
		else
		{
			# Needed empty variables
			$pos_matrix = 0;
			$moy = 0;
			# For each PHRED value
			for ($i = 0; $i <= $#tmp2; $i++)
			{
				# We determine for each range of 25 bases some details
				if (($i % 25 == 0)&&($i != 0))
				{
					# The average value for the 25 bases analyzed
					$moy = $moy/25;
					# We store it in the matrix
					push @{$matrix[$pos_matrix]}, $moy;
					# We move to the next matrix value
					$pos_matrix++;
					# Needed empty variable
					$moy = 0;
				}
				else
				{
					# We add to the average the PHRED value of the base
					$moy = $moy + $tmp2[$i];
				}
			}
		}
	}
	
	# 1.5: We open the result file and write the information (in the defined directory)
	open(F2, ">Summary_files/Eval_qual_$length"."_"."$file.txt")|| die "==> Can't create: Eval_qual_$file.txt in /Summary_files directory<==";
	# 1.5: We print it for the user
	printf (F2 "Defined length threshold to analyze the read:\t$length\n");
	printf (F2 "Total number of reads (with those eliminated by the threshold):\t$nbseq\n");
	printf (F2 "Std\tAverage\tMin\tMax\tQ1\tQ3\t40-35\t35-30\t30-25\t25-0\tTOTAL\n");

	# For each element of the matrix
	LOOP02:foreach $i (@matrix)
	{
		# We organize data based on the values stored
		@tmp = sort { $a <=> $b } @$i;
		# For each element in @tmp
		LOOP03:foreach $val (@tmp)
		{
			# We do the sum and the number of values in each defined range 
			$sum = $sum + $val;
			if ($val > 35) { $tab_num_qual[0]++; next LOOP03; }
			elsif($val > 30) { $tab_num_qual[1]++; next LOOP03; }
			elsif($val > 25) { $tab_num_qual[2]++; next LOOP03; }
			else  {$tab_num_qual[3]++; next LOOP03; }
		}
		# We defined also the minimum and the maximum
		$min = $tmp[0];
		$max = $tmp[$#tmp];
		# The average value is also defined
		$average = $sum / ($#tmp+1);
		$tmp_value = int(($#tmp+1)/4);
		# And the Q1 and the Q3 values
		$Q1 = $tmp[$tmp_value];
		$tmp_value = 3*int(($#tmp+1)/4);
		$Q3 = $tmp[$tmp_value];
		$val = $#tmp +1;
		# We print data in the result file
		printf(F2 sprintf("%04d",$j)."-"); 
		$j = $j+25;
		printf(F2 sprintf("%04d",$j)."\t"); 
		$printed_line = &VerifyingValues($average,$min,$max,$Q1,$Q3,$tab_num_qual[0],$tab_num_qual[1],$tab_num_qual[2],$tab_num_qual[3],$val);
		printf(F2 $printed_line);
		# We need empty variables
		$sum = 0;
		@tmp = ();
		@tab_num_qual = ();
	}
	# 1.5: Close the summary_file
	close F2;
	
	# Print for the user information that the program is terminated
	PerlLib::Miscellaneous::PrintScreen("EVAL_QUAL",$file,"terminated");
	
	# Return the name of the summary file to main program
	return "Eval_qual_$length"."_"."$file.txt";
}
# End of the function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

1;
