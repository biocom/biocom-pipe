#=======================================================================================================
# PACKAGE NAME: DeMultiplexing
#    FUNCTIONS:	 - FormatMIDsResults (Format data from the temporary file to produce the summary file),
#			 - DeMultiplexing (Do the demultiplexing of the data),
#			 - PrintingReads (Print treated data in output file(s) after demultpliexing).
#
# REQUIREMENTS:  Perl language only
#       AUTHOR:  Sebastien TERRAT <sebastien.terrat@dijon.inra.fr>
#      COMPANY:  UMR 1347 Agroecology - Platform GenoSol
#      VERSION:  1.0
#    REALEASED:  18/09/2014
#     REVISION:  XXX
#=======================================================================================================

# Name of the package defined for the main program
package PerlLib::DeMultiplexing;
# Needed libraries
use strict;
use warnings;

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function defined to format all results obtained after DeMultiplexing from file(s) to give back to the
# user summarized and organized results in a file. To do this, this function treats a temporary file
# containing all results for all searched MIDs. It takes as input the name of the temporary file and the
# name of the output file.
sub FormatMIDsResults
{
	#Defined variables and input variables
	my @input_lines = ();
	my @tmp = ();
	my $nb_MIDs = 0;
	my $nb_raw_reads = 0;
	my $nb_kept_reads = 0;

	my $file_input = $_[0];
	my $file_name = $_[1];

	#We format the filename
	$file_name =~ s/.fna//;
	$file_name =~ s/.fasta//;

	#We open the temporary file with needed info and the summary file for the user
	open(F_IN, "Temporary_files/$file_input")|| die "==> Can't open file of the preprocess_mids step of analysis<==";
	open(F_OUT, ">Summary_files/Preprocess_MIDs_".$file_name.".txt")|| die "==> Can't create the summary file of the preprocess_mids step of analysis<==";
	#We read each line of the file
	while(<F_IN>)
	{
		#We analyze only lines corresponding to the chosen raw file by the main program
		if($_ =~ m/$file_name/)
		{
			#We count the number of chosen MIDs
			$nb_MIDs++;
			#We split the information line
			@tmp = split("\t", $_);
			#We define the number of raw and kept reads
			$nb_raw_reads = $tmp[0];
			#2.7: Here, we consider the possibility that one MID was used only (the forward)
			if($#tmp == 5) { $nb_kept_reads += $tmp[5]; }
			#2.7: Here, we consider the possibility that two MIDs was used (forward and reverse)
			elsif($#tmp == 7) { $nb_kept_reads += $tmp[7]; }
			#We kept also the needed line
			push(@input_lines, $_);
		}
	}
	#We close the temporary input file
	close(F_IN);
	#We rearrange by alphabetical order the kept lines
	@input_lines = sort(@input_lines);

	#We then print all formatted info in the summary file
	printf(F_OUT "Number of MIDs chosen by the user:\t$nb_MIDs\nNumber of raw reads found:\t$nb_raw_reads\n");
	#2.7: Here, we consider the possibility that one MID was used only (the forward)
	#if($#tmp == 5) { printf(F_OUT "Number of kept reads after MID filtering:\t$nb_kept_reads\n\nChosen MID\tMID Sequence\tSample name\tNumber of reads\n"); }
	#2.7: Here, we consider the possibility that two MIDs were used (forward and reverse)
	printf(F_OUT "Number of kept reads after MID filtering:\t$nb_kept_reads\n\nChosen MID Forward\tMID Sequence Forward\tChosen MID Reverse\tMID Sequence Reverse\tSample name\tNumber of reads\n");

	foreach (@input_lines)
	{
		@tmp = split("\t", $_);
		#2.7: Here, we consider the possibility that one MID was used only (the forward)
		if($#tmp == 5) { printf(F_OUT "$tmp[2]\t$tmp[3]\t$tmp[2]\t$tmp[3]\t$tmp[4]\t$tmp[5]"); }
		#2.7: Here, we consider the possibility that one MID was used only (the forward)
		elsif($#tmp == 7) { printf(F_OUT "$tmp[2]\t$tmp[3]\t$tmp[4]\t$tmp[5]\t$tmp[6]\t$tmp[7]"); }
	}
	#We finally close the summary file and we quit the program properly
	close(F_OUT);
}
# End of the function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function defined to print the results in output file(s) after treatment. This function needs the
# cleaned reads (fasta and qual), the descriptive line, the $boolean_qual indicating if a quality file
# is treated, and the two handlers of the output files (fasta and qual).
sub PrintingReads
{
	#Needed variables
	my $length = 0;

	my $clean_fna = $_[0];
	my $clean_qual = $_[1];
	my $descr = $_[2];
	my $boolean_qual = $_[3];
	my $output_fna = $_[4];
	my $output_qual = $_[5];

	#2.5: we determine the length of the total read witout the MID
	$length = length($clean_fna);
	#2.5: We store in the result file the descriptive line and the sequence itself
	printf($output_fna "$descr length=$length"."\n".$clean_fna."\n");
	#2.5: Test to verify the qual file existence
	if($boolean_qual == 1)
	{
		$clean_qual =~ s/  / /g;
		#2.5: We store in the result file the descriptive line and the quality sequence itself
		printf($output_qual "$descr length=$length"."\n".$clean_qual."\n");
	}
}
# End of the function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


#=======================================================================================================
#=======================================================================================================
# MAIN FUNCTION CALLED BY THE MAIN PROGRAM
#=======================================================================================================
#=======================================================================================================
# FILE:			Demultiplexing.pm (called by the main program)
# USAGE:		PerlLib::DeMultiplexing::DeMultiplexing(ARGUMENTS)
# ARGUMENTS:	- $_[0]: the name of the input file,
#				- $_[1]: the output folder,
#				- $_[2]: the string "Filename/MID/Samplename" from the Input.txt file.
#
# DESCRIPTION:	This program was developed to preprocess raw sequences given by 454 sequencers. First,
#             this tool ask for chosen parameters by the user and primer sequences. This program needs
#             also a file containing tag sequences and tag names to sort sequences by tags. Then,
#             several filters are applied to treat sequences (by length, by quality : number of 'N').
#			  All sequences are then checked to find primers and potentially reverse complemented if
#             it's needed. Finally, all sequences are stored in different files named using tag names.
#             A statistic file is also created to give information about these treatment to the user.
#
#			  1.5 Modifs (28/03/12): Transformation of the program to be launched automatically with
#			  another PERL program to develop an automatic workflow of analysis (e.g., we delete the
#			  the loop to analyze and search each file needed treatment).
#
#			  2.0 Modifs (31/01/13): The program was completely re-written to simplify and improve the
#			  program itself. Moreover, the quality file is also treated in parallel to potentially
#			  realize again a quality treatment and also correct the single-singletons based on their
#			  alignment.
#
#			  2.1 Modifs (18/06/13): We add new MIDs to the actual list as some new projects were
#			  realized with these particular MIDs.
#
#			  2.2 Modifs (09/07/13): We add new MIDs to the actual list to manage the MIDs of Rapid
#			  Libraries Kit.
#
#			  2.3 Modifs (22/07/13): We modify the program to consider the possibility that the user
#			  did not give the .qual file during the treatment. TO do this, we test the possibility of
#			  its existence at each step.
#
#			  2.5 Modifs (20/08/13): We completely rewrite the program to fast its analysis, and use
#			  only one MID, to parallelize the analysis. With this new program, we can fast the MID
#			  research, for several different raw files at one time. Briefly, the main program, based
#			  on a new function, launch the Preprocess_MIDs program for each raw file and each MID at
#			  one time.
#
#			  2.6 Modifs (07/08/18): We modified the program to manage the fact that a sample can be
#			  empty due to the absence of reads with the good MID. To manage that, the file name will
#			  be empty and will be not treated anymore.
#
#				2.7 Modifs (24/01/19): We modified the program to manage the fact that the use can use
#				two MIDs (one forward, and one reverse) in his multiplexing data. Now, the program Takes
#				this into account (both possibilities : one or two MIDs.
#=======================================================================================================
sub DeMultiplexing
{
	#2.0: Defined variables
	my $nb_seqs_tot = 0;
	my $nb_seqs_kept = 0;
	my $length_deleted = 0;
	my $boolean_qual = 0;
	my $boolean_found_read = 0;
	my $boolean_begin_read = 0;
	my $length = 0;
	my @tmp = ();
	my @tmp_qual = ();
	my @tmp_descr = ();
	my $file_qual = "";
	my $descr_line = "";
	my $input_fna = "";
	my $input_qual = "";
	my $clean_fna = "";
	my $clean_qual = "";
	my $output_file = "";
	#2.5: Input variables given by the main program
	my $file_fna = $_[0];
	my $folder_output = $_[1];
	my $input_parameter = $_[2];
	my $folder_input = $_[3];
	my $chosen_tag_F = "";
	my $chosen_tag_R = "";
	my $chosen_name = "";

	my %Data_FNA = ();
	my %Data_QUAL = ();
	my $length_deleted_end = 0;

	#=======================================================================================================
	# 01: We split the data from the main program (filename/MID/sample) and store it in needed formats.
	#=======================================================================================================

	#We split the parameter to dispatch the chosen MID from the chosen name and the corresponding raw file
	@tmp = split("/", $input_parameter);
	#If the filename is the good one.
	if($file_fna =~ m/\Q$tmp[0]/)
	{
		#2.7: If the user chose only one MID (the forward only)
		if($#tmp == 2)
		{
			#2.7: We store needed MID (here, only one) for further steps
			$chosen_tag_F = &PerlLib::Miscellaneous::ListOfMIDs($tmp[1]);
			#2.7: Print for the user information that the program is started
			PerlLib::Miscellaneous::PrintScreen("PREPROCESS_MIDS",$_[0]." with MID ".$tmp[1],"started");
		}
		#2.7: If the user chose two MIDs (Forward and Reverse)
		if($#tmp == 3)
		{
			#2.7: We store needed MIDs for further steps (the reverse MID is reverse complemented for the
			#analysis)
			$chosen_tag_F = &PerlLib::Miscellaneous::ListOfMIDs($tmp[1]);
			$chosen_tag_R = &PerlLib::Miscellaneous::ListOfMIDs($tmp[2]);
			$chosen_tag_R = &PerlLib::Miscellaneous::DefineRCSequence($chosen_tag_R);
			#2.7: Print for the user information that the program is started
			PerlLib::Miscellaneous::PrintScreen("PREPROCESS_MIDS",$_[0]." with MIDs ".$tmp[1]." and ".$tmp[2],"started");
		}
		#2.7: We always select the last element for the filename
		$chosen_name = $tmp[$#tmp];

	}
	else { return; }

	#=======================================================================================================
	# 02: We search and open all input and result files, except the temporary result file
	#=======================================================================================================

	# We first open all needed files (fasta and quality files) for the analysis, and test where the file(s)
	# are potentially stored depending of previous steps
	if($folder_input eq "") { ($file_fna, $file_qual, $boolean_qual, *F_FNA, *F_QUAL) = PerlLib::Miscellaneous::OpenFastaQualFile("None", $file_fna); }
	else { ($file_fna, $file_qual, $boolean_qual, *F_FNA, *F_QUAL) = PerlLib::Miscellaneous::OpenFastaQualFile($folder_input, $file_fna); }
	# We also open the result files to store the reads
	open(RESULT_FNA, ">Result_files/$folder_output/".$chosen_name."_".$file_fna);
	if ($boolean_qual == 1) { open(RESULT_QUAL, ">Result_files/$folder_output/".$chosen_name."_".$file_qual); }

	#=======================================================================================================
	# 03: We read each line of the input file(s) (fna and potentially qual) and we treat them during
	# this reading. We also write all information in the dedicated output file(s).
	#=======================================================================================================

	#For each line of the file F_FNA
	LOOP01:while (<F_FNA>)
	{
		#2.5: We first store the information for the .fna and the .qual files
		$input_fna = $_;
		if ($boolean_qual == 1) { $input_qual = <F_QUAL> };

		#2.5: If the reading line had the character ">"
		if ($input_fna =~ m/\>/)
		{
			#2.0: Increment the variable $nb_seqs_tot
			$nb_seqs_tot++;
			#2.5: Needed values to treat the read
			@tmp_descr = split(" ", $input_fna);
			$Data_FNA{$tmp_descr[0]} = "";
			if ($boolean_qual == 1) { $Data_QUAL{$tmp_descr[0]} = ""; };
		}
		#2.5: Or the line is a sequence line
		else
		{
			#2.5: We first delete the \n and/or \r character(s)
			chomp $input_fna;
			$Data_FNA{$tmp_descr[0]} .= $input_fna;
			if ($boolean_qual == 1)
			{
				#2.5: We delete the \n and/or \r character(s)
				chomp $input_qual;
				$Data_QUAL{$tmp_descr[0]} .= " ".$input_qual;
			}
		}
	}
	#2.0: Close the file F_FNA
	close F_FNA;
	#2.5: Close the file F_QUAL
	if ($boolean_qual == 1) { close(F_QUAL); }

	#2.7: For each read found in the file
	LOOP02:foreach $descr_line (keys(%Data_FNA))
	{
		#2.7: If the user chose only one MID (the forward only)
		if($#tmp == 2)
		{
			if ($Data_FNA{$descr_line} =~ m/^($chosen_tag_F)(.*)/)
			{
				#2.0: We count the total number of kept reads, and the number of kept reads for each tag
				$nb_seqs_kept++;
				#2.0: We define the deleted length of the tag found
				$length_deleted = length($1);
				#2.5: We store the beginning of the sequence without the MID sequence
				$clean_fna = $2;
				#2.0: If we treat also a QUAL file
				if ($boolean_qual == 1)
				{
					#2.0: We then split the quality read based on space characters
					@tmp_qual = split(" ", $Data_QUAL{$descr_line});
					#2.0: Finally, we recreate the quality read without the part
					#corresponding to the deleted tag
					$clean_qual = join(" ", @tmp_qual[$length_deleted..$#tmp_qual]);
					delete($Data_QUAL{$descr_line});
				}
			}
			#2.5: If the beginning of the read did not contain
			#the searched MID sequence, we do nothing
			else { next LOOP02; }
		}
		#2.7: If the user chose two MIDs (the forward and the reverse)
		if($#tmp == 3)
		{
			if ($Data_FNA{$descr_line} =~ m/^($chosen_tag_F)(.*)($chosen_tag_R)$/)
			{
				#2.0: We count the total number of kept reads, and the number of kept reads for each tag
				$nb_seqs_kept++;
				#2.7: We define the deleted lengths of the tags found
				$length_deleted = length($1);
				$length_deleted_end = length($3);
				#2.5: We store the beginning of the sequence without the MID sequence
				$clean_fna = $2;
				#2.0: If we treat also a QUAL file
				if ($boolean_qual == 1)
				{
					#2.0: We then split the quality read based on space characters
					@tmp_qual = split(" ", $Data_QUAL{$descr_line});
					#2.7: Finally, we recreate the quality read without the part
					#corresponding to the deleted tag
					$clean_qual = join(" ", @tmp_qual[$length_deleted..($#tmp_qual-$length_deleted_end)]);
					delete($Data_QUAL{$descr_line});
				}
			}
			#2.5: If the beginning of the read did not contain
			#the searched MID sequence, we do nothing
			else { next LOOP02; }
		}
		# We launch the dedicated function to print data in output file(s)
		&PrintingReads($clean_fna, $clean_qual, $descr_line, $boolean_qual, *RESULT_FNA, *RESULT_QUAL);
		delete($Data_FNA{$descr_line});
		$clean_qual = "";
		$clean_fna = "";
	}
	#2.5: Close the output fna file
	close RESULT_FNA;
	#2.5: Close the output fna file
	if ($boolean_qual == 1) { close(RESULT_QUAL); }

	#=======================================================================================================
	# 04: We write in the temporary file all needed information, but we also give to the main program the
	# name of the output file for the next step(s).
	#=======================================================================================================

	#2.5: Open the file to store all results statistics
	open(RESULTS_TMP, ">>Temporary_files/Preprocess_mids.txt")|| die "==> Can't create file of the Preprocess_mids step of analysis<==";
	flock(RESULTS_TMP, 2);
	#2.7: If the user chose only one MID (the forward only)
	if($#tmp == 2)
	{
		#2.5: We store all result in a temporary file to print all data in the summary file
		printf(RESULTS_TMP "$nb_seqs_tot\t$file_fna\t$tmp[1]\t$chosen_tag_F\t$chosen_name\t$nb_seqs_kept\n");
		#Print for the user information that the program is terminated
		PerlLib::Miscellaneous::PrintScreen("PREPROCESS_MIDS",$_[0]." with MID ".$tmp[1],"terminated");
	}
	#2.7: If the user chose two MIDs (the forward and the reverse)
	elsif($#tmp == 3)
	{
		#2.5: We store all result in a temporary file to print all data in the summary file
		printf(RESULTS_TMP "$nb_seqs_tot\t$file_fna\t$tmp[1]\t$chosen_tag_F\t$tmp[2]\t$chosen_tag_R\t$chosen_name\t$nb_seqs_kept\n");
		#Print for the user information that the program is terminated
		PerlLib::Miscellaneous::PrintScreen("PREPROCESS_MIDS",$_[0]." with MIDs ".$tmp[1]." and ".$tmp[2],"terminated");
	}
	#We release the right to write into the file
	flock(RESULTS_TMP, 8);
	#2.0: We close the result summary file
	close(RESULTS_TMP);
	#2.5: Here, we gave to the main program the names of created file
	#2.6: Minor modification to avoid the treatment of empty files
	if($nb_seqs_kept == 0) { return(""); }
	else { return($chosen_name."_".$file_fna); }
}
1;
