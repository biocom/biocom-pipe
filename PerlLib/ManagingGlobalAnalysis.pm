#=======================================================================================================
# PACKAGE NAME: ManagingGlobalAnalysis
#    FUNCTIONS:	- ConcatenationStepDefinition (check where the merging of files will be done),
#			- SummaryFilesDefinition (create all summary files for the global analysis),
#			- DefineFileID(Give an ID for each independent file to recognize the origin of reads),
#
#			Modifs (19/07/2018): A minor modification was made to the SummaryFilesDefinition to 
#			manage the copy of the COMPUTATION dedicated file
#
# REQUIREMENTS:  Perl language only
#       AUTHOR:  Sebastien TERRAT <sebastien.terrat@dijon.inra.fr>
#      COMPANY:  GenoSol platform (UMR 1347 INRA/uB/AgroSup)
#      VERSION:  1.2
#    REALEASED:  16/04/2014
#     REVISION:  24/07/2014
#=======================================================================================================

# Name of the package defined for the main program
package PerlLib::ManagingGlobalAnalysis;

#Needed libraries
use strict;
use warnings;
#Needed CPAN to copy a file to another directory
use File::Copy;

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function needed to determine efficiently where the concatenation of files will be realized to 
# to the global analysis step. Uses the SearchInputParameters function (Miscellaneous) and return where
# the merging will be done. Modifications (v1.2): Here, we added
sub ConcatenationStepDefinition
{
	# Needed variables
	my $ref_array = "";
	my @parameters = ();
	my $step_selection = 2;
	my $verif = 0;
	
	# Check where the user chose to do the homogenization step to define where the merging will be done
	$ref_array = PerlLib::Miscellaneous::SearchInputParameters("RANDOM_PREPROCESS_MIDS");
	@parameters = @$ref_array;
	if(uc($parameters[0]) eq "YES") { $step_selection = 1; $verif = 1; }
	$ref_array = PerlLib::Miscellaneous::SearchInputParameters("RANDOM_PREPROCESS");
	@parameters = @$ref_array;
	if(uc($parameters[0]) eq "YES") { $step_selection = 1; $verif = 1; }
	$ref_array = PerlLib::Miscellaneous::SearchInputParameters("RANDOM_CLEANED");
	@parameters = @$ref_array;
	if(uc($parameters[0]) eq "YES") { $step_selection = 2; $verif = 1; }
	#1.2: Here, we do this test to avoid the problem when no random selection is done, and so to select
	#the reads after DEREPLICATION
	if($verif == 0)
	{
		#1.2: Here, we check for the fact that potentiallu, we chose to use already treated samples
		#with the same number of reads. So to avoid the potential problem of the realization
		#of the global alignment without optimization (esl-alimerge), we chose to add some selection
		#steps.
		$ref_array = PerlLib::Miscellaneous::SearchInputParameters("GLOBAL_ANALYSIS");
		@parameters = @$ref_array;
		if(uc($parameters[1]) eq "RAW") { $step_selection = 1; }
		elsif(uc($parameters[1]) eq "CLEAN") { $step_selection = 2; }
	}
	return $step_selection;
}
# End of the function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function needed to create all files needed to summarize results obtained for the global analysis 
# study. Uses the SearchInputParameters function (Miscellaneous) to catch all chosen parameters.
sub SummaryFilesDefinition
{
	# Needed variables
	my $ref_array = "";
	my @parameters = ();
	my $file_name = $_[0];
	my $step_name = $_[1];
	my $global_file = $_[0];
	
	# Catch the chosen parameters
	$ref_array = PerlLib::Miscellaneous::SearchInputParameters("GLOBAL_ANALYSIS");
	@parameters = @$ref_array;
	# Check if the step is done and copy the summary file
	if (uc($parameters[0]) eq "YES")
	{
		if($step_name eq "COMPUTATION") { $global_file =~ s/Independent/Global/; }
		copy("Summary_files/$file_name", "Summary_files/Global_Analysis/$global_file") || die "File $file_name cannot be copied";
	}
	$ref_array = PerlLib::Miscellaneous::SearchInputParameters($step_name);
	@parameters = @$ref_array;
	# Why ??
	if ($step_name eq "COMPUTATION") {$parameters[2] = $parameters[2]/100;} 
}
# End of the function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function developed to manage the Concatenated_files.txt information. This function takes as argument
# the name of the file that will be added and associated to a new ID in the file Concatenated_files.txt
# This function gives as output the chosen number as ID.
sub DefineFileID
{
	#Needed variables
	my $i = 0;
	my $file = $_[0];
	
	#Here, we open the file to read and also to write on it using this particular function
	open (F_Summary, "+<Summary_files/Global_Analysis/Concatenated_files.txt")|| die "==> Can't open file Concatenated_files.txt<==";
	#We then block the access of the file by other programs 
	flock (F_Summary,2);
	#Loop to count the number of files already defined
	while (<F_Summary>) { $i++; }
	#1.6: We then here fomat the $i value printed with a sufficient number of 0
	$i = sprintf ("%09d",$i+1);
	#We then print the formatted value associated with the file name chosen
	printf (F_Summary "$i\t$file\n");
	#We give back the acces of the file
	flock (F_Summary,8);
	#We close the Summary file
	close (F_Summary);
	return ($i);
}
# End of the function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

1;
