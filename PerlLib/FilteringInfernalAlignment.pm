#=======================================================================================================
# PACKAGE NAME: FilteringInfernalAlignment
#    FUNCTIONS:	 - CatchDataFromTmpAlignFile (search into the global alignment for quality scores),
#                - TreatDerepFile (create cleaned FASTA file after the filtering),
#                - TreatAlignFile (create cleaned alignment file after the filtering),
#                - FormatResultsSummaryFile (define the summary file for the user).
#
# REQUIREMENTS:  Perl language, and Math library to compute binary files
#       AUTHOR:  Sebastien TERRAT <sebastien.terrat@dijon.inra.fr>
#      COMPANY:  UMR 1347 Agroecology - Platform GenoSol
#      VERSION:  1.0
#    REALEASED:  23/04/2020
#     REVISION:  XXX
#=======================================================================================================

# Name of the package defined for the main program
package  PerlLib::FilteringInfernalAlignment;
# Needed libraries
use strict;
use warnings;

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function defined to search into the details of the global alignment the details about the quality of
# each read, and chose if the read is kept or not. To do this, this function searched into a temporary
# file from Infernal the score of the alignment, and if the value is negative, the read is deleted (so,
# stored into a hash table to be treated by further functions). This hash table is returned to the main
# program, and a temporary file with information for bad reads is created.
sub CatchDataFromTmpAlignFile
{
  #Defined variables
  my $input_file = $_[0];
  my $folder_output = $_[1];
  my @tmp = ();
  my %deleted_reads = ();
  my $file_name = "";
  my $needed_file = "";

  #Open the directory containing all temporary details of alignment files for all independant samples
  opendir (DIR, "Temporary_files/") || die "can't opendir Temporary_files: $!";
  #For each file found in the directory
  LOOP:while (defined($file_name = readdir(DIR)))
  {
    #We search and catch the needed file name
    if ($file_name eq "Tmp_".$input_file) { $needed_file = $file_name; last LOOP; }
    #We store their names in the align_file_names array
    else { next; }
  }
  #We then close the directory
  closedir DIR;

  #We opened the file containing details about alignments quality
  open(F_TMP, "Temporary_files/$needed_file") || die "Can't open the file $needed_file in the Temporary_files/ folder\n";
  open(F_TMPOUT, ">Result_files/$folder_output/$needed_file") || die "Can't create the file $needed_file in the Results folder/ folder\n";
  printf(F_TMPOUT "idx\tseq_name\tlength\tcm_from\tcm_to\ttrunc\tbit_sc\tavg_pp\tband_calc\talignment(sec)\ttotal\tmem(Mb)\n");
  #We read the file and catch needed data
  while(<F_TMP>)
  {
    #Avoiding descriptive lines
    if($_ =~ m/^#/) { next; }
    #Catching only scores of alignment quality
    else
    {
      $_ =~ s/\s+/\t/g;
      @tmp = split("\t", $_);
      #Here, if the quality score of alignment is negative (not goog at all), we store it in
      #a dedicated hash table and print the line of the bad reads into a tmp file
      if($tmp[7] < 0) { $deleted_reads{$tmp[2]} = $tmp[7]; printf(F_TMPOUT join("\t", @tmp[1..12])."\n"); }
      else { next; }
    }
  }
  close(F_TMP);
  close(F_TMPOUT);
  #We delete the temporary file not needed anymore
  #unlink("Temporary_files/$needed_file");
  #We finally returned the hash table with all non wanted reads
  return(\%deleted_reads);
}
# End of the function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function defined to create a new dereplication file that will be cleaned of reads that did not match to
# the global alignment structure. To do that, this function takes as input the hash table with potentially
# deleted reads, the file name that will be treated, the folder where the derepliced reads are stored and
# the output folder. As output, this function returns two integers, the number of total reads and the
#  number of reads deleted from the alignment. It creates also a new and cleaned file containing
# dereplicated reads.
sub TreatDerepFile
{
  #Needed variables
  my %del_reads = %{$_[0]};
  my $input_file = $_[1];
  my $folder_derep = $_[2];
  my $folder_output = $_[3];
  my @tmp = ();
  my $file_name = "";
  my $del_read = "";
  my $ID = "";
  my ($nb_tot_reads, $nb_del_reads) = (0,0);

  #Open the directory containing all temporary details of alignment files for all independant samples
  opendir (DIR, "Result_files/$folder_derep/") || die "can't opendir Result_files/$folder_derep: $!";
  #For each file found in the directory
  LOOP:while (defined($file_name = readdir(DIR)))
  {
    #No treatment for files '.' and '..'
    if ($file_name =~ m/^\.\.?$/){ next LOOP; }
    #We search and catch the needed file name
    if ($input_file =~ m/$file_name/) { last LOOP; }
    #We store their names in the align_file_names array
    else { next; }
  }
  #We then close the directory
  closedir DIR;

  #We then read the dereplicated file to keep only good reads and count the number of bad reads deleted
  open(F_IN, "Result_files/$folder_derep/$file_name") || die "can't open file $file_name Result_files/$folder_derep: $!";
  open(F_OUT, ">Result_files/$folder_output/FilterAlign_$file_name") || die "can't create file FilterAlign_$file_name: $!";
  #For each line of the Derep File
  while(<F_IN>)
  {
    #We searched for the descriptive line of each read
    if($_ =~ s/^>//)
    {
      #We searched for the number of occurrences of each read
      #and also if this one is deleted or not
      chomp($_);
      $ID = $_;
      @tmp = split("_", $ID);
      $nb_tot_reads += $tmp[1];
      #If the read must be deleted based on the previous search
      #we delete it, and count the number of occurrences of the deleted read
      if(exists($del_reads{$ID}))
      {
        $del_read = <F_IN>;
        delete($del_reads{$_});
        $nb_del_reads += $tmp[1];
      }
      #Or, we print directly the data into the dedicated file
      else { printf(F_OUT ">".$ID."\n"); }
    }
    #Or, we print directly the data into the dedicated file
    else { printf(F_OUT $_); }
  }
  #We finally close both files and return the obtained values and the output filename
  close(F_IN);
  close(F_OUT);
  return($nb_tot_reads, $nb_del_reads, "FilterAlign_$file_name");
}
# End of the function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function defined to define a new alignment file that will be cleaned of reads that did not match to
# the global alignment structure. To do that, this function takes as input the hash table with potentially
# deleted reads, the file name that will be treated, the folder of the alignment and the output folder.
# As output, the function creates a new alignment file in the output folder.
sub TreatAlignFile
{
  #Needed variables
  my %del_reads = %{$_[0]};
  my $input_file = $_[1];
  my $folder_align = $_[2];
  my $folder_output = $_[3];
  my @tmp = ();
  my $del_read = "";

  #We read the align file to keep only good reads
  open(F_IN, "Result_files/$folder_align/$input_file") || die "can't open file $input_file Result_files/$folder_align: $!";
  open(F_OUT, ">Result_files/$folder_output/FilterAlign_$input_file") || die "can't create file FilterAlign_$input_file: $!";
  #For each line of the Alignment File
  while(<F_IN>)
  {
    #We search only for reads IDs (so for lines starting by IDs)
    if($_ =~ m/^#/)   { printf(F_OUT $_); }
    elsif($_ eq "\n") { printf(F_OUT $_); }
    elsif($_ eq "//") { printf(F_OUT $_); }
    #So, for lines starting by IDs, we sort them and check their existence
    else
    {
      @tmp = split(" ", $_);
      #If the read must be deleted, we catch also the second line, as it is the
      #corresponding line score of the global alignment
      if(exists($del_reads{$tmp[0]})) { $del_read = <F_IN>; }
      else { printf(F_OUT $_); }
    }
  }
  #We finally close both files
  close(F_IN);
  close(F_OUT);
}
# End of the function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function defined to print into the Summary file for the user the results after the filtering of reads
# without convergence into the global alignment. We need three elements as input: the file name that will
# be treated, and the number of raw and deleted reads.
sub FormatResultsSummaryFile
{
  #Needed variables
  my $input_file = $_[0];
  my $nb_tot_reads = $_[1];
  my $nb_del_reads = $_[2];
  my $sample_name = "";

  #We first clean the file name to keep only the sample name
  $sample_name = PerlLib::Miscellaneous::ExtractSampleFromFileName($input_file);
  #We first open the summary result file
  open(F_SUM, ">>Summary_files/Filtering_Raw_Infernal_Alignment.txt") || die "Can't append the Filtering_Raw_Infernal_Alignment summary file\n";
  flock(F_SUM,2);
  printf(F_SUM "$sample_name\t$input_file\t$nb_tot_reads\t$nb_del_reads\n");
  flock(F_SUM,8);
  close(F_SUM);
}
# End of the function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#=======================================================================================================
#=======================================================================================================
# MAIN FUNCTION CALLED BY THE MAIN PROGRAM
#=======================================================================================================
#=======================================================================================================
# FILE:         FilteringInfernalAlignment
# USAGE:        PerlLib::FilteringInfernalAlignment::FilteringInfernalAlignment(ARGUMENTS)
# ARGUMENTS:    - $_[0]: the input algniment file,
#				        - $_[1]: the input folder,
#				        - $_[2]: the output folder,
#               - $_[3]: the dereplication folder,
#				        - $_[4]: the alignment folder
#
# DESCRIPTION:	This function was defined to delete sequences based on their quality score after the
#       global alignment of reads. Indeed, some reads (mainly with 18S primers) harbored very low
#       alignment scores after the INFERNAL alignment. This kind of reads, after careful checking, are
#       not reads from small ribosomal subunit. They have no hits in known databases. So, to fast the
#       global alignment of reads after merging, we chose to delete them. To do this, we catch the
#       details of the alignment given by INFERNAL, and we delete reads with negative scores,
#       corresponding to not stable alignments. Then, the program produced a new alignment file
#       associated to a cleaned FASTA file, and a summary file for the user.
#=======================================================================================================

sub FilteringInfernalAlignment
{
  #Defined variables
  my $input_file = $_[0];
  my $folder_input = $_[1];
  my $folder_output = $_[2];
  my $folder_derep = $_[3];
  my $folder_align = $_[4];
  my $ref_array = "";
  my $derep_output_file = "";
  my %deleted_reads = ();
  my ($nb_tot_reads, $nb_del_reads) = (0,0);

  #Printing data into the screen for users (starting the filtering)
  PerlLib::Miscellaneous::PrintScreen("FILTERING INFERNAL ALIGNMENT", $input_file, "started");
  #Here, we searched for the Data form the Global Alignment
  $ref_array = &CatchDataFromTmpAlignFile($input_file, $folder_output);
  %deleted_reads = %{$ref_array};
  #Then, we create the Dereplicated file cleaned based on the Alignment
  ($nb_tot_reads, $nb_del_reads, $derep_output_file) = &TreatDerepFile($ref_array, $input_file, $folder_derep, $folder_output);
  #We also cleaned the Alignment file
  &TreatAlignFile($ref_array, $input_file, $folder_align, $folder_output);
  #We finally print data into the summary file
  &FormatResultsSummaryFile($input_file, $nb_tot_reads, $nb_del_reads);
  #Printing data into the screen for users (finishing the filtering)
  PerlLib::Miscellaneous::PrintScreen("FILTERING INFERNAL ALIGNMENT", $input_file, "terminated");
  #We finally return both filenames for the main program
  return("FilterAlign_$input_file", $derep_output_file);
}
1;
