#=======================================================================================================
# PACKAGE NAME:  Miscellaneous
# FUNCTIONS:	- ComputeClusteringSteps (Define all clustering steps that will be computed),
#			- FileInfoAppending (Create the Infos.info file, essential for the Main program),
#			- CatchTreatedFileNames (Read Infos.info file and return data to the Main program),
#			- PrintScreen (Print information in the Main Screen for the user),
#			- SearchInputParameters (Search and store all chosen parameters for a dedicated step),
#			- CatchRawFiles (Define the raw files stored in the Raw_data folder),
#			- DefineRCSequence (Reverse Complement a sequence given as input),
#			- DefinePrimerRegex (Using a sequence, define a string for regular expressions),
#			- OpenFastaQualFile (Open .fasta and .qual files, and give filehandles),
#			- ReadInputFastaFile (Read and store data from a fasta file, give back an array),
#			- ModelChoice (choice of alignment model based on user choice),
#			- ListFilesInFolder (catch all file names from a defined folder),
#			- ListOfMIDs (give back the sequence for the DeMultiplexing step of the MID),
#			- ExtractSampleFromFileName (give back the sample name cleaned to all pipeline adds).
#
#	   DETAILS: 1.1: Addition of new functions (AlignmentModelChoice, ListFilesInFolder) from the Global
#			Alignment step to use them in other steps. Done the 23/07/2014.
#
#			1.2: Modification of the function CatchRawFiles to send back also fastq files
#			information. Done the 09/09/2014.
#
#			1.3: Addition of a new function called ListOfMIDs storing all known MIDs in GnS-PIPE to
#			simplify the DeMultiplexing library. Done the 17/09/2014.
#
#			1.4: Addition of new function called ExtractSampleFromFileName to obtain easily the
#			sample name cleaned from all prefixes and suffixes. Done the 19/07/218.
#
# REQUIREMENTS:  Perl language only
#       AUTHOR:  Sebastien TERRAT <sebastien.terrat@inra.fr>
#      COMPANY:  UMR 1347 Agroecologie
#      VERSION:  1.4
#    REALEASED:  22/04/2014
#     REVISION:  19/07/2018
#=======================================================================================================

# Name of the package defined for the main program
package  PerlLib::Miscellaneous;
# Needed libraries
use strict;
use warnings;

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function defined to compute the needed clustering steps based on two input parameters, the maximum
# percentage of dissimilarity and the step size for each cluster. This function will return an array
# containing all needed values of clustering
sub ComputeClusteringSteps
{
	# Input variables
	my $max_clustering = $_[0];
	my $step_clustering = $_[1];
	# Needed variables
	my @clust_steps = ();
	my $i = 0;

	# We create here the needed @steps array to realize all clustering steps. Here, we catch the first
	# clustering step
	$i = $step_clustering;
	# Then, all others clustering steps realized based on user choices
	while($i<=$max_clustering + $step_clustering)
	{
		push(@clust_steps, $i);
		$i = $i + $step_clustering;
	}
	# We then define the last one to avoid some not wanted clustering steps.
	for($i=0;$i<=$#clust_steps;$i++)
	{
		if ($clust_steps[$i] >= $max_clustering)
		{
			$clust_steps[$i] = $max_clustering;
			@clust_steps = @clust_steps[0..$i];
			last;
		}
	}
	# We finally return the array containing all values
	return \@clust_steps;
}
# End of the function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function needed to create the Infos.info file needed for the end of loops to give to the main
# program some information (file names, folder, number of folder, etc...)
sub FileInfoAppending
{
	#We avoid empty samples and/or empty folders.
	if(($_[0] eq "")||($_[1] eq "")) { return; }
	#We open the temporary file
	open (F, ">>Temporary_files/infos.info")|| die "==> Can't create file named infos.info<==";
	#We block its access
	flock(F, 2);
	#We store given parameters in the program
	printf(F "$_[0]\t$_[1]\n");
	#We give back the access to the file to other programs
	flock(F, 8);
	#We close its access
	close (F);
}

# End of the function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function to catch needed information and return them to the main program using the infos.info file,
# as this temporary file is used to give back information to the Main program.
sub CatchTreatedFileNames
{
	# Needed local variables
	my @tmp = ();	my @tmp2 = ();
	my $file = "";	my $folder = "";
	# Test for the existence of the file
	if(-e "Temporary_files/infos.info")
	{
		# Then, we open the file containing needed information
		open(FILE, "Temporary_files/infos.info");
		# For each line found in the file
		while(<FILE>)
		{
			# We treat and store data
			$_ =~ s/\n//g;
			@tmp = split("\t", $_);
			push(@tmp2, $tmp[0]);
			$folder = $tmp[1];
		}
		# We finally close and delete the file
		close(FILE);
		unlink("Temporary_files/infos.info");
		# We return the name of the folder and the array
		return ($folder,\@tmp2);
	}
	else { return; }
}
# End of the function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function needed to print to the screen the start and the end of each program. Takes three arguments as
# input: the name of the program, the name of the treated file and if the program starts or stop the
# analysis
sub PrintScreen
{
	# We print the information in the screen to inform the user
	print "$_[0]\ton $_[1] $_[2]\t".localtime(time)."\n";
}
# End of the function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function needed to search parameters corresponding to the wanted program. The function uses as input
# the name of the chosen step, and then give back in an array all chosen parameters.
sub SearchInputParameters
{
	# Needed empty array
	my @parameters = ();
	# Name of the program that will be launched to find parameters
	my $program_name = "";
	my @tmp = ();
	# We catch the name of the program given as input by the user
	$program_name = $_[0];
	# We open the INPUT file
	open(F_Input, "Input.txt")|| die "==> File Not Found: Input.txt<==";
	# We read the file
	LOOP01:while (<F_Input>)
	{
		# We treat particular formats for the Input file
		$_ =~ s/\r//;
		$_ =~ s/\n//;
		# If we found the name of the program, we catch all parameters
		if ($_ =~ m/###$program_name###/)
		{
			while (<F_Input>)
			{
				$_ =~ s/\r//;
				$_ =~ s/\n//;
				# With leave the file if we found the end of the parameters
				if ($_ =~ m/\/\//) { last LOOP01; }
				else
				{
					@tmp = split("\t", $_);
					# We treat particular formats for the Input file
					$tmp[1] =~ s/\r//;
					$tmp[1] =~ s/\n//;
					# We catch the parameter
					push (@parameters, $tmp[1]);
				}
			}
		}
	}
	# We close the INPUT file
	close F_Input;
	return \@parameters;
}
# End of the function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function needed to search, store and give back names and types of raw files. This function reads the
# Raw_data folder and define the names and the types of all rax files found. This function returns three
# arrays: @files_sff, @files_fna and @files_fasta.
sub CatchRawFiles
{
	# Needed empty variables
	my $file = "";
	my @files_sff = ();
	my @files_fna = ();
	my @files_fastq = ();
	my @files_qual = ();

	# Open the directory containing all files needed to be treated by the workflow
	opendir (DIR_RAW, "Raw_data/") || die "can't opendir Raw_data: $!";
	# For each file found in the directory Raw_data/
	LOOP01:while (defined($file = readdir(DIR_RAW)))
	{
		# No treatment for files '.' and '..'
	  	if ($file =~ m/^\.\.?$/){next LOOP01;}
	  	else
	  	{
	  		# Analysis to find what type of file is found and need treatment
	  		if ($file =~ m/.sff/) {push(@files_sff, $file);}
	  		elsif (($file =~ m/.fastq/)||($file =~ m/.fq/)) {push(@files_fastq, $file);}
	  		elsif ($file =~ m/.qual/) {push(@files_qual, $file);}
	  		elsif (($file =~ m/.fna/)||($file =~ m/.fasta/)) {push(@files_fna, $file);}
	  	}
	}
	closedir DIR_RAW;
	# Return three
	return (\@files_sff, \@files_fna, \@files_qual, \@files_fastq);
}
# End of the function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function needed for reverse complementation of sequences to treat the primers and the reads that need
# reverse complementation. Takes as input a sequence, reverse-complement it and give back to the main
# function the produced sequence.
sub DefineRCSequence
{
	# Needed variables
	my @RC = ();
	my $RC_seq = "";
	my $i = 0;
	# For the given sequence, treatment by replacing degeneracies by corresponding bases after reverse
	# complementation of the given sequence
	@RC = split("",$_[0]);
	# Reverse the array to reverse the primer or the sequence itself
	@RC = reverse(@RC);
	# Loop and tests to complement the sequence
	LOOP:for($i=0;$i<=$#RC;$i++)
	{
		if($RC[$i] =~ s/A/T/) {next LOOP;}  if($RC[$i] =~ s/C/G/) {next LOOP;}
		if($RC[$i] =~ s/G/C/) {next LOOP;}  if($RC[$i] =~ s/T/A/) {next LOOP;}
		if($RC[$i] =~ s/M/K/) {next LOOP;}  if($RC[$i] =~ s/R/Y/) {next LOOP;}
		if($RC[$i] =~ s/Y/R/) {next LOOP;}  if($RC[$i] =~ s/K/M/) {next LOOP;}
		if($RC[$i] =~ s/V/B/) {next LOOP;}  if($RC[$i] =~ s/H/D/) {next LOOP;}
		if($RC[$i] =~ s/D/H/) {next LOOP;}  if($RC[$i] =~ s/B/V/) {next LOOP;}
	}
	# Join the table to recreate the sequence of the reverse-complemented sequence of the primer
	$RC_seq = join("",@RC);
	# We finally return the reverse complemented sequence itself to the main program
	return $RC_seq;
}
# End of the function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function needed to define a regular expression to efficiently search and retrieve the primer sequences
# using the given sequence of the primer as input. As input, takes a sequence, then the function
# replaces all degeneracies and ambiguities by needed codes. Gives back a string directly available for
# regular expressions.
sub DefinePrimerRegex
{
	# Needed variable
	my $primer = $_[0];

	# We modify the sequence itself to create efficiently the regular expression needed
	$primer =~ s/M/\[AC\]/g;		$primer =~ s/R/\[AG\]/g;
	$primer =~ s/W/\[AT\]/g;		$primer =~ s/S/\[CG\]/g;
	$primer =~ s/Y/\[CT\]/g;		$primer =~ s/K/\[GT\]/g;
	$primer =~ s/V/\[ACG\]/g;	$primer =~ s/H/\[ACT\]/g;
	$primer =~ s/D/\[AGT\]/g;	$primer =~ s/B/\[CGT\]/g;
	$primer =~ s/N/\[ACGT\]/g;	$primer =~ s/I/\[ACGT\]/g;
	# We return the sequence itself
	return $primer;
}
# End of the function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function needed to open a file based on the input given (the name of the folder, and the name of the
# file) and give back a filhandle easily usable in the main program. This program also gives another
# filehandle if a quality file is found.
sub OpenFastaQualFile
{
	#Needed variables
	my $file_qual = "";
	my $boolean_qual = 0;
	my $folder_input = $_[0];
	my $file_fasta = $_[1];

	# Test to know if the input folder is the "Raw_data" or a folder previously created by the main
	# program and a previous treatment step
	if ($folder_input eq "None")
	{
		# Open file one given by the user and containing cleaned sequences
		open(F_FASTA, "Raw_data/$file_fasta")|| die "==> File Not Found: $file_fasta<==";
		# Needed deletion to catch only the ID of the RUN
		$file_fasta =~ s/.fasta//;
		$file_fasta =~ s/.fna//;
		$file_qual = $file_fasta.".qual";
		# Test for the existence of the .qual file
		if(-e "Raw_data/$file_qual")
		{
			open(F_QUAL, "Raw_data/$file_qual")|| die "==> File Not Found: $file_qual<==";
			$boolean_qual = 1;
		}
	}
	elsif ($folder_input eq "Temporary_files")
	{
		# Open file one given by the user and containing cleaned sequences
		open(F_FASTA, "$folder_input/$file_fasta")|| die "==> File Not Found: $file_fasta<==";
		# Needed deletion to catch only the ID of the RUN
		$file_fasta =~ s/.fasta//;
		$file_fasta =~ s/.fna//;
		$file_qual = $file_fasta.".qual";
		# Test for the existence of the .qual file
		if(-e "Raw_data/$file_qual")
		{
			open(F_QUAL, "Raw_data/$file_qual")|| die "==> File Not Found: $file_qual<==";
			$boolean_qual = 1;
		}
	}
	else
	{
		# Open file one given by the user and containing cleaned sequences
		open(F_FASTA, "Result_files/$folder_input/$file_fasta")|| die "==> File Not Found: $file_fasta<==";
		# Needed deletion to catch only the ID of the RUN
		$file_fasta =~ s/.fasta//;
		$file_fasta =~ s/.fna//;
		$file_qual = $file_fasta.".qual";
		# Test for the existence of the .qual file
		if(-e "Result_files/$folder_input/$file_qual")
		{
			open(F_QUAL, "Result_files/$folder_input/$file_qual")|| die "==> File Not Found: $file_qual<==";
			$boolean_qual = 1;
		}
	}

	# Needed deletion to catch only the ID of the RUN
	$file_qual =~ s/.qual//;
	$file_fasta = $file_qual.".fasta";
	$file_qual = $file_qual.".qual";

	# The file returns files elements, the names of the file(s), a boolean for the existence of the
	# .qual file, and the two filehandles needed
	return($file_fasta, $file_qual, $boolean_qual, *F_FASTA, *F_QUAL);
}
# End of the function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function needed to read a file given as input by th main program, catch all data, and then give back
# all data stored in an array.
sub ReadInputFastaFile
{
	#Needed variables
	my @data = ();
	my $file = $_[0];
	my $info_qual = $_[1];

	while(<$file>)
	{
		#We must avoid empty lines to avoid a problem during the analysis due to the shuffling of reads
		if ($_ eq "\n") {next;}

		#If the reading line starts with the character ">"
		if ($_ =~ m/^>/)
		{
			#Store the sequence in @data
			push(@data, $_);
		}
		else
		{
			#Verify if we have stored the descriptive line in @reads_fasta
			if ($data[$#data] ne "")
			{
				#Eliminate the last character corresponding to a "\n" and/or a "\r"
				$_ =~ s/\n//; $_ =~ s/\r//;
				#Concatenate the sequence and the descriptive line
				if ($info_qual eq "QUAL") { $data[$#data] = $data[$#data]." ".$_; }
				else { $data[$#data] = $data[$#data].$_; }
			}
		}
	}
	return (\@data);
}
# End of the function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function defined to use needed parameters (used matrix: bacteria, archaea, fungi or algae) and define
# what model will be used for the alignment. To do this, the function verify the choice of the
# desired model given by the user and create the good $model data. All models are stored in the
# Matrix-1.1.1 directory and defined using parameters and sequences given by INFERNAL developers.
sub ModelChoice
{
	#Defined variables
	my @organisms = ("BACTERIA", "FUNGI", "ARCHAEA", "ALGAE");
	my $model = uc($_[0]);
	my $check = 0;
	my $organism = "";

	foreach $organism (@organisms) { if($model eq $organism) { $check = 1; } }

	#Choice of the bacteria model of covariance
	if ($model eq "BACTERIA") { $model = "$FindBin::RealBin/Prog_Perl/Matrix-1.1.1/bacteria_model.cm "; }
	#Choice of the bacteria model of covariance
	elsif ($model eq "FUNGI") { $model = "$FindBin::RealBin/Prog_Perl/Matrix-1.1.1/fungi_model.cm "; }
	#Choice of the archaea model of covariance
	elsif ($model eq "ARCHAEA") { $model = "$FindBin::RealBin/Prog_Perl/Matrix-1.1.1/archaea_model.cm "; }
	#Choice of the algea model of covariance
	elsif ($model eq "ALGAE") { $model = "$FindBin::RealBin/Prog_Perl/Matrix-1.1.1/algae_model.cm "; }

	if($check == 0)
	{
		die "===> The BIOCOM-PIPE was stopped due to a coherency error. Indeed, the model that you
defined to use during the analysis ($model) is not available with BIOCOM-PIPE. Please chose
between bacteria, fungi, archaea or algae please. Consequently, check the Input.txt file
and relaunch the analysis <===\n";
	}
	#Finally we return th chosen model
	return($model);
}
# End of the function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function dedicated to catch and store all file names of independent alignments done in a defined
# directory containing only alignments.
sub ListFilesInFolder
{
	#Needed variables
	my $file_name = "";
	my @file_names = ();
	my $folder = $_[0];

	#Open the directory containing all alignment files for all independant samples
	opendir (DIR, "Result_files/$folder/") || die "can't opendir $folder: $!";
	#For each file found in the directory Raw_data/
	LOOP:while (defined($file_name = readdir(DIR)))
	{
		#No treatment for files '.' and '..'
	  	if ($file_name =~ m/^\.\.?$/){ next LOOP; }
	  	#We store their names in the align_file_names array
	  	else { push(@file_names, $file_name); }
	}
	#We then close the directory
	closedir DIR;
	return \@file_names;
}
# End of the function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function dedicated to return a the sequence of the analyzed MID to the DeMultiplexing function to
# realize the analysis and the DeMultiplexing step. This function checks also the given MID based on the
# sequence if it's not known.
sub ListOfMIDs
{
	# Hash containing all known MIDs used in GnS-PIPE and various projects
	my %tags = (
	'MID001'=>'ACGAGTGCGT','MID002'=>'ACGCTCGACA','MID003'=>'AGACGCACTC','MID004'=>'AGCACTGTAG',
	'MID005'=>'ATCAGACACG','MID006'=>'ATATCGCGAG','MID007'=>'CGTGTCTCTA','MID008'=>'CTCGCGTGTC',
	'MID009'=>'TAGTATCAGC','MID010'=>'TCTCTATGCG','MID011'=>'TGATACGTCT','MID012'=>'TACTGAGCTA',
	'MID013'=>'CATAGTAGTG','MID014'=>'CGAGAGATAC','MID015'=>'ATACGACGTA','MID016'=>'TCACGTACTA',
	'MID017'=>'CGTCTAGTAC','MID018'=>'TCTACGTAGC','MID019'=>'TGTACTACTC','MID020'=>'ACGACTACAG',
	'MID021'=>'CGTAGACTAG','MID022'=>'TACGAGTATG','MID023'=>'TACTCTCGTG','MID024'=>'TAGAGACGAG',
	'MID025'=>'TCGTCGCTCG','MID026'=>'ACATACGCGT','MID027'=>'ACGCGAGTAT','MID028'=>'ACTACTATGT',
	'MID029'=>'ACTGTACAGT','MID030'=>'AGACTATACT','MID031'=>'AGCGTCGTCT','MID032'=>'AGTACGCTAT',
	'MID033'=>'ATAGAGTACT','MID034'=>'CACGCTACGT','MID035'=>'CAGTAGACGT','MID036'=>'CGACGTGACT',
	'MID037'=>'TACACACACT','MID038'=>'TACACGTGAT','MID039'=>'TACAGATCGT','MID040'=>'TACGCTGTCT',
	'MID041'=>'TAGTGTAGAT','MID042'=>'TCGATCACGT','MID043'=>'TCGCACTAGT','MID044'=>'TCTAGCGACT',
	'MID045'=>'TCTATACTAT','MID046'=>'TGACGTATGT','MID047'=>'TGTGAGTAGT','MID048'=>'ACAGTATATA',
	'MID049'=>'ACGCGATCGA','MID050'=>'ACTAGCAGTA','MID051'=>'AGCTCACGTA','MID052'=>'AGTATACATA',
	'MID053'=>'AGTCGAGAGA','MID054'=>'AGTGCTACGA','MID055'=>'CGATCGTATA','MID056'=>'CGCAGTACGA',
	'MID057'=>'CGCGTATACA','MID058'=>'CGTACAGTCA','MID059'=>'CGTACTCAGA','MID060'=>'CTACGCTCTA',
	'MID061'=>'CTATAGCGTA','MID062'=>'TACGTCATCA','MID063'=>'TAGTCGCATA','MID064'=>'TATATATACA',
	'MID065'=>'TATGCTAGTA','MID066'=>'TCACGCGAGA','MID067'=>'TCGATAGTGA','MID068'=>'TCGCTGCGTA',
	'MID069'=>'TCTGACGTCA','MID070'=>'TGAGTCAGTA','MID071'=>'TGTAGTGTGA','MID072'=>'TGTCACACGA',
	'MID073'=>'TGTCGTCGCA','MID074'=>'ACACATACGC','MID075'=>'ACAGTCGTGC','MID076'=>'ACATGACGAC',
	'MID077'=>'ACGACAGCTC','MID078'=>'ACGTCTCATC','MID079'=>'ACTCATCTAC','MID080'=>'ACTCGCGCAC',
	'MID081'=>'AGAGCGTCAC','MID082'=>'AGCGACTAGC','MID083'=>'AGTAGTGATC','MID084'=>'AGTGACACAC',
	'MID085'=>'AGTGTATGTC','MID086'=>'ATAGATAGAC','MID087'=>'ATATAGTCGC','MID088'=>'ATCTACTGAC',
	'MID089'=>'CACGTAGATC','MID090'=>'CACGTGTCGC','MID091'=>'CATACTCTAC','MID092'=>'CGACACTATC',
	'MID093'=>'CGAGACGCGC','MID094'=>'CGTATGCGAC','MID095'=>'CGTCGATCTC','MID096'=>'CTACGACTGC',
	'MID097'=>'CTAGTCACTC','MID098'=>'CTCTACGCTC','MID099'=>'CTGTACATAC','MID100'=>'TAGACTGCAC',
	'MID101'=>'TAGCGCGCGC','MID102'=>'TAGCTCTATC','MID103'=>'TATAGACATC','MID104'=>'TATGATACGC',
	'MID105'=>'TCACTCATAC','MID106'=>'TCATCGAGTC','MID107'=>'TCGAGCTCTC','MID108'=>'TCGCAGACAC',
	'MID109'=>'TCTGTCTCGC','MID110'=>'TGAGTGACGC','MID111'=>'TGATGTGTAC','MID112'=>'TGCTATAGAC',
	'MID113'=>'TGCTCGCTAC','MID114'=>'ACGTGCAGCG','MID115'=>'ACTCACAGAG','MID116'=>'AGACTCAGCG',
	'MID117'=>'AGAGAGTGTG','MID118'=>'AGCTATCGCG','MID119'=>'AGTCTGACTG','MID120'=>'AGTGAGCTCG',
	'MID121'=>'ATAGCTCTCG','MID122'=>'ATCACGTGCG','MID123'=>'ATCGTAGCAG','MID124'=>'ATCGTCTGTG',
	'MID125'=>'ATGTACGATG','MID126'=>'ATGTGTCTAG','MID127'=>'CACACGATAG','MID128'=>'CACTCGCACG',
	'MID129'=>'CAGACGTCTG','MID130'=>'CAGTACTGCG','MID131'=>'CGACAGCGAG','MID132'=>'CGATCTGTCG',
	'MID133'=>'CGCGTGCTAG','MID134'=>'CGCTCGAGTG','MID135'=>'CGTGATGACG','MID136'=>'CTATGTACAG',
	'MID137'=>'CTCGATATAG','MID138'=>'CTCGCACGCG','MID139'=>'CTGCGTCACG','MID140'=>'CTGTGCGTCG',
	'MID141'=>'TAGCATACTG','MID142'=>'TATACATGTG','MID143'=>'TATCACTCAG','MID144'=>'TATCTGATAG',
	'MID145'=>'TCGTGACATG','MID146'=>'TCTGATCGAG','MID147'=>'TGACATCTCG','MID148'=>'TGAGCTAGAG',
	'MID149'=>'TGATAGAGCG','MID150'=>'TGCGTGTGCG','MID151'=>'TGCTAGTCAG','MID152'=>'TGTATCACAG',
	'MID153'=>'TGTGCGCGTG','MID160'=>'CGTGAGCTGA','MID161'=>'CGCGACATCT','MID162'=>'CGTGCTGTGT',
	'MID163'=>'CGCACGCTGT','MID164'=>'CGCTATCTAT','MID165'=>'CATCTACTGA','MID166'=>'CATAGTGACA',
	'MID167'=>'CATCGAGCAG','MID168'=>'CGTATATGCT','MID169'=>'CGAGCTCATG','MID170'=>'CATATCTCGT',
	'MID171'=>'CGATCACAGT','MID172'=>'CGAGCTAGCT','MID173'=>'CACTGATGTC','MID174'=>'CACAGAGCTA',
	'MID500'=>'TGATA','MID501'=>'ACGAC','MID502'=>'AGAGC','MID503'=>'AGCAG','MID504'=>'ATCGA',
	'MID506'=>'GACGT','MID507'=>'GAGTA','MID508'=>'GATAG','MID509'=>'TATGA','MID510'=>'TCATC',
	'MID511'=>'TCTCG','MID512'=>'TGTAC','MID301'=>'ATCGA','MID302'=>'ATGCT','MID303'=>'CACAC',
	'MID304'=>'CATCT','MID305'=>'CTAGT','MID306'=>'GTCTC','MID307'=>'TACTG','MID308'=>'TAGAT',
	'MID311'=>'TGACT',
	'RLMID005'=>'ACGAGTAGACT','RLMID007'=>'ACGTACACACT','RLMID008'=>'ACGTACTGTGT',
	'RLMID014'=>'AGTACGAGAGT','RLMID018'=>'AGTGTAGTAGT','RLMID020'=>'CAGTACGTACT',
	'RLMID022'=>'CGACGAGTACT','RLMID024'=>'CGTACGTCGAT','RLMID025'=>'CTACTCGTAGT',
	'RLMID036'=>'ACGAGCGCGCT','RLMID039'=>'ACGCTCTCTCT','RLMID040'=>'ACGTCGCTGAT',
	'RLMID041'=>'ACGTCTAGCAT','RLMID056'=>'AGCAGCGTAGT',
	'MID901'=>'ATCAGACAC','MID902'=>'ATATCGCGA',
	'MID601'=>'TAGTAT','MID602'=>'TCTCTA',
	'MID801'=>'CGTGTCTC','MID802'=>'CTCGCGTG','MID803'=>'TACGAGTA','MID804'=>'CATAGTAG',
	'MID805'=>'CGAGAGAT','MID806'=>'ATACGACG','MID807'=>'TCACGTAC','MID808'=>'CGTCTAGT',
	'MID200'=>'TCATAGACAG','MID201'=>'TATCACTACG','MID202'=>'CTGTACGTAG','MID203'=>'AGTCTCTAGA',
	'MID204'=>'AGATACACAG','MID205'=>'ACTCTAGTCT','MID206'=>'AGTCAGTGTA','MID207'=>'CTACGTCTGT',
	'MID208'=>'CGACTACGAG','MID209'=>'TAGCACACTA','MID210'=>'TACGAGTACA','MID211'=>'TACGAGTACA',
	'MID212'=>'TGCTACTGAG','MID213'=>'CACGATAGCG','MID214'=>'TATATCGACA','MID215'=>'TGTACTACAT',
	'MID216'=>'AGAGCGCGAG','MID217'=>'CGTAGATCGA','MID218'=>'TGATGACGCG','MID219'=>'TCTCTCGAGA',
	'MID220'=>'TAGTGTAGCG','MID221'=>'TCACGACGTA');
	# We catch the name of the known MID
	my $tag_name = $_[0];

	# We test here if the MID is known (based on its name), to return the good sequence
	if (defined($tags{$tag_name})) { return ($tags{$tag_name}); }
	# If not, we check if the MID contain only known bases to avoid problem of analysis
	else
	{

		if($tag_name =~ m/[^ACGTacgt]/)
		{
			die "==> The analysis of $tag_name was stopped due to a coherency error.
Your MID contain unknown characters, please check it
carefully and relaunch the GnS-PIPE <==\n";
		}
		# We finally return the good MID
		else { return($tag_name); }
	}
}
# End of the function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function dedicated to clean the file name given as input and return only the sample name important
# for the user as output. This name will be added to Summary files to clarify user reading.
sub ExtractSampleFromFileName
{
	#Needed variables
	my $input_filename = $_[0];
	#Cleaning all prefixes added to the filename to keep only the sample name
	$input_filename =~ s/Derep_//g;		$input_filename =~ s/Rdm_//g;
	$input_filename =~ s/Hpreprocess_//;	$input_filename =~ s/Mpreprocess_//;
	$input_filename =~ s/Lpreprocess_//;	$input_filename =~ s/FilterAlign_//;
	$input_filename =~ s/PrinSeq_//;		$input_filename =~ s/FLASH_//;
	$input_filename =~ s/Clust_//;		$input_filename =~ s/Clean_//;
	$input_filename =~ s/Hunt_//;			$input_filename =~ s/Qual_//;
	$input_filename =~ s/Taxo_//;			$input_filename =~ s/SILVA_//;
	$input_filename =~ s/RDP_//;			$input_filename =~ s/Align_//;
	#Cleaning also the suffixes from file types
	$input_filename =~ s/\.fasta//;		$input_filename =~ s/\.clust//;
	return($input_filename);
}
# End of the function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

1;
