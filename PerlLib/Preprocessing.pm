#=======================================================================================================
# PACKAGE NAME: Preprocessing
#    FUNCTIONS:	 - DefineAllMMPrimers (creating all primer sequences with mismatches),
#				 - MMPrimers (creating primer sequences with mismatches based on one sequence),
#				 - PreprocessQualSeq (treating the quality file using the treated fasta sequence),
#				 - DefineAllPrimerSeq (producing all needed regular expressions using primer sequences),
#				 - TreatPrintSeq (printing cleaned sequences (qual and/or fasta in ouput files),
#				 - Preprocessing (main function called by the main program, do the preprocessing step).
# REQUIREMENTS:  Perl language only
#       AUTHOR:  Sebastien TERRAT <sebastien.terrat@dijon.inra.fr>
#      COMPANY:  GenoSol platform (UMR 1347 INRA/uB/AgroSup)
#      VERSION:  3.0
#    REALEASED:  03/11/2011
#     REVISION:  16/04/2014
#=======================================================================================================

# Name of the package defined for the main program
package PerlLib::Preprocessing;

# Needed libraries
use strict;
use warnings;

# GLOBAL VARIABLES NEEDED FOR SEVERAL FUNCTIONS
my $tot_length = 0;
my $nb_raw_reads = 0;
my $nb_reads_length_filter = 0;
my $nb_reads_N_filter = 0;
my $nb_reads_primer_filter = 0;
my $nb_reads_final = 0;
my $length = 0;

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function defined to create and give back primer sequences with mismatches (mutations, deletions and/or
# deletions) based on user choices. This function takes as input two arguments (the primer sequence and 
# the number of mismatches defined by the user in the Input.txt file) and return as output an array with
# all created primers. This program uses another function, called MMPrimers.
sub DefineAllMMPrimers
{
	#Needed variables
	my $ref_array = "";
	my @tmp = ();
	my @glob = ();
	my @primers = ();
	my %all_primers = ();
	#Input variables
	my $primer = $_[0];
	my $nb_mm = $_[1];
	#First, we store the perfect primer
	push(@primers, $primer);
	#No more than two differences tolerated in the primer.
	if ($nb_mm > 2) { $nb_mm = 2; }
	#First, we verify if the user wants degenerated primers
	if ($nb_mm == 0) { return(\@primers); }
	#Then, we launch the creation of primers with one degeneracy
	$ref_array = &MMPrimers($primer);
	@tmp = @{$ref_array};
	push(@primers, @tmp);
	$nb_mm = $nb_mm - 1;
	#We then define a loop to create recursively all needed primers
	while ($nb_mm > 0)
	{
		#Loop to treat all primers with at least one degeneracy
		LOOP01:foreach $primer (@primers)
		{
			$ref_array = &MMPrimers($primer);
			@tmp = @{$ref_array};
			push(@glob, @tmp);
		}
		$nb_mm = $nb_mm - 1;
		#We also delete all duplicates
		foreach $primer (@tmp) { $all_primers{$primer}++; }
		@tmp = keys(%all_primers);
		#Finally, we store all primers in the @primers array
		push(@primers, @tmp);
	}
	return(\@primers);
}
# End of the function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function defined to create primers with a mismatch (mutation, deletion and/or insertion) based on the 
# sequence of the primer given as input. Gives as output an array containing all unique primer sequences.
sub MMPrimers
{
	#Needed variables
	my $primer = $_[0];
	my $i = 0;
	my $deb = "";
	my $deb_del = "";
	my $fin = "";
	my @primers = ();
	my %all_primers = ();
	
	#Loop to create all primers with one difference (mutation, insertion, deletion)
	LOOP01:for($i=1;$i<=length($primer);$i++)
	{
		#Needed positions to create primers with deletions, insertions and mutations
		$fin = substr($primer, $i);
		$deb = substr($primer,0,$i);
		$deb_del = substr($primer,0,$i-1);
		#If we are at the end of the primer (last base), we will not create primers with insertions
		if ($i == length($primer))
		{
			$all_primers{"$deb_del"."$fin"}++;
			$all_primers{"$deb_del"."N"."$fin"}++;
		}
		#We create primers with insertions, deletions and mutations
		else
		{
			$all_primers{"$deb_del"."$fin"}++;
			$all_primers{"$deb_del"."N"."$fin"}++;
			$all_primers{"$deb"."N"."$fin"}++;
		}
	}
	#We then catch all unique primers and send them back to the main program
	@primers = keys(%all_primers);
	return (\@primers);
}
# End of the function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function defined to treat the quality file using the information obtained with the treated sequence
# itself. This function takes as input several arguments (the quality sequence itself, a boolean to 
# know if we do or not the reverse complementation of the sequence, the proximal primer sequence, and the
# sequence in fasta format itself). As output, the function give back the quality sequence without the
# quality scores corresponding to primer sequences.
sub PreprocessQualSeq
{
	#Needed variables
	my $read_qual = $_[0];
	my $reverse_qual = $_[1];
	my $proximal_primer = $_[2];
	my $seq = $_[3];
	my $qual_seq = "";
	my @qual_seq = "";
	my @tmp = ();
	my @tmp2 = ();
	
	#2.5: Split and store the descriptive line and the quality sequence
	@tmp = split("\n", $read_qual);
	#2.5: We split the quality sequence based on spaces found
	@tmp2 = split(" ", $tmp[1]);
	#2.0: Finally, we recreate the quality read without the part corresponding to the primers
	@qual_seq = @tmp2[length($proximal_primer)..length($proximal_primer) + length($seq) - 1];
	
	if($reverse_qual == 1)
	{
		#We also reverse the quality sequence as the read was antisense
		@qual_seq = reverse(@qual_seq);
	}
	#2.5: We finally recreate the quality sequence
	$qual_seq = join(" ", @qual_seq);	
	return ($qual_seq);
}
# End of the function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function defined to treat and produce, based on the primer sequences and the stringency, all needed
# regular expressions needed. As input, the function needs the primer sequences (forward and reverse),
# and the "stringency" parameter to know how many primers have to be created. This function use also 
# some functions from the Miscellaneous package (DefineRCSequence and DefinePrimerRegex, to reverse
# complement and create regular expressions respectively). As output, the function gave back two arrays
# containing regular expressions corresponding to forward and reverse primers, and also regular 
# expressions of forward and reverse primers.
sub DefineAllPrimerSeq
{
	#Needed variables
	my $i = 0;
	my $element = "";
	my $forward = $_[0];
	my $reverse = $_[1];
	my $stringency = $_[2];
	my @forward_rc_part = ();
	my @reverse_rc_part = ();
	
	#2.5: For each oligo, treatment by replacing degeneracies by corresponding bases and reverse complement it
	$forward_rc_part[0] = PerlLib::Miscellaneous::DefineRCSequence($forward);
	$reverse_rc_part[0] = PerlLib::Miscellaneous::DefineRCSequence($reverse);

	#2.5: Create the regular expression for each primer (forward and reverse, reverse complemented or not)
	#Each regular expression is stored for further analysis in string variables ($forward, $forward_rc,
	#$reverse and $reverse_rc). To do this efficiently, we use the function DefinePrimerRegex
	$forward = PerlLib::Miscellaneous::DefinePrimerRegex($forward);
	$reverse = PerlLib::Miscellaneous::DefinePrimerRegex($reverse);

	#Test to know if partial primers need to be defined, using the "stringency" parameter
	if ((uc($stringency) eq "MEDIUM")||(uc($stringency) eq "LOW"))
	{
		#2.5: Loops to create and store partial primers to search only partial primers in raw sequences. The smallest
		#primer had 6 bases to keep sufficient specificity.
		for($i=0;$i<length($forward_rc_part[0])-6;$i++) { $forward_rc_part[$i+1] = substr($forward_rc_part[0],0,length($forward_rc_part[0])-$i-1); }
		for($i=0;$i<length($reverse_rc_part[0])-6;$i++) { $reverse_rc_part[$i+1] = substr($reverse_rc_part[0],0,length($reverse_rc_part[0])-$i-1); }
	}

	#2.5: Here, we use the function DefinePrimerRegex to define each regular expression for each partial primer
	#easily and fastly fo further steps.
	foreach $element (@forward_rc_part) { $element = PerlLib::Miscellaneous::DefinePrimerRegex($element); }
	foreach $element (@reverse_rc_part) { $element = PerlLib::Miscellaneous::DefinePrimerRegex($element); }
	
	#3.0: We finally return the two array references and the two primers
	return (\@forward_rc_part, \@reverse_rc_part, $forward, $reverse);
}
# End of the function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function defined to treat and print into output files sequences (fasta and/or quality) using some 
# input variables given by the main function. As input, this function takes 10 arguments (the sequence
# itself, the length of the two primers, the ID of the sequence, the boolean to know if the quality 
# file is given, the quality sequence, the sequence of the primer, a boolean to know if the sequence 
# needs a reverse complementation step, and two filhandles corresponding to the output files.
sub TreatPrintSeq
{
	#Needed input variables
	my $seq = $_[0];
	my $length_primer1 = $_[1];
	my $length_primer2 = $_[2];
	my $descr = $_[3];
	my $boolean_qual = $_[4];
	my $seq_qual = $_[5];
	my $primer_seq = $_[6];
	my $reverse = $_[7];
	my $F_OUT = $_[8];
	my $F_OUT_QUAL = $_[9];
	#Needed variables
	my $read_length = 0;
	my $clean_qual = "";
	
	#2.0: Error detected with the warning due to the absence of definition of some treated sequences,
	#so, we added another test step to define efficiently the $read_length
	if(defined($seq)) { $read_length = length($seq); }
	#Here, we did not found the length of the read, so we can not keep it
	else { $read_length = 0; }
	#2.5: If the read length is higher than the defined length as global, even after the deletion of the primer
	if ($read_length + $length_primer1 + $length_primer2 >= $length)
	{	
		#2.6: Test to avoid the analysis of the qual file if not found
		if ($boolean_qual == 1)
		{
			#We launch the function dedicated to the extraction of data from the quality file
			$clean_qual = &PreprocessQualSeq($seq_qual, $reverse, $primer_seq, $seq);
			#2.5: Here, we print in output files the new descriptive lines and the quality sequences
			printf($F_OUT_QUAL "$descr length=$read_length\n$clean_qual\n");
		}
		#2.5: Here, we print in output files the new descriptive lines and the kept sequence
		printf($F_OUT "$descr length=$read_length\n$seq\n");
		#2.5: We also count the total length to define the median length
		$tot_length += $read_length;
		#2.5: We count the number of reads kept after all filters
		$nb_reads_final++;
	}
}
# End of the function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#=======================================================================================================
#=======================================================================================================
# MAIN FUNCTION CALLED BY THE MAIN PROGRAM
#=======================================================================================================
#=======================================================================================================
# FILE:			Preprocessing.pm (called by the main program)
# USAGE:		PerlLib::Preprocessing::Preprocessing(ARGUMENTS)
# ARGUMENTS:	- $_[0]: the length threshold
#				- $_[1]: the number of tolerated ambiguities
#				- $_[2] and $_[3]: the forward and reverse primer sequences
#				- $_[4]: the input file
#				- $_[5] and $_[6]; the input and output folders,
#				- $_[7]: the defined "stringency",
#				- $_[8] and $_[9]: the number of mismatches tolerated by the user.
#
# DESCRIPTION:	This program was developed to preprocess raw sequences given by 454 sequencers. This 
#			  program takes sequences in FASTA format and search only for length, potential errors and
#			  primer sequences (with and/or without mismatches). First, this tool ask for chosen 
#			  parameters by the user. All sequences are then checked to find length, N and primers and 
#		 	  potentially reverse complemented if it's needed. The proximal primer can contain 
#			  mismatches (deletions, insertions and mutations). The distal primer is searched based on 
#		 	  his complete or partial sequence. Finally, all sequences are stored in a file using 
#		 	  the name of the file and adding the prefix 'preprocess_'.
#          	  A statistic file is also created to give information about these treatment to the user.
#
#		 	  It is important to note that this program does not search for TAGs, as files have been
#		 	  already demultiplexed.
#
#	 	 	  1.1 Modifications : As with several samples and during particular RUNs of pyrosequencing,
#		 	  we can found some reads that are shorter than the defined threshold after deletion of
#		 	  primers, we decided to add another step of length verification at the end of the program.
#		 	  See commentaries for more details.
#
#		 	  2.0 Modifs (11/04/12) : This program was modified to be included in the workflow to fast 
#			  the analysis. All modified steps are described in the program (e.g.: using the name of
#			  the input and a different location for the output file ans summary file, deletion of the 
#	  	 	  parallelization, as it was done by the main PERL program).
#
#		 	  2.1 Modifs (30/01/13): We corrected a small problem based on empty kept sequences after
#		 	  the research of the primers.
#
#			  2.5 Modifs (27/02/13): We rewrite all the program to fast it, to simplify it and also to
#			  add the analysis and treatment of the quality .qual file associated to the fasta file.
#			  Indeed, the quality file is also treated in parallel to potentially realize again a 
#			  quality treatment and also correct the single-singletons based on their alignment.
#
#			  2.6 Modifs (22/07/13): We modify the program to consider the possibility that the user 
#			  did not give the .qual file with .fna or .fasta file needed analysis. To consider this
#			  possibility, we realize some tests, and verify the existence of the .qual file.
#
#			  3.0 Modifs (16/04/14): We completely refactorized the code to simplify it and clearly
#			  reuse it. We also introduce some news functions to create proximal primers with mismatches
#			  as we need it to analyze some samples.
#
#			  3.1 Modifs (07/08/18): We modified the program to manage the fact that a sample can be
#			  empty due to the absence of reads with the good MID. To manage that, the file name will
#			  be empty and will be not treated anymore if the number of reads is equal to 0.
#=======================================================================================================
sub Preprocessing
{
	#Needed variables
	my ($i, $j) = 0;	
	my $read_length = 0;
	my $boolean_qual = 0;
	my $file_qual = "";
	my $seq_rc = "";
	my $element = "";
	my $element_mm = "";
	my $ref_array = "";
	my $ref_array2 = "";
	my @reads_fasta = ();
	my @reads_qual = ();
	my @tmp = ();
	my @descr = ();
	my @forward_rc_part = ();
	my @reverse_rc_part = ();
	my @forward_mm = ();
	my @reverse_mm = ();
	my @len_forward_mm = ();
	my @len_reverse_mm = ();

#=======================================================================================================
#2.0: First, catching for information chosen by the user (N numbers for example)
#=======================================================================================================

	#2.0: We store all given parameters by the main program
	$length = $_[0];
	my $N = $_[1] + 0;
	my $forward = $_[2];
	my $reverse = $_[3];
	my $file_fasta = $_[4];
	my $folder_input = $_[5];
	my $folder_output = $_[6];
	my $stringency = $_[7];
	my $lvl_mmforward = $_[8] + 0;
	my $lvl_mmreverse = $_[9] + 0;
	#2.5: We also store the length of the primers and the primers themselves as we will need them for 
	# the summary step
	my $forward_summary = $forward;
	my $reverse_summary = $reverse;
	
	#3.0: Print for the user information that the program is started
	PerlLib::Miscellaneous::PrintScreen("PREPROCESSING",$file_fasta ,"started");

#=======================================================================================================
#2.5: First step, we treat all primers using previously defined functions
#=======================================================================================================

	($ref_array, $ref_array2, $forward, $reverse) = &DefineAllPrimerSeq($forward, $reverse, $stringency);
	#3.0: We catch all needed data stored in the arrays given as output by the function
	@forward_rc_part = @{$ref_array};
	@reverse_rc_part = @{$ref_array2};
	
	#3.0: Part of the program to define mismatch primers based on user choices
	$ref_array = &DefineAllMMPrimers($forward_summary, $lvl_mmforward);
	@forward_mm = @{$ref_array};
	#3.0: We also store all length of primer with mismatches to efficiently treat quality files
	foreach $element_mm (@forward_mm)
	{
		push(@len_forward_mm, length($element_mm));
		$element_mm = PerlLib::Miscellaneous::DefinePrimerRegex($element_mm);
	}
	$ref_array = &DefineAllMMPrimers($reverse_summary, $lvl_mmreverse);
	@reverse_mm = @{$ref_array};
	foreach $element_mm (@reverse_mm)
	{
		push(@len_reverse_mm, length($element_mm));
		$element_mm = PerlLib::Miscellaneous::DefinePrimerRegex($element_mm);
	}
	
#=======================================================================================================
#2.5: Second step, we read and store all data for the .fasta and .qual files based on the information
# given by the main program
#=======================================================================================================

	#Here, we verify the chosen folder and file, and return all needed names and data for the given file.
	($file_fasta, $file_qual, $boolean_qual, *F_FASTA, *F_QUAL) = PerlLib::Miscellaneous::OpenFastaQualFile($folder_input, $file_fasta);

	#Here, we launch the function to catch and store data from an input file in Fasta format, and give back
	#all information stored in an array.
	$ref_array = PerlLib::Miscellaneous::ReadInputFastaFile(*F_FASTA, "");
	@reads_fasta = @{$ref_array};
	$nb_raw_reads = $#reads_fasta + 1;
	close F_FASTA;

	#2.6: Test to verify if the qual file is existed or not
	if ($boolean_qual == 1)
	{
		#Here, we launch the function to catch and store data from an input file in Fasta format, and give back
		#all information stored in an array.
		$ref_array = PerlLib::Miscellaneous::ReadInputFastaFile(*F_QUAL, "QUAL");
		@reads_qual = @{$ref_array};
		close F_QUAL;
	}

#=======================================================================================================
#2.5: Third step, we treat each read based on the filters (length, N, primer sequences, etc...) to keep
# only high-quality reads.
#=======================================================================================================

	#2.5: Create the new file with tag name added with ".fasta"
	if (uc($stringency) eq "HIGH") { open (F_OUT, ">Result_files/$folder_output/Hpreprocess_$file_fasta"); }
	elsif (uc($stringency) eq "MEDIUM") { open (F_OUT, ">Result_files/$folder_output/Mpreprocess_$file_fasta"); }
	else { open (F_OUT, ">Result_files/$folder_output/Lpreprocess_$file_fasta"); }

	#2.6: Test to verify if the qual file is existed or not, and create the needed result file
	if ($boolean_qual == 1)
	{
		if (uc($stringency) eq "HIGH") { open (F_OUT_QUAL, ">Result_files/$folder_output/Hpreprocess_$file_qual"); }
		elsif (uc($stringency) eq "MEDIUM") { open (F_OUT_QUAL, ">Result_files/$folder_output/Mpreprocess_$file_qual"); }
		else { open (F_OUT_QUAL, ">Result_files/$folder_output/Lpreprocess_$file_qual"); }
	}

	#For each read stored in the array @reads_fasta
	LOOP01:for($i=0;$i<=$#reads_fasta;$i++)
	{
		#Split and store the descriptive line and the sequence
		@tmp = split("\n", $reads_fasta[$i]);
		#2.5: We also split the descriptive line to keep only the ID of the read
		@descr = split(" ", $tmp[0]);
		#2.0: Error detected with the warning due to the absence of definition of some treated sequences,
		#so, we added another test step to define efficiently the $read_length
		if(defined($tmp[1])) { $read_length = length($tmp[1]); }
		#Here, we did not found the length of the read, so we can not keep it
		else {$read_length = 0;}
		#If the length of the read is higher thann the defined threshold given by the user
		if ($read_length >= $length)
		{
			#2.5: We count the number of reads kept after the length filter
			$nb_reads_length_filter++;
			#If the number of N is higher than the defined threshold
			if (($tmp[1] =~ /[N]/g) > $N) { next LOOP01; }
			#If the read can be kept based on the defined filter
			else
			{
				#2.5: We count the number of reads kept after the ambiguities filter
				$nb_reads_N_filter++;
				#3.0: Loop to search for mismatch primers in the proximal region of the sequence
				LOOP02:for($j=0;$j<=$#forward_mm;$j++)
				{
					#2.5: Regular expression to find sequences with the forward primer only to search then for the 
					#second primer partially and reverse complemented
					if ($tmp[1] =~/(^$forward_mm[$j])/)
					{
						#2.5: Used loop to treat each partial primer previously defined 
						LOOP03:foreach $element (@reverse_rc_part)
						{
							#2.5: New regular expression to find sequences with two primers (one at the end of the
							#sequence potentially partial. The "?" is used for a not greedy regular expression 
							#(to avoid some problems with the primer research, particularly with partial primers).
							if($tmp[1] =~/(^$forward_mm[$j])(.*?)($element)/)
							{
								#2.5: We count the number of reads kept after the primer research
								$nb_reads_primer_filter++;
								#3.0: We then launch the function to treat and print cleaned sequences 
								&TreatPrintSeq($2, $len_forward_mm[$j], length($element), $descr[0], $boolean_qual, $reads_qual[$i], $1, 0, *F_OUT, *F_OUT_QUAL);
								next LOOP01;
							}
							else
							{
								#2.5: If the $element analyzed is the last partial primer and we did not go in the previous loop (due
								#to the impossibility to find the last primer).
								if (($element eq $reverse_rc_part[$#reverse_rc_part])&&(uc($stringency) eq "LOW"))
								{
									#2.5: We verify again the presence of the primer at the beginning of the sequence
									if ($tmp[1] =~/(^$forward_mm[$j])(.*)/)
									{
										#2.5: We count the number of reads kept after the primer research
										$nb_reads_primer_filter++;
										#3.0: We then launch the function to treat and print cleaned sequences 
										&TreatPrintSeq($2, $len_forward_mm[$j], 0, $descr[0], $boolean_qual, $reads_qual[$i], $1, 0, *F_OUT, *F_OUT_QUAL);
										next LOOP01;
									}
								}
								else { next LOOP03; }
							}
						}
					}
					else { next LOOP02; }
				}
				#3.0: Loop to search for mismatch primers in the proximal region of the sequence
				LOOP02:for($j=0;$j<=$#reverse_mm;$j++)
				{
					#2.5: Regular expression to find sequences with the reverse primer only to search then for the 
					#second primer partially and reverse complemented
					if ($tmp[1] =~/(^$reverse_mm[$j])/)
					{
						#2.5: Used loop to treat each partial primer previously defined
						LOOP03:foreach $element (@forward_rc_part)
						{
							#2.5: New regular expression to find sequences with two primers (one at the end of the
							#sequence potentially partial. The "?" is used for a not greedy regular expression 
							#(to avoid some problems with the primer research, particularly with partial primers).
							if($tmp[1] =~/(^$reverse_mm[$j])(.*?)($element)/)
							{
								#2.5: We count the number of reads kept after the primer research
								$nb_reads_primer_filter++;
								#2.5: We reverse complement the kept sequence as the read start by the reverse primer
								$seq_rc = PerlLib::Miscellaneous::DefineRCSequence($2);
								#3.0: We then launch the function to treat and print cleaned sequences 
								&TreatPrintSeq($seq_rc, $len_reverse_mm[$j], length($element), $descr[0], $boolean_qual, $reads_qual[$i], $1, 1, *F_OUT, *F_OUT_QUAL);
								next LOOP01;
							}
							else
							{
								#2.5: If the $element analyzed is the last partial primer and we did not go in the previous loop (due
								#to the impossibility to find the last primer).
								if (($element eq $forward_rc_part[$#forward_rc_part])&&(uc($stringency) eq "LOW"))
								{
									#2.5: We verify again the presence of the primer at the beginning of the sequence
									if ($tmp[1] =~/(^$reverse_mm[$j])(.*)/)
									{
										#2.5: We count the number of reads kept after the primer research
										$nb_reads_primer_filter++;
										#2.5: We reverse complement the kept sequence as the read start by the reverse primer
										$seq_rc = PerlLib::Miscellaneous::DefineRCSequence($2);
										#3.0: We then launch the function to treat and print cleaned sequences 
										&TreatPrintSeq($seq_rc, $len_reverse_mm[$j], 0, $descr[0], $boolean_qual, $reads_qual[$i], $1, 1, *F_OUT, *F_OUT_QUAL);
										next LOOP01;
									}
								}
								else { next LOOP03; }
							}
						}
					}
					else { next LOOP02; }
				}
				#Or the read is not restricted by the primers, wa can't keep it
			}
		}
		else { next LOOP01; }
	}
	#2.5: We close the two output files after their utilization
	close F_OUT;
	#2.6: Test to avoid the closing of an unexisted file
	if ($boolean_qual == 1) { close F_OUT_QUAL; }
	#(1.1) Determination of the median length of reads
	if ($nb_reads_final > 0) { $tot_length = sprintf("%.0f", $tot_length/$nb_reads_final); }
	else { $tot_length = sprintf("%.0f", 0); }
	#2.0: Creation of a summary file with all data (more efficient than one result file for each treated file).
	open(RESULTS, ">>Summary_files/Preprocessing_L".$length."_N".$N."_F_".$forward_summary."_R_".$reverse_summary.".txt")|| die "==> File Not Found: Summary file of the preprocessing step <==";
	flock (RESULTS, 2);
	printf (RESULTS "$file_fasta\t$nb_raw_reads\t$nb_reads_length_filter\t$nb_reads_N_filter\t$nb_reads_primer_filter\t$nb_reads_final\t$tot_length\n");
	flock (RESULTS, 8);
	close(RESULTS);
	
	#3.0: Print for the user information that the program is started
	PerlLib::Miscellaneous::PrintScreen("PREPROCESSING",$file_fasta ,"terminated");
	
	#3.1: Check if the number of reads for the sample is higher to 0 or not
	#If the file is empty, the file name will be empty, to inform the main program 
	#that it will be not treated anymore.
	if($nb_reads_final == 0) { return(""); }
	
	#2.0: We give to the main program the name of the created file
	if(uc($stringency) eq "HIGH") { return ("Hpreprocess_$file_fasta"); }
	elsif (uc($stringency) eq "MEDIUM") { return ("Mpreprocess_$file_fasta"); }
	else { return ("Lpreprocess_$file_fasta"); }
}
# End of the function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

1;
