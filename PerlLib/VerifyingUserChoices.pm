#=======================================================================================================
# PACKAGE NAME: VerifyingUserChoices
#    FUNCTIONS:	- InfoForUser (Give back information to the user),
#			- AlignmentCheck (Verify if the alignment is done for clustering steps),
#			- ClusteringCheck (Verify the coherency of all parameters for the clustering),
#			- CheckRawDataCoherency (Check the coherency of steps and files given by the user),
#			- CheckNamesUnicity (Verify the names and MIDs of all samples),
#			- CheckHomogenizationChoice (Check the homogenization step),
#			- CheckClusteringCoherency (Verify if the alignment is done for clustering steps),
#			- CheckHuntingCoherency (Verify if essential steps are done for the hunting),
#			- CheckRecoveringCoherency (Verify if essential steps are done for the recovering),
#			- CheckComputationAnalysisCoherency (Verify if essential steps are chosen),
#			- CheckGlobalAnalysisCoherency (Verify if essential steps are done for the analysis),
#			- CheckUniFracAnalysisCoherency (Verify if essential steps are done for the analysis),
#			- CheckPrinSeqFlashCoherency (Verify coherency of parameters and files for trim and merge),
#			- CheckReClustORCoherency (verify coherency of paramters and files for ReClustOR),
#			- VerifyingUserChoices (main function lauching and testing all choices and steps).
#
#	   DETAILS:  1.1 (09/09/14): We modified the functions "VerifyingUserChoices" and "CheckRawDataCoherency"
#			 to integrate the use of fastq files from Illumina sequencing systems. Done the 09/09/2014.
#
#			 1.2 (02/05/18): Little modification of the function "CheckGlobalAnalysisCoherency" to
#			 efficiently test the realization of the TAXONOMY step.
#
#			 1.3 (17/05/18): Definition of a new function "CheckPrinSeqFlashCoherency" to test
#			 for PRINSEQ and FLASH steps addition to the pipeline. Some modifications have also been
#			 done in the main function to call this new function
#
#			 1.4 (08/08/18): Modification of CheckRecoveringCoherency, as the RECOVERING step was
#			 modified with the addition of the database version choice.
#
#			 1.5 (14/10/19): Addition of the CheckReClustORCoherency, as the step of ReClustOR is
#			 now integrated in BIOCOM-PIPE and fully functional. To avoid some errors, this step
#			 is carefully checked, and compared to previous and essential steps.
#
# REQUIREMENTS:  Perl language, and Math library to compute binary files
#       AUTHOR:  Sebastien TERRAT <sebastien.terrat@inra.fr>
#      COMPANY:  UMR 1347 Agroecologie - BIOCOM team
#      VERSION:  1.5
#    REALEASED:  17/04/2014
#     REVISION:  14/10/2019
#=======================================================================================================

# Name of the package defined for the main program
package PerlLib::VerifyingUserChoices;

# Needed libraries
use strict;
use warnings;

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function to print the information for the user as all tests have been passed for the evaluated step.
# It takes the name of the evaluated step as input.
sub InfoForUser
{
	return "\n#############################################################\nChosen names for the $_[0] step have been checked,
and all validation steps have been passed.\n#############################################################\n";
}
# End of the function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function to check if the global alignment step (RAW or CLEAN) is realized, as this step is essential
# for many steps in BIOCOM-PIPE: like the clustering or the computation ones. It takes as input the name of
# alignment needed to be verified, and the step depending of the alignment.
sub AlignmentCheck
{
	# Needed variables
	my $step = $_[0];
	my $verified_step = $_[1];
	my $ref_array = "";
	my @parameters = ();

	# We catch the alignment parameters chosen by the user
	$ref_array = PerlLib::Miscellaneous::SearchInputParameters($_[0]);
	@parameters = @$ref_array;
	# If the step is not realized
	if(uc($parameters[0]) eq "NO")
	{
		die "==> The BIOCOM-PIPE was stopped due to a coherency error. You take the decision to launch the
$verified_step step, however the $step was not realized. So, the analysis
can't be realized. Please, correct this error in the Input.txt file and relaunch BIOCOM-PIPE.\n";
	}
}
# End of the function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function to check if the clustering step (RAW or CLEAN) is realized, and if the chosen dissimilarity
# percentage is computed based on user parameters. It takes as input the name of the alignment needed to
# be verified, the percentage chosen for the hunting step and the step name depending of the clsutering.
sub ClusteringCheck
{
	# Needed variables
	my $step = $_[0];
	my $chosen_percentage = $_[1];
	my $verified_step = $_[2];
	my $ref_array = "";
	my @parameters = ();
	my @clust_steps = ();
	my $clust_verif = 0;

	# We catch the clustering parameters
	$ref_array = PerlLib::Miscellaneous::SearchInputParameters($step);
	@parameters = @$ref_array;
	# If the step is not realized
	if(uc($parameters[0]) eq "NO")
	{
		die "==> The BIOCOM-PIPE was stopped due to a coherency error. You take the decision
to launch the $verified_step step, however the $step was not
realized. So, the global analysis can't be realized.  Please, correct this
error in the Input.txt file and relaunch BIOCOM-PIPE.\n\n";
	}
	# If the step is chosen by the user
	else
	{
		# We verify the existence of the same percentage chosen for the clustering and the hunting
		$ref_array = PerlLib::Miscellaneous::ComputeClusteringSteps($parameters[3], $parameters[4]);
		@clust_steps = @$ref_array;
		foreach (@clust_steps) { if ($chosen_percentage == $_) { $clust_verif = 1; } }
		if ($clust_verif == 0)
		{
			die "==> The BIOCOM-PIPE was stopped due to a coherency error. You take the decision to
launch the $verified_step step, however the chosen percentage of dissimilarity
in the $verified_step step ($chosen_percentage%) will not be computed based on $step
chosen parameters. So, the global analysis can't be realized.
Please, correct this error in the Input.txt file and relaunch BIOCOM-PIPE.\n\n";
		}
	}
}
# End of the function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function to check if the global analysis step is realized, as this step is essential
# for the ReClustOR in BIOCOM-PIPE. It takes as input the name of
# alignment needed to be verified, and the step depending of the alignment.
sub GlobalAnalysisCheck
{
	# Needed variables
	my $step = $_[0];
	my $verified_step = $_[1];
	my $ref_array = "";
	my @parameters = ();

	# We catch the alignment parameters chosen by the user
	$ref_array = PerlLib::Miscellaneous::SearchInputParameters($_[0]);
	@parameters = @$ref_array;
	# If the step is not realized
	if(uc($parameters[0]) eq "NO")
	{
		die "==> The BIOCOM-PIPE was stopped due to a coherency error. You take the decision to launch the
$verified_step step to define a new database or use an existing one, however the $step
was not realized. So, the analysis can't be realized. Please, correct this error in the Input.txt
file and relaunch BIOCOM-PIPE.\n";
	}
}
# End of the function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function developed to verify the coherency between the files stored in the Raw_data folder, and the
# steps chosen by the user, as it can be impossible to realize some steps (like the #QUALITY_CLEANING one
# if the .qual files are not stored in the Raw_data folder). It takes as input four arguments (the list
# of .sff files, the list of .qual files, the list of .fna/.fasta files and the list of .fastq files.
sub CheckRawDataCoherency
{
	# Needed variables
	my $ref_array = "";
	my $step = "";
	my @parameters = ();
	my @files_sff = @{$_[0]};
	my @files_qual = @{$_[1]};
	my @files_fna = @{$_[2]};
	my @files_fastq = @{$_[3]};
	my @steps_fastq = ("PRINSEQ", "FLASH");
	my @steps_qual = ("EVAL_QUAL", "QUALITY_CLEANING");
	my @steps_fna = ("PREPROCESS_MIDS", "RANDOM_PREPROCESS_MIDS", "PREPROCESSING", "RANDOM_PREPROCESS",
"RAW_INFERNAL_ALIGNMENT", "FILTERING_RAW_ALIGNMENT", "RAW_CLUSTERING", "HUNTING", "RECOVERING", "RANDOM_CLEANED",
"TAXONOMY", "CLEAN_INFERNAL_ALIGNMENT", "CLEAN_CLUSTERING", "COMPUTATION", "GLOBAL_ANALYSIS",
"RECLUSTOR", "UNIFRAC_ANALYSIS");

	# First, loop to test each step where a .fastq file can be implied and used
	foreach $step (@steps_fastq)
	{
		$ref_array = PerlLib::Miscellaneous::SearchInputParameters($step);
		@parameters = @$ref_array;
		if(uc($parameters[0]) eq "YES")
		{
			if($#files_fastq == -1)
			{
				die "==> The BIOCOM-PIPE was stopped due to a coherency error. You take the decision to
realize the $step step, but you did not put in the Raw_data folder a .fastq file. Please
correct this error (in the Input.txt file or in the Raw_data folder) and relaunch BIOCOM-PIPE.\n";
			}
		}
	}
	# Then, as if we have .sff or .fastq files, we can do every other steps
	if($#files_sff > -1) { return "OK"; }
	elsif($#files_fastq > -1) { return "OK"; }

	# Then, loop to test each step where a quality file can be implied and used
	foreach $step (@steps_qual)
	{
		$ref_array = PerlLib::Miscellaneous::SearchInputParameters($step);
		@parameters = @$ref_array;
		if(uc($parameters[0]) eq "YES")
		{
			if($#files_qual == -1)
			{
				die "==> The BIOCOM-PIPE was stopped due to a coherency error. You take the decision to
realize the $step step, but you did not put in the Raw_data folder a .qual file. Please
correct this error (in the Input.txt file or in the Raw_data folder) and relaunch BIOCOM-PIPE.\n";
			}
		}
	}
	# Loop to test each step where a .fasta and/or a .fna file can be implied and used
	foreach $step (@steps_fna)
	{
		$ref_array = PerlLib::Miscellaneous::SearchInputParameters($step);
		@parameters = @$ref_array;
		if(uc($parameters[0]) eq "YES")
		{
			if($#files_fna == -1)
			{
				die "==> The BIOCOM-PIPE was stopped due to a coherency error. You take the decision to
realize the $step step, but you did not put in the Raw_data folder a .fna file. Please
correct this error (in the Input.txt file or in the Raw_data folder) and relaunch BIOCOM-PIPE.\n";
			}
		}
	}


	return "\n#############################################################\nChosen steps coherency have been checked using the given raw
files, and all validation steps have been passed.\n#############################################################\n\n";
}
# End of the function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function developed to verify if the names given by the user for samples, but also MIDs and raw files
# are correct. It uses the function &SearchInputParameters to catch needed information and check the
# choices of the user.
sub CheckNamesUnicity
{
	# Needed variables
	my $ref_array = "";
	my $filename = "";
	my $i = 0;
	my @parameters = ();
	my @tmp = ();
	my %filenames = ();
	my %samples = ();
	my %MIDs = ();

	# First, we catch all parameters for the PREPROCESS_MIDS step
	$ref_array = &PerlLib::Miscellaneous::SearchInputParameters("PREPROCESS_MIDS");
	@parameters = @$ref_array;
	# We avoid the analysis if the step is not used by the user
	if(uc($parameters[0]) eq "NO") { return "OK"; }

	# We first catch the names of the filenames
	LOOP01:for($i=1;$i<=$#parameters;$i++)
	{
		@tmp = split("/", $parameters[$i]);
		$filenames{$tmp[0]}++;
	}
	# We first create %samples and %MIDs to check their unicity
	LOOP02:foreach $filename (keys(%filenames))
	{
		# For each line found
		LOOP03:for($i=1;$i<=$#parameters;$i++)
		{
			if($parameters[$i] =~ m/$filename/)
			{
				# We split the line to separate the filename, the MID(s) and the sample name
				@tmp = split("/", $parameters[$i]);
				# We store needed information in dedicated hash tables, depending on the
				# list of MIDs given by the user (only one, or one for the forward and one
				#for the reverse part)
				if($#tmp == 2) { $MIDs{$tmp[1]}++; }
				elsif($#tmp == 3) { $MIDs{$tmp[1]."_".$tmp[2]}++; }
				else
				{
					die "==> The BIOCOM-PIPE was stopped due to an unicity error. Your line named:
$parameters[$i] is incorrect. It contains
a wrong number of elements based on the analysis (higher than 4 or lower than 2).
Please correct this error in the Input.txt file and relaunch BIOCOM-PIPE\n";
				}
				$samples{$tmp[$#tmp]}++;
			}
		}
		# We verify the unicity of %samples
		LOOP04:foreach (keys(%samples))
		{
			if ($samples{$_} > 1)
			{
				die "==> The BIOCOM-PIPE was stopped due to an unicity error. Your sample named: $_
was present at least two times for the same chosen filename: $filename".".
Please correct this error in the Input.txt file and relaunch BIOCOM-PIPE\n";
			}
		}
		# We verify the unicity of %MIDs
		LOOP05:foreach (keys(%MIDs))
		{
			if ($MIDs{$_} > 1)
			{
				die "==> The BIOCOM-PIPE was stopped due to an unicity error. Your MID named: $_
was present at least two times for the same chosen filename: $filename".".
Please correct this error in the Input.txt file and relaunch BIOCOM-PIPE.\n";
			}
		}
		# We emptied the hash tables to analyze a second potential filename
		%MIDs = ();
		%samples = ();
	}
	# We finally send back information to the user
	&InfoForUser("PREPROCESS_MIDS");
}
# End of the function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function needed to determine efficiently where the concatenation of files will be realized to
# to the global analysis step.
sub CheckHomogenizationChoice
{
	# Needed variables
	my $ref_array = "";
	my $step = "";
	my $verif_rdm = 0;
	my @parameters = ();
	my @potential_steps = ("RANDOM_PREPROCESS_MIDS", "RANDOM_PREPROCESS", "RANDOM_CLEANED");

	# Loop to test the user choices for all potential steps of homogenization
	LOOP01:foreach $step (@potential_steps)
	{
		$ref_array = PerlLib::Miscellaneous::SearchInputParameters($step);
		@parameters = @$ref_array;
		if (uc($parameters[0]) eq "YES") { $verif_rdm++; }
	}
	# We indicate to the user that his selction is wrong, before stopping the complete program
	if ($verif_rdm > 1)
	{
		die "==>You can't realize several random selection during the analysis,\nas you will bias your data !!
		You must modify your choices in the Input.txt file and try again.\n";
	}
	else { return $verif_rdm; }
}
# End of the function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function checking if the RAW_INFERNAL_ALIGNMENT step is realized, as this step is essential for
# many steps in BIOCOM-PIPE: like the FILTERING_RAW_ALIGNMENT. It takes as input the name of the
# checked step.
sub CheckFilteringAlignmentCoherency
{
	# Needed variables
	my $filtering_align_step = $_[0];
	my $ref_array = "";
	my @parameters = ();

	# First, we verify if the user chose to do a filtering step
	$ref_array = PerlLib::Miscellaneous::SearchInputParameters($filtering_align_step);
	@parameters = @$ref_array;
	if (uc($parameters[0]) eq "NO") { return "NO"; }

	# We also check if the RAW_INFERNAL_ALIGNMENT is done, as it is essential for the filtering step
	&AlignmentCheck("RAW_INFERNAL_ALIGNMENT", $filtering_align_step);
	#Finally, we send back information to the user
	&InfoForUser($filtering_align_step);
}
# End of the function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function checking if the clustering step (RAW or CLEAN) is realized, as this step is essential for
# many steps in BIOCOM-PIPE: like the clustering or the computation ones. It takes as input the name of the
# checked clustering  step.
sub CheckClusteringCoherency
{
	# Needed variables
	my $clust_step = $_[0];
	my $ref_array = "";
	my @parameters = ();

	# First, we verify if the user chose to do a clustering step
	$ref_array = PerlLib::Miscellaneous::SearchInputParameters($clust_step);
	@parameters = @$ref_array;
	if (uc($parameters[0]) eq "NO") { return "NO"; }

	# We also check if the alignment is done, as it is essential for the clustering
	if($clust_step eq "RAW_CLUSTERING") { &AlignmentCheck("RAW_INFERNAL_ALIGNMENT", $clust_step); }
	elsif($clust_step eq "CLEAN_CLUSTERING") { &AlignmentCheck("CLEAN_INFERNAL_ALIGNMENT", $clust_step); }
	#Finally, we send back information to the user
	&InfoForUser($clust_step);
}
# End of the function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function to check if the hunting step is realized with all needed previous steps, as this step needs
# a clustering and an alignment step.
sub CheckHuntingCoherency
{
	# Needed variables
	my $ref_array = "";
	my @parameters = ();

	# First, we verify if the user chose to do a hunting step
	$ref_array = PerlLib::Miscellaneous::SearchInputParameters("HUNTING");
	@parameters = @$ref_array;
	if (uc($parameters[0]) eq "NO") { return "NO"; }

	# We check for all essential steps for the hunting.
	&AlignmentCheck("RAW_INFERNAL_ALIGNMENT", "HUNTING");
	&ClusteringCheck("RAW_CLUSTERING", $parameters[1], "HUNTING");
	&InfoForUser("HUNTING");
}
# End of the function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function to check if the recovering step is realized with all needed previous steps, as the RECOVERING
# step needs a clustering, an alignment step, but also the hunting step.
sub CheckRecoveringCoherency
{
	# Needed variables
	my $ref_array = "";
	my @parameters = ();
	my @parameters_recovering = ();

	# First, we verify if the user chose to do a recovering step
	$ref_array = PerlLib::Miscellaneous::SearchInputParameters("RECOVERING");
	@parameters_recovering = @$ref_array;
	if (uc($parameters_recovering[0]) eq "NO") { return "NO"; }
	# Then, we verify if the user chose a good organism (bacteria or fungi)
	if ((uc($parameters_recovering[1]) ne "BACTERIA")&&(uc($parameters_recovering[1]) ne "FUNGI")&&(uc($parameters_recovering[1]) ne "ALGAE"))
	{
		die "\n==> The BIOCOM-PIPE was stopped due to a coherency error. You take the decision to launch the
RECOVERING step, however the choice of the organism ($parameters_recovering[1]) was not coherent. You must
chose between 'bacteria' or 'fungi' So, the analysis can't be realized. Please, correct
this error in the Input.txt file and relaunch BIOCOM-PIPE.\n\n";
	}
	# Then, we verify if the user chose a good database (RDP or Silva)
	if ((uc($parameters_recovering[2]) ne "RDP")&&(uc($parameters_recovering[2]) ne "SILVA"))
	{
		die "\n==> The BIOCOM-PIPE was stopped due to a coherency error. You take the decision to launch the
RECOVERING step, however the choice of the database ($parameters_recovering[2]) was not coherent. You must
chose between 'RDP' or 'Silva' So, the analysis can't be realized. Please, correct
this error in the Input.txt file and relaunch BIOCOM-PIPE.\n\n";
	}
	# Finally, we check if the user chose an available taxonomic level (domain, phylum, class, order,
	# family or genus)
	# 1.4: modification of the position into the array to test the good parameter
	if ((uc($parameters_recovering[4]) ne "DOMAIN")&&(uc($parameters_recovering[4]) ne "PHYLUM")
	&&(uc($parameters_recovering[4]) ne "CLASS")&&(uc($parameters_recovering[4]) ne "ORDER")
	&&(uc($parameters_recovering[4]) ne "FAMILY")&&(uc($parameters_recovering[4]) ne "GENUS"))
	{
		die "\n==> The BIOCOM-PIPE was stopped due to a coherency error. You take the decision to launch the
RECOVERING step, however the choice of the taxonomic level ($parameters_recovering[3]) was not coherent.
You must chose between [domain, phylum, class, order, family or genus]. So, the analysis
can't be realized. Please, correct this error in the Input.txt file and relaunch BIOCOM-PIPE.\n\n";
	}

	# Then, we verify the coherency between the chosen steps of the user
	&AlignmentCheck("RAW_INFERNAL_ALIGNMENT", "RECOVERING");
	$ref_array = PerlLib::Miscellaneous::SearchInputParameters("RAW_INFERNAL_ALIGNMENT");
	@parameters = @$ref_array;
	if(uc($parameters_recovering[1]) ne uc($parameters[1]))
	{
		die "\n==> The BIOCOM-PIPE was stopped due to a coherency error. You take the decision to launch the
RECOVERING step, however the model of the RAW_INFERNAL_ALIGNMENT ($parameters[1]) was not coherent.
So, the analysis can't be realized. Please, correct this error in the Input.txt file
and relaunch BIOCOM-PIPE.\n\n";
	}

	# Finally, we check if the hunting step is done
	$ref_array = PerlLib::Miscellaneous::SearchInputParameters("HUNTING");
	@parameters = @$ref_array;
	if (uc($parameters[0]) eq "NO")
	{
		die "\n==> The BIOCOM-PIPE was stopped due to a coherency error. You take the decision to
launch the RECOVERING step, however the HUNTING was not realized. So, the analysis
can't be realized. Please, correct this error in the Input.txt file and relaunch BIOCOM-PIPE.\n\n";
	}
	# We give back information to the user
	&InfoForUser("RECOVERING");
}
# End of the function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function developed to verify if parameters chosen by the user were coherent: more particularly, if the
# clustering level chosen for the computation step was used during the previous clustering step.
sub CheckComputationAnalysisCoherency
{
	# Needed variables
	my $ref_array = "";
	my @parameters = ();

	# We catch the computation parameters
	$ref_array = PerlLib::Miscellaneous::SearchInputParameters("COMPUTATION");
	@parameters = @$ref_array;

	# We verify that the user chose to do the computation step, or we leave the verification step
	if(uc($parameters[0]) eq "NO") { return "NO"; }

	# We then verify using the ClusteringCheck function if the clusetring is realized and if
	# parameters are correct.
	if((uc($parameters[1]) eq "RAW")||(uc($parameters[1]) eq "BOTH"))
	{
		&AlignmentCheck("RAW_INFERNAL_ALIGNMENT", "COMPUTATION");
		&ClusteringCheck("RAW_CLUSTERING", $parameters[2], "COMPUTATION");
	}
	if((uc($parameters[1]) eq "CLEAN")||(uc($parameters[1]) eq "BOTH"))
	{
		&AlignmentCheck("CLEAN_INFERNAL_ALIGNMENT", "COMPUTATION");
		&ClusteringCheck("CLEAN_CLUSTERING", $parameters[2], "COMPUTATION");
	}
	# We give back information to the user
	&InfoForUser("COMPUTATION");
}
# End of the function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function developed to verify the coherency of all parameters chosen by the user for the global
# analysis step, like if the global alignment or the taxonomy steps are done if needed.
sub CheckGlobalAnalysisCoherency
{
	# Needed variables
	my $ref_array = "";
	my @parameters = ();
	my @parameters_glob = ();

	# We first catch and store chosen parameters fro the global analysis step
	$ref_array = PerlLib::Miscellaneous::SearchInputParameters("GLOBAL_ANALYSIS");
	@parameters_glob = @$ref_array;
	# We verify that the user chose to do the global analysis step, or we leave the verification step
	if(uc($parameters_glob[0]) eq "NO") { return "NO"; }

	# We first verify if the alignment was done (RAW).
	if((uc($parameters_glob[1]) eq "RAW")||(uc($parameters_glob[1]) eq "BOTH"))
	{
		&AlignmentCheck("RAW_INFERNAL_ALIGNMENT", "GLOBAL_ANALYSIS");
		&ClusteringCheck("RAW_CLUSTERING", $parameters_glob[2], "GLOBAL_ANALYSIS");
	}
	# We also verify if the alignment was done (CLEAN).
	if((uc($parameters_glob[1]) eq "CLEAN")||(uc($parameters_glob[1]) eq "BOTH"))
	{
		&AlignmentCheck("CLEAN_INFERNAL_ALIGNMENT", "GLOBAL_ANALYSIS");
		&ClusteringCheck("CLEAN_CLUSTERING", $parameters_glob[2], "GLOBAL_ANALYSIS");
	}

	# We finally verify if the taxonomy was done as the user can need it in the global analysis step.
	# Correction (v1.2) => $parameters_glob[4] to $parameters_glob[3]
	if(uc($parameters_glob[3]) eq "YES")
	{
		# We catch the taxonomy parameters
		$ref_array = PerlLib::Miscellaneous::SearchInputParameters("TAXONOMY");
		@parameters = @$ref_array;
		if(uc($parameters[0]) eq "NO")
		{
			die "==> The BIOCOM-PIPE was stopped due to a coherency error. You take the decision to launch the
GLOBAL_ANALYSIS step, however the TAXONOMY was not realized. As you chose to assign the
OTUs of the global analysis, you need the TAXONOMY step. Please, correct this error in
the Input.txt file and relaunch BIOCOM-PIPE.\n\n";
		}
	}
	# We give back information to the user
	&InfoForUser("GLOBAL_ANALYSIS");
}
# End of the function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function developed to check the chosen parameters by the user to create/use/enrich a database of
# OTUs with treated data. For example, if the user chose to define a new database, the program checked
# if the database name already exists or not, and if the dataset was homogenized.
sub CheckReClustORCoherency
{
	# Needed variables
	my $ref_array = "";
	my @parameters_ReCLUSTOR = ();
	my @parameters_Align = ();
	my @parameters_Clust = ();
	my @databases = ();
	my $element = "";
	my $model = "";
	my $potential_db = "";
	my $clustering = 0;
	my $align = 0;
	my $check_clust = 0;
	my $check_name = 0;

	# We first catch and store chosen parameters from the ReCLUSTOR step
	$ref_array = PerlLib::Miscellaneous::SearchInputParameters("RECLUSTOR");
	@parameters_ReCLUSTOR = @{$ref_array};
	# We verify that the user chose to do the ReCLUSTOR analysis step, or we leave the verification step
	if(uc($parameters_ReCLUSTOR[0]) eq "NO") { return "NO"; }
	# Then, we check if the user choices are coherent (new AND existing database impossible here)
	if(uc($parameters_ReCLUSTOR[1]) eq uc($parameters_ReCLUSTOR[2]))
	{
		die "\n===> The BIOCOM-PIPE was stopped due to a coherency error. You take the decision to launch the
ReClustOR step, however same choices ($parameters_ReCLUSTOR[1] and $parameters_ReCLUSTOR[2]) have been defined both to define a new database AND
to use an existing one. Please, correct this error in the Input.txt file and relaunch BIOCOM-PIPE.\n\n";
	}
	#Then, we check for the given name of the database (without special characters)
	if($parameters_ReCLUSTOR[4] =~ m/[^A-Za-z0-9\_\-]+/)
	{
		die "\n===> The BIOCOM-PIPE was stopped due to a coherency error. You take the decision to launch the
ReClustOR step, however the chosen database name contains not wanted characters. Remember that
you can use only alphabetical characters (A-Za-z), numeric characters (0-9) and two special
characters (- and _). Please, correct this error in the Input.txt file and relaunch BIOCOM-PIPE.\n\n";
	}
	# Then, we check the chosen organism for the global alignment, whatever the type of database
	# If an error exists, the ModelChoice will stop the BIOCOM-PIPE pipeline
	$model = PerlLib::Miscellaneous::ModelChoice($parameters_ReCLUSTOR[5]);
	#We also checked if there is coherency between the ALIGN step and the ReClustOR step when
	#a database is created.
	$ref_array = PerlLib::Miscellaneous::SearchInputParameters("RAW_INFERNAL_ALIGNMENT");
	@parameters_Align = @{$ref_array};
	if(uc($parameters_Align[0]) eq "YES") { if(uc($parameters_Align[1]) eq uc($parameters_ReCLUSTOR[5])) { $align = 1; } }
	$ref_array = PerlLib::Miscellaneous::SearchInputParameters("CLEAN_INFERNAL_ALIGNMENT");
	@parameters_Align = @{$ref_array};
	if(uc($parameters_Align[0]) eq "YES") { if(uc($parameters_Align[1]) eq uc($parameters_ReCLUSTOR[5])) { $align = 1; } }
	if($align == 0)
	{
		die "\n===> The BIOCOM-PIPE was stopped due to a coherency error. You take the decision to launch the
ReClustOR step, however some choices (model used for ReClustOR $parameters_ReCLUSTOR[5]) is different from
the parameter defined by the INFERNAL_ALIGNMENT step. As you chose to define a new database,
these two parameters must be identical.
Please, correct this error in the Input.txt file and relaunch BIOCOM-PIPE.\n\n";
	}

	# Then, we checked also for the chosen percentage of dissimilarity
	$clustering = 100 - $parameters_ReCLUSTOR[6];
	#If it is out of range, we stop the pipeline
	if(($clustering > 100)||($clustering < 1))
	{
		die "\n===> The BIOCOM-PIPE was stopped due to a coherency error. You take the decision to launch the
ReClustOR step, however the chosen percentage of dissimilarity ($parameters_ReCLUSTOR[6]) is out of range [1-100]. Please, correct
this error in the Input.txt file and relaunch BIOCOM-PIPE.<===\n\n";
	}

	# Then, we check the parameters defined by the user for the global alignment, and therefore the clustering step
	#Indeed, we checked if there is coherency between the CLUSTERING step and the ReClustOR step when
	#a database is created.
	$ref_array = PerlLib::Miscellaneous::SearchInputParameters("RAW_CLUSTERING");
	@parameters_Clust = @{$ref_array};
	if(uc($parameters_Clust[0]) eq "YES")
	{
		if(uc($parameters_Clust[1]) ne uc($parameters_ReCLUSTOR[7]))
		{
			die "\n===> The BIOCOM-PIPE was stopped due to a coherency error. You take the decision to launch the
	ReClustOR step, however the parameter chosen to ignore homopolymer differences ($parameters_ReCLUSTOR[7])
	is different from the parameter chosen for the RAW_CLUSTERING step (not available for database definition).
	Please, correct	this error in the Input.txt file and relaunch BIOCOM-PIPE.<===\n\n";
		}
		if(uc($parameters_Clust[2]) ne uc($parameters_ReCLUSTOR[8]))
		{
			die "\n===> The BIOCOM-PIPE was stopped due to a coherency error. You take the decision to launch the
	ReClustOR step, however the parameter chosen to ignnore distances at the beginning of reads ($parameters_ReCLUSTOR[7])
	is different from the parameter chosen for the RAW_CLUSTERING step (not available for database definition).
	Please, correct	this error in the Input.txt file and relaunch BIOCOM-PIPE.<===\n\n";
		}
	}

	$ref_array = PerlLib::Miscellaneous::SearchInputParameters("CLEAN_CLUSTERING");
	@parameters_Clust = @{$ref_array};
	if(uc($parameters_Clust[0]) eq "YES")
	{
		if(uc($parameters_Clust[1]) ne uc($parameters_ReCLUSTOR[7]))
		{
			die "\n===> The BIOCOM-PIPE was stopped due to a coherency error. You take the decision to launch the
ReClustOR step, however the parameter chosen to ignore homopolymer differences ($parameters_ReCLUSTOR[7])
is different from the parameter chosen for the CLEAN_CLUSTERING step (not available for database definition).
Please, correct	this error in the Input.txt file and relaunch BIOCOM-PIPE.<===\n\n";
		}
		if(uc($parameters_Clust[2]) ne uc($parameters_ReCLUSTOR[8]))
		{
			die "\n===> The BIOCOM-PIPE was stopped due to a coherency error. You take the decision to launch the
ReClustOR step, however the parameter chosen to ignnore distances at the beginning of reads ($parameters_ReCLUSTOR[7])
is different from the parameter chosen for the CLEAN_CLUSTERING step (not available for database definition).
Please, correct	this error in the Input.txt file and relaunch BIOCOM-PIPE.<===\n\n";
		}
	}

	#Definition of the potential folder name
	$potential_db = $parameters_ReCLUSTOR[4]."_".uc($parameters_ReCLUSTOR[5])."_".$clustering;
	#Open the directory containing all databases defined and stored by RECLUSTOR
	opendir(DIR, "$FindBin::RealBin/Data/Databases/ReClustOR/") || die "can't opendir ReClustOR: $!";
	#For each element found in the directory ReClustOR/
	while(defined($element = readdir(DIR)))
	{
		#If the element is not a folder, we not keep the information
		if(-d $element) { next; }
		#If it is a folder, we store the name to compare it after
		else { push(@databases, $element); }
	}
	#We then close the directory
	closedir(DIR);
	#We read and check all folders found
	foreach $element (@databases)
	{
		# If the user chose to define a new database, the first step is to check if the given name was not used
		# already used for another existing database
		if(uc($parameters_ReCLUSTOR[1]) eq "YES")
		{
			if($element eq $potential_db)
			{
				die "\n===> The BIOCOM-PIPE was stopped due to a coherency error. You take the decision to launch the
ReClustOR step, however the defined folder name to store the new database (here, $potential_db)
already exists. So, correct this error in the Input.txt file and relaunch BIOCOM-PIPE.<===\n\n";
			}
		}
		# Here, we check if the given database exists or not, as the user wants to be compared
		# with an existing database
		if(uc($parameters_ReCLUSTOR[2]) eq "YES") { if($element eq $potential_db) { $check_name++; } }
	}
	#Message sent to the user if the chosen database is not available
	if((uc($parameters_ReCLUSTOR[2]) eq "YES")&&($check_name == 0))
	{
		die "\n===> The BIOCOM-PIPE was stopped due to a coherency error. You take the decision to launch the
ReClustOR step, however the defined database name to use for the analysis (here, $potential_db)
is not available into stored databases. So, correct this error in the Input.txt file and
relaunch BIOCOM-PIPE.<===\n\n";
	}

	#As the user here wants to define a new database, he must have done the Global Analysis
	&GlobalAnalysisCheck("GLOBAL_ANALYSIS", "RECLUSTOR");
	&CheckGlobalAnalysisCoherency();

	# Finally, we give back information to the user that everything is OK
	&InfoForUser("RECLUSTOR");
}
# End of the function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function developed to verify if parameters chosen by the user were coherent: more particularly, if the
# phylogenetic tree was computed for the UniFrac analysis.
sub CheckUniFracAnalysisCoherency
{
	# Needed variables
	my $ref_array = "";
	my @parameters_unifrac = ();
	my @parameters_glob = ();

	# We first catch and store chosen parameters fro the global analysis step
	$ref_array = PerlLib::Miscellaneous::SearchInputParameters("UNIFRAC_ANALYSIS");
	@parameters_unifrac = @$ref_array;

	# We verify that the user chose to do the UniFrac analysis step, or we leave the verification step
	if(uc($parameters_unifrac[0]) eq "NO") { return "NO"; }

	#We then catch and store chosen parameters for the global analysis step
	$ref_array = PerlLib::Miscellaneous::SearchInputParameters("GLOBAL_ANALYSIS");
	@parameters_glob = @$ref_array;
	# If chosen parameters are incoherent, we stop the program, and end back information to the user
	if(uc($parameters_glob[6]) eq "NO")
	{
		die "==> The BIOCOM-PIPE was stopped due to a coherency error. You take the decision
to launch the UNIFRAC_ANALYSIS step, however the GLOBAL_ANALYSIS phylogenetic
tree will not be realized based on your parameters.
Please, correct this error in the Input.txt file and relaunch BIOCOM-PIPE.\n\n";
	}
	# We give back information to the user that everything is OK
	&InfoForUser("UNIFRAC_ANALYSIS");
}
# End of the function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function developed to verify if parameters and more particularly if files given as input by the user
# let the program to catch paired-end files.
sub CheckPrinSeqFlashCoherency
{
	# Needed variables as input
	my @files_fastq = @{$_[0]};
	my $step = $_[1];
	#Needed empty variables
	my %file_names = ();
	my %paired_files = ();
	my @parameters = ();
	my $key = "";
	my $file = "";
	my $name1 = "";
	my $name2 = "";
	my $ref_array = "";


	# We first catch and store chosen parameters fro the global analysis step
	$ref_array = PerlLib::Miscellaneous::SearchInputParameters($step);
	@parameters = @$ref_array;

	# We verify that the user chose to do the PRINSEQ analysis step, or we leave the verification step
	if(uc($parameters[0]) eq "NO") { return "OK"; }

	#First, we created the needed hash table
	foreach $file (@files_fastq) { $file_names{$file} = ""; }
	#Then, for each file name, we search for the existence of the R1 and R2 files
	foreach $key (keys(%file_names))
	{
		#If the R1 is found, we create the R2 name to check for its existence
		if($key =~ m/_1_1/) { $name1 = $key; ($name2 = $name1) =~ s/_1_1/_1_2/; next; }
		elsif($key =~ m/_1_2/) { $name2 = $key; ($name1 = $name2) =~ s/_1_2/_1_1/; next; }
		elsif($key =~ m/_R1/) { $name1 = $key; ($name2 = $name1) =~ s/_R1/_R2/; next; }
		elsif($key =~ m/_R2/) { $name2 = $key; ($name1 = $name2) =~ s/_R2/_R1/; next; }
		elsif($key =~ m/_1/) { $name1 = $key; ($name2 = $name1) =~ s/_1/_2/; next; }
		elsif($key =~ m/_2/) { $name2 = $key; ($name1 = $name2) =~ s/_2/_1/; next; }

		#Now, we check for the existence of both R1 and R2 files
		if(exists($file_names{$name1}))
		{
			if(exists($file_names{$name2})) { next; }
			#Here, we print the needed dying messages for the user as the program was unable to
			#associate both paired-end files.
			else
			{
				die "\n==> The BIOCOM-PIPE was stopped due to a coherency error. You take the decision to launch the
$step step, however the program was not able to find the $name2 associated to
the $name1 in the Raw_data folder. Please, correct this error checking file names
in the Raw_data folder and relaunch BIOCOM-PIPE.\n\n";
			}
		}
		else
		{
			die "\n==> The BIOCOM-PIPE was stopped due to a coherency error. You take the decision to launch the
$step step, however the program was not able to find the $name1 associated to
the $name2 in the Raw_data folder. Please, correct this error checking file names
in the Raw_data folder and relaunch BIOCOM-PIPE.\n\n";
		}
	}
	return("OK");
}
# End of the function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#=======================================================================================================
#=======================================================================================================
# MAIN FUNCTION CALLED BY THE MAIN PROGRAM
#=======================================================================================================
#=======================================================================================================
# FILE:		VerifyingUserChoices
# USAGE:		PerlLib::VerifyingUserChoices::VerifyingUserChoices(ARGUMENTS)
# ARGUMENTS:	- $_[0]: Reference to the @files_sff list listing all raw .SFF files found,
#			- $_[1]: Reference to the @files_qual list listing all raw .QUAL files found,
#			- $_[2]: Reference to the @files_fna list listing all raw .FNA/.FASTA files found,
#			- $_[3]: Reference to the @files_fastq list listing all raw ./FASTQ files found,
#
# DESCRIPTION:	This function was dedicated to launch all steps and functions dedicated to check
#			the coherency between the user choices and the steps and dependencies of the steps. It
#			takes as input three lists, to verify is needed files are given by the user, and if
#			these files are coherent with his choices.
#=======================================================================================================
sub VerifyingUserChoices
{
	# Input variables
	my $ref_files_sff = $_[0];
	my $ref_files_qual = $_[1];
	my $ref_files_fna = $_[2];
	my $ref_files_fastq = $_[3];

	# Needed variables
	my $validation_text = "";
	my $verif_rdm = "";

	# We check the coherency between chosen steps (EVAL_QUAL and QUALITY_CLEANING for example) and raw data
	# given by the user
	$validation_text = &CheckRawDataCoherency($ref_files_sff, $ref_files_qual, $ref_files_fna, $ref_files_fastq);
	if($validation_text ne "OK") { print $validation_text; }

	# We check the coherency between chosen steps (PRINSEQ and/or FLASH) and raw data given by the user
	$validation_text = &CheckPrinSeqFlashCoherency($ref_files_fastq, "PRINSEQ");
	if($validation_text ne "OK") { print $validation_text; }
	$validation_text = &CheckPrinSeqFlashCoherency($ref_files_fastq, "FLASH");
	if($validation_text ne "OK") { print $validation_text; }

	# We check the unicity of sample names and chosen MIDs given by the user for each raw file.
	$validation_text = &CheckNamesUnicity();
	if($validation_text ne "OK") { print $validation_text; }

	# We check the coherency for the homogenization step.
	$verif_rdm = &CheckHomogenizationChoice();
	if ($verif_rdm == 0)
	{
		print "==> You chose to not homogenize your sequence sets. <==
If a GLOBAL_ANALYSIS or a ReClustOR step is done, the treatment will be realized on:
- the cleaned reads (after hunting and/or recovering steps if they are realized),
- or at least on raw reads.\n";
	}
	# We check the coherency for the filtering alignment step.
	$validation_text = &CheckFilteringAlignmentCoherency("FILTERING_RAW_ALIGNMENT");
	if($validation_text ne "NO") { print $validation_text; }
	# We check the coherency for the raw clustering step.
	$validation_text = &CheckClusteringCoherency("RAW_CLUSTERING");
	if($validation_text ne "NO") { print $validation_text; }
	# We check the coherency for the hunting step.
	$validation_text = &CheckHuntingCoherency();
	if($validation_text ne "NO") { print $validation_text; }
	# We check the coherency for the recovering step.
	$validation_text = &CheckRecoveringCoherency();
	if($validation_text ne "NO") { print $validation_text; }
	# We check the coherency for the clean clustering step.
	$validation_text = &CheckClusteringCoherency("CLEAN_CLUSTERING");
	if($validation_text ne "NO") { print $validation_text; }
	# We check all parameters for the global analysis step, as this particular part of BIOCOM-PIPE would need
	# some results given by previous steps (e.g. clustering, or taxonomy)
	$validation_text = &CheckComputationAnalysisCoherency();
	if($validation_text ne "NO") { print $validation_text; }
	# We check all parameters for the global analysis step, as this particular part of BIOCOM-PIPE would need
	# some results given by previous steps (e.g. clustering, or taxonomy)
	$validation_text = &CheckGlobalAnalysisCoherency();
	if($validation_text ne "NO") { print $validation_text; }
	# We check all parameters for the ReClustOR step, as this particular part of BIOCOM-PIPE would need
	# some results given by previous steps (e.g. clustering, or, Global Analysis)
	$validation_text = &CheckReClustORCoherency();
	if($validation_text ne "NO") { print $validation_text; }
	# We check all parameters for the global analysis step, as this particular part of BIOCOM-PIPE would need
	# some results given by previous steps (e.g. clustering, or taxonomy)
	$validation_text = &CheckUniFracAnalysisCoherency();
	if($validation_text ne "NO") { print $validation_text; }
}
# End of the function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

1;
