#=======================================================================================================
# PACKAGE NAME: PoolingReads
#	 FUNCTIONS:	- DefineFileID (Give an ID for each independent file to recognize the origin of reads),
#			  	- PoolingReads (Do the pool of reads from all independent analyses),
#
#       AUTHOR:  Sebastien TERRAT <sebastien.terrat@dijon.inra.fr>
#      COMPANY:  GenoSol platform (UMR 1347 INRA/uB/AgroSup)
#      VERSION:  1.7
#    REALEASED:  25/10/2012
#     REVISION:  24/07/2014
#=======================================================================================================

# Name of the package defined for the main program
package PerlLib::PoolingReads;

#Print errors and warnings during the implementation of the perl program
use strict;
use warnings;

#=======================================================================================================
#=======================================================================================================
# MAIN FUNCTION CALLED BY THE MAIN PROGRAM
#=======================================================================================================
#=======================================================================================================
# FILE:			PoolingReads
# USAGE:		PerlLib::PoolingReads::PoolingReads (ARGUMENTS)
# ARGUMENTS:	- $_[0]: Fasta file name of the reads added to the concatenated reads [FILE_NAME],
#				- $_[1]: Input folder where the file can be found [FOLDER_NAME],
#				- $_[2]: Output folder where the file will be saved [FOLDER_NAME].
#				- $_[3]: Origin of reads [1 or 2] to know if reads have to be un-dereplicated or not.
#
# REQUIREMENTS: - Perl language,
#				- PerlLib::Miscellaneous (ReadInputFastaFile function),
#				- PerlLib::ManagingGlobalAnalysis (DefineFileID function).
#
# DESCRIPTION:	This program was defined to concatenate efficiently and clearly all treated files to
#			  analyze efficiently all samples between them (and for example, create OTUs matrices and
#			  Venn diagrams between samples). To do this, we use a file named Concatenated_files.txt
#			  stored in a dedicated folder. This file is used to associate a value to the file name.
#			  This value is added to each read to clearly identify its origin.
#
#			  1.5 Modifs (10/04/13): We modify this program to adapt the global analysis step of
#			  analysis to the position of the random selection realized (if the random selection is 
#			  realized by the program). To do this, when the step position is defined, the 
#			  concatenation of all reads is realized, but, first, the dereplicated reads (if the
#			  homogenization is realized after the hunting and/or the recovering) have to be 
#			  de-dereplicated, to realize again the the dereplication on the complete set of reads.
#
#			  1.6 Modifs (27/06/13): We corrected an error caused by the merging of all files, and
#			  particularly by the identification of the file by the number $ID too short when the 
#			  total number of treated files was higher than 100. The sprintf command was then modified
#			  to include a sufficient number of treated files.
#
#			  1.7 Modifs (24/07/14): Complete rewriting of the program to integrate functions and 
#			  modules to simplify and clarify the program.
#
#=======================================================================================================
sub PoolingReads
{
	#1.5: Needed variables
	my $file = $_[0];
	my $folder_input = $_[1];
	my $folder_output = $_[2];
	my $reads_origin = $_[3];
	my $i = 0;
	my $ID = 0;
	my @tmp = ();
	my @tmp2 = ();
	my @input_fasta = ();
	my @input_fasta2 = ();
	my $ref_array = "";
	my $line = "";

	#First step, we define the ID associated to the file treated independently of each child process.
	$ID = PerlLib::ManagingGlobalAnalysis::DefineFileID($file);
	
	# Test to know if the input folder is the "Raw_data" or a folder previously created by the main 
	# program and a previous treatment step
	if ($folder_input eq "None")
	{
		# Open file one given by the user and containing cleaned sequences
		open(F_IN, "Raw_data/$file")|| die "==> File Not Found: $file<==";
	}
	else
	{
		#1.5: We first open the input file to treat then data
		open (F_IN, "Result_files/$folder_input/$file")|| die "==> Can't open file $file for the concatenation step of analysis<==";
	}
	#We read data from the file using a dedicated function
	$ref_array = PerlLib::Miscellaneous::ReadInputFastaFile(*F_IN, "NO");
	@input_fasta = @{$ref_array};
	close (F_IN);

	#1.5: If the global analysis pretreatment is realized after the preprocessing, but before the hunting 
	#and/or the recovering
	if ($reads_origin == 1)
	{
		#We inject the information corresponding to the origin of the read among all treated samples
		foreach $line (@input_fasta) { $line =~ s/^>/\>$ID|/; }
		# Test to know if the input folder is the "Raw_data" or a folder previously created by the main 
		# program and a previous treatment step
		if ($folder_input eq "None")
		{
			open (F_OUT, ">>Temporary_files/Concatenated_reads.fasta.fasta") || die "==> Can't create file Concatenated_reads.fasta.fasta for the concatenation step of analysis<==";
		}
		else
		{
			#1.5: We finally open the output file to store data
			open (F_OUT, ">>Result_files/$folder_output/Concatenated_reads.fasta.fasta") || die "==> Can't create file Concatenated_reads.fasta.fasta for the concatenation step of analysis<==";
		}
	}
	if ($reads_origin == 2)
	{
		#We treat each read from the fasta file
		foreach $line (@input_fasta)
		{
			#We first search for the descriptive line of the read, and we add the file ID
			@tmp = split("\n", $line);
			$tmp[0] =~ s/^>/\>$ID|/;
			#We then search for the number of occurences of the read
			@tmp2 = split("_", $tmp[0]);
			#1.5: Using this number of occurences, we create the good number of descriptives lines
			for($i=0;$i<$tmp2[1];$i++)
			{
				#1.5: Store the sequence in @reads_fasta
				push(@input_fasta2, $tmp2[0]." ".$tmp2[2]."\n".$tmp[1]);
				#1.5: We finally print the results in a temporary file to dereplicate it then
				open (F_OUT, ">>Temporary_files/TMP_Concatenated_reads.fasta.fasta") || die "==> Can't create file TMP_Concatenated_reads.fasta.fasta for the concatenation step of analysis<==";
			}
		}
		#Finally, we store information in the @input_fasta
		@input_fasta = @input_fasta2;
	}
	#1.5: We block the file access to this program only
	flock(F_OUT, 2);
	#1.5: We print information in the result file (temporary file)
	printf(F_OUT join("\n", @input_fasta)."\n");
	#1.5: We give back access to the output file
	flock(F_OUT, 8);
	#1.5: We finally close the output file
	close (F_OUT);
}

1;
