#=======================================================================================================
# PACKAGE NAME: InfernalAlignment
#    FUNCTIONS:	- DefineNbCores (Used to define thenumber of cores used for the alignment),
#		 	- Alignment(Main function of the librayr (see below for details).
#
# REQUIREMENTS:  Perl language only
#       AUTHOR:  Sebastien TERRAT <sebastien.terrat@inra.fr>
#      COMPANY:  UMR 1347 Agroecology
#      VERSION:  3.0
#     RELEASED:  04/08/2011
#     REVISION:  08/02/2018
#=======================================================================================================
#!/usr/bin/perl

# Name of the package defined for the main program
package PerlLib::InfernalAlignment;
#Print errors and warnings during the implementation of the perl program
use strict;
use warnings;
#Needed to do some particular calculations like ceil() or floor()
use POSIX;

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function defined to define the number of cores used to realize the alignment of sequences based on the
# number of maximum cores used by GnS-PIPE and also the number of files that need treatment.
sub DefineNbCores
{
	#Defined variables
	my $Nb_files = $_[0] + 1;
	my $limit_nb_cores = $_[1];
	my $chosen_nb_cores = 1;

	if((($limit_nb_cores-$Nb_files)/$Nb_files)>=1)
	{
		$chosen_nb_cores = ceil($limit_nb_cores/$Nb_files);
	}
	return($chosen_nb_cores);
}
# End of the function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


#=======================================================================================================
#=======================================================================================================
# MAIN FUNCTION CALLED BY THE MAIN PROGRAM
#=======================================================================================================
#=======================================================================================================
# FILE:		Alignment.pm (called by the main program)
# USAGE:		PerlLib::InfernalAlignment::Alignment(ARGUMENTS)
# ARGUMENTS:	- $_[0]: the step name (RAW or CLEAN),
#			- $_[1]: the chosen model for the alignment (bacteria, archaea, fungi, algae),
#			- $_[2]: the number of cores chosen by the user ([1-100]),
#			- $_[3]: the name of the treated file,
#			- $_[4]: the input folder,
#			- $_[5]: the output folder,
#
# DESCRIPTION:	This program was developed to efficiently parallelize the INFERNAL aligner (1.1.1)
#			defined by EP Nawrocki, DL Kolbe and SR Eddy (http://selab.janelia.org). As INFERNAL
#			can be run by Message Passing Interface (MPI) but seems to be not functional with our
#			computational server, we decided to parallelize the alignment step by parsing sequence
#			files. For this, we let the user choose the number of cores he want to use, but also the
#			choice of the used model (bacteria or archaea) and if he want to merge all alignments
#			of treated sequences files.
#			All sequence files needed alignments are stored in the IN/ directory. The Matrix/ one
#			store the models and their alignements used to design them. The Tmp/ directory is used
#			to store all temporary files (sequence subfiles, sub_alignments, etc...). The OUT/
#			directory stores all final results, adding in prefix 'align_' for file names.
#			The first step of the program saves the choices of the user, the second step split all
#			sequence files in subfiles, each one is then aligned in one core using 'cmalign'. The
#			third step is defined to merge all sub_alignments for each treated file in one global
#			alignment using 'cmalign' with the '--merge' option dedicated to this. Finally, to
#			obtain multiple alignments in FASTA format, we add a step to pass files from STOCKHOLM
#			to FASTA format. And, at last, if the user want, we merge all alignments into one single
#			file (stored in the OUT/ directory): Global_align.fasta.
#
#			Details can be found in the source code below, based on all commentaries.
#			1.5 Modifications: As we need to gain time for this step, we modify the step to merge
#			all sub_alignments for each treated file in one global alignment using 'cmalign' with
#			the '--merge' option. Previously, each file was merged with another named "X". Now, all
#			merge steps are done in parallel using an idea given by Anne-Laure BLIEUX to gain time.
#			Using a pyramidal system, all steps are now done faster than before. See details in the
#			third step of the program with commentaries in 1.5 version
#
#			2.0 Modifications: As we need to analyze also fungi data, we integrate also a new
#			model to our program: the fungi (18S) model. This one was constructed using the defined
#			command (see the model.cm to have the detail of the command line) with the seed of the
#			RFAM alignment 01960 containing 79 sequences of eukarya (fungi and others). So, we give
#			another choice of the user: to use this model during the alignment.
#
#			2.1 Modifications: Little modification to fast another time the merging of alignments.
#			To do this, we parallelize each merging step based on the 1.5 modifications. See details
#			in the third step of the program with commentaries in 2.1 version
#
#			2.5 Modis (12/04/12): This program was modified to be included in the workflow to fast the
#			analysis. All modified steps are described in the program (e.g.: using the name of the
#			input and a different location for the output file ans summary file, deletion of the
#			parallelization, as it was done by the main PERL program).
#
#			2.6 Modifs (18/03/13): We modify and add a little element in the preparation of the
#			temporary sequence files, as if we create only one temporary file, we automatically
#			loose the first read analyzed. So, to avoid this problem, we add a little modification
#			to correct this.
#
#			2.7 Modifs (16/06/15): As we need to analyze also algae data targeting the 23S subunit,
#			we integrate also a new model to our program: the algea (23S) model. This one was
#			constructed using the defined command (see the model.cm to have the detail of the command
#			line) with the seed of the RFAM alignment RF02541 containing 102 sequences of bacteria 23S.
#			So, we give another choice of the user: to use this model during the alignment.
#
#			3.0 Modifs (05/02/18): Integration of the Infernal 1.1.1 version to simplify and improve
#			the alignment step. Due to this, we decided to keep the alignment in PFAM format, and treat
#			it for the Clutering step if needed. Moreover, we modified the program to integrate it
#			in a specific library called InfernalAlignment. Many parts of the program were deleted
#			as the new version of Infernal integrate a parallelized system more efficient thant our
#			approach.
#
#=======================================================================================================
sub Alignment
{
	#Defined variables given as input by the program
	my $step = uc($_[0]);
	my $model = $_[1];
	my $file = $_[2];
	my $folder_input = $_[3];
	my $folder_output = $_[4];
	my $Nb_files = $_[5];
	my $limit_nb_cores = $_[6];

	#Needed variables
	my $file_qual = "";
	my $command_align = "";
	my $command_align_part = "";
	my $command_align_final = "";
	my $stdout_tmp = "";
	my $cmalign = "cmalign";
	my $boolean_qual = 0;
	my $cores = 0;

	#3.1: Avoid the message printing as the ReClustOR step can call this function
	#3.0: Print for the user information that the program is started
	if($step ne "") { PerlLib::Miscellaneous::PrintScreen($step." INFERNAL ALIGNMENT", $file, "started"); }
	#3.0: We chose the needed model for the global alignment
	$model = PerlLib::Miscellaneous::ModelChoice($model);
	#3.0: We define thenumber of cores for the global alignment
	$cores = &DefineNbCores($Nb_files, $limit_nb_cores);
	#Needed command line with parameters to launch alignment steps (--hbanded: needed parameter
	#based on developers information, -g: global alignment and not local --dna: to have the alignment
	#in DNA format, not RNA, --outformat pfam: to obtain alignments in PFAM format, not in STOCKHOLM
	#format, -o : to precise the filename where the alignment will be stored.
	#$command_align_part = "$cmalign --hbanded --sub --dna -1 -q -o ";
	$command_align_part = "$cmalign --mxsize 5000 --hbanded -g --outformat pfam --cpu $cores --dna -o ";
	#3.1: We modified the $command_align to manage the special case that ReClustOR launch the alignment to create
	#the aligned database, so we defined also a specific test (if/else)
	#2.5: We add the last parts of the command line (the name of the output file, the used
	#model and the used sequence subfile.
	if($step ne "") { $command_align = $command_align_part."Result_files/$folder_output/Align_".$file." ".$model."Result_files/$folder_input/".$file; }
	else { $command_align = $command_align_part."$folder_output/Align_".$file." ".$model."$folder_input/".$file; }
	#We finally execute this command line
	$stdout_tmp = `$command_align`;
	#3.2: Here, we add the creation of a temporary file containing information regarding the
	#quality of the alignment for each read
	open(F_DETAILS, ">Temporary_files/Tmp_Align_$file") || die "Can't create the temporary details of the alignment $file\n";
	printf(F_DETAILS $stdout_tmp);
	close(F_DETAILS);
	#3.1: Avoid the message printing as the ReClustOR step can call this function
	#3.0: Print for the user information that the program is started
	if($step ne "") { PerlLib::Miscellaneous::PrintScreen($step." INFERNAL ALIGNMENT", $file, "terminated"); }
	#2.5: We pipe the name of the created file (the result file) for the main program
	return("Align_$file");
}
# End of the function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
1;
