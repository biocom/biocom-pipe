#=======================================================================================================
# PACKAGE NAME: GlobalAlignment
#	 FUNCTIONS:	- CatchConsensusSeqs (Catch all consensus sequences from the independent alignments),
#			- CreateLongestConsensusSeq (Define the global consensus sequence from all alignments),
#			- CreateAlignmentPatterns (Define patterns applied to alignments from the global one),
#			- SearchIDsFileNames (link the file name to the ID created for the global analysis),
#			- CatchNeededReads (Link IDs from the global file and the independent alignment),
#			- ApplyPattern (Apply the dedicated pattern to the read from one independent file),
#			- CatchIndepDerepReads (Link IDs from the global file with the Derep_details file),
#			- GlobalAlignment (manage the complete step and all needed functions and data).
#
#       AUTHOR:  Sebastien TERRAT <sebastien.terrat@dijon.inra.fr>
#      COMPANY:  GenoSol platform (UMR 1347 INRA/uB/AgroSup)
#      VERSION:  2.1
#     RELEASED:  22/04/2013
#     REVISION:  18/12/2015
#=======================================================================================================

#!/usr/bin/perl

#Print errors and warnings during the implementation of the perl program
use strict;
use warnings;

# Name of the package defined for the main program
package PerlLib::GlobalAlignment;

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function dedicated to find each consensus sequence from all independent alignments. It takes as input
# the name of the folder where are stored all alignments. It gives back three elements: the name of the
# file where the longest consensus sequence was found, the list of file names and a hash table %seq_cons
# containing the consensus sequences of all associated files.
sub CatchConsensusSeqs
{
	#Needed variables
	my $folder = $_[0];
	my $file = "";
	my $chosen_file_cons = "";
	my @files = ();
	my %seq_cons = ();
	my $len_max = 0;
	
	#We open the needed directory
	opendir (DIR, "Result_files/$folder") || die "can't open the alignment directory: $!";
	#And for each file in the directory
	LOOP:while (defined($file = readdir(DIR)))
	{
		#We avoid the opoening of not needed files or empty files
		if ($file =~ m/^\.\.?$/) { next LOOP; }
		else
		{
			#We store the name of the file in a dedicated array
			push(@files, $file);
			#We open each alignment file
			open(F_IN, "Result_files/$folder/$file")|| die "==> File Not Found: Result_files/$folder/$file<==";
			#For each line in the file
			while(<F_IN>)
			{
				#We search the line corresponding to the ID of the consensus sequence
				if ($_ =~ m/\>\#\=GC_RF/)
				{
					#We then catch the consensus sequence itself
					$seq_cons{$file} = <F_IN>;
					#We keep only the longest consensus sequence from all files
					if($len_max < length($seq_cons{$file}))
					{
						#We store the name of the file and the length of the sequence
						$chosen_file_cons = $file;
						$len_max = length($seq_cons{$file});
					}
				}
			}
			#We close the file
			close(F_IN);
		}
	}
	#We close the directory
	closedir(DIR);
	#We finally return needed data (the name of the file where we found the longest consensus,
	#the names of all files in @files, and all consensus sequences in %seq_cons with file names
	#as associated keys.
	return($chosen_file_cons, \@files, \%seq_cons);
}
# End of the function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function dedicated to the definition of the longest consensus sequence to merge all alignment files.
# It needs as input a hash table with all consensus sequences and also the name of the file where the
# longest consensus sequence was found to avoid its treatment. It gives back as output the created 
# consensus sequence into two variables: a string and an array.
sub CreateLongestConsensusSeq
{
	#Needed variables
	my %seq_cons = %{$_[0]};
	my $chosen_file_cons = $_[1];
	my $file = "";
	my @ref_cons = ();
	my @treated_cons = ();
	my ($i,$j) = (0,0);
	my $ref_cons2 = "";
	my $GC_RF_seq = "";
	
	#First, we store the ref_cons string in an array
	@ref_cons = split("", uc($seq_cons{$chosen_file_cons}));
	#For each alignment file that we have to treat
	LOOP01:foreach $file (keys(%seq_cons))
	{
		#We avoid the treatment of the file corresponding to the longest consensus sequence
		if($file eq $chosen_file_cons) { next LOOP01; }
		#We also store the consensus sequence of the treated alignement in another array
		@treated_cons = split("", uc($seq_cons{$file}));
		#Loop to read all characters of both alignments
		LOOP02:for($i=0;$i<=$#ref_cons;$i++)
		{
			LOOP03:for($j=0;$j<=$#treated_cons;$j++)
			{
				#Here, we manage the particular case that some gaps where added after the end of the reference
				#consensus due some bad reads found in the datasets
				if(($i == $#ref_cons)&&($j < $#treated_cons ))
				{
					#We add a gap at the end of $ref_cons2 and go to the next base
					$ref_cons2 .= ".";
					next LOOP03;
				}
				#If the same characters are found in the two alignments
				if($ref_cons[$i] eq $treated_cons[$j])
				{
					#We store the character and then we go to the next one
					$ref_cons2 .= $ref_cons[$i];
					$i++;
				}
				#If the two characters are different
				else
				{
					#We check which consensus sequence add a gap here to add a gap to the other
					if($ref_cons[$i] eq ".")
					{
						#We store the character of the longest consensus sequence and then we go to the 
						#next one, staying in the good chracter of the treated consensus
						$ref_cons2 .= $ref_cons[$i];
						$i++;
						$j--;
					}
					#We have to add a gap to the reference consensus alignment
					else { $ref_cons2 .= "."; }
				}
			}
			#Here, we define this loop if we reach the end of the treated consensus sequence and
			#the reference_consensus possess gaps added after the reference consensus due to 
			#bad and long reads in the datasets
			while($i <= $#ref_cons) { $ref_cons2 .= $ref_cons[$i]; $i++; }
		}
		#Finally we store the global reference consensus in the array @ref_cons and in a variable
		$GC_RF_seq = $ref_cons2;
		@ref_cons = split("", uc($ref_cons2));
		$ref_cons2 = "";
	}
	return($GC_RF_seq, \@ref_cons);
}
# End of the function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function dedicated to define the modifications to apply for each alignment file based on the longest
# consensus sequence created before. It needs as input a hash table with all independent consensus
# sequences associated with file names, and also the longest consensus sequence created from independent
# alignments. It gives back as output the patterns (position and associated gaps) stored in a hash table
# associated with file names. The patterns are stored in an array, each element are: position_addedgaps.
sub CreateAlignmentPatterns
{
	#Needed variables
	my %seq_cons = %{$_[0]};
	my @ref_cons = @{$_[1]};
	my $file = "";
	my @treated_cons = ();
	my ($i,$j,$k) = (0,0,0);
	my $pos_added_gaps = -1;
	my @pattern = ();
	my $added_gaps = "";
	my %patterns = ();
	
	#For each alignment file that we have to treat
	LOOP01:foreach $file (keys(%seq_cons))
	{
		#We store the consensus sequence of the treated alignement in an array
		@treated_cons = split("", uc($seq_cons{$file}));
		#Loop to read each character and all characters of all alignments
		LOOP02:for($i=0;$i<=$#ref_cons;$i++)
		{
			LOOP03:for($j=0;$j<=$#treated_cons;$j++)
			{
				#If the same characters are found in the two alignments
				if($ref_cons[$i] eq $treated_cons[$j])
				{
					#If the area of gaps is over (as we do not add another gap), we then
					#store all needed data in the @pattern array and erase all not needed 
					#data and variables.
					if($pos_added_gaps > -1)
					{					
						push(@pattern, "$pos_added_gaps"."_".$added_gaps);
						$pos_added_gaps = -1;
						$added_gaps = "";
					}
					#We then go for the next base to analyze
					$i++;
				}
				#If the two characters are different
				else
				{
					#If the reference consensus sequence contain a gap "."
					if($ref_cons[$i] eq ".")
					{
						#Here, we evaluate if it's a new area of gaps or an actual one
						if($pos_added_gaps == -1)
						{
							#If it's a new one, we keep the start position in the treated read
							#and we start also the $added_gaps variable
							$added_gaps = "\.";
							#We also check if there is already a gap opened before to increase
							#the gap instead of opening a new one. To do this, we check the 
							#length of the gap using a while loop.
							$k = -1;
							while($treated_cons[$j + $k] eq ".") { $k--; }
							#We then define the center of the found gap
							$k = int($k/2 + 0.5);
							#And we add to the position where the gap will be added this new value
							$pos_added_gaps = $j + $k;
						}
						#If it's gap area already started, we continue it adding a new gap
						else { $added_gaps .= "\."; }
						$i++;
						$j--;
					}
					else
					{
						#Test to evaluate if it's a new area of gaps or an actual one
						if($pos_added_gaps > -1)
						{
							#If the area of gaps is over (as we do not add another gap), we then
							#store all needed data in the @pattern array and erase all not needed 
							#data and variables.
							push(@pattern, "$pos_added_gaps"."_".$added_gaps);
							$pos_added_gaps = -1;
							$added_gaps = "";
						}
					}
				}
			}
		}
		#We finally store the array of modifications to do for one particular file in a hash table
		#to easily find it
		$patterns{$file} = [@pattern];
		@pattern = ();
	}
	#At the end, we return the list of modifications to do stored in a hash table
	return (\%patterns);
}
# End of the function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function dedicated to link and organize file names of sequences and aligned sequences into one
# hash table to easily and fastly access to them. Take as input only the reference of an array with
# all aligned files names, and give as output the reference of one hash table using as keys the IDs
# given by the main program associated with the names of the files with aligned sequences.
sub SearchIDsFileNames
{
	#Needed variables
	my $file = "";
	my @tmp = ();
	my %ID_align_names = ();
	my @align_file_names = @{$_[0]};
	
	#Here, using the information stored in Concatenated_files.txt, we will create a hash table
	open(FILE, "Summary_files/Global_Analysis/Concatenated_files.txt")|| die "==> File Not Found: Concatenated_files.txt<==";
	#For each line found in the file
	LOOP01:while(<FILE>)
	{
		#We first delete the '\n' character
		$_ =~ s/\n//;
		#We then split the line based on the '\t' character
		@tmp = split("\t", $_);
		#We then create the hash table (File Number as key and File Name as value)
		
		LOOP02:foreach $file (@align_file_names)
		{
			if ($file =~ m/\Q$tmp[1]/)
			{
				#$ID_align_names{$tmp[0]} = $file;
				$ID_align_names{$file} = $tmp[0];
				next LOOP01;
			}
		}
	}
	#We finally close the Concatenated_files.txt
	close (FILE);
	#We give back to the main program the two needed hash tables
	return (\%ID_align_names);
}
# End of the function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function dedicated to search and link using the global file and the file containing details from
# the concatenation and dereplication steps the IDs from one independent file to needed ones in the 
# global file. It takes as input three arguments: the folder where it can find the dereplicated global
# file, the name of the global file and the ID_file defined by the main program to search it. As output,
# it gaves back a hash table using as keys the IDs searched in the independent file, and the 
# corresponding IDs from the global file.
sub CatchNeededReads
{
	#Needed variables
	my $folder_input = $_[0];
	my $global_file = $_[1];
	my $ID_file = $_[2];
	my %ID_reads_file = ();
	my @tmp = ();
	my @tmp2 = ();
	my @tmp3 = ();
	my $test = 0;
	my $i = 0;
	
	#We open first the file containing all merge reads for all samples
	open(F_GLOB, "Result_files/$folder_input/$global_file")|| die "==> File Not Found: $global_file<==";
	#Here, we catch all read IDs for a given alignment file to use them later and not all.
	while(<F_GLOB>) { if($_ =~ m/^>$ID_file\|/) { $ID_reads_file{$_} = $_; } }
	#We close the file as we did not need to keep it open.
	close(F_GLOB);
	
	#Then, we open the file containing details about the dereplication of reads for all samples
	open(F_DEREP, "Summary_files/Global_Analysis/Derep_details_Concatenated_reads.fasta.fasta")|| die "==> File Not Found: Derep_details_Concatenated_reads.fasta.fasta<==";
	#We then search for dereplicated reads in the global file to find the corresponding ID 
	#in the independent alignment file.
	while(<F_DEREP>)
	{
		#We split the line to separate the ID used in the global file and the others from
		#independent files
		@tmp = split("\t", $_);
		
		#If the ID file is found in the global file, it's a dereplicated read between several files
		if(exists($ID_reads_file{">".$tmp[0]."\n"}))
		{
			#We then split the unique reads in @tmp2
			@tmp2 = split(" ", $tmp[1]);
			#We put $test to 1 to avoid some treatments and count the number of
			#times we can find it.
			$test = 1;
			#Loop to count the number of times with have the same read in the line as sometimes,
			#to create the file, we artificially create several unique reads.
			for($i=1;$i<=$#tmp2;$i++) { if($tmp2[0] eq $tmp2[$i]) { $test++; } }
			#If we found only one time the same read in the line
			if($test == 1)
			{
				#We keep the original ID as value and the new ID as key to efficiently treat the file
				$ID_reads_file{">".$tmp2[0]."\n"} = ">".$tmp[0]."\n";
				delete($ID_reads_file{">".$tmp[0]."\n"});
			}
			else
			{
				#Or, we modify the read name to use it efficiently based on the number we found it
				@tmp3 = split("_", $tmp2[0]);
				delete($ID_reads_file{">".$tmp[0]."\n"});		
				$ID_reads_file{">".$tmp3[0]."_".$test."_".$tmp3[2]."\n"} = ">".$tmp[0]."\n";
			}
		}
	}
	#We finally close the file an return the modified hash table
	close(F_DEREP);
	return (\%ID_reads_file);
}
# End of the function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function dedicated to apply the defined pattern to the sequence extracted from one independent 
# alignment. It needs as input two arguments: the aligned read and the corresponding pattern to apply.
# As ouptut, it gives back a string corresponding to the aligned read after treatment.
sub ApplyPattern
{
	#Needed variables
	my $init_read = $_[0];
	my @pattern = @{$_[1]};
	my $element = "";
	my $adds = 0;
	my @treated_read = ();
	my @tmp = ();	
	
	#We first split the sequence and store it in an array
	@treated_read = split("", $init_read);
	#Then, we use the defined pattern, using each element
	foreach $element (@pattern)
	{
		#We first catch the position and the gaps
		@tmp = split("_", $element);
		#We add gaps in the array using the splice function
		splice(@treated_read, $tmp[0]+$adds, 0, $tmp[1]);
		#We count the number of adds done
		$adds++;
	}
	#We finally send back the treated read
	return (join("",@treated_read));
}
# End of the function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function dedicated to manage the particular case of global alignment done with raw sequences (so, 
# after the dereplication step and the creation for the Derep_details file). This function takes as 
# input the $ref_array of the hash table using as keys the IDs searched in the independent file, and the 
# corresponding IDs from the global file, the independent file name, and its ID. As output, it gave the 
# same hash table, with some modifications from the Derep_details file.
sub CatchIndepDerepReads
{
	#Needed variables
	my %ID_reads_file = %{$_[0]};
	my $ID_indep_file = $_[1];
	my $ID_file = $_[2];
	my @tmp = ();
	my @tmp2 = ();
	my $ID = "";
	
	#We modify the name of the 
	$ID_indep_file =~ s/Align\_Derep\_//;
	#First, we open and store the data form the Derep_details summary file of independent file
	open(F_DEREP_INDEP, "Temporary_files/Derep_details_$ID_indep_file") || die "==> File Not Found: Derep_details_$ID_indep_file<==";
	while(<F_DEREP_INDEP>)
	{
		$_ =~ s/\n//;
		@tmp = split("\t", $_);
		@tmp2 = split(" ", $tmp[1]);
		foreach $ID (@tmp2)
		{
			if(exists($ID_reads_file{">".$ID_file."|".$ID."\n"}))
			{
				$ID_reads_file{">".$ID_file."|".$tmp[0]."\n"} = $ID_reads_file{">".$ID_file."|".$ID."\n"};
				delete($ID_reads_file{">".$ID_file."|".$ID."\n"});
			}
		}
	}
	close (F_DEREP_INDEP);
	return(\%ID_reads_file);
}
# End of the function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~




#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function defined to create a temporary file encompassing all independent alignment files to merge. 
# This function needs as input: an array with file names, the folder name and give back the name of the 
# temporay file created. 
sub DefineListAlignFiles
{
	#Defined variables
	my @files = @{$_[0]};
	my $folder = $_[1];
	my $file = "";
	
	open(LIST, ">Temporary_files/List_align") || die "==> Can't create List_align file. <== \n";
	foreach $file (@files) { printf(LIST "Temporary_files/$file\n"); }
	close(LIST);
	return("Temporary_files/List_align");
}
# End of the function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function defined to add the IDs given by the main program to reads to easily identify them and 
# retrieve them into the global alignment file. To do this, the function creates temporary alignment 
# files for the esl-alimerge program. This function needs the list of the files, the hash table with 
# associations between file anmes and their IDs, and the folder to find files. 
sub AddIDFiles
{
	#Defined variables
	my @files = @{$_[0]};
	my %ID_files = %{$_[1]};
	my $folder = $_[2];
	#Needed empty variables
	my $file = "";
	my $spaces = "";
	my $ID = "";
	my $length = 0;
	
	#Foreach file found in @files
	foreach $file (@files)
	{
		#We first define the number of characters added with the sample ID 
		$length = length($ID_files{$file}."|");
		#We then create the needed string of spaces
		while($length > 0) { $spaces .= " "; $length--; }
		#We then read the aligned file and create the temporary file
		open(F_IN, "Result_files/$folder/$file") || die "==> Can't open the align $file in $folder. <==\n";
		open(F_OUT, ">Temporary_files/$file")|| die "==> Can't create the align $file in Temporary_files. <==\n";
		#For each line
		while(<F_IN>)
		{
			#If the line needs no change, we just copy it
			if(($_ =~ m/\#\sSTOCKHOLM/)||($_ =~ m/\#\=GF/)||($_ eq "\n")||($_ =~ m/\/\//)) { printf(F_OUT $_); }
			#If the line if specific to the alignment, we add the needed ID
			elsif($_ =~ m/\#\=GR\s([\w\d\=\_\-]+)\s/)
			{
				$ID = $1;
				$_ =~ s/\Q$ID/$ID_files{$file}\|$ID/;
				printf(F_OUT $_);
			}
			#If the line needs only spaces, we add it 
			elsif($_ =~ s/\#\=GC\sSS_cons/\#\=GC SS_cons$spaces/) { printf(F_OUT $_); }
			elsif($_ =~ s/\#\=GC\sRF/\#\=GC RF$spaces/) { printf(F_OUT $_); }
			#If the line is the alignment, we add the ID at the beginning of the string
			else { printf(F_OUT $ID_files{$file}."\|".$_); }
		}
		close(F_IN);
		close(F_OUT);
	}
}
# End of the function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function defined to delete all temporary files created during the realization of the global alignment.
# To do this, this function open the List_align file previously created for the realization of the 
# global alignment, and then, delete all listed files. Finally, the function delete also the List_align
# file as it was not used anymore.
sub CleanTemporaryFiles
{
	open(LIST, "Temporary_files/List_align") || die "==> Can't open List_align file. <== \n";
	while(<LIST>) { chomp($_); unlink($_); }
	close(LIST);
	unlink("Temporary_files/List_align");
}
# End of the function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function defined to define the sequences that will be modified/deleted in the global alignment file
# using the information from the dereplicated details file. Indeed, as the dereplication was done for
# each file independently, some reads are then depreplicated a second time for the global treatment. To
# create efficiently the global alignment, we need to replace some sequence IDs and delete others from
# the temporary alignment. To do this, two hashes linking unique reads and dereplicated reads were
# created with this function and returned to the main program.
sub CatchLinksDerepUnique
{
	#Needed variables
	my %Derep_Unique = ();
	my %Unique_Derep = ();
	my %Tmp = ();
	my @tmp = ();
	my @tmp2 = ();
	my $ID = "";
	my $ID_derep = "";
	my $ID_new = "";
	my $nb_derep = 0;
	my $nb = 0;
	my $string = "";

	#First, we open the file containing details about the dereplication of reads for all samples
	open(F_DEREP, "Summary_files/Global_Analysis/Derep_details_Concatenated_reads.fasta.fasta")|| die "==> File Not Found: Derep_details_Concatenated_reads.fasta.fasta<==";
	#We then search for dereplicated reads in the global file to find the corresponding ID 
	#in the independent alignment file.
	while(<F_DEREP>)
	{
		$nb = 0;
		%Tmp = ();
		#Clean the line of not needed characters
		chomp($_);
		#We split the line to separate the ID used in the global file and the others from
		#independent files
		@tmp = split("\t", $_);
		#Then, we split the dereplicated ID to keep the read ID itself and the number of reads
		@tmp2 = split("_", $tmp[0]);
		$ID_derep = $tmp2[0];
		$nb_derep = $tmp2[1];	
		#We split the unique reads to separate the others from independent files
		@tmp2 = split(" ", $tmp[1]);
		#We count the number of occurrences of unique reads
		foreach $ID (@tmp2) { if($ID =~ m/\Q$ID_derep/) { $nb++; } }
		#If the dereplicated read was not de replicated before, we have to keep it (a mix of
		#reads from several independent files)
		if($nb != $nb_derep)
		{
			#We keep the good dereplicated read
			$ID_derep = $tmp[0];
			$Derep_Unique{$ID_derep} = "";
			#here, we count for the number of occurences of each unique read
			foreach $ID (@tmp2) { $Tmp{$ID}++; }
			#Then, using these information, we recreate needed IDs to find them and 
			#replace/delete them in the global alignment file
			foreach $ID (keys(%Tmp))
			{
				#We split the unique ID, and add the number of occurences found in is name
				@tmp = split("_", $ID);
				$ID_new = $tmp[0]."_".$Tmp{$ID}."_".$tmp[2];
				#then, we store the links between Dereplicated global and Unique reads to find
				#and delete them along the global alignment file
				$Derep_Unique{$ID_derep} .= $ID_new." ";
				$Unique_Derep{$ID_new} = $ID_derep;
			}
		}
		else { next; }
	}
	#We finally close the file and return the needed hashes
	close(F_DEREP);
	return(\%Derep_Unique, \%Unique_Derep);
}
# End of the function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function defined to treat the global alignment obatined after merging independent files. In fact, this
# program will use two hash tables given as input to treat the global alignment and replace and, or 
# delete dereplicated reads from the global dereplication. After this treatment, the alignment file is
# written in the dedicated folder (given as input).
sub WriteGlobalAlignment
{
	#Needed variables
	my %Derep_Unique = %{$_[0]};
	my %Unique_Derep = %{$_[1]};
	my $global_file = $_[2];
	my $output_folder = $_[3];
	my @tmp = ();
	my %Deleted = ();
	my $ID_Derep = "";
	my $ID_Unique = "";
	my $line = "";
	my $ID = "";
	
	open(F_TMP, "Temporary_files/Align_$global_file") || die "==> File Not Found: Temporary $global_file<==";
	open(F_OUT, ">Result_files/$output_folder/Align_$global_file") || die "==> Can't create the align $global_file in the $output_folder. <==\n";
	while(<F_TMP>)
	{
		#We must not treat not needed lines to avoid a problem during the analysis and also alignment lines 
		#without sequences
		if (($_ eq "\n")||($_ =~ m/^#/)||($_ =~ m/\/\//)) { printf(F_OUT $_); }
		#If the line is an aligned sequence
		else
		{
			#First, we split the line to find the read ID
			@tmp = split(" ", $_);
			#If the read ID is in the list of deleted ones (as already dereplicated)
			if(exists($Deleted{$tmp[0]}))
			{
				#We delete the $ID in the hash table and we avoid to keep the lines (aligment and read)
				delete($Deleted{$tmp[0]});
				$line = <F_TMP>;
			}
			#If the ID is one that have been dereplicated
			elsif(exists($Unique_Derep{$tmp[0]}))
			{
				#We first modify and replace both IDs in lines (alignment and read)
				$_ =~ s/\Q$tmp[0]/$Unique_Derep{$tmp[0]}/;
				$line = <F_TMP>;
				$line =~ s/\Q$tmp[0]/$Unique_Derep{$tmp[0]}/;
				#We then print them in the output file
				printf(F_OUT $_.$line);
				#We keep the global dereplicated ID needed
				$ID_Derep = $Unique_Derep{$tmp[0]};
				$ID_Unique = $tmp[0];
				#And we then delete the unique read already treated
				delete($Unique_Derep{$tmp[0]});
				#We alos split all search IDs to delete them if encountered
				@tmp = split(" ", $Derep_Unique{$ID_Derep});
				#Indeed, we stored them in the %Deleted hash to avoid them in the global alignment file
				foreach $ID (@tmp)
				{
					if($ID eq $ID_Unique) { next; }
					else { $Deleted{$ID} = ""; }
				}
			}
			#We avoid the treatment of not needed reads
			else { printf(F_OUT $_); }
		}
	}
	#We close both files (output and temporary files)
	close(F_TMP);
	close(F_OUT);
	unlink("Temporary_files/Align_$global_file");
}
# End of the function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


#=======================================================================================================
#=======================================================================================================
# MAIN FUNCTION CALLED BY THE MAIN PROGRAM
#=======================================================================================================
#=======================================================================================================
# FILE:		GlobalAlignment.pm (called by the main program)
# USAGE:		PerlLib::GlobalAlignment::GlobalAlignment(ARGUMENTS)
# ARGUMENTS:	- $_[0]: the chosen model of secondary structure used,
#			- $_[1]: the name of the file containing reads for the global analysis,
#			- $_[2]: the input folder containing the file,
#			- $_[3]: the output folder where the alignment file will be stored.
#
# REQUIREMENTS: - Perl language.
#
# DESCRIPTION:	This program was developed to efficiently use the INFERNAL aligner (1.0.2) based on 
#			independant alignments of all analyzed samples. In fact, we will use all alignments of
#			all analyzed samples to redefine efficiently sub-alignments for all samples, and then
#			we will merge them to gain some computation time. This method gave exactly the same 
#			result as the global alignment of all reads, but use the already defined alignments for
#			all independant samples. Briefly, we first define independant alignments for all 
#			samples in the Temporary_files folder. We then merge all these alignments in one, and
#			define the Concatenated_reads global alignment.
#
#			1.1 Modifications: This program was first modified to fast the analysis (the creation of
#			independent alignment files was realized using the order of the reads listed from the 
#			global file). This version regroup using hash tables all needed reads file by file, to 
#			read only once each independent file. Doing this, we also avoid some problems of writing
#			diminish the computation time by 5 (16 hours to 3 hours for example). We also completely
#			reorganize the program in various functions, and we finally implement a dedicated module
#			for this particular step of analysis using also functions from other modules
#			(Miscellaneous, and ManagingFormatFiles).
#
#			1.5 Modifications: The whole program was completely reprogrammed to avoid the use of the
#			Infernal program, as with huge files (such as the complete RMQS), the program Infernal was
#			unable to realize the global alignment and encounter memory errors. To avoid this, we
#			imagine a new system of merging based on the consensus sequences analysis and comparisons
#			between independent alignments to redefine efficiently a new consensus sequence. Based on
#			this new consensus sequence, we defien the needed modifications for each independent 
#			alignment, and we applied it to all needed sequences. This solution gives seme results, 
#			but it's faster and avoid the use of the Infernal program.
#
#			2.0 Modifs (28/05/2015): We corrected some errors in the new program leaning on bad global
#			alignments. We also cut the whole program in different functions to easily modify and 
#			maintain it. We create several functions: CatchConsensusSeqs, CreateLongestConsensusSeq,
#			CreateAlignmentPatterns, CatchNeededReads and ApplyPattern to realize that, see details
#			in their descriptions.
#
#			2.1 Modifications (18/12/2015): We added a new function (CatchIndepDerepReads) that manage
#			the particular case of global alignment done with raw sequences (so, after the 
#			dereplication step and the creation for the Derep_details file). To take into account this
#			file and avoid the loss of sequences, this function is essential as some reads can't be 
#			found without the information from these Derep_details files.
#
#			3.0 Modifications (16/03/2018): We completely modified the global alignment program to
#			integrate the new version of the INFERNAL alignment program (v1.1.1). Several functions
#			have been moedified, or replaced, and the esl-alimerge function was used to fast the 
#			global alignment step.
#=======================================================================================================

sub GlobalAlignment
{
	#Defined variables given by the main program
	my $model = $_[0];
	my $global_file = $_[1];
	my $folder_input = $_[2];
	my $folder_output = $_[3];

	#Needed variables
	my $file = "";
	my @files = ();
	my %seq_cons = ();
	my $chosen_file_cons = "";
	my @ref_cons = ();
	my %patterns = ();
	my @pattern = ();
	my @treated_read = ();
	my $ref_array = "";
	my $ref_array2 = "";
	my %ID_align_names = ();
	my %ID_reads_file = ();
	my $ID_file = "";
	my $GC_RF_seq = "";
	my $GC_SS_seq = "";
	my $treated_read = "";
	my $GC_test = 0;
	
	my $esl_alimerge = "esl-alimerge";
	my $tmp_file = "";
	my $esl_info = "";

	#Here, we catch all needed alignment file names using a dedicated function
	$ref_array = PerlLib::Miscellaneous::ListFilesInFolder($folder_output);
	@files = @{$ref_array};
	#Then, we create a file containing all alignment file names for the esl-alimerge program
	$tmp_file = &DefineListAlignFiles($ref_array, $folder_output);
	#Here, we catch and associate the IDs given by the global alignment to file names
	$ref_array = &SearchIDsFileNames(\@files);
	%ID_align_names = %{$ref_array};
	#We create needed temporary alignment files adding ID of each sample in read IDs
	&AddIDFiles(\@files, \%ID_align_names, $folder_output);
	#We then launch the esl-alimerge binary program with needed paramaters (--dna: sort result as DNA
	#format, -outformat pfam: PFAM format of alignment, -o: indicating th output file, --list: use the
	#temporary file containing the list of independent alignment files that will be merged.
	$esl_info = `$esl_alimerge --dna --outformat pfam -o Temporary_files/Align_$global_file --list $tmp_file`;
	#Then, we delete the temporary files as the global alignment is created
	&CleanTemporaryFiles();
	#Using the derep details file, we catch needed data to correct and modify the global alignment file
	($ref_array, $ref_array2) = &CatchLinksDerepUnique();
	#Using finally all data, we recreate the global alignment without needed aligned reads
	&WriteGlobalAlignment($ref_array, $ref_array2, $global_file, $folder_output);
	#We pipe the name of the created file (the result file) for the main program
	return("Align_$global_file");
	exit(0);

	#Here, we catch and organize all consensus sequences from all treated files using a dedicated function
	#called CatchConsensusSeqs.
	($chosen_file_cons, $ref_array, $ref_array2) = &CatchConsensusSeqs($folder_output);
	@files = @{$ref_array};
	%seq_cons = %{$ref_array2};
	#Here, using the whole bunch of new consensus sequences, we define a global alignment encompassing all
	#needed gaps.
	($GC_RF_seq, $ref_array) = &CreateLongestConsensusSeq(\%seq_cons, $chosen_file_cons);
	@ref_cons = @{$ref_array};
	#Then, we launch the dedicated function to create, using the global alignment defined earlier, and the
	#hash table containing all consensus sequences, the patterns to modify each independent alignment.
	$ref_array = &CreateAlignmentPatterns(\%seq_cons, \@ref_cons);
	%patterns = %{$ref_array};
	#Here, we catch and associate the IDs given by the global alignment to file names
	$ref_array = &SearchIDsFileNames(\@files);
	%ID_align_names = %{$ref_array};
	#We create the output file
	open(F_OUT, ">Result_files/$folder_output"."/Align_$global_file")|| die "==> File Not Found: Can't create file<==";
	#For each found alignment file
	foreach $ID_file (keys(%ID_align_names))
	{
		#This function is dedicated to catch and store efficiently all read IDs from the global file
		#and associates them to the IDs in independent files.
		$ref_array = &CatchNeededReads($folder_input, $global_file, $ID_file);
		#2.1: This function is dedicated to catch and store efficiently all read IDs from the dereplication of independent 
		#files that are stored in the Derep_details file in the Temporary_files/ folder, to avoid the loss of reads for the
		#global alignment.
		$ref_array = &CatchIndepDerepReads($ref_array, $ID_align_names{$ID_file}, $ID_file);
		#2.1: We then catch the needed data from the two treatments.
		%ID_reads_file = %{$ref_array};
		#Here, we catch the needed pattern to modify the aligned reads.
		$file = $ID_align_names{$ID_file};
		@pattern = @{$patterns{$file}};
		#We then open the corresponding alignment file
		open(F_IN, "Result_files/$folder_output/$file")|| die "==> File Not Found: Result_files/$folder_output/$file<==";
		#For each line in the file
		while(<F_IN>)
		{
			#If the line is a descriptive line, we just keep it in the next file
			if($_ =~ s/^>/\>$ID_file\|/)
			{
				#If we found the corresponding ID.
				if(exists($ID_reads_file{$_}))
				{
					#We then store it in the output file
					printf(F_OUT $ID_reads_file{$_});
					#And also we delete it from the hash table to avoid multiple treatments
					delete($ID_reads_file{$_});
					#We catch also the sequence itself 
					$treated_read = <F_IN>;
					#We treat the sequence using the dedicated function
					$treated_read = &ApplyPattern($treated_read, \@pattern);
					#We then print it in the output file
					printf(F_OUT $treated_read);
				}
			}
			if(($_ =~ m/GC_SS_cons/)&&($GC_test == 0))
			{
				#Here, we catch the GC_SS_cons secondary structure sequence
				$GC_SS_seq = <F_IN>;
				#We treat the sequence using the dedicated function
				$GC_SS_seq = &ApplyPattern($GC_SS_seq, \@pattern);
				#We increase it to avoid multiple treatments
				$GC_test++;
			}
		}
		#We close the file
		close(F_IN);
		#We empty the hash table for next treatment
		%ID_reads_file = ();
	}
	#We finally print the GC_RF data to recreate an Infernal alignment
	printf(F_OUT ">#=GC_SS_cons\n".$GC_SS_seq.">#=GC_RF\n".$GC_RF_seq);
	close(F_OUT);
	#We pipe the name of the created file (the result file) for the main program
	return("Align_$global_file");
}
# End of the function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

1;
