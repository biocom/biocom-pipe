#=======================================================================================================
# PACKAGE NAME: Dereplication
#    FUNCTIONS:	 - Dereplication (Launch the dereplication itself),
#
# REQUIREMENTS:  Perl language only
#       AUTHOR:  Sebastien TERRAT <sebastien.terrat@dijon.inra.fr>
#      COMPANY:  GenoSol platform (UMR 1347 INRA/uB/AgroSup)
#      VERSION:  5.0
#    REALEASED:  03/11/2011
#     REVISION:  03/06/2014
#=======================================================================================================

# Name of the package defined for the main program
package PerlLib::Dereplication;

# Needed libraries
use strict;
use warnings;

#Needed to shuffle all reads before the dereplication step
use List::Util 'shuffle';

#=======================================================================================================
#=======================================================================================================
# MAIN FUNCTION CALLED BY THE MAIN PROGRAM
#=======================================================================================================
#=======================================================================================================
# FILE:			Dereplication.pm (called by the main program)
# USAGE:		PerlLib::Preprocessing::Dereplication(ARGUMENTS)
# ARGUMENTS:	- $_[0]: the input file,
#				- $_[1]: the input folder,
#				- $_[2]: the output folder.
#
# DESCRIPTION:	Sequences strict dereplication for every input file given as input and create one file
#			  in the /Result_files directory with dereplicated sequences classed by decreasing
#			  abundance. The name of the new file is starting by derep_name_file.fasta.
#			  This program was developed to parallelize tasks, as one cpu is used for one treated
#			  file to fast the analysis. As numerous files needs this treatment, we need to fast the
#			  analysis, using a C core to increase the efficiency of the comparison program.
#			  Another potentially increase of calculation can be the splicing of files to fast the
#			  analysis, need to think about it.
#
#			  Modifs 3.0 (10/04/2012): This program was modified to be included in the workflow to fast
#			  the analysis. All modified steps are described in the program (e.g.: using the name of the
#			  input and a different location for the output file ans summary file, deletion of the
#			  parallelization, as it was done by the main PERL program).
#
#			  Modifs 3.5 (25/10/2012): We correct and improve some parts of the program to fast the
#			  analysis. And we also modify the program to efficiently create a file for the beta
#			  diversity analysis if needed. To do this, if the treated file is named:
#			  "Concatenated_reads.fasta.fasta", a particular treatment will be realized to define also
#			  a new file containing details of all dereplicated reads created.
#			  See details in the program.
#
#			  Modifs 3.6 (07/11/2012): These modifications have been done to store results in the
#			  dedicated file for the case of the Concatenated file. Moreover, we add another step to
#			  shuffle all reads before the dereplication step, especially for the Concatenated reads,
#			  as we then avoid with this step potential biases due to the previous step.
#			  See details in the program.
#
#			  Modifs 3.7 (17/12/2012): A small correction was realized to avoid the problem of empty
#			  lines after the sequence (see in the program in the LOOP00). This small problem can
#			  cause other problems and errors in further steps.
#
#			  Modifs 4.0 (08/04/2013): We completely rewrite the program based on the hash table
#			  philosophy. Indeed, using each sequence itself as the key of the hash table fast the
#			  analysis and simplify it (see program for details).
#
#			  Modifs 4.2 (18/11/2013): We modify the program to take into account sequences without
#			  the good format of the descriptive line for the pipeline. To treat this, we just add
#			  a test step to verify if reads are formatted or not. We also integrate a slight
#			  modification to the program to take into account also dereplicated reads.
#
#			  Modifs 4.3 (18/12/2013): We just add a step to delete all non wanted-characters,
#			  particularly those from Windows systems like the \r character, generating errors during
#			  the dereplication step.
#
#			  Modifs 4.4 (26/02/2014): We modify the program to delete more non wanted-characters,
#			  particularly those from other sequencing systems like Illumina, and generating errors
#			  during the dereplication step.
#
#			  Modifs 5.0 (03/06/2014): We add some improvements in the program using already defined
#			  PERL libraries and functions, such as PrintScreen or ReadInputFastaFile to simplify the
#			  program itself.
#
#			  Modifs 5.2 (19/03/2018): We modified the Dereplication program to take into account the
#			  fact that dereplicated reads can be used for dereplication (independent files merged
#			  before the dereplication for the global analysis). This particular case we due to a
#			  modification of the main program allowing the merging of dereplicated reads.
#
#				Modifs 5.3 (24/10/2019): A minor modification was done to manage the specific fact that
#				this step was critical. More precisely, here, each sample is checked based on its number
#				of reads, and let passed, or stopped as a too low number of reads was available (less than 9000).
#				For cases with higher number of reads (between 9000 and 10000), a warning was given to the user.
#				To do this, the Dereplication program send back to the main program the number of reads with the
#				sample name.
#
#=======================================================================================================
sub Dereplication
{
	#3.5: We catch the name of the file needed treatment given by the main program and also other parameters
	#given as input
	my $file = $_[0];
	my $folder_input = $_[1];
	my $folder_output = $_[2];
	#4.0: Defined variables
	my $element = "";
	my $file_qual = "";
	my $keys = "";
	my $ID = "";
	my $ref_array = "";
	my $sample = "";
	my %dereplication = ();
	my @tmp = ();
	my @tmp2 = ();
	my $i = 0;
	my $nbseq_tot = 0;
	my $nb_seqs_occur = 0;
	my $length = 0;
	my $boolean_qual = 0;
	my $percent_unique = 0;
	my $check_derep = 0;

#=======================================================================================================
# First step, store all sequences in the @tmp for further analysis
#=======================================================================================================

	# 5.0: Print for the user information that the program is started
	PerlLib::Miscellaneous::PrintScreen("DEREPLICATION", $file ,"started");

	if(($file =~ m/Concatenated\_reads\.fasta\.fasta/)&&($folder_input =~ m/Raw_data/))
	{
		#We open the dedicated concatenated file stored in Raw_data
		open(F_FASTA, "Temporary_files/$file")|| die "==> File Not Found: $file\n<==";
		$file =~ s/.fasta//;
		$file =~ s/.fna//;
		$file_qual = $file.".qual";
		$file = $file.".fasta";
	}
	else
	{
		# 5.0: Here, we verify the chosen folder and file, and return all needed names and data for the given file.
		($file, $file_qual, $boolean_qual, *F_FASTA, *F_QUAL) = PerlLib::Miscellaneous::OpenFastaQualFile($folder_input, $file);
	}

	# 5.0: Here, we launch the function to catch and store data from an input file in Fasta format, and
	# give back all information stored in an array.
	$ref_array = PerlLib::Miscellaneous::ReadInputFastaFile(*F_FASTA, "");
	@tmp = @{$ref_array};

	# 3.6: We then shuffle all the array with all reads to efficiently avoid potential biases.
	@tmp = shuffle(@tmp);

#=======================================================================================================
# Second step, we treat all reads efficiently using a hash table with the sequence itselves used as keys
# of the hash table.
#=======================================================================================================

	#4.0: For each read (ID and sequence) found in the file
	LOOP01:foreach $element (@tmp)
	{
		#4.0: We first split the $element based on the "\n" character to split the ID and the sequence
		@tmp2 = split("\n", $element);
		#3.6, 4.3 and 4.4: We delete not wanted characters
		$tmp2[0] =~ s/^>//;
		$tmp2[0] =~ s/\r//g;
		#$tmp2[0] =~ s/\s//g;
		#4.2: We first verify if the read was not already dereplicated
		if($tmp2[0] =~ m/\_(\d+)\_length\=/)
		{
			#4.0: We add to the hash table the sequence itself ($tmp2[1]) as key to fast the comparison, and
			#we concatenate the ID sequence itself to other values for this particular key (here the sequence).
			$dereplication{$tmp2[1]} .= $tmp2[0]." ";
			#3.5: We catch the number of total reads
			$nbseq_tot += $1;
			#5.2: Needed value to check if values are already dereplicated (cas for example for the global treatment)
			$check_derep = 1;
			next LOOP01;
		}
		#4.2: We test for the existence of the length element in the read
		elsif($tmp2[0] =~ m/.*\slength\=/)
		{
			#3.6: We add in the descriptive line the unique read information _1_ instead of the space between
			#the ID and the length of the read
			$tmp2[0] =~ s/\s/\_1\_/;
			#3.5: We catch the number of total reads
			$nbseq_tot ++;
		}
		#4.2: If the read if not correctly formatted
		else
		{
			$tmp2[0] =~ s/\://g;
			$tmp2[0] =~ s/\-//g;
			$tmp2[0] =~ s/\|//g;
			$tmp2[0] =~ s/\_//g;
			$tmp2[0] =~ s/\s//g;
			#4.2: We catch the length of the read
			$length = length($tmp2[1]);
			#4.2: We format the line to add the dereplication info needed for all further steps
			$tmp2[0] .= "_1_length=".$length;
			#3.5: We catch the number of total reads
			$nbseq_tot ++;
		}
		#4.0: We add to the hash table the sequence itself ($tmp2[1]) as key to fast the comparison, and
		#we concatenate the ID sequence itself to other values for this particular key (here the sequence).
		$dereplication{$tmp2[1]} .= $tmp2[0]." ";
	}
	#4.0: We delete the @tmp array
	@tmp = ();
	#4.0: We determine the number of dereplicated reads
	$nb_seqs_occur = keys(%dereplication);

	$percent_unique = sprintf("%.2f", (100*$nb_seqs_occur/$nbseq_tot));

	#3.6: We open the needed file to store all summary results (we test for the good file
	if ($file =~ m/Concatenated\_reads\.fasta\.fasta/)
	{
		open(F, ">>Summary_files/Global_Analysis/Dereplication_summary_results.txt")|| die "==> File Not Found: Dereplication_summary_results.txt<==";
	}
	else
	{
		open(F, ">>Summary_files/Dereplication_summary_results.txt")|| die "==> File Not Found: Dereplication_summary_results.txt<==";
	}
	#Function to obtain a cleaned sample name for the Summary file
	$sample = PerlLib::Miscellaneous::ExtractSampleFromFileName($file);
	#Block the access of the file for other processes during the addition of data
	flock(F,2);
	printf (F "$sample\t$file\t$nbseq_tot\t$nb_seqs_occur\t$percent_unique\n");
	#Give back the access for other child processes
	flock(F,8);
	close F;

	#4.1: Here, we test each filename to create and/or open the needed file
	if (($folder_input eq "Temporary_files") && ($file =~ m/Concatenated\_reads\.fasta\.fasta/))
	{
		#3.5: We open the result file to store the dereplicated reads.
		open (F2, ">Result_files/$folder_output/Derep_Concatenated_reads.fasta.fasta")|| die "==> Cannot create: Derep_$file<==";
	}
	#4.1: Here, we test each filename to create and/or open the needed file
	else
	{
		#3.5: We open the result file to store the dereplicated reads.
		open (F2, ">Result_files/$folder_output/Derep_$file")|| die "==> Cannot create: Derep_$file<==";
	}

	#4.1: Here, we test each filename to create the needed file for the Global_Analysis step
	if ($file =~ m/Concatenated\_reads\.fasta\.fasta/)
	{
		#3.5: If the Global_Analysis step is done, we create the dedicated summary file and store detailed results of dereplication
		open (F3, ">Summary_files/Global_Analysis/Derep_details_Concatenated_reads.fasta.fasta")|| die "==> Cannot create: Derep_details_Concatenated_reads.fasta.fasta<==";
	}
	else
	{
		#3.6: If the Global_Analysis step is done, we create the dedicated summary file and store detailed results of dereplication
		open (F3, ">Temporary_files/Derep_details_$file")|| die "==> Cannot create: Derep_details_$file<==";
	}

	#4.0: For each dereplicated read (corresponding to each key in the hash table)
	LOOP02:foreach $keys (keys(%dereplication))
	{
		#4.0 : We split the value (all IDs based on the space character)
		@tmp = split(" ", $dereplication{$keys});
		#5.2: We first verify if the read was not already dereplicated
		if($check_derep == 1)
		{
			#5.2: If it is the case, we treat it to avoid errors, first we count the real
			#number of reads
			foreach $ID (@tmp) { $ID =~ m/\_(\d+)\_length\=/; $i += $1; }
			#5.2: Based on that, we modified the ID of the dereplicated read
			$ID = $tmp[0];
			$ID =~ s/\_\d+\_length\=/\_$i\_length\=/;
			#We then print needed data in both files
			printf(F2 ">$ID\n$keys\n");
			printf(F3 "$ID\t");
			$i = 0;

			#5.2: Then, we create the dereplicated details needed for further steps
			foreach $ID (@tmp)
			{
				#5.2: Indeed, each read will be printed corresponding to his number of reads
				$ID =~ m/\_(\d+)\_length\=/;
				#5.2: To do that, we check its number of occurrences, and use it to print data
				if($1 == 1) { printf(F3 "$ID "); }
				else
				{
					#5.2: We store the number of occurrences and use it in a while LOOP
					$nb_seqs_occur = $1;
					$ID =~ s/\_\d+\_length\=/\_1\_length\=/;
					while($nb_seqs_occur != 0) { printf(F3 "$ID "); $nb_seqs_occur--; }
				}
			}
			printf(F3 "\n");
		}
		else
		{
			#4.0: We count the number of reads for this unique sequence
			$i = $#tmp + 1;
			#4.0: If the value is higher than 1
			if ($i >= 2)
			{
				#4.0: We modify the ID of the first read found
				$tmp[0] =~ s/\_1\_/\_$i\_/;
				#4.0: We print the result to create the dereplicated file needed in the temporary_files folder
				#This result corresponds to the list of all unique reads represented by the dereplicated read in the
				#dereplicated result file
				printf(F3 "$tmp[0]\t$dereplication{$keys}\n");
			}
			#4.0: We print the new ID and the sequence itself in the result file
			printf(F2 ">$tmp[0]\n$keys\n");
		}
	}
	#4.0: We close result and temporary files
	close(F2);
	close(F3);

	#5.2: Here, particular case to avoid the presence of two files after CONCATENATION of all independent files for the global analysis
	if(($file =~ m/Concatenated\_reads\.fasta\.fasta/)&&($folder_input eq "DEREPLICATION")&&($folder_output eq "DEREPLICATION")) { unlink("Result_files/$folder_input/$file"); }

	#5.0: Print for the user information that the program is started
	PerlLib::Miscellaneous::PrintScreen("DEREPLICATION", $file ,"terminated");
	#3.0 and 5.3: We pipe the name of the created file (the result file) for the main program
	#and also the number of reads found in the treated file for the ThresholdCheckings library
	return ("Derep_$file", $nbseq_tot);
}
1;
