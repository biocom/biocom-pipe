#=======================================================================================================
# PACKAGE NAME: Clustering
#    FUNCTIONS:	- OrganizeFastaData (Organize and sort fasta data for subsequent analyses),
#		 	- ChoiceOfClustering (Used to define the clustering function based on user choices),
#		 	- SummarizeClusteringData (Create and fill the summary files for the user),
#		 	- BuildIndexFile (Read a file and create an index to treat a file without memory),
#		 	- CatchFileDataFromIndex(Extract a line from a file using its position from an index).
#
# REQUIREMENTS:  Perl language only
#       AUTHOR:  Sebastien TERRAT <sebastien.terrat@dijon.inra.fr>
#      COMPANY:  GenoSol platform (UMR 1347 INRA/uB/AgroSup)
#      VERSION:  3.2
#    REALEASED:  18/05/2011
#     REVISION:  22/05/2015
#=======================================================================================================

# Name of the package defined for the main program
package PerlLib::ClusteringWithIndex;

# Needed libraries
use strict;
use warnings;
# Library containing the C cores and functions used for the clustering
use PerlLib::ClusteringCFunctions;

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function defined to create one array containing organized data for subsequent analyses needed for the
# clustering step. This program takes as input only an array with reads in fasta format. As output, 
# it gives an array containing the IDs of reads, with their abundances from the dereplication step 
# associated in one little array, and also the number of reads treated.
sub OrganizeFastaData
{
	#Needed variables
	my $nb_seq = 0;
	my $read = "";
	my $ID = "";
	my @tmp = ();
	my @IDs = @{$_[0]};
	my @descr = ();
	
	# Loop to treat all reads from the array given as input
	LOOP:foreach $read (@IDs)
	{
		# We avoid some not needed reads
		if ($read =~ m/#=GC_RF/) { next LOOP; }
		elsif ($read =~ m/#=GC_SS_cons/) { next LOOP; }
		else
		{
			# We clean the read itself
			$read =~ s/>//;
			# We store the descriptive line to use it in further steps
			$ID = $read;
			# Split the line to store it in @tmp2 based on the '_' character
			@tmp = split("_",$ID);
			# Count the number of sequences (not dereplicated to give in results file)
			$nb_seq += $tmp[1];
			# We finally store all needed data (number of dereplicated reads, name of the sequence)
			push(@descr, [$tmp[1],$ID]);
		}
	}	
	# Reorganizing the table @tmp defined previouly by decreasing abundance based on the occurences
	@descr = reverse sort { $a->[0] <=> $b->[0] } @descr;
	# We finally return two references and a variable
	return (\@descr, $nb_seq);
}
# End of the function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function needed to determine which C function will be used to realize the clustering based on the 
# defined parameters of the user. First, this function check if the user choose to ignore or not the 
# homopolymers differences, and then the differences at the beginning of the sequences if their 
# respective lengths are different. This function gives as output a reference for the corresponding 
# function.
sub ChoiceOfClustering
{
	#Needed variables
	my $choice_hm = uc($_[0]);
	my $choice_len = uc($_[1]);
	my $ref = "";
	# We first test for homopolymer differences
	if ($choice_hm eq "YES")
	{
		# We then test for differences of length
		if ($choice_len eq "YES") { $ref = \&PerlLib::ClusteringCFunctions::c_compare_hm_len; }
		else { $ref = \&PerlLib::ClusteringCFunctions::c_compare_hm; }
	}
	# We first test for homopolymer differences
	else
	{
		# We then test for differences of length
		if ($choice_len eq "YES") { $ref = \&PerlLib::ClusteringCFunctions::c_compare_len; }
		else { $ref = \&PerlLib::ClusteringCFunctions::c_compare; }
	}
	# We finally return the reference of the adapted clustering function
	return $ref;
}
# End of the function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function dedicated to fill summary files for the user after the clustering step. It also delete some
# temporary files at the end of the program. It takes as input three arguments (the @steps array, the 
# name of the treated file, and the summary file name).
sub SummarizeClusteringData
{
	#Needed variables
	my $nb_seq = 0;
	my $nb_clust = 0;
	my $i = 0;
	my $clustering = "";
	my $step_size = "";
	my @steps = @{$_[0]};
	my $file = $_[1];
	my $summary_file_name = $_[2];
	
	#We analyze all needed steps based on user choices
	foreach $i (@steps)
	{
		# We format data for the user lisibility
		$step_size = sprintf("%.2f", $i*100);
		$clustering = sprintf("%.3f", $i);
		# Test for the dedicated file for the GLOBAL_ANALYSIS step
		if ($file =~ m/Concatenated\_reads\.fasta\.fasta/)
		{
			# Create the needed file to store all summary results in the OUT directory
			open(F_SUM, ">>Summary_files/Global_Analysis/$summary_file_name"."$step_size.txt")|| die "==> File Not Found: Summary_files/Global_Analysis/$summary_file_name"."$step_size.txt<==";
		}
		else
		{
			# Create the needed file to store all summary results in the OUT directory
			open(F_SUM, ">>Summary_files/$summary_file_name"."$step_size.txt")|| die "==> File Not Found: Summary_files/$summary_file_name"."$step_size.txt<==";
		}
		# Open file needed to be treated
		open(F_IN, "Temporary_files/Clust_$file.txt")|| die "==> File Not Found: Clust_$file<==";
		# For every line of the file given by the user
		LOOP:while (<F_IN>)
		{
			# We search for the number of reads
			if ($_ =~ m/Total sequences\t(\d*)\s\n/) { $nb_seq = $1; }
			# We search also for the number of clusters at the chosen level by the user
			if ($_ =~ m/$clustering\t(\d*)/) { $nb_clust = $1; }
		}
		# Print all data in the result file
		printf(F_SUM "$file\t$nb_seq\t$nb_clust\n");
		# We empty variables to avoid errors
		$nb_seq = 0;
		$nb_clust = 0;
		# We close not needed files
		close F_SUM;
		close F_IN;
	}
	#2.5: Deletion of the temporary file created by the clustering
	unlink("Temporary_files/Clust_$file.txt");
}
# End of the function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function dedicated to build an index of the alignment file analyzed to avoid its storage in the memory
# and treat huge files (e.g. Illumina files or big datasets such as RMQS). This function is very simple
# and takes as input only the filehandle of the file. As output, it gives back two arrays, one hash 
# containing the IDs of the sequences and their position in the file in bytes, and an array (@data) 
# containing the IDs of the sequences essential for further steps.
sub BuildIndexFile
{
	#Needed variables
	my $file = $_[0];
	my $offset = 0;
	my $ID = "";
	my %data = ();
	my @data = ();
	my @offsets = ();

	#Loop to read the input file line by line
	while(<$file>)
	{
		#Return the current position in bytes for the given filehandle (so the end of the line) and 
		#store it in @offsets.
		$offset = tell($file);
		push(@offsets, $offset);
		#We must avoid empty lines to avoid a problem during the analysis and also alignment lines 
		#without sequences
		if (($_ eq "\n")||($_ =~ m/^#/)||($_ =~ m/\/\//)) { next; }
		#If the reading line starts with by the sequence ID
		else
		{
			#We delete the non wanted characters and store the ID in $ID
			$_ =~ m/([A-Za-z\_\=\d]*)\s+/;
			$ID = $1;
			#Store the sequence ID also in an array (@data) for further use
			push(@data, $ID);
			#We store the offset un bytes of the ID of the read
			$data{">".$ID} = $offsets[$#offsets - 1];
		}
	}
	#We return two types of arrays
	return (\@data, \%data);
}
# End of the function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function dedicated to read a line based on the filhandle of the opened file and moving it with the 
# previously constructed index. This function needs as input the index of the wanted line and the 
# filehandle where we can find the element. As output, the function gives back the line found.
sub CatchFileDataFromIndex
{
	#Needed variables
	my $index = $_[0];
	my $file = $_[1];
	my $line = "";
	my @tmp = ();
	
	#First, with seek(), we move the filhandle to the good position in the file using the index
	#from the constructed index.
	seek($file,$index,0);
	#We then read the line
	$line = <$file>;
	#We treat the line and give back it to the main program.
	$line =~ s/\s+/\n/;
	@tmp = split("\n", $line);
	#We return only the sequence itself
	return $tmp[1];
}
# End of the function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


#=======================================================================================================
#=======================================================================================================
# MAIN FUNCTION CALLED BY THE MAIN PROGRAM
#=======================================================================================================
#=======================================================================================================
# FILE:			Clustering.pm (called by the main program)
# USAGE:		PerlLib::Clustering::Clustering(ARGUMENTS)
# ARGUMENTS:	- $_[0]: the step name (RAW or CLEAN),
#				- $_[1]: the user choice for homopolymer differences (yes or no),
#				- $_[2]: the user choice for length differences or reads (yes or no),
#				- $_[3]: the name of the treated file,
#				- $_[4]: the maximum percentage of differences tolerated defined by the user ([0-100]),
#				- $_[5]: the step percentage defined by the user for the clustering ([0-100]),
#				- $_[6]: the input folder,
#				- $_[7]: the output folder,
#				- $_[8]: the name of the summary file.
#
# DESCRIPTION:	This program was developped to run on a multiprocessor computer to fast the analysis.
#		First, the program will read each file in the IN/ directory to treat them (aligned
#		sequence files from the INFERNAL tool). Then, for each found file, the program will
#		create one child process to fast the analysis. Each child analysis will then 
#		reorganize the aligned sequence file by decreased abundance of each dereplicated 
#		sequence and split descriptive lines and sequences itselves. After that, each 
#		sequence will be compared by others (using the C core to fast the analysis) to 
#		create clusters. To do that fastly, each child process will create grandchild
#		processes for each clustering step (as we treat the same file several times). All
#		results will be stored in two result files in the OUT/ directory for each treated
#		file. 
#		To fast the analysis, we use the fork() function to create child and grandchild 
#		processes (multiprocessors treatment) and a C core to compare sequences. We also
#		delete the step to delete similarities between sequences, as it blocks the program
#		for large files (more than 200000 sequences).
#
#		1.5 Modifications: This program was modified to be included in the workflow to fast
#		the analysis. All modified steps are described in the program (e.g.: using the 
#		name of the input and a different location for the output file and summary file, 
#		deletion of the parallelization, as it was done by the main PERL program).
#
#		1.6 Modifs (10/01/2013): We modify the loop based on the $dist_cutoff_step and the 
#		$dist_cutoff to do all needed steps (one or several) and to store all needed data. We
#		also add another test to avoid not needed analysis when $dist_cutoff and 
#		$dist_cutoff_step have the same value.
#
#		2.0 Modifs (03/07/2013): We completely redesign the clustering step to fast the 
#		analysis. Briefly, we use the same logic as CD-HIT named "greedy alignment". The 
#		principle is to read only one time the array, and create clusters during the 
#		analysis. See details in the program.
#
#		2.1 Modifs (22/07/2013): We modify the program to improve the realization of 
#		multiple clustering steps. Indeed, if the user chose a max of 5.0 and a clustering
#		step of 3.0, the program will now realize two clusterings : 3.0 and 5.0. Moreover,
#		we correct some errors due to the realization of multiple clustering steps.
#
#		2.5 Modifs (03/09/2014): The program was completely rewritten and modified to be
#	    	included in libraries. Moreover, as some steps of this program were used in other 
#		programs, some other functions have been used and integrated in the program.
#		Furthermore, we simplify four different programs in two libraries (Clustering and 
#		ClusteringCFunctions) to easily maintain the program.
#
#		3.0 Modifs (22/04/2015): The program was modified to integrate a solution to treat
#		huge files that not fill in memory (e.g. RMQS, or Illumina data). To do this, we 
#		use a new function : Tie::File, and implement a new function called TieInputFastaFile.
#		See details in the program.
#
#		3.2 Modifs (22/05/2015): We decided to use an other approach to treat huge files that
#		not fill in memory (e.g. RMQS, or Illumina data). We finally avoid the use of the 
#		Tie::File package, as this one lean on an increasing of the memory usage when lines are
#		read several times. So, we decided to construct an index of the file to find the needed
#		lines without filling the memory.
#
#=======================================================================================================
sub Clustering
{
	#Defined variables
	my ($i, $j, $h) = (0,0,0);
	my $count_diff = 0;
	my $nb_seq_tot_derep = 0;
	my $length01 = 0;
	my $length02 = 0;
	my $max = 0;
	my $nb_OTUs = 0;
	my $boolean_qual = 0;
	my $clust_function = "";
	my $file_qual = "";
	my $seq01 = "";
	my $seq02 = "";
	my $ref_array = "";
	my $data_clust = "";
	my %ID_index = ();
	my @ref_array = ();
	my @Clusters_nb_reads = ();
	my @descr = ();
	my @steps = ();
	
	my $step = uc($_[0]);
	my $choice_hm = $_[1];
	my $choice_len = $_[2];
	my $file = $_[3];
	my $dist_cutoff = $_[4] + 0;
	my $dist_cutoff_step = $_[5] + 0;
	my $folder_input = $_[6];
	my $folder_output = $_[7];
	my $file_summary_name = $_[8];
	
#=======================================================================================================
# First step, we read the alignment file, and we store in a hash table the aligned_reads based on the
# descriptive line without the ">" character. We store also the corresponding number of occurences for
# each read in @descr, to sort the table @descr based on it descreased abundance.
#=======================================================================================================

	#2.5: Print for the user information that the program is started
	PerlLib::Miscellaneous::PrintScreen($step." CLUSTERING", $file, "started");
	# Here, we verify the chosen folder and file, and return all needed names and data for the given file.
	($file, $file_qual, $boolean_qual, *F_FASTA, *F_QUAL) = PerlLib::Miscellaneous::OpenFastaQualFile($folder_input, $file);

	#3.2: Function dedicated to read a file and create an index to find easily all lines. This program avoid the
	#storage of the whole file in memory.
	@ref_array = &BuildIndexFile(*F_FASTA);
	@descr = @{$ref_array[0]};
	%ID_index = %{$ref_array[1]};

	#2.5: Here, we launch the function to organize data for the clustering step.
	($ref_array, $nb_seq_tot_derep) = &OrganizeFastaData(\@descr);
	@descr = @{$ref_array};
		
	#Determine the number of tolerated differences to cluster sequences and the step size
	$dist_cutoff = $dist_cutoff/100;
	$dist_cutoff_step = $dist_cutoff_step/100;

	#1.5: Opening needed files to to store results (details and summary)
	$file =~ m/(\w*)\./;
	open (FILE_R_DETAILS, ">Result_files/$folder_output/Clust_$file.clust");
	open (FILE_R, ">Temporary_files/Clust_$file.txt");
	#Print needed data in files for further steps
	printf (FILE_R_DETAILS "File(s):\t$file\nSequences:\t$nb_seq_tot_derep \n\n");
	printf (FILE_R "Total sequences\t$nb_seq_tot_derep \n\nCutoff\tClusters(OTUs)\n");
	#Close files to give them for grandchild processes
	close (FILE_R_DETAILS);
	close (FILE_R);

#===================================================================================================
# Second step, we treat and cluster all reads, based on two loops (LOOP03 and LOOP04), and we
# print results in the dedicated result and summary files. 
#===================================================================================================

	#2.5: We create here the @steps array to realize all clustering steps with a dedicated function.
	$ref_array = PerlLib::Miscellaneous::ComputeClusteringSteps($dist_cutoff, $dist_cutoff_step);
	@steps = @$ref_array;
	#2.5: We then launch the choice of the clustering method based on user choices.
	$clust_function = &ChoiceOfClustering($choice_hm, $choice_len);
	#2.1: Loop to realize the analysis for each file at different clustering steps needed and to do 
	#all clustering steps
	LOOP01:foreach $h (@steps)
	{
		#3.2: We create a temporary file to store the list of all IDs joined in the same cluster
		open(F_TEMP, ">Temporary_files/$file.$descr[$i][1]");
		printf(F_TEMP "$descr[$i][1]");
		close(F_TEMP);
		$nb_OTUs = 1;
		#2.1: We also store here the number of reads in the cluster, and its composition (the IDs).
		push(@Clusters_nb_reads, [$descr[0][0],$descr[0][1]]);		
		#2.0: For each read in @descr, so for each read needed to be clustered
		LOOP02:for($i=1;$i<=$#descr;$i++)
		{
			#2.0: And for each already defined seed cluster
			LOOP03:for($j=0;$j<=$#Clusters_nb_reads;$j++)
			{
				#2.0: We first catch the length of the seed
				$Clusters_nb_reads[$j][1] =~ m/length=(\d*)/;
				$length01 = $1;
				#2.0 We also catch the length of the read we want to cluster
				$descr[$i][1] =~ m/length=(\d*)/;
				$length02 = $1;
				#2.0: Based on these two lengths, we compute the maximum of tolerated differences
				if ($length01<=$length02) { $max = $length01*$h; }
				else { $max = $length02*$h; }
				#3.2: Here, we store reads after uppercasing in two variables to use them in the 
				#clustering analysis.
				$seq02 = uc(&CatchFileDataFromIndex($ID_index{">".$Clusters_nb_reads[$j][1]}, *F_FASTA));
				$seq01 = uc(&CatchFileDataFromIndex($ID_index{">".$descr[$i][1]}, *F_FASTA));
				#2.0: We determine the number of differences between the two aligned reads using the C program developed
				$count_diff = &$clust_function($seq02, $seq01, $max);
				#2.0: We then compute this number of differences based on the length of the two compared reads
				if ($length01<=$length02) {$count_diff = $count_diff/$length01;}
				else {$count_diff = $count_diff/$length02;}
				#2.0: If the $count_diff is lower than the chosen threshold
				if ($count_diff <= $h)
				{
					#3.2: We add the ID to the defined cluster
					open(F_TEMP, ">>Temporary_files/$file.$Clusters_nb_reads[$j][1]");
					printf(F_TEMP " $descr[$i][1]");
					close(F_TEMP);
					#2.0: We also add the number of occurences to define the size of the cluster
					$Clusters_nb_reads[$j][0] += $descr[$i][0];
					#2.0: We then treat the next read
					next LOOP02;
				}
				#2.0 : If the $count_diff is higher than the chosen threshold, we then compare the read to the next seed
				else { next LOOP03; }
			}
			#3.2: If the read was shown to be different from all known seeds, we create a new seed, creating a new
			#temporary file
			open(F_TEMP, ">Temporary_files/$file.$descr[$i][1]");
			printf(F_TEMP "$descr[$i][1]");
			close(F_TEMP);
			$nb_OTUs++;
			#2.0 : We also add in @descr the number of occurences of the seed and its descriptive line
			push(@Clusters_nb_reads, [$descr[$i][0],$descr[$i][1]]);
			#2.0: We then treat the next read
			next LOOP02;
		}
		# We then define the needed dissimilarity value
		$j = sprintf ("%0.3f", $h);
		#1.5: Open needed files to store data (appending in each file)
		open (FILE_R, ">>Temporary_files/Clust_$file.txt");
		#Print needed data in result files (corresponding to one clustering step)
		printf (FILE_R "$j\t$nb_OTUs\n");
		close (FILE_R);
	
		#1.5: Open needed files to store data (appending in each file)
		open (FILE_R_DETAILS, ">>Result_files/$folder_output/Clust_$file.clust");
		#Print needed data in result files (corresponding to one clustering step)
		printf (FILE_R_DETAILS "distance cutoff:\t$j\nTotal Clusters:\t$nb_OTUs\n");
		#2.0: We then print needed results in the result file.
		LOOP04:for($j=0;$j<=$#Clusters_nb_reads;$j++)
		{
			#3.2: We read the temporary file to catch the list of all reads in the cluster
			open(F_TEMP, "Temporary_files/$file.$Clusters_nb_reads[$j][1]");
			$data_clust = <F_TEMP>;
			close(F_TEMP);
			#3.2: We delete the temporary file
			unlink("Temporary_files/$file.$Clusters_nb_reads[$j][1]");
			#3.2: We finally print the needed data in the result file
			printf(FILE_R_DETAILS "$j\t$file\t$Clusters_nb_reads[$j][0]\t$data_clust\n");
		}
		printf (FILE_R_DETAILS "\n");
		close (FILE_R_DETAILS);
		#2.1: Needed empty variables to define efficiently all clusters
		@Clusters_nb_reads = ();
	}
	#2.5: We launch the SummarizeClusteringData program to summarize all results in clear files
	&SummarizeClusteringData(\@steps, $file, $file_summary_name);
	#2.5: Print for the user information that the program is started
	PerlLib::Miscellaneous::PrintScreen($step." CLUSTERING", $file, "terminated");
	#3.0: We finally close the filehandle as we did not need it anymore
	close(F_FASTA);
	# Needed return name for the main program.
	return "Clust_$file.clust";
}
# End of the function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
1;
