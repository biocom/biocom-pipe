#=======================================================================================================
# PACKAGE NAME:	ManagingDirectories
#    FUNCTIONS:	- DirectoriesCleaning (Deletion of all files and folders in a defined directory),
#			- ChosenStepsList (Store all steps chosen by the user, based on the Input.txt file.),
#			- DirectoriesCreation (Create all folders needed for the treatment),
#			- FindDirectory (Find in the array given as input the good directory name),
#			- DeleteDirectory (Deletion of a particular directory).
#
#			Modifs 1.1 (17/08/2018): Addition of one test in the DirectoriesCleaning function to
#			manage the potential fact of the absence of three folders (Result_files, Summary_files
#			and Temprorary_files). If they are not here, they will be created by the pipeline.
#
# REQUIREMENTS:	Perl language only
#       AUTHOR:	Sebastien TERRAT <sebastien.terrat@inra.fr>
#      COMPANY:	UMR 1347 Agroécologie
#      VERSION:	1.1
#    REALEASED:	16/04/2014
#     REVISION:	17/08/2018
#=======================================================================================================

# Name of the package defined for the main program
package PerlLib::ManagingDirectories;

# Needed libraries
use strict;
use warnings;
#Needed CPAN to delete a not empty directory (with the rmtree function)
use File::Path;

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function needed to delete all elements in some directories before the start of a new analysis. Take
# as input the name of the directory that needs to be cleaned.
sub DirectoriesCleaning
{
	# Needed empty variable
	my $file = "";
	# Test to check if the folder exists to clean it
	if(-d $_[0])
	{
		# We open the dedicated directory given as argumenty by the function
		opendir (DIR, "$_[0]/") || die "can't open the directory $_[0]: $!";
		# We then read and catch all file names and sub-directories in the directory
		LOOP01:while (defined($file = readdir(DIR)))
		{
			# We delete all files
			if (-f "$_[0]/$file")
			{
				unlink("$_[0]/$file");
			}
			# We also delete all sub-directories found except the files "." and ".."
			elsif(-d "$_[0]/$file")
			{
				if ($file =~ m/^\.\.?$/) { next LOOP01; }
				else { rmtree(["$_[0]/$file"]); }
			}
		}
		# We finally close the function
		closedir DIR;
	}
	#If the folder is not here, we create it to easily store and treat data
	else { mkdir("$_[0]"); }
}
# End of the function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function needed to find and store all steps chosen by the user, based on the Input.txt file. Give back
# all steps stored in an array.
sub ChosenStepsList
{
	# Needed empty variables
	my $program_name = "";
	my @steps = ();
	my @tmp = ();
	# We open the INPUT file
	open(F_Input, "Input.txt")|| die "==> File Not Found: Input.txt<==";
	# We read the file
	LOOP01:while (<F_Input>)
	{
		# We treat particular formats for the Input file
		$_ =~ s/\r//;
		$_ =~ s/\n//;
		# If we found the name of the program, we catch its name
		if ($_ =~ m/###(.*)###/)
		{
			$program_name = $1;
			#We then catch the line corresponding to the user choice
			$_ = <F_Input>;
			#We treat particular formats for the Input file
			$_ =~ s/\r//; $_ =~ s/\n//;
			@tmp = split("\t", $_);
			#We store the name of the stp in the dedicated array @steps
			if(uc($tmp[1]) eq "YES") { push(@steps, $program_name); }
			#Else, we delete the name of the step
			else { $program_name = ""; }
		}
	}
	# We close the INPUT file
	close F_Input;
	# We finally return the reference of the created array
	return (\@steps);
}
# End of the function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function needed to create all directories corresponding to chosen steps by the user. Call the function
# ChosenStepsList to know all chosen steps, and then define and create directories. Give as output two
# elements : if the dereplication step is done, and the list of created folders.
sub DirectoriesCreation
{
	# Needed empty variables
	my $ref_array = "";
	my $step = "";
	my $check_preprocessing_demultiplexing = 0;
	my $derep_done = 0;
	my @tmp_folders = ();

	# We first catch all steps chosen by the user
	$ref_array = &ChosenStepsList;
	@tmp_folders = @{$ref_array};
	# Test to avoid the treatment of an empty array (the user chose 0 steps).
	if($#tmp_folders == -1) { return (\@tmp_folders); }
	#Check if both steps (Demultiplexing and Preprocessing is done or not)
	LOOP00:foreach $step (@tmp_folders)
	{
		if($step eq "PREPROCESSING") { $check_preprocessing_demultiplexing++; }
		if($step eq "PREPROCESS_MIDS") { $check_preprocessing_demultiplexing++; }
	}

	#For each step, we first check to know if we need to do the dereplication step or not
	LOOP01:foreach $step (@tmp_folders)
	{
		#Test for the dereplication step, as it is essential for these steps.
		if(($step eq "RAW_INFERNAL_ALIGNMENT")||($step eq "RAW_CLUSTERING")||($step eq "HUNTING")||($step eq "RECOVERING")||
		($step eq "RANDOM_CLEANED")||($step eq "TAXONOMY")||($step eq "CLEAN_INFERNAL_ALIGNMENT")||($step eq "CLEAN_CLUSTERING")||
		($step eq "COMPUTATION")||($step eq "GLOBAL_ANALYSIS")||($step eq "RECLUSTOR")||($step eq "UNIFRAC_ANALYSIS"))
		{
			push (@tmp_folders, "DEREPLICATION");
			$derep_done = 1;
			last LOOP01;
		}
	}
	#For each step, we first check to know if we need a folder or not
	LOOP02:foreach $step (@tmp_folders)
	{
		#Step to verify and to create empty folders as results are saved in the Summary_files directory
		if (($step =~ m/LENGTH/)||($step =~ m/EVAL/)) { next LOOP02; }
		elsif ($step =~ m/COMPUTATION/) { mkdir("Summary_files/Figures"); next LOOP02; }
		elsif(($step =~m/PREPROCESS_MIDS/)&&($check_preprocessing_demultiplexing == 2)) { next LOOP02; }
		else
		{
			#We need also to keep the name of the folder storing taxonomic data
			if ($step =~ m/TAXONOMY/)
			{
				mkdir("Summary_files/Figures");
				mkdir("Summary_files/Figures/Taxonomy_Pies");
				mkdir("Summary_files/Figures/Taxonomy_Barplots");
				mkdir("Summary_files/Figures/Taxonomy_Heatmaps");
			}
			if ($step =~ m/GLOBAL_ANALYSIS/)
			{
				mkdir("Summary_files/Global_Analysis");
				next LOOP02;
			}
			if ($step =~ m/RECLUSTOR/)
			{
				mkdir("Summary_files/ReClustOR");
				mkdir("Result_files/ReClustOR");
				next LOOP02;
			}
			if ($step =~ m/UNIFRAC_ANALYSIS/)
			{
				mkdir("Summary_files/Global_Analysis/Unifrac_data");
				next LOOP02;
			}
			if ($step =~ m/QUALITY_CLEANING/) { mkdir("Summary_files/Quality_Cleaning"); }
			#Creation of the folder itself
			mkdir("Result_files/$step");
		}
	}
	#We close the INPUT file
	close F_Input;

	#We finally return the reference of the created array to the main program
	return ($derep_done, \@tmp_folders);
}
# End of the function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function used to find in the array given as input the good directory to lauch efficiently all PERL and
# Python programs by the main program. As output, give back the name of the folder.
sub FindDirectory
{
	#Needed variables (the name of the searched directory, and the folders list)
	my $research = uc($_[0]);
	my @tmp_folders = @{$_[1]};
	#Needed empty variables
	my $element = "";
	my $chosen_folder = "None";
	LOOP:foreach $element (@tmp_folders)
	{
		if($element eq $research) { $chosen_folder = $element; last LOOP;}
	}
	return $chosen_folder;
}
# End of the function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function needed to delete all elements in a defined directory and the directory itself. This function
# takes as input the name of the directory that needs to be cleaned.
sub DeleteDirectory
{
	# Needed variables
	my $folder = $_[0];
	my $file = "";

	# We open the dedicated directory given as argumenty by the function
	if (-d "$folder/")
	{
		opendir (DIR, "$folder/") || die "can't open the directory $folder: $!";
		# We then read and catch all file names and sub-directories in the directory
		LOOP01:while (defined($file = readdir(DIR)))
		{
			# We delete all files
			if (-f "$folder/$file")
			{
				unlink("$folder/$file");
			}
			# We also delete all sub-directories found except the files "." and ".."
			elsif(-d "$folder/$file")
			{
				if ($file =~ m/^\.\.?$/) { next LOOP01; }
				else { rmtree(["$folder/$file"]); }
			}
		}
		# We finally close the function
		closedir DIR;
		if (-d "$folder/") { rmtree(["$folder"]); }
	}
	else { return 1; }
}
# End of the function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

1;
