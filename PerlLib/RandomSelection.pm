#=======================================================================================================
# PACKAGE NAME: RandomSelection
#    FUNCTIONS:	 - CatchData (Read input files),
#				 - InverseDereplication (Create unique reads after dereplication for random selection),
#				 - WriteSummaryData (write summary information into the dedicated file for the user),
#				 - FastDereplication (Function to do the dereplication of reads after random selection).
#
# REQUIREMENTS:  Perl language only
#       AUTHOR:  Sebastien TERRAT <sebastien.terrat@dijon.inra.fr>
#      COMPANY:  GenoSol platform (UMR 1347 INRA/uB/AgroSup)
#      VERSION:  1.0
#    REALEASED:  03/11/2011
#     REVISION:  10/12/2015
#=======================================================================================================

# Name of the package defined for the main program
package PerlLib::RandomSelection;

# Needed libraries
use strict;
use warnings;

#Needed to shuffle all reads before the random selection step
use List::Util 'shuffle';

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function defined to find the input folder, catch all needed (FASTA and/or QUAL files), and return all
# stored data in an array called @reads_fasta as output, with two file names (fasta and qual) and also
# the $boolean_qual information. This function needs as input only the name of the treated file and the
# name of the input folder.
sub CatchData
{
	#Needed variables
	my $file = $_[0];
	my $folder_input = $_[1];
	my $file_qual = "";
	my $boolean_qual = undef;
	my $ref_array = "";
	my $i = 0;
	my @reads_fasta = ();
	my @reads_qual = ();
	
	#Here, we verify the chosen folder and file, and return all needed names and data for the given file.
	($file, $file_qual, $boolean_qual, *F_FASTA, *F_QUAL) = PerlLib::Miscellaneous::OpenFastaQualFile($folder_input, $file);
	#Here, we launch the function to catch and store data from an input file in Fasta format, and 
	#give back all information stored in an array.
	$ref_array = PerlLib::Miscellaneous::ReadInputFastaFile(*F_FASTA, "");
	@reads_fasta = @{$ref_array};
	close(F_FASTA);
	#4.4: Based on the definition of the $boolean_qual, we add the quality elements to the @reads_fasta array
	if ($boolean_qual)
	{
		#Here, we launch the function to catch and store data from an input file in Fasta format, and 
		#give back all information stored in an array.
		$ref_array = PerlLib::Miscellaneous::ReadInputFastaFile(*F_QUAL, "QUAL");
		@reads_qual = @{$ref_array};
		close(F_QUAL);
		#4.2: Here, we concatenate the .fasta and the .qual to select them at the same time efficiently
		LOOP01:for($i=0;$i<=$#reads_fasta;$i++) { $reads_fasta[$i] .= "'##'".$reads_qual[$i]; }
		#4.2: Needed empty variables
		$i = 0;
		@reads_qual = ();
	}
	#We finally return all needed data
	return(\@reads_fasta, $file, $file_qual, $boolean_qual);
}
# End of the function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function defined to inverse the dereplication of reads to realize efficiently the random selection of
# data, even with associated quality sequences. This function takes as input an array of reads, and the
# information that a quality file exists or not. It gives as output another array, for random selection, 
# containing only unique sequences
sub InverseDereplication
{
	#Needed variables
	my @reads_fasta = @{$_[0]};
	my $boolean_qual = $_[1];
	my $data = "";
	my @derep_reads_fasta = ();
	my @fasta_qual = ();
	my @tmp = ();
	my @tmp2 = ();
	my @tmp_qual = ();
	my $i = 0;
	
	#4.4: Based on the definition of the $boolean_qual, we add the quality elements to the @reads_fasta array
	if ($boolean_qual)
	{
		#For every sequence of the file needed treatment
		foreach $data (@reads_fasta)
		{
			#Here, we first split fasta and quality data
			@fasta_qual = split("'##'", $data);
			#Here, we split the descriptive line and the sequence itself in @tmp
			@tmp = split("\n", $fasta_qual[0]);
			#We also split the descriptive line based on the "_" character to known the number of 
			#occurences of each dereplicated read
			@tmp2 = split("_", $tmp[0]);
			#Using this number of occurences, we create the good number of descriptives lines
			for($i=0;$i<$tmp2[1];$i++) { push(@derep_reads_fasta, $tmp2[0]."_1_".$tmp2[2]."\n"); }
			#Based also on the number of occurences, we add to each descriptive line the sequences, but also
			#all needed data for the quality file (new ID and quality sequence itself)
			for($i=0;$i<$tmp2[1];$i++)
			{
				@tmp_qual = split("\n", $fasta_qual[1]);
				$derep_reads_fasta[$#derep_reads_fasta-$i] .= $tmp[1]."'##'".$tmp_qual[1];
			}
		}
	}
	else
	{
		#For every sequence of the file needed treatment
		foreach $data (@reads_fasta)
		{
			#Here, we split the descriptive line and the sequence itself in @tmp
			@tmp = split("\n", $data);
			#We also split the descriptive line based on the "_" character to known the number of 
			#occurences of each dereplicated read
			@tmp2 = split("_", $tmp[0]);
			#Using this number of occurences, we create the good number of descriptives lines
			for($i=0;$i<$tmp2[1];$i++) { push(@derep_reads_fasta, $tmp2[0]."_1_".$tmp2[2]."\n"); }
			#Based also on the number of occurences, we add to each descriptive line the sequences
			for($i=0;$i<$tmp2[1];$i++) { $derep_reads_fasta[$#derep_reads_fasta-$i] .= $tmp[1]; }
		}
	}
	return(\@derep_reads_fasta);
}
# End of the function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function defined to write summary information into the dedicated file for the user. This program takes
# as input four variables: the output folder, the number of wanted reads, the file name and if the file
# was treated or just copied. This function gives no output.
sub WriteSummaryData
{
	#Needed variables
	my $folder_output = $_[0];
	my $seq_num_chosen = $_[1];
	my $file = $_[2];
	my $k = $_[3];
	
	#4.3: We add a test to determine where the random selection is realized to define efficiently the 
	#name of the summary file(s).
	if ($folder_output eq "RANDOM_PREPROCESS_MIDS")
	{
		#4.3: File to store results
		open(F_Summary, ">>Summary_files/Random_MIDs_$seq_num_chosen.txt")|| die "==> Can't create file of the Random step of analysis<==";
	}
	elsif ($folder_output eq "RANDOM_PREPROCESS")
	{
		#4.3: File to store results
		open(F_Summary, ">>Summary_files/Random_Preprocessed_$seq_num_chosen.txt")|| die "==> Can't create file of the Random step of analysis<==";
	}
	elsif ($folder_output eq "RANDOM_CLEANED")
	{
		#4.3: File to store results
		open(F_Summary, ">>Summary_files/Random_Cleaned_$seq_num_chosen.txt")|| die "==> Can't create file of the Random step of analysis<==";
	}
	#We block the access of the file for other processes
	flock (F_Summary,2);
	#We print that the treatment is done (even when the number of reads is not sufficient)
	if ($k == 0) { printf(F_Summary "No treatment for $file because of the low number of reads, the program will only copy the file\n"); }
	else { printf (F_Summary "Treatment of $file [OK]\n"); }
	#We give back access to the summary file
	flock (F_Summary,8);
	#Close file when it's finished
	close F_Summary;
}
# End of the function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

sub FastDereplication
{
	#Defined variables
	my @reads_fasta = @{$_[0]};
	my $data = "";
	my @tmp = ();
	my @tmp2= ();
	my %derep_reads = ();
	my $id = "";
	
	foreach $data (@reads_fasta)
	{
		@tmp = split("\n", $data);
		#We verify if the $descr variable is not empty, to then create here the ID of the hash table.
		#As we define unique reads, some reads originated from the same dereplicated read will have
		#the same ID, and the same read, so we can use a has table to fast the analysis and the
		#dereplication step
		if ($tmp[0] ne "") { $derep_reads{$tmp[0]."\n".$tmp[1]}++; }
	}
	@reads_fasta = ();
	
	#For each element stored in the hash table
	foreach $id (keys(%derep_reads))
	{
		#We split the information based on the "\n" character, and then 
		#the descriptive line by the "_" character
		@tmp = split("\n", $id);
		@tmp2 = split("_", $tmp[0]);
		#Finally, we redefine the descriptive line and we add the sequence itself to reconstruct the 
		#dereplicated file needed for further steps.
		push(@reads_fasta, $tmp2[0]."_".$derep_reads{$id}."_".$tmp2[2]."\n".$tmp[1]);
	}
	return(\@reads_fasta);
}
# End of the function
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#=======================================================================================================
#=======================================================================================================
# MAIN FUNCTION CALLED BY THE MAIN PROGRAM
#=======================================================================================================
#=======================================================================================================
# FILE:			RandomSelection.pm (called by the main program)
# USAGE:		PerlLib::RandomSelection::RandomSelection(ARGUMENTS)
# ARGUMENTS:	- $_[0]: the number of wanted randomly selected reads,
#				- $_[1]: the file name that will be treated,
#				- $_[2]: the input folder,
#				- $_[3]: the output folder.
#
# DESCRIPTION:	Random selection of sequences in the input file given by the user and create one file
#				in the result_file directory (the number is chosen by the user).
#				The name of the new file is starting by rdm_name_file.fasta
#				Optimization of this time-consuming program using a new algorithm.Moreover, addition 
#				of a new step with the shuffling of the complete table before random selection.
#
#			  4.2 Modifs (12/02/13): We rewrite a part of the program to treat in parallel the 
#			  quality .qual file associated to the fasta file. Indeed, the quality file is also 
#			  treated in parallel to potentially realize again a quality treatment and also correct 
#			  the single-singletons based on their alignment.
#		
#			  4.3 Modifs (29/03/13): We add a small 'if' test to determine the summary_file used to
#			  store the results depending of the user chosen step. See the program for details.
#
#			  5.0 Modifs (10/12/15): Two previously defined programs (Select_Random_cleaned_1.0.pl and 
#			  Select_random_4.4.pl) were merged and simplified in this new library called 
#			  RandomSelection. Some parts have been defined as functions, others simplified.
#
#=======================================================================================================
sub RandomSelection
{
	#We catch the arguments given by the main program and also other parameters given as input
	my $seq_num_chosen = $_[0];
	my $file = $_[1];
	my $folder_input = $_[2];
	my $folder_output = $_[3];
	#Needed variables
	my $file_qual = "";
	my $boolean_qual = undef;
	my $invderep = undef;
	my $ref_array = "";
	my @tmp = ();
	my @tmp2 = ();
	my @reads_fasta = ();
	my ($i,$j,$k,$x) = (0,0,0,0);
	my $rdm_value = 0;
	my $string_tmp = "";
	
	#Print for the user information that the program is started
	PerlLib::Miscellaneous::PrintScreen("RANDOM SELECTION", $file ,"started");
	
	#We then launch the first step of the analysis: the catch of all input data (fasta and or qual).
	($ref_array, $file, $file_qual, $boolean_qual) = &CatchData($file, $folder_input);
	@reads_fasta = @{$ref_array};
	
	#Shuffle the array @reads_fasta before random selection
	@tmp = shuffle(@reads_fasta);
	@reads_fasta = ();
	@reads_fasta = @tmp;
	@tmp = ();
	
	#Here, based on the information that the reads were dereplicated, we have to do this particular step
	if($reads_fasta[0] =~ m/\_(\d+)\_length\=/)
	{
		$invderep = "INVDEREP";
		$ref_array = &InverseDereplication(\@reads_fasta, $boolean_qual);
		@reads_fasta = @{$ref_array};
	}

	#Needed value to distinguish between copy files and real selection of random sequences.
	$k=1;
	#For the number of wanted sequences chosen by the user
	LOOP01:for($j=0;$j<$seq_num_chosen;$j++)
	{
		if ($#reads_fasta + 1 <= $seq_num_chosen)
		{
			$i = $#reads_fasta;
			#Change the value to 0 for copied files
			$k = 0;
			last LOOP01;
		}
		#Random number chosen between 0 and length of the table @reads_fasta stored in $rdm_value
		#This length is decreased at each step using $i
		$rdm_value = int(rand($#reads_fasta - $i));
		#The chosen sequence is then temporarily stored in $string_tmp
		$string_tmp = $reads_fasta[$rdm_value];
		#We replace the chosen cell of the table by the last cell of the table
		$reads_fasta[$rdm_value] = $reads_fasta[$#reads_fasta - $i];
		#Then, we store in the last cell of the table the $string_tmp
		$reads_fasta[$#reads_fasta - $i] = $string_tmp;
		#We finally increment $i
		$i++;
	}
	
	#Write summary information into the dedicated file for the user.
	&WriteSummaryData($folder_output, $seq_num_chosen, $file, $k);
	
	#Here, the loop to select data after the random selection from the global array
	for($x = ($#reads_fasta - $i);($x + $k) <= $#reads_fasta;$x++) { push(@tmp, $reads_fasta[$x]); }
	@reads_fasta = ();
	
	#Here, we launch the treatment for the fast strict dereplication of reads, after random selection,
	#only based on the ID of the reads.
	if($invderep) { $ref_array = &FastDereplication(\@tmp); @reads_fasta = @{$ref_array}; }
	else { @reads_fasta = @tmp; }
	
	#4.4: Create and open files to store selected sequences and potentially quality sequences
	open(F_OUT_FASTA, ">Result_files/$folder_output/Rdm_$file")|| die "==> Can't create Rdm_$file<==";
	#4.4: Based on the definition of the $boolean_qual, we treat the qual file
	if ($boolean_qual) { open(F_OUT_QUAL, ">Result_files/$folder_output/Rdm_$file_qual")|| die "==> Can't create Rdm_$file_qual<=="; }
	#For each element in the array @reads_fasta, stored in $seq at each step
	foreach $string_tmp (@reads_fasta)
	{
		#4.4: Based on the definition of the $boolean_qual, we treat the qual file
		if ($boolean_qual)
		{
			#4.2 : Here, after selection of the subset, we split the sequence and the quality itself, to
			#store them independently in two sequence files
			@tmp = split("'##'", $string_tmp);
			@tmp2 = split("\n", $tmp[0]);
			printf(F_OUT_FASTA $tmp2[0]."\n".$tmp2[1]."\n");
			#Test to verify if the reads were reversed-dereplicated or not, to Store the sequence and the quality
			#sequence in two files to use them in further steps of analysis
			if($invderep) { printf(F_OUT_QUAL $tmp2[0]."\n".$tmp[1]."\n"); }
			else { printf(F_OUT_QUAL $tmp[1]."\n"); }
		}
		#4.4: If we don't need to treat the qual file, we just printf the result (fasta file)
		else { printf(F_OUT_FASTA $string_tmp."\n"); }
	}
	#We close the result files
	close F_OUT_FASTA;
	#4.4: Based on the definition of the $boolean_qual, we treat the qual file
	if ($boolean_qual) { close F_OUT_QUAL; }
	
	#Print for the user information that the program is started
	PerlLib::Miscellaneous::PrintScreen("RANDOM SELECTION", $file ,"terminated");
	#Needed for the main program
	return("Rdm_$file");	
}
1;
