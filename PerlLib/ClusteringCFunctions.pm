#=======================================================================================================
# PACKAGE NAME:  ClusteringCFunctions
#    FUNCTIONS:	 - c_compare (function comparing two reads using a classical method),
#				 - c_compare_len (function comparing two reads ignoring length differences),
#				 - c_compare_hm (function comparing two reads ignoring homopolymer differences),
#				 - c_compare_hm_len (function comparing two reads ignoring homopolymer and length 
#				 differences).
#
#	   DETAILS:  These C cores were implemented to increase the efficiency of the comparison program. 
#				 These programs takes 3 arguments (2 strings and one threshold: respectively str_a, 
#				 str_b and max). These programs will then compare the two strings, and then the function
#				 returns the number of differences and stop, or returns the number of differences or the
#				 max. The max will help to gain time of computation. Note that we can take into account
#				 homopolymers errors and/or length differences at the beginning of reads depending of
#				 user choices.
#
# REQUIREMENTS:  Perl language only
#       AUTHOR:  Sebastien TERRAT <sebastien.terrat@dijon.inra.fr>
#      COMPANY:  GenoSol platform (UMR 1347 INRA/uB/AgroSup)
#      VERSION:  1.0
#    REALEASED:  03/09/2014
#     REVISION:  XXX
#=======================================================================================================

# Name of the package defined for the main program
package PerlLib::ClusteringCFunctions;

# Needed libraries
use strict;
use warnings;

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Functions developed in C language to fast the analysis of the clustering step. Several C programs have
# been defined to compare sequences. All takes same arguments, but are less or more stringent during 
# the comparison, based on user choices (see details in functions).
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Here, we used the dedicated library to create directly C functions in Perl programs.
use Inline C => <<'END_OF_C';

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
/*Function c_compare_hm_len ignoring homopolymer differences and also uncommon parts of the two sequences
 given as input. This program takes also the number of differences tolerated (int max) based on the user
 choices.*/
int c_compare( SV* str_a, SV* str_b, int max )
{
	int diff = 0;
	STRLEN len_a, len_b, len, i;
	//Definition of pointers
	char *a, *b;
	char letter;
	//SvPV() function is used to convert the PERL scalar to a string in the C program
	a = SvPV( str_a, len_a );
	b = SvPV( str_b, len_b );
	len = len_a < len_b ? len_a : len_b;
	for( i = 0; i < len; i++ )
	{
		if(a[i] != b[i]) { diff++; }
		if(diff > max) return diff;
	}
	return diff;
}
/*End of function*/
/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
/*Function c_compare_len ignoring uncommon parts of the two sequences given as input but taking into 
account homopolymer differences. This program takes also the number of differences tolerated (int max)
based on the user choices.*/
int c_compare_len( SV* str_a, SV* str_b, int max )
{
	int diff = 0;
	int verif = 0;
	STRLEN len_a, len_b, len, i;
	//Definition of pointers
	char *a, *b;
	char letter;
	//SvPV() function is used to convert the PERL scalar to a string in the C program
	a = SvPV( str_a, len_a );
	b = SvPV( str_b, len_b );
	len = len_a < len_b ? len_a : len_b;
	
	for( i = 0; i < len; i++ )
	{
		if ((a[i] == 'A'||a[i] == 'C'||a[i] == 'G'||a[i] == 'T')
		&& (b[i] == 'A'||b[i] == 'C'||b[i] == 'G'||b[i] == 'T')
		&& (verif == 0))
		{
			verif = 1;
		}
		if((a[i] != b[i])&&(verif == 1))
		{
			diff++;	
		}
		if(diff > max) return diff;
	}
	//Needed verification as we wait for the start of the two compared sequences
	if (verif == 0) { diff = len; }
	return diff;
}
/*End of function*/
/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
/*Function c_compare_hm ignoring homopolymer differences of the two sequences given as input. This 
program takes also the number of differences tolerated (int max) based on the user choices.*/
int c_compare_hm( SV* str_a, SV* str_b, int max )
{
	int diff = 0;
	int homopolymer_a = 0;
	int homopolymer_b = 0;
	int x = 0;
	STRLEN len_a, len_b, len, i;
	//Definition of pointers
	char *a, *b;
	char letter;
	//SvPV() function is used to convert the PERL scalar to a string in the C program
	a = SvPV( str_a, len_a );
	b = SvPV( str_b, len_b );
	len = len_a < len_b ? len_a : len_b;
	for( i = 0; i < len; i++ )
	{
		if(a[i] != b[i])
		{
			x = 0;
			homopolymer_a = 0;
			if(a[i]=='.'||a[i]=='-') {letter = b[i];}
			else {letter = a[i];}
			//printf("%c ", letter);
			while ((letter == a[i-x]||a[i-x]=='.'||a[i-x]=='-')&& i-x >= 0)
			{
				if (letter == a[i-x]) { homopolymer_a++; }
				x++;
			}
			x = 1;
			while ((letter == a[i+x]||a[i+x]=='.'||a[i+x]=='-')&& i+x <= len)
			{
				if (letter == a[i+x]) { homopolymer_a++; }
				x++;
			}
			//printf("%d ", homopolymer_a);
			
			x = 0;
			homopolymer_b = 0;
			if(b[i]=='.'||b[i]=='-') { letter = a[i]; }
			else {letter = b[i];}
			while ((letter == b[i-x]||b[i-x]=='.'||b[i-x]=='-')&& i-x >= 0)
			{
				if (letter ==b[i-x]) { homopolymer_b++; }
				x++;
			}
			x = 1;
			while ((letter == b[i+x]||b[i+x]=='.'||b[i+x]=='-')&& i+x <= len)
			{
				if (letter ==b[i+x]) { homopolymer_b++; }
				x++;
			}
			if (homopolymer_a > 2 || homopolymer_b > 2)
			{
				if (((homopolymer_a - homopolymer_b == 1)&&(a[i]==b[i]))||((homopolymer_b - homopolymer_a == 1)&&(a[i]==b[i]))
				||(a[i]=='.'||a[i]=='-'||b[i]=='.'||b[i]=='-')) {diff = diff;}
				else { diff++; }
			}
			else { diff++; }	
		}
		if(diff > max) return diff;
	}
	return diff;
}
/*End of function*/
/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
/*Function c_compare_hm_len ignoring homopolymer differences and also uncommon parts of the two 
sequences given as input. This program takes also the number of differences tolerated (int max) based on 
the user choices.*/
int c_compare_hm_len( SV* str_a, SV* str_b, int max )
{
	int diff = 0;
	int verif = 0;
	int homopolymer_a = 0;
	int homopolymer_b = 0;
	int x = 0;
	STRLEN len_a, len_b, len, i;
	//Definition of pointers
	char *a, *b;
	char letter;
	//SvPV() function is used to convert the PERL scalar to a string in the C program
	a = SvPV( str_a, len_a );
	b = SvPV( str_b, len_b );
	len = len_a < len_b ? len_a : len_b;
	
	for( i = 0; i < len; i++ )
	{
		if ((a[i] == 'A'||a[i] == 'C'||a[i] == 'G'||a[i] == 'T')
		&& (b[i] == 'A'||b[i] == 'C'||b[i] == 'G'||b[i] == 'T')
		&& (verif == 0))
		{
			verif=1;
		}
		if((a[i] != b[i])&&(verif == 1))
		{
			x = 0;
			homopolymer_a = 0;
			if(a[i]=='.'||a[i]=='-') { letter = b[i]; }
			else {letter = a[i];}
			//printf("%c ", letter);
			while ((letter == a[i-x]||a[i-x]=='.'||a[i-x]=='-')&& i-x >= 0)
			{
				if (letter ==a[i-x]) { homopolymer_a++; }
				x++;
			}
			x = 1;
			while ((letter == a[i+x]||a[i+x]=='.'||a[i+x]=='-')&& i+x <= len)
			{
				if (letter ==a[i+x]) { homopolymer_a++; }
				x++;
			}
			//printf("%d ", homopolymer_a);
			x = 0;
			homopolymer_b = 0;
			if(b[i]=='.'||b[i]=='-') { letter = a[i]; }
			else {letter = b[i];}
			//printf("%c ", letter);
			while ((letter == b[i-x]||b[i-x]=='.'||b[i-x]=='-')&& i-x >= 0)
			{
				if (letter ==b[i-x]) { homopolymer_b++; }
				x++;
			}
			x = 1;
			while ((letter == b[i+x]||b[i+x]=='.'||b[i+x]=='-')&& i+x <= len)
			{
				if (letter ==b[i+x]) { homopolymer_b++; }
				x++;
			}
			if (homopolymer_a > 2 || homopolymer_b > 2)
			{
				if (((homopolymer_a - homopolymer_b == 1)&&(a[i]==b[i]))||((homopolymer_b - homopolymer_a == 1)&&(a[i]==b[i]))
				||(a[i]=='.'||a[i]=='-'||b[i]=='.'||b[i]=='-')) {diff = diff;}
				else { diff++; }
			}
			else { diff++; }	
		}
		if(diff > max) return diff;
	}
	//Needed verification as we wait for the start of the two compared sequences
	if (verif == 0) { diff = len; }
	return diff;
}
/*End of function*/
/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

END_OF_C

1;
