# Repository of metagenomic analysis pipeline `BIOCOM-PIPE 1.20`

See the publication [here](https://doi.org/10.1186/s12859-020-03829-3).

The user guide is available in [gitlab repo](https://forgemia.inra.fr/biocom/biocom-pipe_manual) as pdf.

Data and tools (`Data/Databases` and `Data/Tools/`) are available as a tarball stored in Zenodo [![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.4428756.svg)](https://doi.org/10.5281/zenodo.4428756)

After getting the code, use these command lines to get the databases and tools needed to use the pipeline. 

**Warning: total size of the uncompressed databases and tools is around 22Go, and 8Go in .tgz format**

```bash
cd BIOCOM-PIPE-V1.20
wget https://zenodo.org/record/4428756/files/archive_BIOCOM-PIPE-V1.20.tgz
tar -xvf archive_BIOCOM-PIPE-V1.20.tgz
mkdir Data/Databases/ReClustOR
``` 
