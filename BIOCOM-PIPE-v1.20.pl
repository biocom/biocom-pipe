#!/usr/bin/perl

#=======================================================================================================
#         FILE:  Complete_treatment.pl
#        USAGE:  perl Complete_treatment.pl
#  DESCRIPTION:
# REQUIREMENTS:  Perl language only
#       AUTHOR:  Sebastien TERRAT <sebastien.terrat@dijon.inra.fr>
#      COMPANY:  GenoSol platform (UMR 1347 INRA/uB/AgroSup)
#      VERSION:  0.1
#    REALEASED:  21/03/2012
#     REVISION:
#=======================================================================================================
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#Print errors and warnings during the implementation of the perl program
use strict;
use warnings;
use Cwd;
use FindBin;
use lib $FindBin::RealBin;
#Needed homemade modules stored in the PerlLib library
use PerlLib::ManagingGlobalAnalysis;
use PerlLib::ProgramLaunchers;
use PerlLib::Miscellaneous;
use PerlLib::ManagingFormatFiles;
use PerlLib::ManagingQuality;
use PerlLib::Preprocessing;
use PerlLib::Dereplication;
use PerlLib::PoolingReads;
use PerlLib::Clustering;
use PerlLib::ClusteringWithIndex;
use PerlLib::RandomSelection;
use PerlLib::InfernalAlignment;
use PerlLib::GlobalAlignment;
use PerlLib::FastQTreatment;
use PerlLib::Taxonomy;
use PerlLib::ReClustOR;
use PerlLib::InfernalAlignment;
use PerlLib::DeMultiplexing;
use PerlLib::ManagingDirectories;
use PerlLib::CheckInputFile;
use PerlLib::VerifyingUserChoices;
use PerlLib::SummaryFilesDefinition;
use PerlLib::FilteringInfernalAlignment;
use PerlLib::ManagingInputArguments;
use PerlLib::ThresholdCheckings;
use PerlLib::PreprocessingMergedDemultiplexing;

#Defined variables
my @files_qual = ();
my @files_sff = ();
my @files_fna = ();
my @files_fastq = ();
my $ref_files_qual = "";
my $ref_files_sff = "";
my $ref_files_fna = "";
my $ref_files_fastq = "";

my @files_Mids = ();
my @files_Treated = ();
my @files_Align = ();
my @files_Hunt = ();
my @folders = ();
my @param_preprocessing = ();
my @steps = ("PRINSEQ", "FLASH", "EVAL_QUAL", "PREPROCESS_MIDS", "RANDOM_PREPROCESS_MIDS", "PREPROCESSING", "QUALITY_CLEANING",
"RANDOM_PREPROCESS", "LENGTH_RANGE_PREPROCESS", "RAW_INFERNAL_ALIGNMENT", "FILTERING_RAW_ALIGNMENT", "RAW_CLUSTERING", "HUNTING",
"RECOVERING", "RANDOM_CLEANED", "TAXONOMY", "CLEAN_INFERNAL_ALIGNMENT", "CLEAN_CLUSTERING", "COMPUTATION",
"GLOBAL_ANALYSIS", "RECLUSTOR", "UNIFRAC_ANALYSIS");

my $folder_input = "";
my $folder_output = "";
my $folder_input_globanalysis = "";
my $folder_output_globanalysis = "";
my $folder_input_fasta = "";
my $folder_taxonomy = "";
my $folder_align = "";
my $folder_derep = "";
my $folder_clustering = "";
my $ref_array = "";
my $step = "";
my $choice_preprocessing = "";
my $choice_demultiplexing = "";
my $concatenation_step = 0;
my $verif_derep = 0;
my $verif_fastq = 0;
my $validation_text = "";
my $nbfiles_Mids = 0;
my $taille = 0;
my $nbreads_derep = 0;
my $nbreads_recovering = 0;
my $file = "";
my $file_clust = "";
my $file_derep = "";
my $file_fasta = "";
my $i = 0;
my $k = 0;
my $h = 0;

my @parameters = ();
my @parameters_glob = ();
my $pid = 0;
my @childs = ();
my $pid2 = 0;
my @childs2 = ();

my $file_name_pipe = "";
my $tmp = "";
my $limit_nb_cores = 1;
my $nb_processes = 0;
my $nb_processes2 = 0;
my @tmp = ();

my $current_path = "";
my $bin_path = "";
my %Input_args = ();

#=======================================================================================================
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#=======================================================================================================
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#=======================================================================================================
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#=======================================================================================================
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#=======================================================================================================
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#=======================================================================================================

#=======================================================================================================
# #01# Initiating steps: First, we read all files stored and given by the user in the Raw_data/ folder.
# We then store the found information in several arrays (@files_sff, @files_qual, @files_fna). We also
# defined a new Input.txt file automatically, based on the informaion stored in the Project_files
# folder.
#=======================================================================================================

#First, we catch both paths to work (where the program is stored, and where it was launched)
$current_path = getcwd();
$bin_path =$FindBin::RealBin;

#Then, we cacth all arguments given as input (prep, launch, graphs, and cores)
$ref_array = PerlLib::ManagingInputArguments::ManagingInputArguments();
#Then, first, based on the argument given by the user, we defined the maximum number of cores
%Input_args = %{$ref_array};
$limit_nb_cores = $Input_args{"-c"};

#Then, we launch if needed the python program to create a new Input.txt file.
if(exists($Input_args{"-p"}))
{
	#First, we catch both paths to work (where the program is stored, and where it was launched)
	$current_path = getcwd();
	$bin_path = $FindBin::RealBin;
	$file_name_pipe = `python2.7 $FindBin::RealBin/Prog_Python/biocomPipe_prep_files.py $bin_path $current_path`;
	print $file_name_pipe;
	exit(0);
}
elsif(exists($Input_args{"-g"}))
{
	#First, we catch both paths to work (where the program is stored, and where it was launched)
	$current_path = getcwd();
	$bin_path = $FindBin::RealBin;
	$file_name_pipe = `python2.7 $FindBin::RealBin/Prog_Python/biocomPipe_figs.py $bin_path $current_path`;
	print $file_name_pipe;
	exit(0);
}
#Second checking of the desire of the user to launch the analysis
if(!exists($Input_args{"-l"})) { exit(0); }

#We launch the dedicated function to define the raw files given by the user.
($ref_files_sff, $ref_files_fna, $ref_files_qual, $ref_files_fastq) = PerlLib::Miscellaneous::CatchRawFiles();
@files_sff = @{$ref_files_sff};
@files_fna = @{$ref_files_fna};
@files_qual = @{$ref_files_qual};
@files_fastq = @{$ref_files_fastq};

#=======================================================================================================
# #02# Initiating steps: Here, we launch the functions developed to check user choices and also chosen
# parameters before the launching of the GnS-PIPE, to avoid errors during the treatment and analysis
# of data.
#=======================================================================================================

PerlLib::CheckInputFile::CheckInputFile(\@steps);
PerlLib::VerifyingUserChoices::VerifyingUserChoices(\@files_sff, \@files_qual, \@files_fna, \@files_fastq);
#Here, we launch the function "ConcatenationStepDefinition" to know when the concatenation of reads will
#be done, as some programs and functions needs that information
$concatenation_step = PerlLib::ManagingGlobalAnalysis::ConcatenationStepDefinition();

#=======================================================================================================
# #03# Initiating steps: Here, we clean the needed directories, we then create needed subdirectories and
# summary files for all chosen steps, and also extract from raw_data needed files.
#=======================================================================================================

#We call the cleaning (or creation) of all previous results to start efficiently a new analysis
PerlLib::ManagingDirectories::DirectoriesCleaning("Result_files");
PerlLib::ManagingDirectories::DirectoriesCleaning("Summary_files");
PerlLib::ManagingDirectories::DirectoriesCleaning("Temporary_files");
#We launch also the creation of all needed directories and then we catch the needed ref_array
#to find the information and store it in @folders.
($verif_derep, $ref_array) = PerlLib::ManagingDirectories::DirectoriesCreation();
if(defined($ref_array)) { @folders = @{$ref_array}; }
else { exit(0); }

#Test to create the needed summary_file if needed
if ($verif_derep == 1) { push(@steps, "DEREPLICATION"); }
#Function to define all needed summary files.
foreach $step (@steps) { PerlLib::SummaryFilesDefinition::SummaryFilesDefinition($step, $concatenation_step); }
#We first verify if the file(s) given by the user are .sff and/or .fastq or not.
if(($#files_sff > -1)||($#files_fastq  > -1))
{
	#We also check that we don't have available .fna or .qual files
	if(($#files_fna == -1)&&($#files_qual == -1))
	{
		#We first treat .sff files
		if($#files_sff > -1)
		{
			foreach $file (@files_sff)
			{
				#This test is defined to realize the treatment on a maximum of X different files defined by the
				#$limit_nb_cores : if the array @childs stored the good number of child preocesses pids.
				if($nb_processes >= $limit_nb_cores) { $pid = wait(); $nb_processes --; }
				#Creation of a parent and a child process. One child for each found file needed treatment
				$pid = fork();
				#If the program is the parent! (only one)
				if ($pid) { push(@childs, $pid); $nb_processes ++; }
				#If the program is a child (number as number of files)
				elsif ($pid == 0)
				{
					#We launch the dedicated function designed to extract fna and quality data from SFF files
					PerlLib::ManagingFormatFiles::ExtractionFromSFF($file);
					exit(0);
				}
				#If we can't fork (create parent and child processes, die the process)
			 	else { die "couldn’t fork: $!\n"; }
		 	}
		 	#The parent process wait for all child results
			foreach (@childs) { waitpid($_, 0); }
		}

		#Here, before the treatment of the FASTQ files, we checked if the PRINSEQ and/or FLASH steps are
		#chosen by the user,, as we need the files as .fastq for these steps, before other treatments
		$ref_array = PerlLib::Miscellaneous::SearchInputParameters("PRINSEQ");
		@parameters = @$ref_array;
		if(uc($parameters[0]) eq "YES") { $verif_fastq = 1; }
		#We then launch the search parameters function defined earlier
		$ref_array = PerlLib::Miscellaneous::SearchInputParameters("FLASH");
		@parameters = @$ref_array;
		if(uc($parameters[0]) eq "YES") { $verif_fastq = 1; }
		#We then treat .fastq files if they were not used for FLASH or PRINSEQ steps
		if(($#files_fastq  > -1)&&($verif_fastq == 0))
		{
			foreach $file (@files_fastq)
			{
				#This test is defined to realize the treatment on a maximum of X different files defined by the
				#$limit_nb_cores : if the array @childs stored the good number of child preocesses pids.
				if($nb_processes >= $limit_nb_cores) { $pid = wait(); $nb_processes --; }
				#Creation of a parent and a child process. One child for each found file needed treatment
				$pid = fork();
				#If the program is the parent! (only one)
				if ($pid) { push(@childs, $pid); $nb_processes ++; }
				#If the program is a child (number as number of files)
				elsif ($pid == 0)
				{
					#We launch the dedicated function designed to extract fna and quality data from SFF files
					PerlLib::ManagingFormatFiles::ExtractionFromFASTQ($file, "", "");
					exit(0);
				}
				#If we can't fork (create parent and child processes, die the process)
			 	else { die "couldn’t fork: $!\n"; }
		 	}
		 	#The parent process wait for all child results
			foreach (@childs) { waitpid($_, 0); }
		}
		#Needed empty array
		@childs = ();
		$pid = 0;
		$nb_processes = 0;
		#We launch the dedicated function to define the raw files given by the user and/or defined
		#by the program
		($ref_files_sff, $ref_files_fna, $ref_files_qual, $ref_files_fastq) = PerlLib::Miscellaneous::CatchRawFiles();
		@files_sff = @$ref_files_sff;
		@files_fna = @$ref_files_fna;
		@files_qual = @$ref_files_qual;
		@files_fastq = @$ref_files_fastq;
	}
}

#=======================================================================================================
# Treatment steps: We potentially launch the treatments using the PRINSEQ and FLASH programs to trim
# and merge the fastq files given as input by the user.
#=======================================================================================================

#We then launch the search parameters function defined earlier
$ref_array = PerlLib::Miscellaneous::SearchInputParameters("PRINSEQ");
@parameters = @$ref_array;
if (uc($parameters[0]) eq "YES")
{
	#Here, addition of PRINSEQ and FLASH
	$file_name_pipe = PerlLib::FastQTreatment::PrinSeqTreatment(\@files_fastq, $limit_nb_cores);
	#Then, we launch the CatchTreatedFileNames defined function to catch needed filenames after the
	#potential treatment of PRINSEQ
	($folder_input, $ref_array) = PerlLib::Miscellaneous::CatchTreatedFileNames();
	@files_fastq = @{$ref_array};
}
#We then launch the search parameters function defined earlier
$ref_array = PerlLib::Miscellaneous::SearchInputParameters("FLASH");
@parameters = @$ref_array;
if (uc($parameters[0]) eq "YES")
{
	#Here, addition of PRINSEQ and FLASH
	$file_name_pipe = PerlLib::FastQTreatment::FLASHTreatment($folder_input, \@files_fastq, $limit_nb_cores);
	#Then, we launch the CatchTreatedFileNames defined function to catch needed filenames after the
	#potential treatment of PRINSEQ
	($folder_input, $ref_array) = PerlLib::Miscellaneous::CatchTreatedFileNames();
	@files_fastq = @{$ref_array};
}

if(($#files_fastq  > -1)&&($verif_fastq == 1))
{
	foreach $file (@files_fastq)
	{
		#This test is defined to realize the treatment on a maximum of X different files defined by the
		#$limit_nb_cores : if the array @childs stored the good number of child preocesses pids.
		if($nb_processes >= $limit_nb_cores) { $pid = wait(); $nb_processes --; }
		#Creation of a parent and a child process. One child for each found file needed treatment
		$pid = fork();
		#If the program is the parent! (only one)
		if ($pid) { push(@childs, $pid); $nb_processes ++; }
		#If the program is a child (number as number of files)
		elsif ($pid == 0)
		{
			#We launch the dedicated function designed to extract fna and quality data from SFF files
			PerlLib::ManagingFormatFiles::ExtractionFromFASTQ($file, $folder_input, $folder_input);
			# We modify the name of the $file variable
			$file =~ s/\.fastq//;
			$file =~ s/\.fq//;
			$file .= ".fasta";
			#We create with this function a temp file (infos.info) storing everything needed to relaunch treatments
			PerlLib::Miscellaneous::FileInfoAppending($file, $folder_input);
			exit(0);
		}
		#If we can't fork (create parent and child processes, die the process)
	 	else { die "couldn’t fork: $!\n"; }
 	}
 	#The parent process wait for all child results
	foreach (@childs) { waitpid($_, 0); }
	#Then, we launch the CatchTreatedFileNames defined function to catch needed filenames after the
	#potential treatment of PRINSEQ and/or FLASH
	($folder_input, $ref_array) = PerlLib::Miscellaneous::CatchTreatedFileNames();
	@files_fna = @{$ref_array};
	foreach $file_fasta (@files_fna)
	{
		($file = $file_fasta) =~ s/\.fasta/\.qual/;
		push(@files_qual, $file);
	}
}
#Needed empty array
@childs = ();
$pid = 0;
$nb_processes = 0;

#=======================================================================================================
# #01# Treatment steps: We first analyze and read .qual files to define the quality of the reads using
# a dedicated function names QualityEvaluation.
#=======================================================================================================
foreach $file (@files_qual)
{
	#This test is defined to realize the treatment on a maximum of X different files defined by the
	#$limit_nb_cores : if the array @childs stored the good number of child preocesses pids.
	if($nb_processes >= $limit_nb_cores) { $pid = wait(); $nb_processes --; }
	#Creation of a parent and a child process. One child for each found file needed treatment
	$pid = fork();
	#If the program is the parent! (only one)
	if ($pid) { push(@childs, $pid); $nb_processes ++; }
	#If the program is a child (number as number of files)
	elsif ($pid == 0)
	{
		#We then launch the search parameters function defined earlier
		$ref_array = PerlLib::Miscellaneous::SearchInputParameters("EVAL_QUAL");
		@parameters = @$ref_array;
		if (uc($parameters[0]) eq "YES")
		{
			#We first launch the function dedicated to the evaluation of the quality of reads.
			$file_name_pipe = PerlLib::ManagingQuality::QualityEvaluation($parameters[1], $file, $folder_input);
		}
		exit(0);
	}
	#If we can't fork (create parent and child processes, die the process)
 	else {die "couldn’t fork: $!\n";}
}
#The parent process wait for all child results
foreach (@childs) {waitpid($_, 0);}
#Needed empty array
@childs = ();
$pid = 0;
$nb_processes = 0;

#=======================================================================================================
# Third step: Evaluation and preprocessing of the .fna file using given parameters
#=======================================================================================================
foreach $file (@files_fna)
{
	#This test is defined to realize the treatment on a maximum of X different files defined by the
	#$nb_processes and the $limit_nb_cores
	if($nb_processes >= $limit_nb_cores) { $pid = wait(); $nb_processes --; }
	#Creation of a parent and a child process. One child for each found file needed treatment
	$pid = fork();
	#If the program is the parent! (only one)
	if ($pid) { push(@childs, $pid); $nb_processes++; }
	#If the program is a child (number as number of files)
	elsif ($pid == 0)
	{

		#PREPROCESSING_MIDS---We then launch the search parameters function defined earlier
		#V20 MODIF: First, we searched if the PREPROCESSING step is also done in parallel or not,
		#storing the user choice in the $choice_preprocessing variable
		$ref_array = PerlLib::Miscellaneous::SearchInputParameters("PREPROCESSING");
		@param_preprocessing = @$ref_array;
		$choice_preprocessing = uc($param_preprocessing[0]);
		#V20 MODIF: Then, we catch user choices from the Preprocess_MIDS step
		$ref_array = PerlLib::Miscellaneous::SearchInputParameters("PREPROCESS_MIDS");
		@parameters = @$ref_array;
		$choice_demultiplexing = uc($parameters[0]);
		#We checked if the user wants to do this step
		if (uc($parameters[0]) eq "YES")
		{
			#V20 MODIF: First, we search for needed folder names (input and/or output for the treatment)
			if($choice_preprocessing eq "YES") { $folder_output = PerlLib::ManagingDirectories::FindDirectory("PREPROCESSING", \@folders); }
			else { $folder_output = PerlLib::ManagingDirectories::FindDirectory("PREPROCESS_MIDS", \@folders); }
			#We start the command line (parameters) such as names of MIDs/samples, etc...
			for ($i=1;$i<=$#parameters;$i++)
			{
				@tmp = split(/\./, $file);
				#Then, as before the PREPROCESS_MIDS step, we can find the PRINSEQ and FLASH steps
				$ref_array = PerlLib::Miscellaneous::SearchInputParameters("PRINSEQ");
				if(uc($$ref_array[0]) eq "YES") { if($parameters[$i] !~ m/PrinSeq_/) { $parameters[$i] = "PrinSeq_".$parameters[$i]; } }
				$ref_array = PerlLib::Miscellaneous::SearchInputParameters("FLASH");
				if(uc($$ref_array[0]) eq "YES") { if($parameters[$i] !~ m/FLASH_/) { $parameters[$i] = "FLASH_".$parameters[$i]; } }
				#Here, we test if the raw filename is also the filename given as input
				if($parameters[$i] !~ m/\Q$tmp[0]/) {  next; }
				else
				{
					$nbfiles_Mids++;
					#This test is defined to realize the treatment on a maximum of X different files defined by the
					#$limit_nb_cores : if the array @childs stored the good number of child processes pids.
					if($nb_processes2 * ($#files_fna + 1) >= $limit_nb_cores)
					{
						$pid2 = wait();
						$nb_processes2 --;
					}
					#Creation of a parent and a grandchild process. One grandchild for each found file needed treatment
					$pid2 = fork();
					#If the program is the parent! (only one)
					if ($pid2)
					{
						push(@childs2, $pid2);
						$nb_processes2 ++;
					}
					#If the program is a grandchild (number as number of files)
					elsif ($pid2 == 0)
					{
						#V20 MODIF: We checked user choice to know if we launched the function PreprocessingMergedDemultiplexing program to treat
						#efficiently both demultiplexing and preprocessing steps
						if($choice_preprocessing eq "YES") { $file_name_pipe = PerlLib::PreprocessingMergedDemultiplexing::Preprocessing($param_preprocessing[1], $param_preprocessing[2], $param_preprocessing[3], $param_preprocessing[4], $file, $folder_input, $folder_output, $param_preprocessing[7], $param_preprocessing[5], $param_preprocessing[6], $parameters[$i]); }
						#V20 MODIF: We launch the function PreprocessingMIDsLauncher to treat efficiently the PREPROCESS_MIDS step
						else { $file_name_pipe = PerlLib::DeMultiplexing::DeMultiplexing($file, $folder_output, $parameters[$i], $folder_input); }
						#We create with this function a temp file (infos.info) storing everything needed to relaunch treatments
						PerlLib::Miscellaneous::FileInfoAppending($file_name_pipe, $folder_output);
						exit(0);
					}
					#If we can't fork (create parent and child processes, die the process)
	 				else { die "couldn’t fork: $!\n"; }
	 			}
			}
			#The parent process wait for all grandchild results
 			foreach (@childs2) { waitpid($_, 0); }
 			#V20 MODIF: We launch the program to format the results of the Preprocess_MIDs step chosen by the user, but only if we search reads in it
 			if(($nbfiles_Mids > 0)&&($choice_preprocessing eq "NO")) { PerlLib::DeMultiplexing::FormatMIDsResults("Preprocess_mids.txt", $file); }
 			$folder_input = $folder_output;
		}
		exit(0);
	}
	#If we can't fork (create parent and child processes, die the process)
 	else { die "couldn’t fork: $!\n"; }
}
#The parent process wait for all child results
foreach (@childs) { waitpid($_, 0); }

$ref_array = PerlLib::Miscellaneous::SearchInputParameters("PREPROCESS_MIDS");
@parameters = @$ref_array;
if (uc($parameters[0]) eq "YES")
{
	#unlink("Temporary_files/Preprocess_mids.txt");
	#Then, we launch the CatchTreatedFileNames defined function to catch needed parameters after the children treatment
	($folder_input, $ref_array) = PerlLib::Miscellaneous::CatchTreatedFileNames();
	@files_Mids = @$ref_array;
}
else
{
	@files_Mids = @files_fna;
	#Test needed to avoid the modification of the folder_input variable if PRINSEQ and/or FLASH was previously launched
	if($folder_input eq "") { $folder_input = "None"; }
}

#Needed empty variables
@childs = ();
$pid = 0;
$nb_processes = 0;

foreach $file (@files_Mids)
{
	#This test is defined to realize the treatment on a maximum of X different files defined by the
	#$limit_nb_cores : if the array @childs stored the good number of child preocesses pids.
	if($nb_processes >= $limit_nb_cores)
	{
		$pid = wait();
		$nb_processes --;
	}
	#Creation of a parent and a grandchild process. One grandchild for each found file needed treatment
	$pid = fork();
	#If the program is the parent! (only one)
	if ($pid)
	{
		push(@childs, $pid);
		$nb_processes ++;
	}
	#If the program is a grandchild (number as number of files)
	elsif ($pid == 0)
	{
#RANDOM_MIDS--------We then launch the search parameters function defined earlier
		$ref_array = PerlLib::Miscellaneous::SearchInputParameters("RANDOM_PREPROCESS_MIDS");
		@parameters = @$ref_array;
		if (uc($parameters[0]) eq "YES")
		{
			if ($parameters[1] != 0)
			{
				#First, we search for needed folder names (input and/or output for the treatment)
				$folder_output = PerlLib::ManagingDirectories::FindDirectory("RANDOM_PREPROCESS_MIDS", \@folders);
				#We then launch the function dedicated to the realization of the homogenization step with needed arguments
				$file = PerlLib::RandomSelection::RandomSelection($parameters[1], $file, $folder_input, $folder_output);
				$folder_input = $folder_output;
			}
		}


#PREPROCESSING------We then launch the search parameters function defined earlier
		#V20 MODIF: First, we catch user choices for the Preprocess_MIDS step
		$ref_array = PerlLib::Miscellaneous::SearchInputParameters("PREPROCESS_MIDS");
		@parameters = @$ref_array;
		$choice_demultiplexing = uc($parameters[0]);
		#V20 MODIF: Then, we catch the user choices for the PREPROCESSING step
		$ref_array = PerlLib::Miscellaneous::SearchInputParameters("PREPROCESSING");
		@parameters = @$ref_array;
		#V20 MODIF: Double test to know if the program is needed or not
		if((uc($parameters[0]) eq "YES")&&($choice_demultiplexing eq "NO"))
		{
			#First, we search for needed folder names (input and/or output for the treatment)
			$folder_output = PerlLib::ManagingDirectories::FindDirectory("PREPROCESSING", \@folders);
			#We then launch the function dedicated to the realization of the preprocessing step with needed arguments.
			#We catch the return of the program in the $file_name_pipe variable to be used further
			$file_name_pipe = PerlLib::Preprocessing::Preprocessing($parameters[1], $parameters[2], $parameters[3], $parameters[4], $file, $folder_input, $folder_output, $parameters[7], $parameters[5], $parameters[6]);
			#We then catch the name of the created file, and manage the fact that the file can be empty or not
			if($file_name_pipe eq "") { exit(0); }
			$file = $file_name_pipe;
			$folder_input = $folder_output;
		}

#QUALITY_CLEANING--We then launch the search parameters function defined earlier
		$ref_array = PerlLib::Miscellaneous::SearchInputParameters("QUALITY_CLEANING");
		@parameters = @$ref_array;
		if (uc($parameters[0]) eq "YES")
		{
			#First, we search for needed folder names (input and/or output for the treatment)
			$folder_output = PerlLib::ManagingDirectories::FindDirectory("QUALITY_CLEANING", \@folders);
			#Print for the user information that the program is started
			PerlLib::Miscellaneous::PrintScreen("QUALITY CLEANING",$file,"started");
			#Needed command line constructed. Launch the command line instruction using the "`". And we catch
			#the print in the $file_name_pipe variable to be used further
			$file_name_pipe = `perl $FindBin::RealBin/Prog_Perl/Quality_cleaning_1.1.pl $file $parameters[1] $parameters[2] $folder_input $folder_output`;
			#Print for the user information that the program is terminated
			PerlLib::Miscellaneous::PrintScreen("QUALITY CLEANING",$file,"terminated");
			#We then catch the name of the created file, and manage the fact that the file can be empty or not
			if($file_name_pipe eq "") { exit(0); }
			$file = $file_name_pipe;
			$folder_input = $folder_output;
		}

#RANDOM_PREPROCESS--We then launch the search parameters function defined earlier
		$ref_array = PerlLib::Miscellaneous::SearchInputParameters("RANDOM_PREPROCESS");
		@parameters = @$ref_array;
		if (uc($parameters[0]) eq "YES")
		{
			#First, we search for needed folder names (input and/or output for the treatment)
			$folder_output = PerlLib::ManagingDirectories::FindDirectory("RANDOM_PREPROCESS", \@folders);
			if ($parameters[1] != 0)
			{
				#We then launch the function dedicated to the realization of the homogenization step
				$file = PerlLib::RandomSelection::RandomSelection($parameters[1], $file, $folder_input, $folder_output);
				$folder_input = $folder_output;
			}
		}

#LENGTH-------------We then launch the search parameters function defined earlier
		$ref_array = PerlLib::Miscellaneous::SearchInputParameters("LENGTH_RANGE_PREPROCESS");
		@parameters = @$ref_array;
		if (uc($parameters[0]) eq "YES")
		{
			#Print for the user information that the program is started
			PerlLib::Miscellaneous::PrintScreen("LENGTH RANGE DISTRIBUTION",$file,"started");
			#Needed command line constructed. Launch the command line instruction using the "`". And we catch
			#the print in the $file_name_pipe variable to be used further
			$file_name_pipe = `perl $FindBin::RealBin/Prog_Perl/Length_range_preprocess_1.5.pl $parameters[1] $file $folder_input`;
			#Print for the user information that the program is terminated
			PerlLib::Miscellaneous::PrintScreen("LENGTH RANGE DISTRIBUTION",$file,"terminated");
		}



#DEREPLICATION------We then launch strict dereplication as we will need it (no question for the user to realize or not the
		#dereplication step, as it is essential).
		if ($verif_derep == 1)
		{
			#First, we search for needed folder names (input and/or output for the treatment)
			$folder_output = PerlLib::ManagingDirectories::FindDirectory("DEREPLICATION", \@folders);
			#Needed command line constructed. Launch the command line instruction using the "`". And we catch
			#the print in the $file_name_pipe variable to be used further
			($file_name_pipe, $nbreads_derep) = PerlLib::Dereplication::Dereplication($file, $folder_input, $folder_output);
			#V20 MODIF: Addition of the test program to evaluate the quality of the sequencing
			PerlLib::ThresholdCheckings::ThresholdCheckings($file_name_pipe, $nbreads_derep, "DEREPLICATION");
			#We then catch the name of the created file
			$file = $file_name_pipe;
			$folder_input = $folder_output;
		}

#GLOBAL_ANALYSIS-----We launch the GLOBAL_ANALYSIS pretreatment using the defined parameters by the user if needed
		$ref_array = PerlLib::Miscellaneous::SearchInputParameters("GLOBAL_ANALYSIS");
		@parameters = @$ref_array;
		if (uc($parameters[0]) eq "YES")
		{
			if ($concatenation_step == 1)
			{
				#First, we look for the DEREPLICATION directory
				$folder_output_globanalysis = PerlLib::ManagingDirectories::FindDirectory("DEREPLICATION", \@folders);
				if ($folder_output_globanalysis eq "None")
				{
					#First, we look for the RANDOM_PREPROCESS directory
					$folder_output_globanalysis = PerlLib::ManagingDirectories::FindDirectory("RANDOM_PREPROCESS", \@folders);
					if ($folder_output_globanalysis eq "None")
					{
						#Then, we look for the QUALITY_CLEANING directory
						$folder_output_globanalysis = PerlLib::ManagingDirectories::FindDirectory("QUALITY_CLEANING", \@folders);
						if ($folder_output_globanalysis eq "None")
						{
							#Then, we look for the PREPROCESSING directory
							$folder_output_globanalysis = PerlLib::ManagingDirectories::FindDirectory("PREPROCESSING", \@folders);
							if ($folder_output_globanalysis eq "None")
							{
								#Then, we look for the RANDOM_PREPROCESS_MIDS directory
								$folder_output_globanalysis = PerlLib::ManagingDirectories::FindDirectory("RANDOM_PREPROCESS_MIDS", \@folders);
								if ($folder_output_globanalysis eq "None")
								{
									#Then, we look for the PREPROCESS_MIDS directory
									$folder_output_globanalysis = PerlLib::ManagingDirectories::FindDirectory("PREPROCESS_MIDS", \@folders);
									if ($folder_output_globanalysis eq "None")
									{
										#Finally, we chose to use the Raw_data directory
										$folder_output_globanalysis = "Raw_data";
									}
								}
							}
						}
					}
				}
				#Print for the user information that the program is started
				PerlLib::Miscellaneous::PrintScreen("GLOBAL MERGING",$file,"started");
				#Needed command line constructed. Launch the command line instruction using the "`". And we catch
				#the print in the $file_name_pipe variable to be used further
				PerlLib::PoolingReads::PoolingReads($file, $folder_input, $folder_output_globanalysis, $concatenation_step);
				#Print for the user information that the program is terminated
				PerlLib::Miscellaneous::PrintScreen("GLOBAL MERGING",$file,"terminated");
			}
		}

		#RAW-ALIGN-GLOBAL_ANALYSIS-Here, we launch the independent alignments for all samples, if the user chose the GLOBAL_ANALYSIS
		#treatment, and also to do multiple alignments.
		$ref_array = PerlLib::Miscellaneous::SearchInputParameters("GLOBAL_ANALYSIS");
		@parameters = @$ref_array;
		if ((uc($parameters[0]) eq "YES") && ($concatenation_step == 1))
		{
			$ref_array = PerlLib::Miscellaneous::SearchInputParameters("RAW_INFERNAL_ALIGNMENT");
			@parameters = @$ref_array;
			if (uc($parameters[0]) eq "YES")
			{
				#First, we search for needed folder names (input and/or output for the treatment)
				$folder_output = PerlLib::ManagingDirectories::FindDirectory("RAW_INFERNAL_ALIGNMENT", \@folders);
				#Launching the dedicated function from the library for the INFERNAL alignment program
				$file_name_pipe = PerlLib::InfernalAlignment::Alignment("RAW", $parameters[1], $file, $folder_input, $folder_output, $#files_Mids, $limit_nb_cores);
				#We then catch the name of the created file to use it for further steps
				$file = $file_name_pipe;
				$folder_input = $folder_output;
			}
		}
		#We create with this function a temp file (infos.info) storing everything needed to relaunch treatments
		PerlLib::Miscellaneous::FileInfoAppending($file, $folder_output);
		#We then quit the loop to simplify the analysis (and treat the case of the GLOBAL_ANALYSIS analysis)
		exit(0);
	}
	#If we can't fork (create parent and child processes, die the process)
	else { die "couldn’t fork: $!\n"; }
}
#The parent process wait for all grandchild results
foreach (@childs) { waitpid($_, 0); }

#Then, we launch the CatchTreatedFileNames defined function to catch needed parameters after the children treatment
($folder_input, $ref_array) = PerlLib::Miscellaneous::CatchTreatedFileNames();
if (defined($ref_array)) { @files_Treated = @$ref_array; }
else { @files_Treated = (); }

#Here, we verify the choice of the user for the GLOBAL_ANALYSIS step (if it needs to be realized, but also if
#this step is realized at this position).
$ref_array = PerlLib::Miscellaneous::SearchInputParameters("GLOBAL_ANALYSIS");
@parameters = @$ref_array;
if ((uc($parameters[0]) eq "YES") && ($concatenation_step == 1))
{
	#Here, we search for the two needed folders used by the GLOBAL_ANALYSIS treatment
	$folder_output_globanalysis = PerlLib::ManagingDirectories::FindDirectory("DEREPLICATION", \@folders);
	#First, we look for the DEREPLICATION directory
	$folder_input_globanalysis = PerlLib::ManagingDirectories::FindDirectory("DEREPLICATION", \@folders);
	if ($folder_input_globanalysis eq "None")
	{
		#Then, we look for the RANDOM_PREPROCESS directory
		$folder_input_globanalysis = PerlLib::ManagingDirectories::FindDirectory("RANDOM_PREPROCESS", \@folders);
		if ($folder_input_globanalysis eq "None")
		{
			#Then, we look for the QUALITY_CLEANING directory
			$folder_input_globanalysis = PerlLib::ManagingDirectories::FindDirectory("QUALITY_CLEANING", \@folders);
			if ($folder_input_globanalysis eq "None")
			{
				#Then, we look for the PREPROCESSING directory
				$folder_input_globanalysis = PerlLib::ManagingDirectories::FindDirectory("PREPROCESSING", \@folders);
				if ($folder_input_globanalysis eq "None")
				{
					#Then, we look for the RANDOM_PREPROCESS_MIDS directory
					$folder_input_globanalysis = PerlLib::ManagingDirectories::FindDirectory("RANDOM_PREPROCESS_MIDS", \@folders);
					if ($folder_input_globanalysis eq "None")
					{
						#Then, we look for the PREPROCESS_MIDS directory
						$folder_input_globanalysis = PerlLib::ManagingDirectories::FindDirectory("PREPROCESS_MIDS", \@folders);
						if ($folder_input_globanalysis eq "None")
						{
							#Finally, we chose to use the Raw_data directory
							$folder_input_globanalysis = "Raw_data";
						}
					}
				}
			}
		}
	}
	#We also launch the treatment (if needed) of the concatenated file
	$file_name_pipe = PerlLib::Dereplication::Dereplication("Concatenated_reads.fasta.fasta", $folder_input_globanalysis, $folder_output_globanalysis);
	$folder_input_globanalysis = $folder_output_globanalysis;

	$ref_array = PerlLib::Miscellaneous::SearchInputParameters("RAW_INFERNAL_ALIGNMENT");
	@parameters = @$ref_array;
	if (uc($parameters[0]) eq "YES")
	{
		#Here, we just search for the output directory to save the alignment
		$folder_output_globanalysis = PerlLib::ManagingDirectories::FindDirectory("RAW_INFERNAL_ALIGNMENT", \@folders);
		#Print for the user information that the program is started
		PerlLib::Miscellaneous::PrintScreen("GLOBAL ALIGNMENT","Derep_Concatenated_reads.fasta.fasta","started");
		#Needed command line constructed. Launch the command line instruction using the "`". And we catch
		#the print in the $file_name_pipe variable to be used further
		$file_name_pipe = PerlLib::GlobalAlignment::GlobalAlignment($parameters[1], "Derep_Concatenated_reads.fasta.fasta", $folder_input_globanalysis, $folder_output_globanalysis);
		#Print for the user information that the program is terminated
		PerlLib::Miscellaneous::PrintScreen("GLOBAL ALIGNMENT","Derep_Concatenated_reads.fasta.fasta","terminated");
		push(@files_Treated, "Align_Derep_Concatenated_reads.fasta.fasta");
	}
	else { push(@files_Treated, "Derep_Concatenated_reads.fasta.fasta"); }
}

#Needed empty variables
$pid = 0;
@childs = ();
$nb_processes = 0;

#=======================================================================================================
# Fourth step: Treatment (alignment, clustering, hunting, recovering, random selection) of chosen files
#=======================================================================================================
#Based on the chosen files, we create the number of children needed
foreach $file (@files_Treated)
{
	#This test is defined to realize the treatment on a maximum of X different files defined by the
	#$limit_nb_cores : if the array @childs stored the good number of child preocesses pids.
	if($nb_processes >= $limit_nb_cores)
	{
		$pid = wait();
		$nb_processes --;
	}
	#Creation of a parent and a child process. One child for each found file needed treatment
	$pid = fork();
	#If the program is the parent! (only one)
	if ($pid)
	{
		push(@childs, $pid);
		$nb_processes ++;
	}
	#If the program is a child (number of children as the number of treated files)
	elsif ($pid == 0)
	{
		#Here, we have to define, if the RANDOM_CLEANED step is not used, the $file_fasta
		$file_fasta = $file;
#RAW_ALIGNMENT----------We then launch the search parameters function defined earlier
		$ref_array = PerlLib::Miscellaneous::SearchInputParameters("GLOBAL_ANALYSIS");
		@parameters = @$ref_array;
		if ((uc($parameters[0]) eq "NO") || ($concatenation_step == 2))
		{
			$ref_array = PerlLib::Miscellaneous::SearchInputParameters("RAW_INFERNAL_ALIGNMENT");
			@parameters = @$ref_array;
			if (uc($parameters[0]) eq "YES")
			{
				#First, we search for needed folder names (input and/or output for the treatment)
				$folder_output = PerlLib::ManagingDirectories::FindDirectory("RAW_INFERNAL_ALIGNMENT", \@folders);
				#Launching the dedicated function from the library for the INFERNAL alignment program
				$file_name_pipe = PerlLib::InfernalAlignment::Alignment("RAW", $parameters[1], $file, $folder_input, $folder_output, $#files_Treated, $limit_nb_cores);
				#We then catch the name of the created file to use it for further steps
				$file = $file_name_pipe;
				$folder_input = $folder_output;
			}
		}
#FILTERING_RAW_ALIGNMENT---------We then launch the search parameters function defined earlier
		$ref_array = PerlLib::Miscellaneous::SearchInputParameters("FILTERING_RAW_ALIGNMENT");
		@parameters = @$ref_array;
		if (uc($parameters[0]) eq "YES")
		{
			#First, we search for needed folder names (input and/or output for the treatment)
			$folder_output = PerlLib::ManagingDirectories::FindDirectory("FILTERING_RAW_ALIGNMENT", \@folders);
			#Then, we also search for the folder containing dereplicated reads and alignments
			$folder_derep = PerlLib::ManagingDirectories::FindDirectory("DEREPLICATION", \@folders);
			$folder_align = PerlLib::ManagingDirectories::FindDirectory("RAW_INFERNAL_ALIGNMENT", \@folders);
			#Launching the dedicated function from the library for the INFERNAL alignment program
			($file_name_pipe, $file_derep) = PerlLib::FilteringInfernalAlignment::FilteringInfernalAlignment($file, $folder_input, $folder_output, $folder_derep, $folder_align);
			#We then catch the name of the created file to use it for further steps
			$file = $file_name_pipe;
			$folder_input = $folder_output;
		}

#RAW_CLUSTERING---------We then launch the search parameters function defined earlier
		$ref_array = PerlLib::Miscellaneous::SearchInputParameters("RAW_CLUSTERING");
		@parameters = @$ref_array;
		if (uc($parameters[0]) eq "YES")
		{
			# First, we search for needed folder names (input and/or output for the treatment)
			$folder_output = PerlLib::ManagingDirectories::FindDirectory("RAW_CLUSTERING", \@folders);
			#Definiton of the size of the file (in Octet)
			$taille = (stat("Result_files/$folder_input/$file"))[7];
			# We then launch the dedicated clustering function with chosen parameters based
			# on the size of the file
			#if ($taille > 100000000000) { $file = PerlLib::ClusteringWithIndex::Clustering("RAW", $parameters[1], $parameters[2], $file, $parameters[3], $parameters[4], $folder_input, $folder_output, "Raw_Clustering_summary_"); }
			#else { $file = PerlLib::Clustering::Clustering("RAW", $parameters[1], $parameters[2], $file, $parameters[3], $parameters[4], $folder_input, $folder_output, "Raw_Clustering_summary_"); }
			$file = PerlLib::Clustering::Clustering("RAW", $parameters[1], $parameters[2], $file, $parameters[3], $parameters[4], $folder_input, $folder_output, "Raw_Clustering_summary_");
			$folder_input = $folder_output;
		}

#HUNTING------------We then launch the search parameters function defined earlier
		$ref_array = PerlLib::Miscellaneous::SearchInputParameters("HUNTING");
		@parameters = @$ref_array;
		if (uc($parameters[0]) eq "YES")
		{
			#First, we search for needed folder names (input and/or output for the treatment)
			$folder_output = PerlLib::ManagingDirectories::FindDirectory("HUNTING", \@folders);
			#Particular test to check if the FILTERING_RAW_ALIGNMENT step is done or not to chose
			#the right folder to treat reads.
			$ref_array = PerlLib::Miscellaneous::SearchInputParameters("FILTERING_RAW_ALIGNMENT");
			@parameters = @$ref_array;
			if (uc($parameters[0]) eq "YES") { $folder_input_fasta = PerlLib::ManagingDirectories::FindDirectory("FILTERING_RAW_ALIGNMENT", \@folders); }
			else { $folder_input_fasta = PerlLib::ManagingDirectories::FindDirectory("DEREPLICATION", \@folders) }
			#Then, we used againg the same function to catch the parameters of the HUNTING step
			$ref_array = PerlLib::Miscellaneous::SearchInputParameters("HUNTING");
			@parameters = @$ref_array;
			#Print for the user information that the program is started
			PerlLib::Miscellaneous::PrintScreen("SINGLETON_HUNTING",$file,"started");
			#Needed command line constructed. Launch the command line instruction using the "`". And we catch
			#the print in the $file_name_pipe variable to be used further
			$file_name_pipe = `perl $FindBin::RealBin/Prog_Perl/Hunt_ssingleton_1.8.pl $parameters[1] $file $folder_input $folder_input_fasta $folder_output`;
			#Print for the user information that the program is terminated
			PerlLib::Miscellaneous::PrintScreen("SINGLETON_HUNTING",$file,"terminated");

			$ref_array = PerlLib::Miscellaneous::SearchInputParameters("RECOVERING");
			@parameters = @$ref_array;
			#V18: We then catch the name of the created file, and manage the fact that the
			#file can be empty or not #giving as third element of the $file_name_pipe string
			if(($file_name_pipe =~ m/EMPTY/)&&(uc($parameters[0]) eq "NO")) { exit(0); }
			$file = $file_name_pipe;
			$folder_input = $folder_output;
		}

#RECOVERING---------We then launch the search parameters function defined earlier
		$ref_array = PerlLib::Miscellaneous::SearchInputParameters("RECOVERING");
		@parameters = @$ref_array;
		if (uc($parameters[0]) eq "YES")
		{
			#First, we search for needed folder names (input and/or output for the treatment)
			$folder_output = PerlLib::ManagingDirectories::FindDirectory("RECOVERING", \@folders);
			@files_Hunt = split(" ", $file);
			#Print for the user information that the program is started
			PerlLib::Miscellaneous::PrintScreen("SINGLETON_RECOVERING",$files_Hunt[0],"started");
			#V18: Needed command line constructed. Launch the command line instruction using the "`". And we catch
			#the print in the $file_name_pipe variable to be used further
			$file_name_pipe = `perl $FindBin::RealBin/Prog_Perl/Taxo_recovery_3.3.pl $files_Hunt[1] $parameters[1] $parameters[2] $parameters[4] $parameters[5] $parameters[6] $files_Hunt[0] $folder_input $folder_output $parameters[7] $parameters[3] $files_Hunt[2]`;
			#V20 MODIF: Modification of the Taxo_recovery output to catch both file names and
			#the corresponding number of reads for each file treated, to give this info to the
			#ThresholdCheckings library
			@tmp = split(" ", $file_name_pipe);
			$file_name_pipe = $tmp[0];
			$nbreads_recovering = $tmp[1];
			PerlLib::ThresholdCheckings::ThresholdCheckings($file_name_pipe, $nbreads_recovering, "RECOVERING");
			#Print for the user information that the program is terminated
			PerlLib::Miscellaneous::PrintScreen("SINGLETON_RECOVERING",$files_Hunt[0],"terminated");
			#We then catch the name of the created file, and manage the fact that the file can be empty or not
			if($nbreads_recovering == 0) { exit(0); }
			$file = $file_name_pipe;
			$folder_input = $folder_output;
		}
		#Part needed to manage the chosen possibility that the recovering was not realized
		else
		{
			$ref_array = PerlLib::Miscellaneous::SearchInputParameters("HUNTING");
			@parameters = @$ref_array;
			if (uc($parameters[0]) eq "YES")
			{
				#We realized next steps of the analysis using only the cleaned reads.
				@files_Hunt = split(" ", $file);
				$file = $files_Hunt[0];
			}
			else { $file = $file_fasta; }
		}

#RANDOM_CLEANED-----This step will be done based on the user choice
		$ref_array = PerlLib::Miscellaneous::SearchInputParameters("RANDOM_CLEANED");
		@parameters = @$ref_array;
		if (uc($parameters[0]) eq "YES")
		{
			#First, we search for needed folder names (input and/or output for the treatment)
			$folder_output = PerlLib::ManagingDirectories::FindDirectory("RANDOM_CLEANED", \@folders);
			if ($parameters[1] != 0)
			{
				#We then launch the function dedicated to the realization of the homogenization step
				$file = PerlLib::RandomSelection::RandomSelection($parameters[1], $file, $folder_input, $folder_output);
				$folder_input = $folder_output;
			}
		}

#TAXONOMY-----------We then launch the search parameters function defined earlier
		$ref_array = PerlLib::Miscellaneous::SearchInputParameters("TAXONOMY");
		@parameters = @$ref_array;
		if (uc($parameters[0]) eq "YES")
		{
			#First, we search for needed folder names (input and/or output for the treatment)
			$folder_output = PerlLib::ManagingDirectories::FindDirectory("TAXONOMY", \@folders);
			#We then launch the function dedicated to the realization of the taxonomy assignation step
			PerlLib::Taxonomy::Taxonomy($file, $parameters[1], $parameters[2], $parameters[3], $parameters[5], $folder_output, $parameters[4], \@folders);
		}


#GLOBAL_ANALYSIS-----We launch the GLOBAL_ANALYSIS pretreatment using the defined parameters by the user if needed
		$ref_array = PerlLib::Miscellaneous::SearchInputParameters("GLOBAL_ANALYSIS");
		@parameters = @$ref_array;
		if (uc($parameters[0]) eq "YES")
		{
			if ($concatenation_step == 2)
			{
				#First, we look for the RANDOM_PREPROCESS directory
				$folder_output_globanalysis = PerlLib::ManagingDirectories::FindDirectory("RANDOM_CLEANED", \@folders);
				if ($folder_output_globanalysis eq "None")
				{
					#Then, we look for the PREPROCESSING directory
					$folder_output_globanalysis = PerlLib::ManagingDirectories::FindDirectory("RECOVERING", \@folders);
					if ($folder_output_globanalysis eq "None")
					{
						#Then, we look for the RANDOM_PREPROCESS_MIDS directory
						$folder_output_globanalysis = PerlLib::ManagingDirectories::FindDirectory("HUNTING", \@folders);
						if ($folder_output_globanalysis eq "None")
						{
							#Then, we look for the PREPROCESS_MIDS directory
							$folder_output_globanalysis = PerlLib::ManagingDirectories::FindDirectory("RAW_INFERNAL_ALIGNMENT", \@folders);
							if (($folder_output_globanalysis eq "None") || ($folder_output_globanalysis eq "RAW_INFERNAL_ALIGNMENT"))
							{
								#Finally, we chose to use the dereplication directory defined previously
								$folder_output_globanalysis = "DEREPLICATION";
							}
						}
					}
				}
				#Print for the user information that the program is started
				PerlLib::Miscellaneous::PrintScreen("GLOBAL MERGING",$file,"started");
				#Needed command line constructed. Launch the command line instruction using the "`". And we catch
				#the print in the $file_name_pipe variable to be used further
				PerlLib::PoolingReads::PoolingReads($file, $folder_input, $folder_output_globanalysis, $concatenation_step);
				#Print for the user information that the program is terminated
				PerlLib::Miscellaneous::PrintScreen("GLOBAL MERGING",$file,"terminated");
			}
		}

#ALIGN-GLOBAL_ANALYSIS---Here, we launch the independent alignments for all samples, if the user chose the GLOBAL_ANALYSIS
		#treatment, and also to do multiple alignments.
		$ref_array = PerlLib::Miscellaneous::SearchInputParameters("GLOBAL_ANALYSIS");
		@parameters = @$ref_array;
		if (uc($parameters[0]) eq "YES")
		{
			$ref_array = PerlLib::Miscellaneous::SearchInputParameters("CLEAN_INFERNAL_ALIGNMENT");
			@parameters = @$ref_array;
			if (uc($parameters[0]) eq "YES")
			{
				#First, we search for needed folder names (input and/or output for the treatment)
				$folder_output = PerlLib::ManagingDirectories::FindDirectory("CLEAN_INFERNAL_ALIGNMENT", \@folders);
				#Launching the dedicated function from the library for the INFERNAL alignment program
				$file_name_pipe = PerlLib::InfernalAlignment::Alignment("CLEAN", $parameters[1], $file, $folder_input, $folder_output, $#files_Treated, $limit_nb_cores);
				#We then catch the name of the created file to use it for further steps
				$file = $file_name_pipe;
				$folder_input = $folder_output;
			}
		}
		if(defined($file))
		{
			#We create with this function a temp file (infos.info) storing everything needed to relaunch treatments
			PerlLib::Miscellaneous::FileInfoAppending($file, $folder_output);
		}
		#We then quit the loop to simplify the analysis (and treat the case of the GLOBAL_ANALYSIS analysis)
		exit(0);
	}
	#If we can't fork (create parent and child processes, die the process)
	else { die "couldn’t fork: $!\n"; }
}
#The parent process wait for all child results
foreach (@childs) {waitpid($_, 0);}

#Then, we launch the CatchTreatedFileNames defined function to catch needed parameters after the children
#treatment (to find again all information like $folder_input, and file names in the parent process).
($folder_input, $ref_array) = PerlLib::Miscellaneous::CatchTreatedFileNames();
if (defined($ref_array)) { @files_Treated = @$ref_array; }

#Here, we search for the user choice to add if needed the name of the concatenated file to treat it during the defined steps
#of analysis. If it is the case, we also launch the strict dereplication for the Concatenated file as we just create it with
#the dedicated function
$ref_array = PerlLib::Miscellaneous::SearchInputParameters("GLOBAL_ANALYSIS");
@parameters = @$ref_array;
if ((uc($parameters[0]) eq "YES") && ($concatenation_step == 2))
{
	#First, we look for the RANDOM_CLEANED directory
	$folder_output_globanalysis = PerlLib::ManagingDirectories::FindDirectory("RANDOM_CLEANED", \@folders);
	if ($folder_output_globanalysis eq "None")
	{
		#Then, we look for the RECOVERING directory
		$folder_output_globanalysis = PerlLib::ManagingDirectories::FindDirectory("RECOVERING", \@folders);
		if ($folder_output_globanalysis eq "None")
		{
			#Then, we look for the HUNTING directory
			$folder_output_globanalysis = PerlLib::ManagingDirectories::FindDirectory("HUNTING", \@folders);
			if ($folder_output_globanalysis eq "None")
			{
				#Then, we look for the RAW_INFERNAL_ALIGNMENT directory
				$folder_output_globanalysis = PerlLib::ManagingDirectories::FindDirectory("RAW_INFERNAL_ALIGNMENT", \@folders);
				if (($folder_output_globanalysis eq "None") || ($folder_output_globanalysis eq "RAW_INFERNAL_ALIGNMENT"))
				{
					#Finally, we chose to use the dereplication directory defined previously
					$folder_output_globanalysis = "DEREPLICATION";
				}
			}
		}
	}

	#Needed command line constructed. Launch the command line instruction using the "`". And we catch
	#the print in the $file_name_pipe variable to be used further
	$file_name_pipe = PerlLib::Dereplication::Dereplication("TMP_Concatenated_reads.fasta.fasta", "Temporary_files", $folder_output_globanalysis);
	$folder_input_globanalysis = $folder_output_globanalysis;
}
#We then launch again the GLOBAL_ANALYSIS research to define if the multiple alignment is realized efficiently or not
$ref_array = PerlLib::Miscellaneous::SearchInputParameters("GLOBAL_ANALYSIS");
@parameters = @$ref_array;
if ((uc($parameters[0]) eq "YES") && ($concatenation_step == 2))
{
	$ref_array = PerlLib::Miscellaneous::SearchInputParameters("CLEAN_INFERNAL_ALIGNMENT");
	@parameters = @$ref_array;
	if (uc($parameters[0]) eq "YES")
	{
		#Here, we just search for the output directory to save the alignment
		$folder_output_globanalysis = PerlLib::ManagingDirectories::FindDirectory("CLEAN_INFERNAL_ALIGNMENT", \@folders);
		#Print for the user information that the program is started
		PerlLib::Miscellaneous::PrintScreen("GLOBAL ALIGNMENT","Derep_Concatenated_reads.fasta.fasta","started");
		#Needed command line constructed. Launch the command line instruction using the "`". And we catch
		#the print in the $file_name_pipe variable to be used further
		$file_name_pipe = PerlLib::GlobalAlignment::GlobalAlignment($parameters[1], "Derep_Concatenated_reads.fasta.fasta", $folder_input_globanalysis, $folder_output_globanalysis);
		#Print for the user information that the program is terminated
		PerlLib::Miscellaneous::PrintScreen("GLOBAL ALIGNMENT","Derep_Concatenated_reads.fasta.fasta","terminated");
		#We then add in the @files_Treated array the name of the new file (alignement file) for further analysis
		push(@files_Treated, "Align_Derep_Concatenated_reads.fasta.fasta");
	}
	#We then add in the @files_Treated array the name of the new file (dereplicated file if no multiple
	#alignment is realized) for further analysis
	else { push(@files_Treated, "Derep_Concatenated_reads.fasta.fasta"); }
}

#We clean these variables to use them in the next step of analysis
$pid = 0;
@childs = ();
$nb_processes = 0;

#=======================================================================================================
# Fifth step: Treatment (alignment, clustering, taxonomy) of chosen files and sequences
#=======================================================================================================

#Based on the chosen files, we create the number of children needed
foreach $file (@files_Treated)
{
	#This test is defined to realize the treatment on a maximum of X different files defined by the
	#$limit_nb_cores : if the array @childs stored the good number of child preocesses pids.
	if($nb_processes >= $limit_nb_cores)
	{
		$pid = wait();
		$nb_processes --;
	}
	#Creation of a parent and a child process. One child for each found file needed treatment
	$pid = fork();
	#If the program is the parent! (only one)
	if ($pid)
	{
		push(@childs, $pid);
		$nb_processes ++;
	}
	#If the program is a child (number of children as the number of treated files)
	elsif ($pid == 0)
	{

#CLEAN_ALIGNMENT----We then launch the search parameters function defined earlier
		$ref_array = PerlLib::Miscellaneous::SearchInputParameters("GLOBAL_ANALYSIS");
		@parameters = @$ref_array;
		if (uc($parameters[0]) eq "NO")
		{
			$ref_array = PerlLib::Miscellaneous::SearchInputParameters("CLEAN_INFERNAL_ALIGNMENT");
			@parameters = @$ref_array;
			if (uc($parameters[0]) eq "YES")
			{
				#First, we look for the RANDOM_PREPROCESS directory
				$folder_input = PerlLib::ManagingDirectories::FindDirectory("RANDOM_CLEANED", \@folders);
				if ($folder_input eq "None")
				{
					#Then, we look for the PREPROCESSING directory
					$folder_input = PerlLib::ManagingDirectories::FindDirectory("RECOVERING", \@folders);
					if ($folder_input eq "None")
					{
						#Then, we look for the RANDOM_PREPROCESS_MIDS directory
						$folder_input = PerlLib::ManagingDirectories::FindDirectory("HUNTING", \@folders);
						if ($folder_input eq "None")
						{
							#Then, we look for the RAW_INFERNAL_ALIGNMENT directory
							$folder_input = PerlLib::ManagingDirectories::FindDirectory("RAW_INFERNAL_ALIGNMENT", \@folders);
							if (($folder_input eq "None") || ($folder_output_globanalysis eq "RAW_INFERNAL_ALIGNMENT"))
							{
								#Finally, we chose to use the dereplication directory defined previously
								$folder_input = "DEREPLICATION";
							}
						}
					}
				}

#CLEAN_ALIGNMENT----We then launch the search parameters function defined earlier
				#First, we search for needed folder names (input and/or output for the treatment)
				$folder_output = PerlLib::ManagingDirectories::FindDirectory("CLEAN_INFERNAL_ALIGNMENT", \@folders);
				#Launching the dedicated function from the library for the INFERNAL alignment program
				$file_name_pipe = PerlLib::InfernalAlignment::Alignment("CLEAN", $parameters[1], $file, $folder_input, $folder_output, $#files_Treated, $limit_nb_cores);
				#We then catch the name of the created file to use it for further steps
				$file = $file_name_pipe;
				$folder_input = $folder_output;

			}
		}

#CLEAN_CLUSTERING---We then launch the search parameters function defined earlier
		$ref_array = PerlLib::Miscellaneous::SearchInputParameters("CLEAN_CLUSTERING");
		@parameters = @$ref_array;
		if (uc($parameters[0]) eq "YES")
		{

			#First, we search for needed folder names (input and/or output for the treatment)
			$folder_output = PerlLib::ManagingDirectories::FindDirectory("CLEAN_CLUSTERING", \@folders);
			$taille = (stat("Result_files/$folder_input/$file"))[7];
			# V18 We then launch the dedicated clustering function with chosen parameters based
			# on the size of the file
			# if ($taille > 1000000000) { $file = PerlLib::ClusteringWithIndex::Clustering("CLEAN", $parameters[1], $parameters[2], $file, $parameters[3], $parameters[4], $folder_input, $folder_output, "Clean_Clustering_summary_"); }
			# else { $file = PerlLib::Clustering::Clustering("CLEAN", $parameters[1], $parameters[2], $file, $parameters[3], $parameters[4], $folder_input, $folder_output, "Clean_Clustering_summary_"); }
			$file = PerlLib::Clustering::Clustering("CLEAN", $parameters[1], $parameters[2], $file, $parameters[3], $parameters[4], $folder_input, $folder_output, "Clean_Clustering_summary_");
			$folder_input = $folder_output;
		}
		exit(0);
	}
	#If we can't fork (create parent and child processes, die the process)
	else {die "couldn’t fork: $!\n";}
}
#The parent process wait for all child results
foreach (@childs) {waitpid($_, 0);}

#=======================================================================================================
# Sixth step: Analysis and definition of all summary files, figures, etc...
#=======================================================================================================

#We search another time the TAXONOMY step to analyze raw data
$ref_array = PerlLib::Miscellaneous::SearchInputParameters("TAXONOMY");
@parameters = @$ref_array;
if (uc($parameters[0]) eq "YES")
{
	#First, we search for needed folder names (input and/or output for the treatment)
	$folder_input = PerlLib::ManagingDirectories::FindDirectory("TAXONOMY", \@folders);
	#Print for the user information that the program is started
	PerlLib::Miscellaneous::PrintScreen("TAXONOMY",$folder_input,"started");
	#V18: Needed command line constructed. Launch the command line instruction using the "`". And we catch
	#the print in the $file_name_pipe variable to be used further
	$file_name_pipe = `perl $FindBin::RealBin/Prog_Perl/Taxo_treatment_2.8.pl $parameters[1] $parameters[2] $parameters[3] $parameters[4] $folder_input`;
	#Print for the user information that the program is terminated
	PerlLib::Miscellaneous::PrintScreen("TAXONOMY",$folder_input,"terminated");

	#Print for the user information that the program is started
	PerlLib::Miscellaneous::PrintScreen("TAXONOMY_GRAPH_COMPUTATIONS",$folder_input,"started");
	#V18: Needed command line constructed. Launch the command line instruction using the "`". And we catch
	#the print in the $file_name_pipe variable to be used further
	$file_name_pipe = `python2.7 $FindBin::RealBin/Prog_Python/Taxo_graph_computations_2.1.py`;
	#Print for the user information that the program is terminated
	PerlLib::Miscellaneous::PrintScreen("TAXONOMY_GRAPH_COMPUTATIONS",$folder_input,"terminated");
}

#We then search COMPUTATION step to compute rarefaction curves and indices
$ref_array = PerlLib::Miscellaneous::SearchInputParameters("COMPUTATION");
@parameters = @$ref_array;
if (uc($parameters[0]) eq "YES")
{
	if ((uc($parameters[4]) eq "YES")||(uc($parameters[5]) eq "YES"))
	{
		mkdir("Summary_files/Curves_Data");
		mkdir("Summary_files/Figures/Curves");
	}
	#Test to determine if the analysis is done on raw, clean or both clustering
	#Based on the test result, we will run with different parameters the ComputationLauncher function
	if (uc($parameters[1]) eq "RAW")
	{
		#First, we search for needed folder names (input and/or output for the treatment)
		$folder_input = PerlLib::ManagingDirectories::FindDirectory("RAW_CLUSTERING", \@folders);
		PerlLib::ProgramLaunchers::ComputationLauncher("Raw", $folder_input, \@parameters);
	}
	elsif (uc($parameters[1]) eq "CLEAN")
	{
		#First, we search for needed folder names (input and/or output for the treatment)
		$folder_input = PerlLib::ManagingDirectories::FindDirectory("CLEAN_CLUSTERING", \@folders);
		PerlLib::ProgramLaunchers::ComputationLauncher("Clean", $folder_input, \@parameters);
	}
	elsif (uc($parameters[1]) eq "BOTH")
	{
		#First, we search for needed folder names (input and/or output for the treatment)
		$folder_input = PerlLib::ManagingDirectories::FindDirectory("RAW_CLUSTERING", \@folders);
		PerlLib::ProgramLaunchers::ComputationLauncher("Raw", $folder_input, \@parameters);
		$folder_input = PerlLib::ManagingDirectories::FindDirectory("CLEAN_CLUSTERING", \@folders);
		PerlLib::ProgramLaunchers::ComputationLauncher("Clean", $folder_input, \@parameters);
	}
}

#We then search for the GLOBAL_ANALYSIS step to compute the needed MOTU matrix, and define the
#needed graphics
$ref_array = PerlLib::Miscellaneous::SearchInputParameters("GLOBAL_ANALYSIS");
@parameters = @$ref_array;
if (uc($parameters[0]) eq "YES")
{
	$folder_taxonomy = PerlLib::ManagingDirectories::FindDirectory("TAXONOMY", \@folders);
	#Test to determine if the analysis is done on raw, clean or both clustering
	#Based on the test result, we will run with different parameters the &GlobalAnalysisLauncher function
	if ((uc($parameters[1]) eq "RAW")||(uc($parameters[1]) eq "BOTH"))
	{
		#First, we search for needed folder names (input and/or output for the treatment)
		$folder_input = PerlLib::ManagingDirectories::FindDirectory("RAW_CLUSTERING", \@folders);
		$folder_align = PerlLib::ManagingDirectories::FindDirectory("RAW_INFERNAL_ALIGNMENT", \@folders);
		PerlLib::ProgramLaunchers::GlobAnalysisLauncher("Raw", $folder_input, $parameters[2], $folder_taxonomy, $parameters[3], $parameters[4], $parameters[5], $folder_align, $parameters[6]);
	}
	if ((uc($parameters[1]) eq "CLEAN")||(uc($parameters[1]) eq "BOTH"))
	{
		#First, we search for needed folder names (input and/or output for the treatment)
		$folder_input = PerlLib::ManagingDirectories::FindDirectory("CLEAN_CLUSTERING", \@folders);
		$folder_align = PerlLib::ManagingDirectories::FindDirectory("CLEAN_INFERNAL_ALIGNMENT", \@folders);
		PerlLib::ProgramLaunchers::GlobAnalysisLauncher("Clean", $folder_input, $parameters[2], $folder_taxonomy, $parameters[3], $parameters[4], $parameters[5], $folder_align, $parameters[6]);
	}
}

#We then search for the GLOBAL_ANALYSIS step to compute the needed MOTU matrix, and define the
#needed graphics
$ref_array = PerlLib::Miscellaneous::SearchInputParameters("RECLUSTOR");
@parameters = @$ref_array;
if (uc($parameters[0]) eq "YES")
{
	#We search for the clustering folder using the global analysis step
	$ref_array = PerlLib::Miscellaneous::SearchInputParameters("GLOBAL_ANALYSIS");
	@parameters_glob = @$ref_array;
	#First, we search for needed folder names (input and/or output for the treatment)
	if(uc($parameters_glob[0]) eq "YES")
	{
		if (uc($parameters[1]) eq "RAW")
		{
			$folder_clustering = PerlLib::ManagingDirectories::FindDirectory("RAW_CLUSTERING", \@folders);
			$folder_align = PerlLib::ManagingDirectories::FindDirectory("RAW_INFERNAL_ALIGNMENT", \@folders);
		}
		else
		{
			$folder_clustering = PerlLib::ManagingDirectories::FindDirectory("CLEAN_CLUSTERING", \@folders);
			$folder_align = PerlLib::ManagingDirectories::FindDirectory("CLEAN_INFERNAL_ALIGNMENT", \@folders);
		}
	}
	#Or ??
	else
	{
	}
	#Print for the user information that the program is started
	PerlLib::Miscellaneous::PrintScreen("ReClustOR","The whole dataset","started");
	PerlLib::ReClustOR::ReClustOR(@parameters, $folder_clustering, $folder_align, $folder_input_globanalysis, $limit_nb_cores);
	PerlLib::Miscellaneous::PrintScreen("ReClustOR","The whole dataset","terminated");
}

#We finally realize the UNIFRAC_ANALYSIS step to compute all mathematical analyses, realized using
#the Newick tree and the ID mapping file from the GLOBAL_ANALYSIS step
$ref_array = PerlLib::Miscellaneous::SearchInputParameters("UNIFRAC_ANALYSIS");
@parameters = @$ref_array;
if (uc($parameters[0]) eq "YES")
{
	#We first verify that this particular step is realized, as we need its results to compute
	#mathematical significance tests
	$ref_array = PerlLib::Miscellaneous::SearchInputParameters("GLOBAL_ANALYSIS");
	@parameters = @$ref_array;
	if ((uc($parameters[0]) eq "YES")&&(uc($parameters[6]) eq "YES"))
	{
		$ref_array = PerlLib::Miscellaneous::SearchInputParameters("UNIFRAC_ANALYSIS");
		@parameters = @$ref_array;
		#Print for the user information that the program is started
		PerlLib::Miscellaneous::PrintScreen("UNIFRAC_ANALYSIS","Concatenated_reads.fasta","started");
		#V18: Needed command line constructed. Launch the command line instruction using the "`". And we catch
		#the print in the $file_name_pipe variable to be used further
		$file_name_pipe = `python2.7 $FindBin::RealBin/Prog_Python/Unifrac_computation_1.1.py $parameters[1] $parameters[2] $parameters[3]`;
		#Print for the user information that the program is terminated
		PerlLib::Miscellaneous::PrintScreen("UNIFRAC_ANALYSIS","Concatenated_reads.fasta","terminated");
	}
	else
	{
		#Print for the user information that the program is started
		PerlLib::Miscellaneous::PrintScreen("Unifrac_computation_1.0.py","Concatenated_reads.fasta","started");
		#We inform the user that this step is not realized as the GLOBAL_ANALYSIS step did
		#not produce the needed files (Tree and ID mapping file).
		print "==> This step was not realized contrary to your choice !!
		Indeed, no GLOBAL_ANALYSIS results (Phylogenetic Tree and ID
		mapping file) are available to compute analyses. <==";
		#Print for the user information that the program is terminated
		PerlLib::Miscellaneous::PrintScreen("Unifrac_computation_1.0.py","Concatenated_reads.fasta","terminated")
	}
}
exit(0);
