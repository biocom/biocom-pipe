## -*- coding: utf-8 -*-

import biocomPipe_utils
import sys

python_script, path_biocom_pipe, path_analysis = sys.argv
# print 'Number of arguments:', len(sys.argv), 'arguments.'
# print 'Argument List:', str(sys.argv)

DIR_BIOCOM_PIPE = path_biocom_pipe
DIR_BIOCOM_PIPE_ANALYSIS = path_analysis


if __name__ == '__main__':
    # DIR_BIOCOM_PIPE = "/Users/cdjemiel/Documents/professionnel/postdoc/INRA-dijon/script/biocom-pipe/utils/"
    # DIR_BIOCOM_PIPE_ANALYSIS = "/Users/cdjemiel/Documents/professionnel/postdoc/INRA-dijon/script/biocom-pipe"
    run_figs = biocomPipe_utils.BiocomPipeUtils(DIR_BIOCOM_PIPE,DIR_BIOCOM_PIPE_ANALYSIS)
    print "===Step: Generate figs for the website===".format()
    run_figs.copy_templates()
    run_figs.read_input_file()
    run_figs.create_figs_folders_tree()
    run_figs.read_releases_details_file()
    run_figs.get_sample_names_pf()
    run_figs.create_figs_prinseq()
    run_figs.create_figs_flash()
    run_figs.create_figs_eval_qual()
    run_figs.create_figs_preprocess_mids()
    run_figs.create_figs_hunting()
    run_figs.create_figs_recovering()
    run_figs.create_figs_reclustor()
    run_figs.create_figs_index()
    run_figs.create_figs_rank_abundance()
    run_figs.create_figs_rarefaction_curve()
    run_figs.create_figs_unifrac_tree()
    run_figs.create_figs_unifrac_pd()
    run_figs.create_figs_taxonomy()
    # #TODO
    # # run_figs.create_figs_krona()
    # # run_figs.get_databases_tax()
    print "\n===Step compress results files==="
    run_figs.compress_files()
