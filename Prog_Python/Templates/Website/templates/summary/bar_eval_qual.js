$(document).ready(function() {
    var options = {
        chart: {
            renderTo: 'container-eval_qual_bar',
            defaultSeriesType: 'area',
            // ignoreHiddenSeries: false,
        },
        credits: {
          enabled: false
        },
        plotOptions: {
          area: {
              stacking: 'normal',
              lineColor: '#666666',
              lineWidth: 1,
              marker: {
                  lineWidth: 1,
                  lineColor: '#666666'
              }
          }
        },
        legend: {
          itemWidth: 150,
          adjustChartSize: true,
          navigation: {
            enabled: false
          },
          title: {
            text: 'Quality<br/><span style="font-size: 9px; color: #666; font-weight: normal">(Click to hide)</span>',
          },
        },
        xAxis: {
            categories:categories_eval_qual
        },
        tooltip: {
            pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}<br/>',
            split: true
        },
        colors: color_bar_eval_qual,
        exporting: {
                    sourceWidth: 1400,
                    sourceHeight: 1200,
                    // scale: 2 (default)
        }
    };
    //Set Highcharts options in function of select value
    var chart = new Highcharts.Chart(options);
    var selVal_c = $("#mySelect").val();
    options.series = dict_bar_eval_qual_data[selVal_c];
    options.title = { text: 'Aera ' + selVal_c} ;
    var chart = new Highcharts.Chart(options);
    $("#mySelect").on('change', function(){
        var selVal = $("#mySelect").val();
        options.series = dict_bar_eval_qual_data[selVal];
        options.title = { text: 'Aera ' + selVal} ;
        var chart = new Highcharts.Chart(options);
    });
});// End document
