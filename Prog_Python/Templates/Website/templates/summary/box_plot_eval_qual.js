$(document).ready(function() {
    var options = {
        chart: {
            renderTo: 'container-eval_qual_box_plot',
            defaultSeriesType: 'boxplot',
            // ignoreHiddenSeries: false,
        },
        // title: {
        //   text: 'Highcharts Box Plot Example'
        // },
        credits: {
          enabled: false
        },
        plotOptions: {
          area: {
              stacking: 'normal',
              lineColor: '#666666',
              lineWidth: 1,
              marker: {
                  lineWidth: 1,
                  lineColor: '#666666'
              }
          }
        },
        legend: {
          enabled: false,
          itemWidth: 150,
          adjustChartSize: true,
          navigation: {
            enabled: false
          },
          title: {
            text: 'Quality<br/><span style="font-size: 9px; color: #666; font-weight: normal">(Click to hide)</span>',
          },
        },
        xAxis: {
            categories:categories_eval_qual
        },
        tooltip: {
            headerFormat: '<em>Range {point.key} bp</em><br/>'
        },
        // colors: color_bar_eval_qual,
        exporting: {
                    sourceWidth: 1400,
                    sourceHeight: 1200,
                    // scale: 2 (default)
        }
    };
    //Set Highcharts options in function of select value
    var chart = new Highcharts.Chart(options);
    var selVal_c = $("#mySelect").val();
    options.series = dict_box_plot_eval_qual_data[selVal_c];
    options.title = { text: 'Box plot ' + selVal_c} ;
    var chart = new Highcharts.Chart(options);
    $("#mySelect").on('change', function(){
        var selVal = $("#mySelect").val();
        options.series = dict_box_plot_eval_qual_data[selVal];
        options.title = { text: 'Box plot ' + selVal} ;
        var chart = new Highcharts.Chart(options);
    });
});// End document
