$(document).ready(function() {
    //Create Highcharts options by defaults
    var options = {
        chart: {
            renderTo: 'container-rank_abundance',
            // defaultSeriesType: 'lines',
            ignoreHiddenSeries: false,
        },
        credits: {
          enabled: false
        },
        title: {
          text: 'Rank abundance part_1',
        },
        legend: {
          itemWidth: 150,
          adjustChartSize: true,
          navigation: {
            enabled: false
          }
        },
        xAxis: {
          title: {
            text: 'Rank of defined MOTUs',
          },
        },
        yAxis: {
            title: {
              text: 'Relative abundance (log)',
            },
            type: 'logarithmic',
            minorTickInterval: 0.1
        },
        series: dict_rank_abundance["part_1"],
        exporting: {
                    sourceWidth: 1400,
                    sourceHeight: 1200,
                    // scale: 2 (default)
        }
    };
    //Set Highcharts options in function of select value
    var chart = new Highcharts.Chart(options);
    var selVal_c = $("#mySelect_rank_abundance").val();
    options.title = { text: 'Rank abundance ' + selVal_c} ;
    $("#mySelect_rank_abundance").on('change', function(){
        var selVal = $("#mySelect_rank_abundance").val();
        options.series = dict_rank_abundance[selVal];
        options.title = { text: 'Rank abundance ' + selVal} ;
        var chart = new Highcharts.Chart(options);
    });
});// End document
