$(document).ready(function() {
    //Create Highcharts options by defaults
    var options = {
        chart: {
            renderTo: 'container-rarefaction_curve',
            defaultSeriesType: 'line',
            ignoreHiddenSeries: false,
        },
        credits: {
          enabled: false
        },
        title: {
          text: 'Rarefaction curve part_1',
        },
        legend: {
          itemWidth: 150,
          adjustChartSize: true,
          navigation: {
            enabled: false
          }
        },
        xAxis: {
          title: {
            text: 'Rank of defined MOTUs',
          },
          categories: x_rarefaction_curve,
        },
        yAxis: {
            title: {
              text: 'Relative abundance (%)',
            },
            // type: 'logarithmic',
            // minorTickInterval: 0.1
        },
        series: dict_rarefaction_curve["part_1"],
        exporting: {
                    sourceWidth: 1400,
                    sourceHeight: 1200,
                    // scale: 2 (default)
        }
    };
    //Set Highcharts options in function of select value
    var chart = new Highcharts.Chart(options);
    var selVal_c = $("#mySelect_rarefaction_curve").val();
    options.title = { text: 'rarefaction_curve ' + selVal_c} ;
    $("#mySelect_rarefaction_curve").on('change', function(){
        var selVal = $("#mySelect_rarefaction_curve").val();
        options.series = dict_rarefaction_curve[selVal];
        options.title = { text: 'rarefaction_curve ' + selVal} ;
        var chart = new Highcharts.Chart(options);
    });
});// End document
