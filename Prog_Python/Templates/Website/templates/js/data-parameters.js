var data_prinseq = {
  title: 'prinseq',
  content: "Step to do [yes-no] = yes <br  />Lowest quality score tolerated for the trimming from the 3'-end of the read [0-40] = 30 <br  />Lowest quality score tolerated for the trimming from the 5'-end of the read [0-40] = 30 <br  />Minimum Length threshold tolerated to keep reads (default =  30) <br  />Number of ambiguities (N's) tolerated (default = 1 <br  />Sliding window size used to calculate quality score [1-7] = 7 <br  />Step size used to move the sliding window [1-10] = 1"
  }

  var data_flash = {
  title: 'flash',
  content: "Step to do [yes-no] = yes <br  />Minimum overlap length between two reads (default =  10) [10-100] <br  />Maximum overlap length between two reads (default =  65) [10-100] <br  />Maximum allowed ratio between the number of mismatched base pairs and the overlap length (default =  0.04) [0-100]"
  }

  var data_eval_qual = {
  title: 'eval_qual',
  content: "Step to do [yes-no] = yes <br  />Length = 300"
  }

  var data_preprocess_mids = {
  title: 'preprocess_mids',
  content: "Filename/MID/Sample = /CGG_AAOSTA/MID007/MadaTR_2014_RN34_4 <br  />Filename/MID/Sample = /CGG_AAOSTA/MID010/MadaTR_2014_RN4_54 <br  />Filename/MID/Sample = /CGG_AAOSTA/MID013/MadaTR_2014_RN4_52 <br  />Filename/MID/Sample = /CGG_AAOSTA/MID017/MadaTR_2014_RN4_57 <br  />Filename/MID/Sample = /CGG_AAOSTA/MID019/103018C14 <br  />Filename/MID/Sample = /CGG_AAOSTA/MID021/103024C14 <br  />Filename/MID/Sample = /CGG_AAOSTA/MID035/103133C14_b <br  />Filename/MID/Sample = /CGG_AAOSTA/MID036/103033C14 <br  />Filename/MID/Sample = /CGG_AAOSTA/MID056/103014C16 <br  />Filename/MID/Sample = /CGG_AAOSTA/MID057/MadaTR_2014_RN34_1 <br  />Filename/MID/Sample = /CGG_AAOSTA/MID058/103009C16 <br  />Filename/MID/Sample = /CGG_AAOSTA/MID059/103169C14 <br  />Filename/MID/Sample = /CGG_AAOSTA/MID089/MadaTR_2014_RN4_36 <br  />Filename/MID/Sample = /CGG_AAOSTA/MID090/MadaTR_2014_RN7_94 <br  />Filename/MID/Sample = /CGG_AAOSTA/MID091/103100C14 <br  />Filename/MID/Sample = /CGG_AAOSTA/MID092/103135C14 <br  />Filename/MID/Sample = /CGG_AAOSTA/MID093/MadaTR_2014_RN7_98 <br  />Filename/MID/Sample = /CGG_AAOSTA/MID094/MadaTR_2014_RN7_85 <br  />Filename/MID/Sample = /CGG_AAOSTA/MID095/103007C16 <br  />Filename/MID/Sample = /CGG_AAOSTA/MID129/103029C14 <br  />Filename/MID/Sample = /CGG_AAOSTA/MID132/103055C14 <br  />Filename/MID/Sample = /CGG_AAOSTA/MID160/103051C14 <br  />Filename/MID/Sample = /CGG_AAOSTA/MID161/103171C14 <br  />Filename/MID/Sample = /CGG_AAOSTA/MID162/103001C16 <br  />Filename/MID/Sample = /CGG_AAOSTA/MID163/MadaTR_2014_RN4_49 <br  />Filename/MID/Sample = /CGG_AAOSTA/MID164/103003C16 <br  />Filename/MID/Sample = /CGG_AAOSTA/MID166/MadaTR_2014_RN4_46 <br  />Filename/MID/Sample = /CGG_AAOSTA/MID167/MadaTR_2014_RN34_6 <br  />Filename/MID/Sample = /CGG_AAOSTA/MID169/MadaTR_2014_RN7_84 <br  />Filename/MID/Sample = /CGG_AAOSTA/MID170/MadaTR_2014_RN4_55 <br  />Filename/MID/Sample = /CGG_AAOSTA/MID171/103035C14 <br  />Filename/MID/Sample = /CGG_AAOSTA/MID172/MadaTR_2014_RN7_80 <br  />Filename/MID/Sample = /CGG_AAOSTA/MID174/103011C16 <br  />Filename/MID/Sample = /CGG_AAOSTA/MID034/G4_N1 <br  />Filename/MID/Sample = /CGG_ABOSTA/MID007/MadaTR_2014_RN7_83 <br  />Filename/MID/Sample = /CGG_ABOSTA/MID010/MadaTR_2014_RN7_79 <br  />Filename/MID/Sample = /CGG_ABOSTA/MID013/MadaTR_2014_RN4_37 <br  />Filename/MID/Sample = /CGG_ABOSTA/MID017/103036C14 <br  />Filename/MID/Sample = /CGG_ABOSTA/MID019/103049C14 <br  />Filename/MID/Sample = /CGG_ABOSTA/MID021/103032C14 <br  />Filename/MID/Sample = /CGG_ABOSTA/MID035/103154C14 <br  />Filename/MID/Sample = /CGG_ABOSTA/MID036/103044C14 <br  />Filename/MID/Sample = /CGG_ABOSTA/MID056/103038C14_b <br  />Filename/MID/Sample = /CGG_ABOSTA/MID057/MadaTR_2014_RN4_41 <br  />Filename/MID/Sample = /CGG_ABOSTA/MID058/MadaTR_2014_RN7_87 <br  />Filename/MID/Sample = /CGG_ABOSTA/MID059/103046C14 <br  />Filename/MID/Sample = /CGG_ABOSTA/MID089/103027C14 <br  />Filename/MID/Sample = /CGG_ABOSTA/MID090/MadaTR_2014_RN4_44 <br  />Filename/MID/Sample = /CGG_ABOSTA/MID091/MadaTR_2014_RN34_9 <br  />Filename/MID/Sample = /CGG_ABOSTA/MID092/MadaTR_2014_RN7_81 <br  />Filename/MID/Sample = /CGG_ABOSTA/MID093/MadaTR_2014_RN4_51 <br  />Filename/MID/Sample = /CGG_ABOSTA/MID094/MadaTR_2014_RN7_99 <br  />Filename/MID/Sample = /CGG_ABOSTA/MID095/MadaTR_2014_RN4_45 <br  />Filename/MID/Sample = /CGG_ABOSTA/MID129/103056C14 <br  />Filename/MID/Sample = /CGG_ABOSTA/MID132/103012C16 <br  />Filename/MID/Sample = /CGG_ABOSTA/MID160/MadaTR_2014_RN34_10 <br  />Filename/MID/Sample = /CGG_ABOSTA/MID161/103005C16 <br  />Filename/MID/Sample = /CGG_ABOSTA/MID162/103013C16 <br  />Filename/MID/Sample = /CGG_ABOSTA/MID163/MadaTR_2014_RN7_82 <br  />Filename/MID/Sample = /CGG_ABOSTA/MID164/103099C14_b <br  />Filename/MID/Sample = /CGG_ABOSTA/MID166/MadaTR_2014_RN7_90 <br  />Filename/MID/Sample = /CGG_ABOSTA/MID167/MadaTR_2014_RN4_50 <br  />Filename/MID/Sample = /CGG_ABOSTA/MID169/MadaTR_2014_RN34_5 <br  />Filename/MID/Sample = /CGG_ABOSTA/MID170/MadaTR_2014_RN7_93 <br  />Filename/MID/Sample = /CGG_ABOSTA/MID171/MadaTR_2014_RN4_53 <br  />Filename/MID/Sample = /CGG_ABOSTA/MID172/103016C16 <br  />Filename/MID/Sample = /CGG_ABOSTA/MID174/G4_N7 <br  />Filename/MID/Sample = /CGG_ABOSTA/MID034/G4_N3 <br  />Filename/MID/Sample = /CGG_ACOSTA/MID007/MadaTR_2014_RN7_88 <br  />Filename/MID/Sample = /CGG_ACOSTA/MID010/103017C16 <br  />Filename/MID/Sample = /CGG_ACOSTA/MID013/MadaTR_2014_RN7_96 <br  />Filename/MID/Sample = /CGG_ACOSTA/MID017/MadaTR_2014_RN7_76 <br  />Filename/MID/Sample = /CGG_ACOSTA/MID019/MadaTR_2014_RN7_100 <br  />Filename/MID/Sample = /CGG_ACOSTA/MID021/MadaTR_2014_RN34_13 <br  />Filename/MID/Sample = /CGG_ACOSTA/MID035/103040C14 <br  />Filename/MID/Sample = /CGG_ACOSTA/MID036/103069C14 <br  />Filename/MID/Sample = /CGG_ACOSTA/MID056/103023C14 <br  />Filename/MID/Sample = /CGG_ACOSTA/MID057/MadaTR_2014_RN7_95 <br  />Filename/MID/Sample = /CGG_ACOSTA/MID058/103002C16 <br  />Filename/MID/Sample = /CGG_ACOSTA/MID059/103042C14 <br  />Filename/MID/Sample = /CGG_ACOSTA/MID089/MadaTR_2014_RN7_78 <br  />Filename/MID/Sample = /CGG_ACOSTA/MID090/103031C14 <br  />Filename/MID/Sample = /CGG_ACOSTA/MID091/MadaTR_2014_RN4_40 <br  />Filename/MID/Sample = /CGG_ACOSTA/MID092/103004C16 <br  />Filename/MID/Sample = /CGG_ACOSTA/MID093/MadaTR_2014_RN4_47 <br  />Filename/MID/Sample = /CGG_ACOSTA/MID094/MadaTR_2014_RN4_48 <br  />Filename/MID/Sample = /CGG_ACOSTA/MID095/103098C14_b <br  />Filename/MID/Sample = /CGG_ACOSTA/MID129/103058C14 <br  />Filename/MID/Sample = /CGG_ACOSTA/MID132/103026C14 <br  />Filename/MID/Sample = /CGG_ACOSTA/MID160/MadaTR_2014_RN7_86 <br  />Filename/MID/Sample = /CGG_ACOSTA/MID161/MadaTR_2014_RN7_75 <br  />Filename/MID/Sample = /CGG_ACOSTA/MID162/103132C14_b <br  />Filename/MID/Sample = /CGG_ACOSTA/MID163/103173C14 <br  />Filename/MID/Sample = /CGG_ACOSTA/MID164/103006C16 <br  />Filename/MID/Sample = /CGG_ACOSTA/MID166/MadaTR_2014_RN34_3 <br  />Filename/MID/Sample = /CGG_ACOSTA/MID167/MadaTR_2014_RN4_39 <br  />Filename/MID/Sample = /CGG_ACOSTA/MID169/103043C14 <br  />Filename/MID/Sample = /CGG_ACOSTA/MID170/MadaTR_2014_RN7_92 <br  />Filename/MID/Sample = /CGG_ACOSTA/MID171/MadaTR_2014_RN7_97 <br  />Filename/MID/Sample = /CGG_ACOSTA/MID172/MadaTR_2014_RN7_89 <br  />Filename/MID/Sample = /CGG_ACOSTA/MID174/103085C14_b <br  />Filename/MID/Sample = /CGG_ACOSTA/MID034/G4_N5 <br  />Filename/MID/Sample = /CGG_ADOSTA/MID007/MadaTR_2014_RN4_56 <br  />Filename/MID/Sample = /CGG_ADOSTA/MID010/103084C14 <br  />Filename/MID/Sample = /CGG_ADOSTA/MID013/MadaTR_2014_RN7_91 <br  />Filename/MID/Sample = /CGG_ADOSTA/MID017/103015C16 <br  />Filename/MID/Sample = /CGG_ADOSTA/MID019/103071C14 <br  />Filename/MID/Sample = /CGG_ADOSTA/MID021/103037C14_b <br  />Filename/MID/Sample = /CGG_ADOSTA/MID035/MadaTR_2014_RN7_77 <br  />Filename/MID/Sample = /CGG_ADOSTA/MID036/103041C14_b <br  />Filename/MID/Sample = /CGG_ADOSTA/MID056/MadaTR_2014_RN34_12 <br  />Filename/MID/Sample = /CGG_ADOSTA/MID057/MadaTR_2014_RN4_42 <br  />Filename/MID/Sample = /CGG_ADOSTA/MID058/MadaTR_2014_RN34_11 <br  />Filename/MID/Sample = /CGG_ADOSTA/MID059/MadaTR_2014_RN34_8 <br  />Filename/MID/Sample = /CGG_ADOSTA/MID089/103210C14_b <br  />Filename/MID/Sample = /CGG_ADOSTA/MID090/103131C14 <br  />Filename/MID/Sample = /CGG_ADOSTA/MID091/103025C14 <br  />Filename/MID/Sample = /CGG_ADOSTA/MID092/103022C14 <br  />Filename/MID/Sample = /CGG_ADOSTA/MID093/MadaTR_2014_RN34_2 <br  />Filename/MID/Sample = /CGG_ADOSTA/MID094/103054C14 <br  />Filename/MID/Sample = /CGG_ADOSTA/MID095/103021C14 <br  />Filename/MID/Sample = /CGG_ADOSTA/MID129/103019C14 <br  />Filename/MID/Sample = /CGG_ADOSTA/MID132/103020C14 <br  />Filename/MID/Sample = /CGG_ADOSTA/MID160/113012C16 <br  />Filename/MID/Sample = /CGG_ADOSTA/MID161/103008C16 <br  />Filename/MID/Sample = /CGG_ADOSTA/MID162/MadaTR_2014_RN4_38 <br  />Filename/MID/Sample = /CGG_ADOSTA/MID163/MadaTR_2014_RN34_7 <br  />Filename/MID/Sample = /CGG_ADOSTA/MID164/103010C16 <br  />Filename/MID/Sample = /CGG_ADOSTA/MID166/103153C14 <br  />Filename/MID/Sample = /CGG_ADOSTA/MID167/103030C14 <br  />Filename/MID/Sample = /CGG_ADOSTA/MID170/MadaTR_2014_RN4_43 <br  />Filename/MID/Sample = /CGG_ADOSTA/MID172/G4_N6 <br  />Filename/MID/Sample = /CGG_ADOSTA/MID174/G4_N7 <br  />Filename/MID/Sample = /CGG_ADOSTA/MID034/G4_N5"
  }

  var data_random_preprocess_mids = {
  title: 'random_preprocess_mids',
  content: "Step to do [yes-no] = yes <br  />Number of reads in the subset = 1000"
  }

  var data_preprocessing = {
  title: 'preprocessing',
  content: "Step to do [yes-no] = yes <br  />Minimum Length threshold = 350 <br  />Number of ambiguities (N's) tolerated = 0 <br  />Forward primer = CGA-TAA-CGA-ACG-AGA-CCT <br  />Reverse primer = ANC-CAT-TCA-ATC-GGT-ANT <br  />Number of differences tolerated in your forward primer sequence [0-2] = 0 <br  />Number of differences tolerated in your reverse primer sequence [0-2] = 0 <br  />Stringency [High, Medium or Low] = High"
  }

  var data_quality_cleaning = {
  title: 'quality_cleaning',
  content: "Step to do [yes-no] = yes <br  />Lowest quality score tolerated [0-40] (default =  0) <br  />Lowest average quality score tolerated [0-40] (default =  0)"
  }

  var data_random_preprocess = {
  title: 'random_preprocess',
  content: "Step to do [yes-no] = yes <br  />Number of reads in the subset = 1000"
  }

  var data_length_range_preprocess = {
  title: 'length_range_preprocess',
  content: "Step to do [yes-no] = yes <br  />Length = 350"
  }

  var data_raw_infernal_alignment = {
  title: 'raw_infernal_alignment',
  content: "Step to do [yes-no] = yes <br  />Covariance model used [bacteria, archaea, algae or fungi] = bacteria"
  }

  var data_raw_clustering = {
  title: 'raw_clustering',
  content: "Step to do [yes-no] = yes <br  />Ignoring homopolymers differences [yes-no] = no <br  />Ignoring distances at the beginning of sequences [yes-no] = yes <br  />Maximum percentage of dissimilarity for clustering (%) = 5.0 <br  />Step size for each cluster (%) = 5.0"
  }

  var data_hunting = {
  title: 'hunting',
  content: "Step to do [yes-no] = yes <br  />Chosen clustering percentage of dissimilarity for the hunting step (%) = 5.0"
  }

  var data_recovering = {
  title: 'recovering',
  content: "Step to do [yes-no] = yes <br  />Organism studied [bacteria, or fungi] = bacteria <br  />Database used for the taxonomic assignment [RDP (bacteria only), or Silva (bacteria and fungi)] = Silva <br  />Taxonomic level checked [domain, phylum, class, order, family or genus] = phylum <br  />Confidence estimates threshold [0-100] = 90 <br  />Keep Taxonomy file for deleted reads [yes-no] = yes <br  />Number of cores or processors used for each sample to do the BLAST analysis [1-32] = 1"
  }

  var data_random_cleaned = {
  title: 'random_cleaned',
  content: "Step to do [yes-no] = yes <br  />Number of reads in the subset = 150"
  }

  var data_taxonomy = {
  title: 'taxonomy',
  content: "Step to do [yes-no] = yes <br  />Organism studied [bacteria, or fungi] = bacteria <br  />Database used for the taxonomic assignment [RDP (bacteria only), or Silva (bacteria and fungi)] = Silva <br  />Confidence estimates threshold [0-100] = 80 <br  />Number of cores or processors used for each sample to do the BLAST analysis [1-32] = 1"
  }

  var data_clean_infernal_alignment = {
  title: 'clean_infernal_alignment',
  content: "Step to do [yes-no] = yes <br  />Covariance model used [bacteria, archaea, algae or fungi] = bacteria"
  }

  var data_clean_clustering = {
  title: 'clean_clustering',
  content: "Step to do [yes-no] = yes <br  />Ignoring homopolymers differences [yes-no] = no <br  />Ignoring distances at the beginning of sequences [yes-no] = yes <br  />Maximum percentage of dissimilarity for clustering (%) = 5.0 <br  />Step size for each cluster (%) = 5.0 <br  />Rank-abundance curves computation [yes-no] = yes"
  }

  var data_computation = {
  title: 'computation',
  content: "Step to do [yes-no] = yes <br  />Chosen clustering step for analysis [Raw, Clean, Both] = Clean <br  />Chosen percentage of dissimilarity for clustering (%) = 5.0 <br  />Determination of rarefaction curve(s) [yes-no] = yes <br  />Determination of rank-abundance curve(s) [yes-no] = yes <br  />Computation of the full bias corrected Chao1 richness estimator [yes-no] = yes <br  />Computation of the ACE richness estimator [yes-no] = yes <br  />Computation of bootstrap estimate, and shannon and simpson indexes [yes-no] = yes"
  }

  var data_global_analysis = {
  title: 'global_analysis',
  content: "Step to do [yes-no] = yes <br  />Chosen clustering step for analysis [Raw, Clean, Both] = Clean <br  />Chosen percentage of dissimilarity for the treated clustering (%) = 5.0 <br  />Defining the taxonomic assignment of all OTUs in the global matrix [yes-no] = yes <br  />Confidence estimates (RDP) or similarity percentage (SILVA) threshold [0-100] = 80 <br  />Realization of a phylogenetic tree and an ID mapping file for UNIFRAC analysis (time-consuming step !!) [yes-no] = yes <br  />Selection of the most abundant read to represent each OTU for UNIFRAC analysis (tree and mapping file) [yes-no] = yes"
  }

  var data_unifrac_analysis = {
  title: 'unifrac_analysis',
  content: "Step to do [yes-no] = yes <br  />Calculate a UniFrac Distance Matrix and apply PCoA and UPGMA [yes-no] = yes <br  />Compute the Phylogenetic Diversity (PD) of all samples [yes-no] = yes <br  />Perform a single UniFrac significance test on the whole tree [yes-no] = no <br  />Perform pairwise tests of whether samples are significantly different with UniFrac [yes-no] = no <br  />Perform tests for each sample individually to define if they are significantly different with UniFrac [yes-no] = no <br  />Perform a single P-test significance test on the whole tree [yes-no] = no <br  />Perform pairwise tests of whether samples are significantly different with P-test [yes-no] = no <br  />Number of permutations [10-10000] = 1000 <br  />Use abundance weigths (select whether the abundance of reads will be used or not for UniFrac tests) [yes-no] = yes <br  />Use Normalized abundance weights (for UniFrac tests) [yes-no] = yes"
  }

  var template = $('#message-template').html();
  var html_prinseq = Mustache.render(template, data_prinseq);
  $('#pane-1').append(html_prinseq);
  var html_flash = Mustache.render(template, data_flash);
  $('#pane-1').append(html_flash);
  var html_eval_qual = Mustache.render(template, data_eval_qual);
  $('#pane-1').append(html_eval_qual);
  var html_preprocess_mids = Mustache.render(template, data_preprocess_mids);
  $('#pane-1').append(html_preprocess_mids);
  var html_random_preprocess_mids = Mustache.render(template, data_random_preprocess_mids);
  $('#pane-1').append(html_random_preprocess_mids);
  var html_preprocessing = Mustache.render(template, data_preprocessing);
  $('#pane-1').append(html_preprocessing);
  var html_quality_cleaning = Mustache.render(template, data_quality_cleaning);
  $('#pane-1').append(html_quality_cleaning);
  var html_random_preprocess = Mustache.render(template, data_random_preprocess);
  $('#pane-1').append(html_random_preprocess);
  var html_length_range_preprocess = Mustache.render(template, data_length_range_preprocess);
  $('#pane-1').append(html_length_range_preprocess);
  var html_raw_infernal_alignment = Mustache.render(template, data_raw_infernal_alignment);
  $('#pane-1').append(html_raw_infernal_alignment);
  var html_raw_clustering = Mustache.render(template, data_raw_clustering);
  $('#pane-1').append(html_raw_clustering);
  var html_hunting = Mustache.render(template, data_hunting);
  $('#pane-1').append(html_hunting);
  var html_recovering = Mustache.render(template, data_recovering);
  $('#pane-1').append(html_recovering);
  var html_random_cleaned = Mustache.render(template, data_random_cleaned);
  $('#pane-1').append(html_random_cleaned);
  var html_taxonomy = Mustache.render(template, data_taxonomy);
  $('#pane-1').append(html_taxonomy);
  var html_clean_infernal_alignment = Mustache.render(template, data_clean_infernal_alignment);
  $('#pane-1').append(html_clean_infernal_alignment);
  var html_clean_clustering = Mustache.render(template, data_clean_clustering);
  $('#pane-1').append(html_clean_clustering);
  var html_computation = Mustache.render(template, data_computation);
  $('#pane-1').append(html_computation);
  var html_global_analysis = Mustache.render(template, data_global_analysis);
  $('#pane-1').append(html_global_analysis);
  var html_unifrac_analysis = Mustache.render(template, data_unifrac_analysis);
  $('#pane-1').append(html_unifrac_analysis);
