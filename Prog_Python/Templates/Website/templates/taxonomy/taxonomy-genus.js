$(document).ready(function() {
    // var selectList = document.createElement("select");
    // selectList.setAttribute("id", "mySelect");
    // myDiv.appendChild(selectList);
    // //Create and append the options in select list
    // for (var i = 0; i < array_part.length; i++) {
    //     var option = document.createElement("option");
    //     option.setAttribute("value", array_part[i]);
    //     option.text = array_part[i];
    //     selectList.appendChild(option);
    // };
    //Create Highcharts options by defaults
    var options = {
        chart: {
            renderTo: 'container-genus',
            defaultSeriesType: 'column',
            ignoreHiddenSeries: false,
        },
        credits: {
          enabled: false
        },
        plotOptions: {
            column: {
              stacking: 'percent'
            }
        },
        legend: {
          itemWidth: 150,
          adjustChartSize: true,
          navigation: {
            enabled: false
          }
        },
        xAxis: {
            categories:categories_tax["part_1"]
        },
        series: dict_genus["part_1"],
        exporting: {
                    sourceWidth: 1400,
                    sourceHeight: 1200,
                    // scale: 2 (default)
        }
    };
    //Set Highcharts options in function of select value
    var chart = new Highcharts.Chart(options);
    $("#mySelect").on('change', function(){
        var selVal = $("#mySelect").val();
        options.series = dict_genus[selVal];
        options.xAxis.categories = categories_tax[selVal];
        var chart = new Highcharts.Chart(options);
    });
});// End document
