$(document).ready(function() {
    var options = {
        chart: {
            renderTo: 'container-phyla',
            defaultSeriesType: 'column',
            ignoreHiddenSeries: false,
        },
        credits: {
          enabled: false
        },
        plotOptions: {
            column: {
              stacking: 'percent'
            }
        },
        legend: {
          itemWidth: 150,
          adjustChartSize: true,
          navigation: {
            enabled: false
          },
          title: {
            text: 'Taxa<br/><span style="font-size: 9px; color: #666; font-weight: normal">(Click to hide)</span>',
          },
        },
        xAxis: {
            categories:categories_tax["part_1"],
            labels: {
                style: {
                    fontSize: '0.8em'
                }
            }
        },
        series: dict_phylum["part_1"],
        exporting: {
                    sourceWidth: 1400,
                    sourceHeight: 1200,
                    // scale: 2 (default)
        }
    };
    //Set Highcharts options in function of select value
    var chart = new Highcharts.Chart(options);
    $('#font-size').bind('input', function () {
      // $('#container-phyla').highcharts().container.parentNode.style.fontSize = this.value + 'px';
      $('#font-size-value').html(this.value + 'px');
      // $('#container-phyla').highcharts(options);
      var new_fontsize = this.value+"px"
      options.xAxis.labels.style.fontSize = new_fontsize;
      var chart = new Highcharts.Chart(options);
    });
    $("#mySelect").on('change', function(){
        var selVal = $("#mySelect").val();
        options.series = dict_phylum[selVal];
        options.xAxis.categories = categories_tax[selVal];
        var chart = new Highcharts.Chart(options);
    });

});// End document
