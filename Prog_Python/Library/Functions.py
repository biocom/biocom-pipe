#Needed libraries
import os
import matplotlib
import random
import decimal
#Modification essential to how set 'backend' in matplotlib in python (not shown, but written in a file)
matplotlib.use("Agg")
import matplotlib.pyplot as plt
import matplotlib.font_manager as fm
import math
import sys

decimal.getcontext().prec = 6

def my_chosen_colors():
    """List of colors chosen based on their differences given as output of this function, used by various programs to draw curves."""
    my_defined_colors = ['Black','Blue','DeepSkyBlue','Green','LimeGreen','Purple','Gray','Red','Orange','Gold']
    return my_defined_colors


def color_generation():
    """Automatic generator of colors in RGB format. This simple function vill return a tuple (R,G,B) values to define a
    random color for the definition of various graphs."""
    RGB = tuple()
    R = random.random()
    G = random.random()
    B = random.random()
    RGB = (R,G,B)
    return RGB


def read_folder(path):
    """read_folder (to acquire all file names in a defined directory). This function need just as input the
    directory used to search and list all files found."""
    tab = []
    tab = os.listdir(path)
    tab.sort()
    return tab


def found_file_name(file_name, cutoff=1.00):
    """found_file_name (to simplify the name of the file containing used data). This function is dedicated to
    clean all names to keep only the needed information based on the steps already realized. This function
    needs only the file_name, and an optional value cutoff to be used by some programs."""
    tmp_list = file_name.split("_")
    name_final = ""
    #Loop to test each element of the name to keep only needed parts.
    for name in tmp_list:
        if name == "Rdm":
            continue
        elif (name == "Hpreprocess") or (name == "Mpreprocess") or (name == "Lpreprocess"):
            continue
        elif (name == "Derep") or (name == "Clust") or (name == "FilterAlign"):
            continue
        elif (name == "Raw") or (name == "Clean") or (name == "Hunt") or (name == "Align"):
            continue
        elif (name == "Rarefactcurve") or (name == "Taxo"):
            continue
        elif (name == "Rank-abundance") or (name =="curve"):
            continue
        elif (name == "RDP") or (name == "SILVA"):
            continue
        elif name == str(cutoff):
            continue
        else:
            name_final += name+"_"
    #We then again treat the name of the file (to keep only the name of the samples)
    tmp_list = name_final.split(".f")
    return tmp_list[0]


def define_colors(my_samples):
    """define_colors (we select colors from a list already defined using HTML color names). This function
    was dedicated to the selection and organization of used colors in the different graphs realized by
    the program. As output, this function return a list of colors."""

    my_defined_colors = list()

    for element in my_samples:
        my_defined_colors.append(color_generation())
    my_colors=list()
    i = 0
    #Loop to use particular colors for defined groups
    for element in my_samples:
        if ("Unknown" in element):
            my_colors.append('LightGray')
        elif ("Unclassified" in element) or ("Undefined" in element):
            my_colors.append('DimGray')
        elif ("Environmental" in element):
            my_colors.append('Black')
        elif ("Lower Groups" in element):
            my_colors.append('White')
        else:
            my_colors.append(my_defined_colors[i])
            i = i + 1
    return my_colors


def compute_percents(my_count_list):
    """compute_percents (compute and store the percentages of abundances for organisms). This function
    is defined to compute the percents of relative abundance of each taxonomic group for all studied
    samples chosen by the user. This program takes as input a list containing the name of the sample and
    the number of reads affifiated at each taxonomic group, and return the same list, with the relative
    abundance computed, and without the name fo the sample."""
    result_list = list()
    computing = float()
    my_sum = 0
    for element in my_count_list:
        #We avoid the analysis of the sample name
        if ("Taxo_" in str(element)):
            continue
        #If it's an element
        else:
            my_sum += int(element)
    for element in my_count_list:
        #We keep the name of the sample
        if ("Taxo_" in str(element)):
            result_list.append(element)
        #If it's an element
        else:
            computing = (float(element)/my_sum)*100
            result_list.append(computing)
    return result_list


def compute_row_column_pages(my_length):
    """compute_row_column_pages (define the number of subgraphs per page, and the number of rows and columns in each defined figure).
    Using the number of sampels studied, this program define the number of pages, rows and columns used for the drawing of graphs."""
    Nbcols = 0
    Nbrows = 0
    Nbpages  = 0
    if (my_length > 6):
        Nbpages = int(math.ceil(float(my_length)/6))
        my_length = 6
    else:
        Nbpages = 1
    if ((my_length%3) == 0):
        Nbcols = 3
        Nbrows = (my_length)/Nbcols
    elif ((my_length%2) == 0):
        Nbcols = 2
        Nbrows = (my_length)/Nbcols
    else:
        Nbcols = 3
        Nbrows = (my_length)/Nbcols + 1
    return Nbpages, Nbcols, Nbrows


def data_file_catching(file_name):
    """data_file_catching (catch, organize and store data used to draw each curve, whatever it was (rarefaction, rank abundance, etc...).
    This function is dedicated to catch and return x and y values used to draw curves using the file_name given by the user."""
    my_opened_file = open("Summary_files/Curves_Data/"+file_name, 'r')
    Found_lines = my_opened_file.readlines()
    x_values = list()
    y_values = list()
    for my_line in Found_lines:
        #We delete the \n character
        my_line = my_line.replace("\n","")
        #The line is splitted based on "\t" and stored in tmp
        tmp = my_line.split("\t")
        #We catch the "x" values and the "y" values
        x_values.append(float(tmp[0]))
        y_values.append(float(tmp[1]))
        #Close the treated file as it's not needed now
    my_opened_file.close()
    #We reorganize data just in case of errors to give efficient data to the next programs
    x_values.sort()
    y_values.sort()
    return x_values, y_values


def nb_cols_legend(nb_elements, defined_max):
    """Function defined to compute the number of columns for the legend to be used to draw efficiently all graphs like the
    pies for examples. This function used only the number of elements and the maximum of elements to compute and return the
    number of columns in the legend."""
    #Here, we define the number of columns for all taxonomic names in the legend
    cols = 1
    if (nb_elements > (defined_max - 1)):
        cols = int((nb_elements/defined_max))
        if (nb_elements%defined_max > 0):
            cols = cols + 1
    return cols

def draw_curves(treated_files, cutoff, my_colors_rc, user_choice, file_type, my_xlabel, my_ylabel, my_title, my_save_name):
    """draw_curves (function define to draw efficient curves using several input information efficiently). This function
    is defined to draw curves using the x and y values catched and the input given. Several arguments are needed: the
    list of chosen files, the cutoff chosen by the user, the colors chosen, the user_choice (Both, Clean, Raw), the
    file_type (Rarefaction curve, rank_abundance, other), the labels of the axis, the title and the save_name of the
    curves defined."""
    #Needed empty variables to use each color defined and create all files and subfiles
    j = 0
    k = 0
    list_list_treated_files = list()
    part_treated_files = list()
    cleaned_file_names = list()
    cutoff = (100-cutoff)/100
    cutoff_keep = cutoff

    #Loop to read all file names and create sub lists to define efficiently graphs
    for file_name in treated_files:
        if (0<=file_name.find(str(file_type)+str(user_choice)+"_")):
            part_treated_files.append(file_name)
            j = j + 1
            #When we obtain the maximum of curves in one graph
            if j == 10:
                #We add to the main list the sub list
                list_list_treated_files.append(part_treated_files)
                #We emptied the sub list
                part_treated_files = list()
                j = 0
    #At the end of the loop, we redefine a last sub list for all not added files
    list_list_treated_files.append(part_treated_files)
    part_treated_files = list()
    j = 0

    #Then, we define a graph for all sub lists in the main list
    for part_treated_files in list_list_treated_files:
        my_curve_fig = plt.figure()
        my_curve_graph = my_curve_fig.add_subplot(1,1,1, title=my_title)
        #Loop to treat all files found in the Summary_files folder
        for file_name in part_treated_files:
            #If the name of the file contain Rarefactcurve_ or rank-abundance_curve, we analyze this file
            if (0<=file_name.find(str(file_type)+str(user_choice)+"_")):
                #Automatic treatment of file names found needed treatment
                cleaned_file_name = found_file_name(file_name, cutoff)
                cleaned_file_names.append(cleaned_file_name)
                #We then catch all the needed information of the file to draw the curves
                x_values, y_values = data_file_catching(file_name)
                if ("Rank-abundance_curve_" in file_type):
                    x_values.reverse()
                #Add to the plot in X the Nb_seqs, in Y the Nb_OTUs, the defined color and
                #the width of the lines
                my_curve_graph.plot(x_values,y_values, color=my_colors_rc[j], linewidth=2.5)
                j = j + 1
                if ("Rank-abundance_curve_" in file_type):
                    my_curve_graph.set_yscale('log')
                    my_curve_graph.set_ylim([0.001,50])
                    my_curve_graph.set_yticklabels((0.001,0.01,0.1,1,10))
        #We then modify again the cutoff for the graph title
        #cutoff = 100 - (cutoff*100)
        prop = fm.FontProperties(size=8)
        cols = nb_cols_legend(len(cleaned_file_names), 25)
        my_legend = my_curve_graph.legend(cleaned_file_names, loc='center left', prop = prop, ncol = cols, bbox_to_anchor=(1.0,0.5))

        #We then add the labels of axes
        my_curve_graph.set_xlabel(my_xlabel)
        my_curve_graph.set_ylabel(my_ylabel)
        #And the title of the graphic
        #plt.title(my_title)
        #We finally do this function to adapt the graphics to its size
        my_curve_fig.tight_layout()
        #We finally save the figure in various formats for each type of clustering
        #my_curve_fig.savefig(my_save_name+str(user_choice)+'_'+str(k+1)+'.svg', bbox_extra_artists=(my_legend,), bbox_inches='tight', dpi=150)
        my_curve_fig.savefig(my_save_name+str(user_choice)+'_'+str(k+1)+'.pdf', bbox_extra_artists=(my_legend,), bbox_inches='tight', dpi=150)
        #my_curve_fig.savefig(my_save_name+str(user_choice)+'_'+str(k+1)+'.png', bbox_extra_artists=(my_legend,), bbox_inches='tight', dpi=150)
        #Finally, we delete all data in the variables to treat another file type
        my_curve_fig.clf()
        plt.close()
        #Needed empty variables
        j = 0
        k = k + 1
        cleaned_file_names = list()


def sort_chosen_cluster_details(Lines, cutoff):
    """Function defined, based on the data found in the read file, all needed data. This function used the lines read from
    a file, and also the chosen cutoff, to find the number of sequences, of clusters and also the details of all OTUs: reads
    by OTU. All these data are returned at the end of the function. Used in the indices computation."""
    #Needed empty variables
    verif = 0
    Nb_seqs_tot = 0
    Nb_clusters = 0
    tmp = list()
    Cluster = list()
    #Loop to read all lines in the chosen file
    for Line in Lines:
        if ("Sequences" in Line):
            #The line is splitted based on "\t" and stored in tmp
            tmp = Line.split("\t")
            #The number of sequences in stored in Nb_seqs_tot after conversion in int
            Nb_seqs_tot = int(tmp[1])
            continue
        if ("distance" in Line):
            #The line is splitted based on "\t" and stored in tmp
            tmp = Line.split("\t")
            if (float(tmp[1]) >= cutoff):
                verif += 1
            continue
        if (verif == 1):
            #Stop the analysis if we found the end of the cluster
            if Line == "\n":
                break
            #Copy and storage of cluster number needed to calculate rarefact curve
            if ("Clusters" in Line):
                tmp = Line.split("\t")
                Nb_clusters = int(tmp[1])
            #Copy only the needed data (number of sequences in the cluster)
            elif Line != "":
                tmp = Line.split("\t")
                Cluster.append(int(tmp[2]))
    #Rearrange the list by values (ascending 1 to n)
    Cluster.sort()
    #We then return all needed values and data
    return Nb_seqs_tot, Nb_clusters, Cluster


def computing_matrix_OTUs(Clusters):
    """This function is dedicated to the computing of the number of OTUs for each composition. To do this,
    we use a dictionnary to simplify and fast the analysis. In input, we used the clusters, and in output the
    Matrix is given. Used in the indices computation."""
    #Needed empty variables
    Dict = dict()
    Matrix = list()
    #For each element in Clusters
    for element in Clusters:
        #We count the number of MOTUs with the same number of sequences using a dictionnary
        if element in Dict:
            Dict[element] += 1
        #If we found a new number of sequences in a MOTU, we create it in the Dict and store the
        #data obtained for the previous MOTU type
        else:
            Dict[element] = 1
    #We then here read all elements found in the dictionnary and transform it into a list of associated
    #keys and values for further steps
    for key, value in Dict.items():
        Matrix.append([key, value])
    Matrix.sort()
    #We finally return
    return Matrix


def chao_computation(Nbseqs, Nbclusters, Matrix):
    """This function is dedicated to the computation of the Chao1 estimator based on the Matrix given as input.
    To do this, we use also as input the number of sequences and clusters, compute needed values, and return
    them as different variables. Used in indices computation and also for the global analysis."""
    #Needed empty variables
    S_chao1 = 0
    var_S_chao1 = 0
    C_chao1 = 0
    LCI95_chao1 = 0
    UCI95_chao1 = 0
    Result_tmp = 0
    n1,n2 = 0,0
    #For each element in the given Matrix
    for element in Matrix:
        if element[0] == 1:
            n1 = element[1]
        if element[0] == 2:
            n2 = element[1]
    #Calculation of S_chao1 based on n1 and n2 values
    if n1>0 and n2 >= 0 or n1==0 and n2==0:
        S_chao1 = Nbclusters + (n1*(n1-1))/(2*(n2+1))
    if n1==0 and n2 >= 0:
        S_chao1 = Nbclusters + (n1*n1)/(2*n2)
    #Calculation of var_S_chao1 based on n1 and n2 values
    if n1>0 and n2>0:
        #Bias corrected formula
        var_S_chao1 = (n1*(n1-1))/(2*(n2+1))+(n1*(2*n1-1)*(2*n1-1))/(4*(n2+1)*(n2+1))+\
            (n1*n1*n2*(n1-1)*(n1-1))/(4*(n2+1)*(n2+1)*(n2+1)*(n2+1))
        #Classic formula
        #var_S_chao1 = n2*(0.5*math.pow(n1/n2,2)+math.pow(n1/n2,3)+0.25*math.pow(n1/n2,4))
    #Calculation of var_S_chao based on the numbers n1 and n2 and also on experimentally values
    if n1>0 and n2==0:
        var_S_chao1 = ((n1*(n1-1))/(n1*(2*n1-1))*((2*n1-1))/4)-(n1*n1*n1*n1/(4*S_chao1))
    if n1==0 and n2>0:
        var_S_chao1 = Nbclusters*math.exp((-Nbseqs)/Nbclusters)*\
            (1-math.exp((0-Nb_seqs_tot)/Nbclusters))
    if S_chao1 == Nbclusters:
        S_chao1+=1
    #Calculation of C_chao1 based on previous data calculated
    Result_tmp = math.log((1 + (var_S_chao1)/math.pow(S_chao1-Nbclusters,2)),math.e)
    #Simple modification as when we have very 0 values regarding singletons and/or doubletons,
    #we can obtain problems during the computation
    if Result_tmp < 0:
        Result_tmp = 0.001
    C_chao1 = math.exp((1.96*math.sqrt(Result_tmp)))
    #Calculation of LCI95_chao1 and UCI95_chao1 based on previous data calculated
    LCI95_chao1 = Nbclusters + (S_chao1-Nbclusters)/C_chao1
    UCI95_chao1 = Nbclusters + (S_chao1-Nbclusters)*C_chao1
    #We finally return the computed values for the main program
    return S_chao1, var_S_chao1, LCI95_chao1, UCI95_chao1


def ACE_computation(Nbclusters, Matrix):
    """This function is dedicated to the computation of the ACE estimator based on the Matrix given as input.
    To do this, we use also as input the number of clusters, compute needed values, and return
    them as different variables. Used in indices computation and also for the global analysis."""
    #Needed variables
    S_rare_ACE = 0
    N_rare_ACE = 0
    n1_ACE = 0
    C_ACE = 0
    Gamma_ACE = 0
    S_ACE = 0
    Result_tmp = decimal.Decimal(0)

    #First we need to calculate S_rare_ACE and S_abund_ACE based on the matrix
    #In the same time, we determine N_rare_ACE based on the number of OTUs with i individuals
    for element in Matrix:
        if element[0] <= 10:
            S_rare_ACE += element[1]
            N_rare_ACE += (element[0]*element[1])
        S_abund_ACE = Nbclusters - S_rare_ACE
        if element[0] == 1:
            n1_ACE = element[1]
    #Determination of C_ACE based on previous calculations
    if(decimal.Decimal(n1_ACE) <= 1):
        C_ACE = (decimal.Decimal(n1_ACE)/decimal.Decimal(N_rare_ACE))
    else:
        C_ACE = decimal.Decimal(1) - (decimal.Decimal(n1_ACE)/decimal.Decimal(N_rare_ACE))

    #Determination of Gamma_ACE based on previous calculations
    for element in Matrix:
        if element[0] <= 10:
            Result_tmp += decimal.Decimal(element[0])*decimal.Decimal((element[0]-1))*\
                          decimal.Decimal(element[1])
    if(N_rare_ACE > 1) and (C_ACE > 0):
        Gamma_ACE = ((decimal.Decimal(S_rare_ACE)/decimal.Decimal(C_ACE))*\
        (decimal.Decimal(Result_tmp)/(decimal.Decimal(N_rare_ACE)*\
        (decimal.Decimal(N_rare_ACE)-1))))-1
    #Finally, determination of S_ACE based on these calculations
    if(N_rare_ACE > 1) and (C_ACE > 0):
        S_ACE = decimal.Decimal(S_abund_ACE) + (decimal.Decimal(S_rare_ACE)/decimal.Decimal(C_ACE))+\
            ((decimal.Decimal(n1_ACE)/decimal.Decimal(C_ACE))*decimal.Decimal(Gamma_ACE))
    else:
        S_ACE = decimal.Decimal(S_abund_ACE) + decimal.Decimal(S_rare_ACE)
    return S_ACE, S_rare_ACE, S_abund_ACE

def Bootstrap_Shannon_Simpson_computation(Nbseqs, Nbclusters, Matrix):
    """This function is dedicated to the computation of some indices (bootstrap, shannon and simpson) based
    on the Matrix given as input. To do this, we use also as input the number of sequences and clusters,
    compute needed values, and return them as different variables. Used in indices computation and also
    for the global analysis."""

    #Needed variables (empty or initialized)
    S_bootstrap = Nbclusters
    Result_tmp = decimal.Decimal(0)
    H_Shannon = decimal.Decimal(0)
    var_H_Shannon = decimal.Decimal(0)
    Evenness = decimal.Decimal(0)
    D_Simpson = decimal.Decimal(0)
    D_1_Simpson = decimal.Decimal(0)

    #We compute needed values for the indices using the matrix
    for element in Matrix:
       S_bootstrap += element[1]*math.pow((1-(element[0]/Nbseqs)),Nbseqs)
       x = decimal.Decimal(element[0])/decimal.Decimal(Nbseqs)
       y = math.log(x)
       z = x*element[1]
       y = decimal.Decimal(str(y))
       H_Shannon += -decimal.Decimal(z*y)
       Result_tmp += decimal.Decimal(z)*decimal.Decimal(str(math.pow(y,2)))
       e = decimal.Decimal(element[1])*decimal.Decimal(element[0])
       h = decimal.Decimal(element[0]) - decimal.Decimal(str(1.0))
       h = decimal.Decimal(h)*decimal.Decimal(e)
       g = decimal.Decimal(Nbseqs)-decimal.Decimal(str(1.0))
       g = decimal.Decimal(g)*decimal.Decimal(Nbseqs)
       D_Simpson += decimal.Decimal(h)/decimal.Decimal(g)
    o = decimal.Decimal(Result_tmp)-decimal.Decimal(str(math.pow(H_Shannon,2)))
    o = decimal.Decimal(o) / decimal.Decimal(Nbseqs)
    p = decimal.Decimal(Nbclusters) - decimal.Decimal(1)
    q = decimal.Decimal(2)*decimal.Decimal(str(math.pow(Nbseqs,2)))
    p = decimal.Decimal(p)/decimal.Decimal(q)
    var_H_Shannon = decimal.Decimal(o) + decimal.Decimal(p)
    D_1_Simpson = decimal.Decimal(1)/decimal.Decimal(D_Simpson)
    Result_tmp = 0
    Evenness = decimal.Decimal(H_Shannon)/decimal.Decimal(math.log(decimal.Decimal(Nbclusters)))
    return S_bootstrap, H_Shannon, var_H_Shannon, Evenness, D_Simpson, D_1_Simpson
