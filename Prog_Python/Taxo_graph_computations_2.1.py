#!/usr/bin/python2.7

#================================================================================================================================================
#         FILE:  Taxo_graph_computations_2.1.py
#        USAGE:  python2.7 Taxo_graph_computations_2.1.py
#  DESCRIPTION:  This python program is dedicated to the graphical representation of results given by the taxonomy analysis below. To do this,
#                all summary files produced by other programs will be analyzed and treated as a first part, and then, several graphical
#                representations (pies and stacked barplots) will be then done at each studied taxonomic level. Some particular colors will be
#                dedicated to particular groups (e.g. Environmental, Unclassified, Unknown and Lower groups). All minor groups in all samples
#                (with a lower relative abundance than 0.1% in all samples) will be groupes in the Lower groups. Moreover, if we have too many
#                samples to analyze, several subgraphs will be defined for pies to simplify the reading of graphics.
#
#                1.5 Modifications: all these modifications have been done to simplify and clarify the program using dedicated functions defined
#                in the program. All details can be found in the program itself.
#
#                1.6 Modifications: This part was realized to manage the nmber of sampels for pies graphs, to define also barplots, and to
#                efficiently define and draw the legend for all graphs. All details can be found in the program itself.
#
#                2.0 Modifications (05/12/2013): We modify this program to realize efficiently the bar plots with random selected colors and a
#		     maximum of 40 samples in the graphic. If the program founds more than 40 samples, it will create several bar plots.
#
#		     2.1 Modifications (18/07/2018): We deleted the various extensions of each figure to keep only the PDF format (the SVG and PNG
#		     were not kept anymore, as they were not used).
#
# REQUIREMENTS:  Python language (version 2.7)
#                Python-Matplotlib package (version 1.1.1)
#       AUTHOR:  Sebastien TERRAT <sebastien.terrat@inra.fr>
#      COMPANY:  UMR 1347 Agroecologie
#      VERSION:  2.1
#    REALEASED:  30/08/2012
#     REVISION:  18/07/2018
#================================================================================================================================================
#     This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
#================================================================================================================================================
#Needed libraries
import Library
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
import matplotlib.pyplot as plt
import matplotlib as mpl
import matplotlib.cm as CM
import sys

#------------------------------------------------------------------------------------------------------------------------------------------------
#Needed functions
#------------------------------------------------------------------------------------------------------------------------------------------------
def format_string_percent_pies(value):
    """format_string_percent_pies (function defined to avoid the printing of 0.0% values in the pies defined by the program."""
    if (value == 0):
        return ''
    else:
        return '{value:.1f}%'.format(value=value)

def compute_presence_absence(my_percent_list):
    """compute_presence_absence (function define to delete all taxonomic names with no sequences for all studied samples in the list)."""
    result_list = list()
    i = 0
    for i in range(len(my_percent_list[0])):
        result_list.append(0.0)
    for element in my_percent_list:
        for i in range(len(element)):
            result_list[i] = element[i] + result_list[i]
    return result_list

def define_lower_groups(taxo_names, samples):
    """define_lower_groups (function defined here to regroup all lower groups based on their relative abundance (lower than 1.0% for
    all studied samples) in a new created group named "Lower Groups"."""
    #Needed empty variables
    clean_samples = list()
    j = 0
    lower_groups = list()
    #We first create an empty list with the needed number of 0
    lower_groups = [0 for element in taxo_names]
    #Loop to evaluate the presence of lower groups (with all values below 1.0%)
    for element in samples:
        lower_groups = [0 if element[i] < 1.0 and lower_groups[i] == 0 else 1 for i in range(len(element))]
    #If one group is considered as lower than others, we add the Lower groups to taxo_level2
    if (lower_groups.count(0) > 0):
        taxo_names.append("Lower Groups")
        lower_groups.append(1)
    #Or, we leave the function as no lower groups is needed for this taxonomic level representation
    else:
        return taxo_names, samples
    #We then delete all lower groups names to define colors efficiently
    taxo_names = [taxo_names[i] for i in range(len(lower_groups)) if lower_groups[i] == 1]
    #Then, we transfer all lower values to my_low_values, corresponding to lower groups for each sample
    for element in samples:
        my_low_values = 0.0
        clean_samples.append(list())
        for i in range(len(element)):
            if (lower_groups[i] == 0):
                my_low_values = element[i] + my_low_values
            else:
                clean_samples[j].append(element[i])
        clean_samples[j].append(my_low_values)
        j = j + 1
    return taxo_names, clean_samples

def clean_labels_handles(labels, handles):
    """clean_labels_handles (function based on the cleaning of the labels and handles created by temporary figures to use them further
    for the legend of the figure)."""
    dict = {}
    for i in range(len(labels)):
        dict[labels[i]] = handles[i]
    my_cleaned_labels = dict.keys()
    my_cleaned_handles = dict.values()
    return my_cleaned_labels, my_cleaned_handles

def define_common_legend(sample_names_list, samples, taxo_names, used_colors):
    """define_common_legend (function using several lists to create a dedicated legend for all pies created. To do this, we first create
    temporary figures using this function and catch the labels and handles creating by the creation of graphs."""
    #Needed empty variables
    my_handles_tot = list()
    my_labels_tot = list()
    #We define a temporary figure
    pie_fig_tmp = plt.figure()
    #We then launch the creation of all needed data
    for i in range(len(sample_names_list)):
        #First, we create a temporary subplot named _tmp in the temporary figure
        my_sub_graph_tmp = pie_fig_tmp.add_subplot(1, 1, 0, title=sample_names_list[i])
        #my_sub_graph_tmp = pie_fig_tmp.add_subplot(1, 1, 1, title=sample_names_list[i])
        #We add in this subplot a temporary "pie" with labels and values
        my_sub_graph_tmp.pie(samples[i], labels = taxo_names, colors = used_colors, autopct = '%1.1f%%', pctdistance = 1.2, shadow = True)
        #We then catch the "handles" and "labels" from this temporary "pie"
        my_handles, my_labels = my_sub_graph_tmp.get_legend_handles_labels()
        my_handles_tot = my_handles_tot + my_handles
        my_labels_tot = my_labels_tot + my_labels
        #We finally delete the temporary "pie" to not show labels and values in the plot
        pie_fig_tmp.delaxes(my_sub_graph_tmp)
        pie_fig_tmp.clf()
    #At last steps, we close the figure itself, and we return the handles and labels
    plt.close()
    return my_handles_tot, my_labels_tot

def creating_figures_pies(Nbpages, Nbrows, Nbcols, sample_names, samples, used_colors, handles, labels, file_name):
    """creating_figure_pies (function defined to create the figures for the samples analyzed based on the number of pages,
    or columns and rows). All graphs are saved in various formats: svg, png and pdf."""
    #Needed empty variables
    my_num_pies = 0
    k = 0
    #k = 1
    #Loop to create a file for each group of six samples
    for i in range(Nbpages):
        k = 0
        #k = 1
        pie_fig = plt.figure()
        for j in range(6):
            #Test to avoid the cases of pages with a smaller amount of samples
            if (len(my_sample_names) < my_num_pies + 1):
                break
            else:
                #Here, we add a subplot, with the title
                my_sub_graph = pie_fig.add_subplot(Nbrows, Nbcols, k, aspect = 'equal', title = sample_names[my_num_pies])
                #We then add in the subpie the values and their corresponding colors
                my_sub_graph.pie(samples[my_num_pies], colors = used_colors, autopct = format_string_percent_pies, pctdistance = 1.2, shadow = True)
                my_sub_graph.grid(True)
                k = k + 1
                my_num_pies = my_num_pies + 1
        cols = Library.Functions.nb_cols_legend(len(labels),20)
        #Then, we create the dedicated legend for this figure
        pie_fig.tight_layout()
        if (Nbrows*Nbcols < 5):
            lgd = pie_fig.legend(handles, labels, loc = 'center left', ncol = cols, bbox_to_anchor = (0.8,0.5))
        else:
            lgd = pie_fig.legend(handles, labels, loc = 'center left', ncol = cols, bbox_to_anchor = (1.0,0.5))
        file_name = file_name.replace('.txt', '')
        #When we have added sufficient pies to the figure, we save it in various formats
        pie_fig.savefig('Summary_files/Figures/Taxonomy_Pies/Pie'+str(i+1)+'_'+file_name+'.pdf', bbox_extra_artists=(lgd,), bbox_inches='tight', dpi=150)
        #pie_fig.savefig('Summary_files/Figures/Taxonomy_Pies/Pie'+str(i+1)+'_'+file_name+'.png', bbox_extra_artists=(lgd,), bbox_inches='tight', dpi=150)
        #pie_fig.savefig('Summary_files/Figures/Taxonomy_Pies/Pie'+str(i+1)+'_'+file_name+'.svg', bbox_extra_artists=(lgd,), bbox_inches='tight', dpi=150)
        plt.close()

def creating_figures_barplots(sample_names, samples, used_colors, handles, labels, file_name, nb_fig):
    """creating_figures_barplots (function defined to create fastly and efficiently barplots for all studied samples using some input,
    e.g. sample_names, handles and labels). All graphs are saved in various formats: svg, png and pdf."""
    #Needed empty variables
    j = 0
    pos_bar = list()
    my_bottom = list()
    #Here, we create one empty list, and one to define the bottom position of each added barplot
    pos_bar = [i for i in range(len(sample_names))]
    my_bottom = [0 for i in range(len(sample_names))]
    #We then create a figure element named my_bar_fig, and we then add a subplot to this figure
    my_bar_fig = plt.figure()
    my_bar_plot = my_bar_fig.add_subplot(1,1,1, title = 'Relative abundance (percent) of detected taxonomic groups for studied samples')
    #Loop to read each element in samples (corresponding to each taxonomic group among all samples)
    for element in samples:
        #We add to the barplot each new values common to all samples
        my_bar_plot.bar(pos_bar, element, width = 0.65, align = 'center', color = used_colors[j], bottom = my_bottom)
        #We then define the new "bottom" values for the next addition in the barplot
        for i in range(len(my_bottom)):
            my_bottom[i] = my_bottom[i] + element[i]
        #Needed variable to count the number of used colors
        j = j + 1
    #We also define for the figure the limit of x and y based mainly on the number of samples
    my_bar_plot.set_xlim([-0.75,len(sample_names)-0.25])
    my_bar_plot.set_ylim([0,100])
    #We add the legend of the y-axis
    my_bar_plot.set_ylabel("Relative abundance (percent)")
    #We put in vertical the sample name
    plt.xticks(pos_bar, sample_names, rotation = 'vertical')
    #Here, we define the number of columns for all taxonomic names in the legend
    cols = Library.Functions.nb_cols_legend(len(labels), 20)
    #We define also the legend, anchored at the right using the bbox_to_anchor function
    lgd = my_bar_plot.legend(handles, labels, loc='center left', ncol = cols, bbox_to_anchor=(1.0,0.5))
    #We finally save the figure in various formats and we close the figure to free space
    file_name = file_name.replace('.txt', '')
    if (nb_fig == 0):
        my_bar_fig.savefig('Summary_files/Figures/Taxonomy_Barplots/Barplot_'+file_name+'.pdf', bbox_extra_artists=(lgd,), bbox_inches='tight', dpi=150)
        #my_bar_fig.savefig('Summary_files/Figures/Taxonomy_Barplots/Barplot_'+file_name+'.png', bbox_extra_artists=(lgd,), bbox_inches='tight', dpi=150)
        #my_bar_fig.savefig('Summary_files/Figures/Taxonomy_Barplots/Barplot_'+file_name+'.svg', bbox_extra_artists=(lgd,), bbox_inches='tight', dpi=150)
    else:
        my_bar_fig.savefig('Summary_files/Figures/Taxonomy_Barplots/Barplot_'+file_name+'_'+str(nb_fig)+'.pdf', bbox_extra_artists=(lgd,), bbox_inches='tight', dpi=150)
        #my_bar_fig.savefig('Summary_files/Figures/Taxonomy_Barplots/Barplot_'+file_name+'_'+str(nb_fig)+'.png', bbox_extra_artists=(lgd,), bbox_inches='tight', dpi=150)
        #my_bar_fig.savefig('Summary_files/Figures/Taxonomy_Barplots/Barplot_'+file_name+'_'+str(nb_fig)+'.svg', bbox_extra_artists=(lgd,), bbox_inches='tight', dpi=150)
    plt.close()

def creating_figures_heatmap(sample_names, samples, taxo_names, file_name):
    """creating_figures_heatmap (function to define and draw easily heatmaps for each level studied using several inputs: the sample_names, the samples
    but also the taxo_names for each value and finally the file_name to save all graphs. This program start by the creating all positions for major and
    minor ticks (useful for legends and grids), then the program create the figure itself, the colorbar and finally the grid. All graphs are saved in
    various formats: svg, png and pdf."""
    #We first determine several arrays of values to define positions of ticks (major and minor)
    my_xticks = [i for i in range(len(taxo_names))]
    my_yticks = [i for i in range(len(sample_names))]
    my_xticks_minor = [(i-0.5) for i in range(len(taxo_names))]
    my_yticks_minor = [(i-0.5) for i in range(len(sample_names))]
    #We must determine the figsize based on the number of samples and the number of groups studied
    my_height = 3.0 + (0.19*len(my_yticks))
    my_width = 3.0 + (0.13*len(my_xticks))
    #We then create a figure element named heatmap_fig, define its title and we then add a subplot to this figure
    heatmap_fig = plt.figure(figsize = (my_width, my_height))
    heatmap_fig.suptitle("Heatmap distribution of microbial groups detected\n(%) for studied samples.", fontsize = 15)
    my_sub_plot = heatmap_fig.add_subplot(1, 1, 1)
    #Here, we define a colorbar (its color, and the number of color chosen)
    my_cmap = CM.get_cmap('bone_r', 100)
    #We then add a imshow suplot to the defined subplot, and we used the colorbar defined earlier to compute the image itself
    my_heatmap = my_sub_plot.imshow(samples, aspect = 'auto', interpolation="nearest", cmap=my_cmap)
    #Here, we define the y and x positions of major ticks
    my_sub_plot.set_yticks(my_yticks)
    my_sub_plot.set_xticks(my_xticks)
    #Then, we define the legends based on the major ticks (for the taxo_names, we rotate and italize them)
    my_sub_plot.set_yticklabels(sample_names)
    my_sub_plot.set_xticklabels(taxo_names, rotation = 'vertical', ha = 'center', style = 'italic')
    #The next step is to define the minor ticks with their positions to create the grid
    my_sub_plot.set_yticks(my_yticks_minor, minor=True)
    my_sub_plot.set_xticks(my_xticks_minor, minor=True)
    plt.grid(True, which = 'minor', linestyle='-', linewidth=1.0)
    #Finally, for the major ticks, we define their length to 0 to delete them from the graphic
    my_sub_plot.tick_params(axis = 'both', length=0)
    #Here, we create a particular axis (an inset axis) whose size is fixed, to link the colorbar to it, to
    #easily control the creation, position and quality of the colorbar.
    axins = inset_axes(my_sub_plot, width=0.12, height=1.3, loc=3.0, bbox_to_anchor=(1.0, 0.5, 1.0, 1.0), borderpad=0.0)
    #We also define the label sizes for the colorbar
    axins.tick_params(labelsize = 6)
    #Then, we create and add the colorbar to the figure itself using the inset_axis
    my_colorbar = heatmap_fig.colorbar(my_heatmap, axins)
    #We also add the legend of the colorbar
    my_colorbar.set_label("Relative abundance (%)", fontsize = 7)
    #We then used this function to compute the image itself
    heatmap_fig.subplots_adjust(top = 0.88)
    #We finally save the figure itself
    file_name = file_name.replace('.txt', '')
    heatmap_fig.savefig('Summary_files/Figures/Taxonomy_Heatmaps/Heatmap_'+file_name+'.pdf', bbox_inches='tight', dpi=300)
    #heatmap_fig.savefig('Summary_files/Figures/Taxonomy_Heatmaps/Heatmap_'+file_name+'.png', bbox_inches='tight', dpi=300)
    #heatmap_fig.savefig('Summary_files/Figures/Taxonomy_Heatmaps/Heatmap_'+file_name+'.svg', bbox_inches='tight', dpi=300)
    #Needed to clean the figure in the memory and avoid some warnings
    heatmap_fig.clf()
    plt.close()
    plt.clf()

#End of defined functions
#------------------------------------------------------------------------------------------------------------------------------------------------


#-------------------------------------------------------------------------------------------------------
#Part to treat, format and analyze the raw data given by the previous programs
#-------------------------------------------------------------------------------------------------------

#First, we define the fontsize parameters for all elements in the graph
#Here, the legend font size
mpl.rcParams['legend.fontsize'] = 8.5
#Here, the font size of percent values visualized
mpl.rcParams['font.size'] = 8.5
#Finally, the size of the font of the title
mpl.rcParams['axes.titlesize'] = 11.0

#Defined global variables initially
Treated_file_names = list()
j = 0
chosen_treated_files = list()

#Automatic acquisition of file names in the target directory using the Function already define "read_folder" in
#the Library folder in the Functions.py file
treated_files = Library.Functions.read_folder("Summary_files/")
#Loop to treat all files found in the Summary_files folder
for file_name in treated_files:
    if ("Taxonomy_" in file_name):
        if ("sub" not in file_name):
            chosen_treated_files.append(file_name)

#Loop to treat only the chosen files
for file_name in chosen_treated_files:
    #Needed variables
    my_sample_names = list()
    my_samples = list()
    #We then open each file and catch all lines
    Taxo_file = open("Summary_files/"+file_name, 'r')
    Read_tot_lines = Taxo_file.readlines()
    #For each read line of the file (as the line is not empty)
    for each_line in Read_tot_lines:
        #We first delete the \n at the end
        each_line = each_line.replace("\n","")
        #We avoid the empty line
        if (each_line == ''):
            continue
        #We do not keep intermediate taxonomic information
        if ("Taxonomy_intermediate" in each_line):
            continue
        #We store also the names for the studied taxonomic level
        elif ("Taxonomy_" in each_line):
            taxo_level = each_line.split("\t")
            taxo_level.remove(taxo_level[0])
        #Finally, we store the computed percents of all studied samples
        else:
            tmp = each_line.split("\t")
            my_sample_names.append(tmp.pop(0))
            my_samples.append(Library.Functions.compute_percents(tmp))
    #We finally close the file containing all sata
    Taxo_file.close()
    #We also compute the percents of values for all samples studied
    my_verif = compute_presence_absence(my_samples)

    #Needed variables
    my_samples2 = list()
    taxo_level2 = list()
    #Loop to add easily lists to the my_samples2 list needed
    for element in my_samples:
        my_samples2.append(list())
    #Then, here, we create new lists with cleaned needed data (without 0 values)
    for i in range(len(my_verif)):
        j=0
        if (my_verif[i] > 0):
            taxo_level2.append(taxo_level[i])
            for element in my_samples:
                my_samples2[j].append(element[i])
                j = j + 1
    #We also clean the my_verif list to use it again
    my_verif = [element for element in my_verif if element != 0]
    #Here, we apply the found_file_name function to each names of sample found
    my_sample_names = [Library.Functions.found_file_name(element) for element in my_sample_names]
    #To gain space for the analysis, we delete not used lists
    del(taxo_level[:])
    del(my_samples[:])

#-------------------------------------------------------------------------------------------------------
#Function to create heatmaps based on data given by the main program
#-------------------------------------------------------------------------------------------------------
    #Function to clean and add the lower group in the graphic
    taxo_level2, my_samples2 = define_lower_groups(taxo_level2, my_samples2)
    #Function to create a heatmap using the defined function
    creating_figures_heatmap(my_sample_names, my_samples2, taxo_level2, file_name)

#-------------------------------------------------------------------------------------------------------
#Part to compute and organize data to create pies based on data given by the main program
#-------------------------------------------------------------------------------------------------------
    #Here, we then apply the compute_row_colum_pages function to define clearly how many
    #graphs we will create (a maximum of six sub graphs will be created in one graph
    Nbpages, Nbcols, Nbrows = Library.Functions.compute_row_column_pages(len(my_samples2))
    #Function to define used colors using the already defined colors
    my_chosen_colors = Library.Functions.define_colors(taxo_level2)
    #This function was then used to find and catch all labels and handles for the common legend of pies
    my_handles_tot, my_labels_tot = define_common_legend(my_sample_names, my_samples2, taxo_level2, my_chosen_colors)
    #This function was then dedicated to clean these labels and handles
    my_labels_tot, my_handles_tot = clean_labels_handles(my_labels_tot, my_handles_tot)

#-------------------------------------------------------------------------------------------------------
#Function to create pies based on data given by the main program
#-------------------------------------------------------------------------------------------------------
    #We then used this function to create the pies subplots and organize them based on the Nbpages, Nbrows and Nbcols
    creating_figures_pies(Nbpages, Nbrows, Nbcols, my_sample_names, my_samples2, my_chosen_colors, my_handles_tot, my_labels_tot, file_name)

#-------------------------------------------------------------------------------------------------------
#Part to create bar charts based on data given by the main program
#-------------------------------------------------------------------------------------------------------

    #2.0: Needed empty variables
    #my_samples2_sub = list()
    #my_sample_names_sub = list()
    #j = 0
    #l = 0
    #2.0: We first evaluate the number of samples (higher than 40 or not)
    #if (len(my_samples2)>40):
        #2.0: Loop to split the samples by groups of 40
    #     for k in range(len(my_samples2)):
    #         my_samples2_sub.append(my_samples2[k])
    #         my_sample_names_sub.append(my_sample_names[k])
    #         j = j + 1
    #         #2.0: When we obtain 40 samples, we create the barplot
    #         if (j == 40):
    #             l = l + 1
    #             #This list comprehension is used to transpose the matrix of values to create the bar chart needed
    #             my_samples2_sub = [[element[i] for element in my_samples2_sub] for i in range(len(taxo_level2))]
    #             #Then, we launch the dedicated function to draw needed
    #             creating_figures_barplots(my_sample_names_sub, my_samples2_sub, my_chosen_colors, my_handles_tot, my_labels_tot, file_name, l)
    #             #2.0: We emptied the two sub lists
    #             my_samples2_sub = list()
    #             my_sample_names_sub = list()
    #             j = 0
    #     #2.0: When we finished the samples, we defined a last figure
    #     l = l + 1
    #     my_samples2_sub = [[element[i] for element in my_samples2_sub] for i in range(len(taxo_level2))]
    #     creating_figures_barplots(my_sample_names_sub, my_samples2_sub, my_chosen_colors, my_handles_tot, my_labels_tot, file_name, l)
    # #2.0: When we have less than 40 samples
    # else:
    #     #This list comprehension is used to transpose the matrix of values to create the bar chart needed
    #     my_samples2 = [[element[i] for element in my_samples2] for i in range(len(taxo_level2))]
    #     #Then, we launch the dedicated function to draw needed
    #     creating_figures_barplots(my_sample_names, my_samples2, my_chosen_colors, my_handles_tot, my_labels_tot, file_name, l)

#We finally leave the program itself using this line of code
sys.exit(0)
