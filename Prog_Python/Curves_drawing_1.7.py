#=======================================================================================================
#         FILE:  Curves_drawing_1.7.py
#        USAGE:  python 2.7 Curves_drawing_1.7.py user_choice cutoff Rarefact_chose Rank_abundance_chose
#  DESCRIPTION:  This program was developed to launch the drawing of curves (rarefaction curves, but
#                also rank-abundance curves, and others if needed) using the matplotlib library. To do
#                this, this program needs some input given by the user or by the main program: the choice
#                of the user ("Raw" or "Clean") to analyze clusters, the defined cutoff percentage, and
#                the wanted graphs (rarefaction curves and/or rank-abundance curves).
#
#                1.5 Modifications: We add the drawing of rank-abundance curves using the same logic as
#                for rarefaction curves. The raw files have been computed by another python program
#                (Computation_rc_estimators_2.5.py). See program for details.
#
#                1.6 Modifications: We correct some errors and mistakes (e.g. the log of the y-axis of
#                rank-abundance curves), and simpify the program using the Library import.
#
#                1.7 Modifications: We import the drawing of curves in an external function in the
#                Functions.py program stored in the Library library. See the Functions.py for details.
#
#                DEPRECATED (since 12/02/2020)
#
# REQUIREMENTS:  Python language only (version 2.7)
#       AUTHOR:  Sebastien TERRAT <sebastien.terrat@dijon.inra.fr>
#      COMPANY:  GenoSol Platform
#      VERSION:  1.7
#    REALEASED:  30/08/2012
#     REVISION:  DEPRECATED (12/02/2020)
#=======================================================================================================
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#=======================================================================================================

import Library
import sys

#--------------------------------------------------------------------------------------------------------
#MAIN PROGRAM
#--------------------------------------------------------------------------------------------------------
#Needed variables given as input by the main program
#Catch the user choice for clustering analysis
user_choice = str(sys.argv[1])
#Asking for the desired threshold of dissimilarity
cutoff = sys.argv[2]
cutoff = float(cutoff)
#Catch also the choices of the user for the two types of curves
Rarefact_chose = sys.argv[3]
Rank_abundance_chose = sys.argv[4]

#Automatic acquisition of file names in the target directory
treated_files = Library.Functions.read_folder("Summary_files/Curves_Data/")
my_colors_rc = Library.Functions.my_chosen_colors()

#Here, we launch the defined function "draw_rarefact_curves" using some input like the cutoff,
#the my_colors_rc list, etc... to draw automatically rarefaction curves.
if Rarefact_chose.lower() == "yes":
    cutoff_rarefact = 100 - cutoff
    my_title = 'Rarefaction curves of studied samples at '+str(cutoff_rarefact)+' percent of similarity'
    Library.Functions.draw_curves(treated_files, cutoff_rarefact, my_colors_rc, user_choice, "Rarefactcurve_", 'Number of high-quality reads',\
        'Detected number of MOTUs', my_title, 'Summary_files/Figures/Curves/Rarefaction_Curves_')

if Rank_abundance_chose.lower() == "yes":
    cutoff_rank_abundance = 100 - cutoff
    my_title = 'Rank-abundance curves based on defined MOTUs of studied \nsamples at '+str(cutoff_rank_abundance)+' percent of similarity'
    Library.Functions.draw_curves(treated_files, cutoff_rank_abundance, my_colors_rc, user_choice, "Rank-abundance_curve_", 'Rank of defined MOTUs',\
        'Relative abundance (percent)', my_title, 'Summary_files/Figures/Curves/Rankabundance_Curves_')
#We finally leave the program itself unsing this command
sys.exit(0)
