#================================================================================================================================================
#         FILE:  Heatmap_computation_1.0.py
#        USAGE:  python2.7 Heatmap_computation_1.0.py
#  DESCRIPTION:  Program developed to compute the heatmap values for the drawing of a heatmap at the OTU level.
#
# REQUIREMENTS:  Python2.7 language only (version 2.7)
#       AUTHOR:  Sebastien TERRAT <sebastien.terrat@dijon.inra.fr>
#      COMPANY:  GenoSol Platform
#      VERSION:  1.0
#    REALEASED:  X
#     REVISION:  X
#================================================================================================================================================
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#================================================================================================================================================

#Needed libraries
import sys
import Library
import matplotlib.pyplot as plt
import matplotlib.cm as CM


Data_file = open("Summary_files/Global_Analysis/Clean_OTU_matrix_5.00_horizontal.txt", 'r')
Data_lines = Data_file.readlines()
Data_file.close()
my_matrix = list()
my_tmp_matrix = list()

for line in Data_lines:
    if ("OTU" in line):
        continue
    else:
        my_tmp_matrix = line.split("\t")
        my_tmp_matrix.remove(my_tmp_matrix[0])
        my_tmp_matrix = Library.Functions.compute_percents(my_tmp_matrix)
        my_matrix.append(my_tmp_matrix)

my_matrix = [[element[i] for element in my_matrix] for i in range(len(my_tmp_matrix))]
my_matrix = my_matrix[0:20]

heatmap_fig = plt.figure()

#Definition de la colormap (regarder quelles sont celles qui existent dans dir(matlplotlib.cm))
cmap = CM.get_cmap('afmhot_r', 100)

my_sub_plot = heatmap_fig.add_subplot(1,1,1, title="blabla")
my_heatmap = my_sub_plot.imshow(my_matrix,interpolation="nearest",cmap=cmap)
#heatmap_fig.tight_layout()
heatmap_fig.colorbar(my_heatmap)
#Ca, ca marche, si on lui fournit une matrice.
#plt.imshow(A2, interpolation="nearest", cmap=cmap)
#plt.colorbar()
#plt.show()
plt.savefig('example.pdf')

#------------------------------------------------------------------------------------------------------------------------------------------------
#MAIN PROGRAM
#------------------------------------------------------------------------------------------------------------------------------------------------
#Needed variables given as input by the main program
#We finally leave the program itself unsing this command
sys.exit(0)
