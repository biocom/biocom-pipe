#=======================================================================================================
#         FILE:  Computation_rc_estimators_3.1.py
#        USAGE:  python2.7 Computation_rc_estimators_3.1.py
#  DESCRIPTION:  To use this program, you first need cluster files (one or more) stored in the 'IN/'
#		 directory. This tool then will take each cluster file, and treat them to calculate a
#		 rarefaction curve based on the formula defined by several authors. For each cluster,
#		 a maximum of 1000 points are determined, and results are stored in 'OUT/' directory
#		 in a new file named : "rc_cutoff_inputfilename.clust". This tool treat all files found,
#		 and need only the defined cutoff by the user.
#                This tool is also dedicated specifically to be used in a server efficiently. Indeed,
#		 for each step of calculation for the rarefaction curve, a cpu is used. This is very fast
#		 for small sets of sequences, and fastly enough for large sets of sequences.
#
#                2.5 Modifs (30/08/2012): We modify the program to use parameters givn as input by the
#                main program to define what steps are done and, what not, as chosen by the user.
#
#                3.0 Modifs (06/11/2012): We modify and clarify the Python program using the Functions.py
#                as some parts of the program was already implemented in ohter systems ans functions, e.g.
#                the percent computing. Moreover, some parts of the program have also been simplifed,
#                modified and optimized in specific functions for the program itself (like the
#                rarefaction_computation function for example).
#
#                3.1 Modifs (19/07/2018): We moved some indice computation into the Functions librayr to
#                simplify the code and easily modify it to add the indices computation for the
#                GLOBAL_ANALYSIS step.
#
# REQUIREMENTS:  Python language only (version 2.7)
#       AUTHOR:  Sebastien TERRAT <sebastien.terrat@inra.fr>
#      COMPANY:  UMR 1347 Agroecologie
#      VERSION:  3.1
#    REALEASED:  24/01/2011
#     REVISION:  19/07/2018
#=======================================================================================================
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#=======================================================================================================

#Import needed modules (needed : comb for gmpy, corresponding at combinatory calculations)
import Library
import math
import gmpy
import os
import sys
import decimal

decimal.getcontext().prec = 6

#Defined variables for the main program and for the calculation of the rarefaction curve
treated_files = list()
Result_tmp = decimal.Decimal(0)
Result2 = ""
x,result = 0,0
Rarefact_chose = ""
string_printed = "Sample\t"
tmp_printed = list()
Result_printed = [""]
Children = []
Child_pid = 0

NbOTUs_list = list()
Nbseqs_list = list()
Matrix_list = list()
Treated_files = list()
Samples_list = list()
#-------------------------------------------------------------------------------------------------------
#2.5: First part of the program, catching data chosen by the user to analyze files in the directory
#and also estimators needed to be calculated and chose by the user
#-------------------------------------------------------------------------------------------------------
#Asking for the desired threshold of dissimilarity
cutoff = sys.argv[2]
printed_cutoff = sys.argv[2]
cutoff = float(cutoff)
cutoff = cutoff/100
#1.5: Catching user choices given by the main program
User_choice = sys.argv[1]
Rarefact_chose = sys.argv[3]
Rank_abundance_chose = sys.argv[4]
Chao_chose = sys.argv[5]
ACE_chose = sys.argv[6]
Index_chose = sys.argv[7]
Folder_name = sys.argv[8]
treated_file = sys.argv[9]
sample_name = sys.argv[10]
Results = ""

#-------------------------------------------------------------------------------------------------------
#Needed functions
#-------------------------------------------------------------------------------------------------------
def combinatory(N,n):
    """Combinatory (permit the calculation of combinatorial values)"""
    X = math.factorial(N)
    Xb = math.factorial(n)
    Xc = math.factorial(N-n)
    final = X/(Xb*Xc)
    return final

def rank_abundance_computation(Clusters, User_choice, cutoff, treated_file):
    """Function defined to compute the values for the rank_abundance curve."""
    #Needed variable
    j = 1
    #Computation to determine the relative abundance of each defined OTU
    Rank_abundance_data = Library.Functions.compute_percents(Clusters)
    #We then reverse it to organize data by descending order
    Rank_abundance_data.reverse()
    #We open the fils hat will contain computed data
    Rank_abundance_curve = open("Summary_files/Curves_Data/"+"Rank-abundance_curve_"+ User_choice +"_"+str(cutoff)+"_"+treated_file, 'w')
    #Then, we create a loop to create relative abundance of each cluster compared to the total
    for value in Rank_abundance_data:
        Rank_abundance_curve.write(str(j) + "\t"+ str(value) + "\n")
        j += 1
    Rank_abundance_curve.close()

def rarefaction_computation(Matrix, User_choice, cutoff, treated_file, Nb_seqs_tot, Nb_clusters):
    """Function defined to compute the values for the rarefaction curve."""
    #Needed empty variables
    n = 0
    N_Ni = 0
    Result = float()
    Result_fin = float()
    #2.5: Create a new file to store the rarefaction curve calculation data
    Rarefact_curve = open("Summary_files/Curves_Data/"+"Rarefactcurve_"+ User_choice +"_"+str(cutoff)+"_"+treated_file, 'w')
    Rarefact_curve.close()
    #A maximum of 1000 points are determined, verification of the number of sequences
    #If the number of sequences if less than 1000, we treat each point
    if Nb_seqs_tot < 1000:
        Nb_steps_calc = 1
    #Else, we calculate the needed step to do 1000 points
    else:
        Nb_steps_calc = math.floor(Nb_seqs_tot/1000)
    #Loop to calculate the rarefaction curve
    while n < Nb_seqs_tot:
        Child_pid = os.fork()
        if Child_pid == 0:
            #Calculation of the rarefaction curve points (details in the formula)
            for element in Matrix:
                #For each element in the matrix, we calculate N_Ni
                N_Ni = Nb_seqs_tot - element[0]
                #Sum of combinatory values in the upper formula using N_Ni, n and Nb_seqs_tot
                Result = Result + element[1]*gmpy.comb(gmpy.mpz(N_Ni),gmpy.mpz(n))
                #Determination of the value of the rarefaction curve for n sequences in Nb_seqs_tot data
                Result_fin = float(Nb_clusters) - Result/(gmpy.comb(gmpy.mpz(Nb_seqs_tot),gmpy.mpz(n)))
            #In the result file, storage of results for this step
            Result2 = str(n) + "\t" + str(Result_fin) + "\n"
            #Append the file to store results
            Rarefact_curve = open("Summary_files/Curves_Data/"+"Rarefactcurve_"+ User_choice +"_"+str(cutoff)+"_"+treated_file, 'a')
            Rarefact_curve.write(Result2)
            Rarefact_curve.close()
            os._exit(0)
        else:
            Children.append(Child_pid)
            #Increment n using the step calculated before
            n += Nb_steps_calc
    #Wait for all child processes to continue the program
    for Child_pid in Children:
        status = os.waitpid(Child_pid,0)

def format_data_from_glob_matrix(Lines):
    """This function compte the needed data to treat and define the indices from the global matrix
    encompassing all samples and all reads. To do this, it first takes as input the list of lines,
    read and treat data, and give back first the name fo the sample, the number of sequences, the
    number of clusters and finally the cluster composition as output."""

    #Needed empty variables
    NbOTUs_list = list()
    Nbseqs_list = list()
    Matrix_list = list()
    Treated_files = list()
    Samples_list = list()
    Dict = dict()
    Matrix = list()
    Nbseqs = 0
    NbOTUs = 0
    Sample = ""
    Deleted_infos = ['Derep_','Rdm_','Hpreprocess_','Mpreprocess','Lpreprocess_',\
                     'FilterAlign_','Clust_','PrinSeq_','FLASH','Clean_','Hunt_',\
                     'Qual_', 'Taxo_', 'SILVA_','RDP_','Align_',\
                     '.fasta', '.clust']

    for line in Lines:
        #We avoid the treatment of the first line (the file header)
        if ("OTU" in line):
            continue
        #Then, for each clustering file
        else:
            #We split the line, store the name in a dedicated list, and remove it
            Clusters = line.split("\t")
            Treated_files.append(Clusters[0])
            Sample = Clusters[0]
            for info in Deleted_infos:
                Sample = Sample.replace(info,'')
            Clusters.remove(Clusters[0])
            #We have to transform all elements in Clusters as int
            Clusters = [ int(x) for x in Clusters ]
            #For each element in Clusters
            for element in Clusters:
                #We avoid the treatment of empty clusters
                if element == 0:
                    continue
                #We count the number of MOTUs with the same number of sequences using a dictionnary
                if element in Dict:
                    Dict[element] += 1
                    Nbseqs += element
                    NbOTUs += 1
                #If we found a new number of sequences in a MOTU, we create it in the Dict and store the
                #data obtained for the previous MOTU type
                else:
                    Dict[element] = 1
                    Nbseqs += element
                    NbOTUs += 1
            #We then here read all elements found in the dictionnary and transform it into a list of associated
            #keys and values for further steps
            for key, value in Dict.items():
                Matrix.append([key, value])
            Matrix.sort()
            #Finally, we append to the lists the data
            Samples_list.append(Sample)
            NbOTUs_list.append(NbOTUs)
            Nbseqs_list.append(Nbseqs)
            Matrix_list.append(Matrix)
            #We emptied the variables for another treatment
            Dict = dict()
            Matrix = list()
            Nbseqs = 0
            NbOTUs = 0
    #We finally return the results
    return Samples_list, Treated_files, Nbseqs_list, NbOTUs_list, Matrix_list

#-------------------------------------------------------------------------------------------------------
#-------------------------------------------------------------------------------------------------------
#Main program : selection and storage of the chosen cluster
#-------------------------------------------------------------------------------------------------------
#-------------------------------------------------------------------------------------------------------

if Folder_name == "Global_Analysis":
    #Defined the name of the global matrix based on user choices
    treated_file = User_choice+"_OTU_matrix_"
    treated_file += "%.2f" % (cutoff*100)
    treated_file +="_horizontal.txt"
    #Open the targeted global matrix file
    Global_matrix_file = open("Summary_files/"+Folder_name+"/"+treated_file, 'r')
    #For each read line of the file (as the line is not empty)
    Read_lines = Global_matrix_file.readlines()
    #We then close the file
    Global_matrix_file.close()
    #We launch the function dedicated to the treatment of the Global matrix
    Samples_list, Treated_files, Nbseqs_list, NbOTUs_list, Matrix_list = format_data_from_glob_matrix(Read_lines)
else:
    #Open the targeted cluster file
    Cluster_file = open("Result_files/"+Folder_name+"/"+treated_file, 'r')
    #For each read line of the file (as the line is not empty)
    Read_lines = Cluster_file.readlines()
    #We then close the file
    Cluster_file.close()
    #We use the function to found and catch all needed data (nb_seqs_tot, nb_clusters and the details of OTUs)
    (Nb_seqs_tot, Nb_clusters, Cluster_analyzed) = Library.Functions.sort_chosen_cluster_details(Read_lines, cutoff)
    #We compute here the Rank_abundance curve based on examples found in various publications
    if Rank_abundance_chose.lower() == "yes":
        if ("Concatenated_reads" not in treated_file):
            rank_abundance_computation(Cluster_analyzed, User_choice, cutoff, treated_file)

    #Here, we used the needed function to create the needed matrices to determine rarefaction curves and all indexes.
    Matrix = Library.Functions.computing_matrix_OTUs(Cluster_analyzed)

    #Computation of the rarefaction curve based on the formula found in various publications
    if Rarefact_chose.lower() == "yes":
        if ("Concatenated_reads" not in treated_file):
            rarefaction_computation(Matrix, User_choice, cutoff, treated_file, Nb_seqs_tot, Nb_clusters)

    NbOTUs_list.append(Nb_clusters)
    Nbseqs_list.append(Nb_seqs_tot)
    Matrix_list.append(Matrix)
    Treated_files.append(treated_file)
    Samples_list.append(sample_name)
#Here, for each element in all four lists, the treatment will be done based on user choices given as input
#by the main program. Moreover, with this loop, we can treat both unique independent files and a global matrix
for sample, treated_file, Nb_clusters, Nb_seqs_tot, Matrix in zip(Samples_list, Treated_files, NbOTUs_list, Nbseqs_list, Matrix_list):
    #-------------------------------------------------------------------------------------------------------
    #Calculation of the chao estimator based on the formula found in various publications if wanted
    #for details : see Appendix.A
    #-------------------------------------------------------------------------------------------------------
    Results = ""
    Results+= str(sample)+"\t"+str(treated_file)+"\t"+str(Nb_seqs_tot)+"\t"+str(Nb_clusters)+"\t"
    #Verification for the choice of the user for calculation of the chao1 richness estimator
    if Chao_chose.lower() == "yes":
        #We then use the dedicated function to compute the needed indices
        (S_chao1, var_S_chao1, LCI95_chao1, UCI95_chao1) = Library.Functions.chao_computation(Nb_seqs_tot, Nb_clusters, Matrix)
        Results+=str(round(S_chao1,3))+"\t"+str(round(var_S_chao1,3))\
                        +"\t"+str(round(LCI95_chao1,3))+"\t"+str(round(UCI95_chao1,3))+"\t"

    #-------------------------------------------------------------------------------------------------------
    #Calculation of the ACE richness estimator based on the formula found in various publications if wanted
    #for details : see Appendix.A
    #-------------------------------------------------------------------------------------------------------
    #Verification for the choice of the user for calculation of the ACE richness estimator
    if ACE_chose.lower() == "yes":
        (S_ACE, S_rare_ACE, S_abund_ACE) = Library.Functions.ACE_computation(Nb_clusters, Matrix)
        Results+=str(round(S_ACE,3))+"\t"+str(S_rare_ACE)+"\t"+str(S_abund_ACE)+"\t"

    #-------------------------------------------------------------------------------------------------------
    #Calculation of bootstrap estimate and also shannon and simpson indexes based on formulas found
    #in various publications if wanted (for details : see Appendix.A)
    #-------------------------------------------------------------------------------------------------------
    #Verification for the choice of the user for calculation of the shannon and simpson indexes
    if Index_chose.lower() == "yes":
        (S_bootstrap, H_Shannon, var_H_Shannon, Evenness, D_Simpson, D_1_Simpson)\
             = Library.Functions.Bootstrap_Shannon_Simpson_computation(Nb_seqs_tot, Nb_clusters, Matrix)
        Results+=str(round(S_bootstrap,3))+"\t"+str(round(H_Shannon,3))\
                        +"\t"+str(var_H_Shannon)+"\t"+str(Evenness)+"\t"+str(D_Simpson)\
                        +"\t"+str(round(D_1_Simpson,3))

    #-------------------------------------------------------------------------------------------------------
    #Final tasks to print all data acquired for various estimators and indexes.
    #-------------------------------------------------------------------------------------------------------
    #3.0 : Test to store results in the dedicated file and the dedicated folder.
    if Folder_name == "Global_Analysis":
        Estimators = open("Summary_files/Global_Analysis/"+"Indices_Global_"+ User_choice +"_"+str(cutoff)+".txt", 'a')
    else:
        Estimators = open("Summary_files/"+"Indices_Independent_"+ User_choice +"_"+str(cutoff)+".txt", 'a')
    Estimators.write(Results+"\n")
    Estimators.close()
