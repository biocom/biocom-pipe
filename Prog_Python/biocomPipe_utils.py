# -*- coding: utf-8 -*-
from __future__ import division
import ConfigParser
import glob
import os
import sys
import csv
import re
import subprocess
import pprint
import math
import errno
import shutil
from collections import Counter
from datetime import datetime

__version__     = 1.0

__author__      = "Christophe DJEMIEL"
__copyright__   = "Copyright 2018, The BIOCOM-PIPE Project"
__credits__     = ["Christophe DJEMIEL", "Sebastien TERRAT"]
__license__     = "GPL"
__maintainer__  = "Christophe DJEMIEL"
__email__       = "christophe.djemiel@gmail.com"

class BiocomPipeUtils(object):
    '''
        Class to check raw data files, create input file and generate figures with a website.
    '''
    def __init__(self,biocom_pipe_dir,analysis_dir):
        '''
            Constructor method with init all parameters for BIOCOM-PIPE utils.
        '''
        self.biocom_pipe_dir = biocom_pipe_dir
        self.analysis_dir = analysis_dir
        self.config = ConfigParser.ConfigParser()
        # self.config.read([self.biocom_pipe_dir+'biocomPipe_config.ini',self.biocom_pipe_dir+'biocomPipe_input_config.ini'])
        self.config.read([self.biocom_pipe_dir+'/Prog_Python/biocomPipe_config.ini',self.biocom_pipe_dir+'/Prog_Python/biocomPipe_input_config.ini'])
        self.raw_data_dir = self.config.get('PATH_CONFIGURATION', 'raw_data_dir')
        self.project_files_dir = self.config.get('PATH_CONFIGURATION', 'project_files_dir')
        self.project_file = self.config.get('PATH_CONFIGURATION', 'project_file')
        self.project_template_file = self.config.get('PATH_CONFIGURATION', 'project_template_file')
        self.results_dir = self.config.get('PATH_CONFIGURATION', 'results_dir')
        self.summary_dir = self.config.get('PATH_CONFIGURATION', 'summary_dir')
        self.curves_data_dir = self.config.get('PATH_CONFIGURATION', 'curves_data_dir')
        self.databases_dir = self.config.get('PATH_CONFIGURATION', 'databases_dir')
        self.global_analysis_dir = self.config.get('PATH_CONFIGURATION', 'global_analysis_dir')
        self.unifrac_dir = self.config.get('PATH_CONFIGURATION', 'unifrac_dir')
        self.unifrac_tree_data_file = self.config.get('PATH_CONFIGURATION', 'unifrac_tree_data_file')
        self.input_file = self.config.get('PATH_CONFIGURATION', 'input_file')
        self.releases_details_file = self.config.get('PATH_CONFIGURATION', 'releases_details_file')
        self.hunting_file = self.config.get('PATH_CONFIGURATION', 'hunting_file')
        self.recovering_file = self.config.get('PATH_CONFIGURATION', 'recovering_file')
        self.reclustor_file = self.config.get('PATH_CONFIGURATION', 'reclustor_file')
        self.website_dir_templates = self.config.get('PATH_CONFIGURATION', 'website_dir_templates')
        self.website_dir = self.config.get('PATH_CONFIGURATION', 'website_dir')
        self.website_dir_taxonomy = self.config.get('PATH_CONFIGURATION', 'website_dir_taxonomy')
        self.website_dir_parameters_pipe = self.config.get('PATH_CONFIGURATION', 'website_dir_parameters_pipe')
        self.website_dir_about = self.config.get('PATH_CONFIGURATION', 'website_dir_about')
        self.website_dir_summary = self.config.get('PATH_CONFIGURATION', 'website_dir_summary')
        self.website_dir_alpha_diversity = self.config.get('PATH_CONFIGURATION', 'website_dir_alpha_diversity')
        self.website_dir_beta_diversity = self.config.get('PATH_CONFIGURATION', 'website_dir_beta_diversity')
        self.krona_outputdir = self.config.get('PATH_CONFIGURATION', 'krona_outputdir')
        self.clean_project_files = self.config.get('GLOBAL_CONFIGURATION', 'clean_project_files')
        self.check_raw_data_files = self.config.get('GLOBAL_CONFIGURATION', 'check_raw_data_files')
        self.compress_results_files = self.config.get('GLOBAL_CONFIGURATION', 'compress_results_files')
        self.format_compress_results_files = self.config.get('GLOBAL_CONFIGURATION', 'format_compress_results_files')
        self.platform = self.config.get('SEQUENCING', 'platform')
        self.target = None
        self.organism = None
        self.list_preprocess_mids = []
        self.forward_primer = self.config.get('PREPROCESSING', 'forward_primer')
        self.reverse_primer = self.config.get('PREPROCESSING', 'reverse_primer')
        self.list_sample_names_pf = []
        # self.flash = self.config.options('FLASH')
        # print self.prinseq

    def convert_size(self,size_bytes):
       if size_bytes == 0:
           return "0B"
       size_name = ("B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB")
       i = int(math.floor(math.log(size_bytes, 1024)))
       p = math.pow(1024, i)
       s = round(size_bytes / p, 2)
       return "%s %s" % (s, size_name[i])

    def get_file_size(self,file_size):
        return self.convert_size((os.path.getsize(file_size)))

    def create_JSTree(self,path_jstree):
        dico_jstree = {'text': "{0} ({1})".format(os.path.basename(path_jstree),self.get_file_size(path_jstree))}
        if os.path.isdir(path_jstree):
            dico_jstree['type'] = "directory"
            dico_jstree['children'] = [self.create_JSTree(os.path.join(path_jstree,pj)) for pj in os.listdir(path_jstree)]
        else:
            dico_jstree['icon'] = "fa fa-file"
            dico_jstree['type'] = "file"
        return dico_jstree

    def check_project_file_exist(self):
        '''
            Check if a project.csv file exists.
        '''
        return os.path.isfile(self.analysis_dir+self.project_file)

    def copy_directory(self,src, dest):
        try:
            shutil.copytree(src, dest)
        except OSError as e:
            # If the error was caused because the source wasn't a directory
            if e.errno == errno.ENOTDIR:
                shutil.copy(src, dest)
            else:
                print('Directory not copied. Error: %s' % e)

    def copy_templates(self):
        '''
            Copy the template files to create the website of the current analysis.
        '''
        state_dir_projecf = self.check_dir_is_empty(self.analysis_dir+self.website_dir)
        #create the dir
        if state_dir_projecf == None:
            print u" \u2713 The results folder has been created.".encode('utf8')
            self.copy_directory(self.biocom_pipe_dir+self.website_dir_templates,self.analysis_dir+self.website_dir)
            mydir_dist = os.path.join(self.analysis_dir+self.project_files_dir,"website")
            self.website_dir = mydir_dist
        #a dir result exist so create a new dir
        if state_dir_projecf == False:
            print "A results folder already exists, so as not to lose these results a new folder has been created."
            mydir_dist = os.path.join(self.analysis_dir+self.project_files_dir, datetime.now().strftime('%Y-%m-%d_%H-%M-%S')+"_website")
            self.copy_directory(self.biocom_pipe_dir+self.website_dir_templates,mydir_dist)
            self.website_dir = mydir_dist

    def check_raw_data_dir(self):
        '''
            Check the files format from Raw_data folder.
        '''
        format_files_permitted = {'sff' : [],
        'sff.gz' : [],
        'qual' : [],
        'qual.gz' : [],
        'fastq' : [],
        'fq' : [],
        'fq.gz' : [],
        'fastq.gz' : [],
        'fasta': [],
        'fasta.gz': [],
        'fna': [],
        'fna.gz': [],
        'others': []}
        total_files = len(self.list_files_from_dir(self.analysis_dir+self.raw_data_dir,"*",""))
        total_format_files_not_permitted = 0
        for ffp_k,ffp_v in format_files_permitted.items():
            raw_data_files = self.list_files_from_dir(self.analysis_dir+self.raw_data_dir,ffp_k,"")
            format_files_permitted[ffp_k].extend(raw_data_files)
            #display number of files by group
            if len(raw_data_files) == 1:
                print "\tThe Raw_data folder contains {0} {1} file.".format(len(raw_data_files),ffp_k)
            elif len(raw_data_files) > 1:
                print "\tThe Raw_data folder contains {0} {1} files.".format(len(raw_data_files),ffp_k)
        #decompress files if necessary
        if any("ffp_v" for ffp_k, ffp_v in format_files_permitted.items() if ffp_k.endswith(".gz") and len(ffp_v) >0):
            self.decompress_files(self.analysis_dir+self.raw_data_dir)
        #display number of files not permitted
        total_format_files_permitted = sum(map(len, format_files_permitted.values()))
        total_format_files_not_permitted = total_files - total_format_files_permitted
        if total_format_files_not_permitted > 0:
            print "\t/!\ {0} files are not in the appropriate format (sff, qual,fastq, fasta, fna or *.gz).".format(total_format_files_not_permitted)

    def clean_project_files_dir(self):
        '''
            Clean the project files folder.
        '''
        if not os.path.exists(self.analysis_dir+self.project_files_dir): # if the directory does not exist
            os.makedirs(self.analysis_dir+self.project_files_dir) # make the directory
            os.popen("cp {0} {1}".format(self.biocom_pipe_dir+self.project_template_file,self.analysis_dir+self.project_files_dir))
            print "\tThe '{0}' folder has has been created.\n\t /!\ Please complete the project file template 'project.csv' in {0} because it is essential to create the Input.txt file.".format(self.analysis_dir+self.project_files_dir)
            sys.exit(0)
        else: # the directory exists
            #removes all files in a folder
            for the_file in os.listdir(self.analysis_dir+self.project_files_dir):
                if "project" not in the_file and "Project" not in the_file:
                    file_path = os.path.join(self.analysis_dir+self.project_files_dir, the_file)
                    try:
                        if os.path.isfile(file_path):
                            os.unlink(file_path) # unlink (delete) the file
                    except Exception, e:
                        print e
            #removes all subdirectories and all contents
            for project_files_dirs in [os.path.join(self.analysis_dir+self.project_files_dir, o) for o in os.listdir(self.analysis_dir+self.project_files_dir) if os.path.isdir(os.path.join(self.analysis_dir+self.project_files_dir,o))]:
                try:
                    shutil.rmtree(project_files_dirs)
                except Exception as e:
                    print(e)
                    raise
            #if the project file does not exist it is created
            if self.check_project_file_exist() == False:
                os.popen("cp {0} {1}".format(self.biocom_pipe_dir+self.project_template_file,self.analysis_dir+self.project_files_dir))
                print "\t /!\ Please, complete the project file template in '{0}' because it is essential to create the Input.txt file.".format(self.analysis_dir+self.project_files_dir)
                sys.exit(0)

    def check_dir_is_empty(self,dirName):
        '''
            Check if a Directory is empty and also check exceptional situations.
        '''
        state_dir_empty = None
        if os.path.exists(dirName) and os.path.isdir(dirName):
            if not os.listdir(dirName):
                state_dir_empty = True
            else:
                # print  "Directory {0} is not empty.".format(dirName)
                state_dir_empty = False
        else:
            # print "Given Directory don't exists."
            state_dir_empty = None
        return state_dir_empty

    def list_files_from_dir(self,dirNameFiles,extentionFile,motifSearch):
        '''
            Return the list of files with a specific motif in the filename and an extension from directory.
        '''
        return glob.glob('{0}{1}*.{2}'.format(dirNameFiles,motifSearch,extentionFile))

    def decompress_files(self,folder_files_compressed):
        '''
            Decompress files from directory.
        '''
        state_dir = self.check_dir_is_empty(folder_files_compressed)
        if state_dir == False:
            list_gz_files = []
            for gz_file in glob.glob(folder_files_compressed+"*.gz"):
                list_gz_files.append(gz_file)
            if list_gz_files:
                for gz in list_gz_files:
                    cmd_gunzip = "gzip -df {0}".format(gz)
                    os.system(cmd_gunzip)
                    #print cmd_gunzip
                    print "\tThe '{0}' has been extract into '{1}'.".format(gz,os.path.splitext(gz)[0])
            else:
                print "All files in '{0}' were always extracted.".format(folder_files_compressed)
        elif state_dir == True:
            print "\tDirectory '{0}' is empty. Please put the files in the folder {0}".format(folder_files_compressed)
        else:
            print "\tDirectory '{0}' does not exists. Please create '{0}' and put the files in the folder '{0}'.".format(folder_files_compressed)

    def decompress_files_raw_folder(self):
        '''
            Decompress files from Raw_data directory.
        '''
        state_raw_data_dir = self.check_dir_is_empty(self.raw_data_dir)
        if state_raw_data_dir == False:
            list_fastqgz_files = []
            for fastqgz_file in glob.glob(self.raw_data_dir+"*.fastq.gz"):
                list_fastqgz_files.append(fastqgz_file)
            if list_fastqgz_files:
                for fqgz in list_fastqgz_files:
                    cmd_gunzip = "gzip -d {0}".format(fqgz)
                    #os.system(cmd_gunzip)
                    #print cmd_gunzip
                    print "\tThe '{0}' has been extract into '{1}'.".format(fqgz,os.path.splitext(fqgz)[0])
            else:
                print "All files in '{0}' were always extracted.".format(self.raw_data_dir)
        elif state_raw_data_dir == True:
            print "\tDirectory '{0}' is empty. Please put the raw files in the folder {0}".format(self.raw_data_dir)
        else:
            print "\tDirectory '{0}' does not exists. Please create '{0}' and put the raw files in the folder '{0}'.".format(self.raw_data_dir)

            #print list_fastqgz_files

    def transpose_matrix(self, matrix_to_transpose):
        return [list(mtt) for mtt in zip(*matrix_to_transpose)]

    def chunks_list(self,list_to_chunk,nb_len):
        return [list_to_chunk[i:i + nb_len] for i in xrange(0, len(list_to_chunk), nb_len)]

    def merge_path_filename_format(self,dir_name,base_filename,filename_suffix):
        return os.path.join(dir_name, '.'.join((base_filename, filename_suffix)))

    def check_project_file_data(self,column):
        '''
            Check if a project.csv contains data from a specific column.
        '''
        with open(self.analysis_dir+self.project_file, "rb") as f:
            csvreader = csv.reader(f, delimiter=",")
            lines_csvreader = [ row for row in csvreader]
            header_position =  {x:i for i,x in enumerate(lines_csvreader.pop(0))}
            temp_column_data = []
            state_column = None
            if len(lines_csvreader) !=0:
                for lcsv in lines_csvreader:
                    if len([y.strip() for y in lcsv if y.strip()])!=0:
                        temp_column_data.append(row[header_position[column]])
                    else:
                        state_column = False
                        print "\t/!\ Error: The line is corrupt. Please check the file '{0}' because the column '{1}' maybe does not exists.".format(self.analysis_dir+self.project_file,column)
            else:
                state_column = False
                print "\n\t/!\ Error: The file '{0}' does not contains lines.".format(self.analysis_dir+self.project_file)
            if len(lines_csvreader) == len(temp_column_data) and len(lines_csvreader) !=0:
                state_column = True
        return state_column

    def get_sample_names(self):
        '''
            Get sample names from Raw data.
        '''
        extention = ""
        if self.platform == "Illumina":
            extention = "fastq"
        else:
            extention = "sff"
        list_sample_names = [os.path.splitext(os.path.basename(sample))[0] for sample in glob.glob('{0}*.{1}'.format(self.raw_data_dir,extention))]
        return list_sample_names

    def get_platform(self):
        '''
            Get the platform name from exension file in Raw_data.
        '''
        list_extention = [os.path.splitext(os.path.basename(sample.replace(".gz","")))[-1] for sample in glob.glob('{0}*'.format(self.analysis_dir+self.raw_data_dir))]
        list_extention_filter = Counter(list_extention).keys()
        if len(list_extention_filter) == 1:
            list_extention_filter_good = list_extention_filter[0]
            if list_extention_filter_good == ".sff":
                platf = "roche"
            else:
                platf = "illumina"
        else:
            print "\t/!\ Error: Various extension file are found ({1}). Please check the raw files '{0}'.".format(self.analysis_dir+self.project_file,", ".join(list_extention_filter))
        return platf

    def get_sample_names_pf(self):
        '''
            get sample name from project file
        '''
        with open(self.analysis_dir+self.project_file) as myFile:
            reader = csv.DictReader(myFile,delimiter = ',')
            project_file_lines = [r for r in reader]
            for num_l_pf,pf in enumerate(project_file_lines):
                self.list_sample_names_pf.append(pf['SAMPLE_NAME'])
        return self.list_sample_names_pf

    def filter_samplenames(self,listSampleNamesToFilter):
        '''
            Try to filter filename.
        '''
        new_list_samples_names = []
        #check if list of lists and true
        if any(isinstance(el, list) for el in listSampleNamesToFilter) == True:
            for lsntf in listSampleNamesToFilter:
                tmp_spleName = []
                for sampleF in lsntf:
                    for sampleNamePF in self.list_sample_names_pf:
                        if sampleNamePF.upper() in sampleF.upper():
                            tmp_spleName.append(sampleNamePF)
                new_list_samples_names.append(tmp_spleName)
        # is a string
        elif isinstance(listSampleNamesToFilter, str):
            for sampleNamePF in self.list_sample_names_pf:
                if sampleNamePF.upper() in listSampleNamesToFilter.upper():
                    new_list_samples_names = sampleNamePF
        #is just a list
        else:
            pass
        return new_list_samples_names

    def set_input_file(self):
        '''
            Create preprocess mids if necessary.
            Set primers if present in project file.
        '''
        forward_primer_list = []
        reverse_primer_list = []
        target_list = []
        dico_target = {'16S':'bacteria',
        '18S':'fungi',
        '23S':'algae'}
        self.check_raw_data_dir()
        with open(self.analysis_dir+self.project_file) as myFile:
            reader = csv.DictReader(myFile,delimiter = ',')
            project_file_lines = [r for r in reader]
            for num_l_pf,pf in enumerate(project_file_lines):
                num_l_pf = num_l_pf+1
                if pf['LIBRARY_NAME_RECEIVED'] and pf['MID_F'] and pf['MID_R'] and pf['SAMPLE_NAME']:
#                    if pf['MID_F'] == pf['MID_R']:
#                        self.list_preprocess_mids.append('Filename/MID/Sample:\t{0}/{1}/{2}\n'.format(pf['LIBRARY_NAME_RECEIVED'],pf['MID_F'],pf['SAMPLE_NAME']))
#                    else:
                    self.list_preprocess_mids.append('Filename/MID_F/MID_R/Sample:\t{0}/{1}/{2}/{3}\n'.format(pf['LIBRARY_NAME_RECEIVED'],pf['MID_F'],pf['MID_R'],pf['SAMPLE_NAME']))
                else:
                    if not pf['LIBRARY_NAME_RECEIVED']:
                        print "\tFilename not found line '{0}' in '{1}' file.".format(num_l_pf,self.analysis_dir+self.project_file)
                    if not pf['MID']:
                        print "\tMID not found line '{0}' in '{1}' file.".format(num_l_pf,self.analysis_dir+self.project_file)
                    if not pf['SAMPLE_NAME']:
                        print "\tSample Name not found line '{0}' in '{1}' file.".format(num_l_pf,self.analysis_dir+self.project_file)

                if pf['SEQUENCE_PRIMER_F'] not in forward_primer_list and pf['SEQUENCE_PRIMER_F']!=None:
                    forward_primer_list.append(pf['SEQUENCE_PRIMER_F'])
                if pf['SEQUENCE_PRIMER_R'] not in reverse_primer_list and pf['SEQUENCE_PRIMER_R']!=None:
                    reverse_primer_list.append(pf['SEQUENCE_PRIMER_R'])
                if pf['TARGET'] not in target_list and pf['TARGET']!=None:
                    target_list.append(pf['TARGET'])
        #set target (e.g. 16S, 18S or 23S)
        target_list_len = len(target_list)
        if target_list_len==0:
            print "\tNo target (e.g. 16S, 18S or 23S) found in '{0}' file.".format(self.analysis_dir+self.project_file)
        elif target_list_len==1:
            self.target = target_list[0]
            self.organism = dico_target[self.target.upper()]
            print "\t{0} target found in {1} and set {2} to '{3} file'.".format(self.target,self.analysis_dir+self.project_file,self.organism,self.analysis_dir+self.input_file)
        else:
            print "\t{0} targets found in '{1}' file. Please choose one (e.g. 16S, 18S or 23S).".format(forward_primer_list_len,self.analysis_dir+self.project_file)
        #set primers
        forward_primer_list_len = len(forward_primer_list)
        reverse_primer_list_len = len(reverse_primer_list)
        if forward_primer_list_len==0:
            print "\tNo forward primer found in '{0}' file.".format(self.analysis_dir+self.project_file)
        elif forward_primer_list_len==1:
            self.forward_primer = forward_primer_list[0].replace("-","").upper()
            print "\t{0} forward primer found in {1} and set to '{2} file'.".format(self.forward_primer,self.analysis_dir+self.project_file,self.analysis_dir+self.input_file)
        else:
            print "\t{0} forward primers found in '{1}' file. Please choose one".format(forward_primer_list_len,self.analysis_dir+self.project_file)
        if reverse_primer_list_len==0:
            print "\tNo reverse primer found in '{0}' file.".format(self.analysis_dir+self.project_file)
        elif reverse_primer_list_len==1:
            self.reverse_primer = reverse_primer_list[0].replace("-","").upper()
            print "\t{0} reverse primer found in {1} and set to '{2}' file.".format(self.reverse_primer,self.analysis_dir+self.project_file,self.analysis_dir+self.input_file)
        else:
            print "\t{0} reverse primers found in '{1}' file. Please choose one.".format(reverse_primer_list_len,self.analysis_dir+self.project_file)
        #print "\tDefine the directory where to get the document and the samples to be analyzed and the MID assigned to these samples.".format()
        return self.list_preprocess_mids

    def create_input_file(self):
        sequencing_pf = self.get_platform()
        input_file_to_write = open(self.analysis_dir+self.input_file,"w+")
        input_file_to_write.write("###PRINSEQ###\n")
        input_file_to_write.write("Step to do [yes-no]:\t{0}\n".format(self.config.get('PRINSEQ', 'step_to_do')))
        input_file_to_write.write("Lowest quality score tolerated for the trimming from the 3'-end of the read [0-40]:\t{0}\n".format(self.config.get('PRINSEQ', 'lowest_quality_score_3')))
        input_file_to_write.write("Lowest quality score tolerated for the trimming from the 5'-end of the read [0-40]:\t{0}\n".format(self.config.get('PRINSEQ', 'lowest_quality_score_5')))
        input_file_to_write.write("Minimum Length threshold tolerated to keep reads (default: 30):\t{0}\n".format(self.config.get('PRINSEQ', 'minimum_length_threshold')))
        input_file_to_write.write("Number of ambiguities (N's) tolerated (default:\t{0}\n".format(self.config.get('PRINSEQ', 'number_of_ambiguities')))
        input_file_to_write.write("Sliding window size used to calculate quality score [1-7]:\t{0}\n".format(self.config.get('PRINSEQ', 'sliding_window_size')))
        input_file_to_write.write("Step size used to move the sliding window [1-10]:\t{0}\n".format(self.config.get('PRINSEQ', 'step_size')))
        input_file_to_write.write("//\n")
        input_file_to_write.write("###FLASH###\n")
        input_file_to_write.write("Step to do [yes-no]:\t{0}\n".format(self.config.get('FLASH', 'step_to_do')))
        input_file_to_write.write("Minimum overlap length between two reads (default: 10) [10-100]:\t{0}\n".format(self.config.get('FLASH', 'minimum_overlap_length')))
        input_file_to_write.write("Maximum overlap length between two reads (default: 65) [10-100]:\t{0}\n".format(self.config.get('FLASH', 'maximum_overlap_length')))
        input_file_to_write.write("Maximum allowed ratio between the number of mismatched base pairs and the overlap length (default: 0.04) [0-100]:\t{0}\n".format(self.config.get('FLASH', 'ratio_mismatched_overlap')))
        input_file_to_write.write("//\n")
        input_file_to_write.write("###EVAL_QUAL###\n")
        input_file_to_write.write("Step to do [yes-no]:\t{0}\n".format(self.config.get('EVAL_QUAL', 'step_to_do')))
        if self.organism == "fungi":
            input_file_to_write.write("Length:\t{0}\n".format(self.config.get('EVAL_QUAL', 'length_fungi')))
        else:
            input_file_to_write.write("Length:\t{0}\n".format(self.config.get('EVAL_QUAL', 'length')))
        input_file_to_write.write("//\n")
        input_file_to_write.write("###PREPROCESS_MIDS###\n")
        input_file_to_write.write("Step to do [yes-no]:\t{0}\n".format(self.config.get('PREPROCESS_MIDS', 'step_to_do')))
        for lpm in self.list_preprocess_mids:
            input_file_to_write.write(lpm)
        input_file_to_write.write("//\n")
        input_file_to_write.write("###RANDOM_PREPROCESS_MIDS###\n")
        input_file_to_write.write("Step to do [yes-no]:\t{0}\n".format(self.config.get('RANDOM_PREPROCESS_MIDS', 'step_to_do')))
        input_file_to_write.write("Number of reads in the subset:\t{0}\n".format(self.config.get('RANDOM_PREPROCESS_MIDS', 'number_reads_subset')))
        input_file_to_write.write("//\n")
        input_file_to_write.write("###PREPROCESSING###\n")
        input_file_to_write.write("Step to do [yes-no]:\t{0}\n".format(self.config.get('PREPROCESSING', 'step_to_do')))
        if self.organism == "fungi":
            input_file_to_write.write("Minimum Length threshold:\t{0}\n".format(self.config.get('PREPROCESSING', 'minimum_length_threshold_fungi')))
        else:
            input_file_to_write.write("Minimum Length threshold:\t{0}\n".format(self.config.get('PREPROCESSING', 'minimum_length_threshold')))
        input_file_to_write.write("Number of ambiguities (N's) tolerated:\t{0}\n".format(self.config.get('PREPROCESSING', 'number_ambiguities')))
        input_file_to_write.write("Forward primer:\t{0}\n".format(self.forward_primer))
        input_file_to_write.write("Reverse primer:\t{0}\n".format(self.reverse_primer))
        input_file_to_write.write("Number of differences tolerated in your forward primer sequence [0-2]:\t{0}\n".format(self.config.get('PREPROCESSING', 'number_of_differences_f')))
        input_file_to_write.write("Number of differences tolerated in your reverse primer sequence [0-2]:\t{0}\n".format(self.config.get('PREPROCESSING', 'number_of_differences_r')))
        input_file_to_write.write("Stringency [High, Medium or Low]:\t{0}\n".format(self.config.get('PREPROCESSING', 'stringency')))
        input_file_to_write.write("//\n")
        input_file_to_write.write("###QUALITY_CLEANING###\n")
        input_file_to_write.write("Step to do [yes-no]:\t{0}\n".format(self.config.get('QUALITY_CLEANING', 'step_to_do')))
        input_file_to_write.write("Lowest quality score tolerated [0-40] (default: 0):\t{0}\n".format(self.config.get('QUALITY_CLEANING', 'lowest_quality_score')))
        input_file_to_write.write("Lowest average quality score tolerated [0-40] (default: 0):\t{0}\n".format(self.config.get('QUALITY_CLEANING', 'lowest_average_quality')))
        input_file_to_write.write("//\n")
        input_file_to_write.write("###RANDOM_PREPROCESS###\n")
        input_file_to_write.write("Step to do [yes-no]:\t{0}\n".format(self.config.get('RANDOM_PREPROCESS', 'step_to_do')))
        input_file_to_write.write("Number of reads in the subset:\t{0}\n".format(self.config.get('RANDOM_PREPROCESS', 'number_reads_subset')))
        input_file_to_write.write("//\n")
        input_file_to_write.write("###LENGTH_RANGE_PREPROCESS###\n")
        input_file_to_write.write("Step to do [yes-no]:\t{0}\n".format(self.config.get('LENGTH_RANGE_PREPROCESS', 'step_to_do')))
        if self.organism == "fungi":
            input_file_to_write.write("Length:\t{0}\n".format(self.config.get('LENGTH_RANGE_PREPROCESS', 'length_fungi')))
        else:
            input_file_to_write.write("Length:\t{0}\n".format(self.config.get('LENGTH_RANGE_PREPROCESS', 'length')))
        input_file_to_write.write("//\n")
        input_file_to_write.write("###RAW_INFERNAL_ALIGNMENT###\n")
        input_file_to_write.write("Step to do [yes-no]:\t{0}\n".format(self.config.get('RAW_INFERNAL_ALIGNMENT', 'step_to_do')))
        input_file_to_write.write("Covariance model used [bacteria, archaea, algae or fungi]:\t{0}\n".format(self.organism))
        input_file_to_write.write("//\n")
        input_file_to_write.write("###FILTERING_RAW_ALIGNMENT###\n")
        input_file_to_write.write("Step to do [yes-no]:\t{0}\n".format(self.config.get('FILTERING_RAW_ALIGNMENT', 'step_to_do')))
#        input_file_to_write.write("Quality alignment threshold (read percentage not aligned to the structure, default:10%) [1-99]:\t{0}\n".format(self.config.get('FILTERING_RAW_ALIGNMENT', 'quality_alignment_threshold')))
        input_file_to_write.write("//\n")
        input_file_to_write.write("###RAW_CLUSTERING###\n")
        input_file_to_write.write("Step to do [yes-no]:\t{0}\n".format(self.config.get('RAW_CLUSTERING', 'step_to_do')))
        if sequencing_pf == "roche":
            input_file_to_write.write("Ignoring homopolymers differences [yes-no]:\t{0}\n".format(self.config.get('RAW_CLUSTERING', 'ignoring_homopolymers_differences_roche')))
        else:
            input_file_to_write.write("Ignoring homopolymers differences [yes-no]:\t{0}\n".format(self.config.get('RAW_CLUSTERING', 'ignoring_homopolymers_differences')))
        input_file_to_write.write("Ignoring distances at the beginning of sequences [yes-no]:\t{0}\n".format(self.config.get('RAW_CLUSTERING', 'ignoring_distances_beginning_seq')))
        input_file_to_write.write("Maximum percentage of dissimilarity for clustering (%):\t{0}\n".format(self.config.get('RAW_CLUSTERING', 'maximum_percentage_dissimilarity_clustering')))
        input_file_to_write.write("Step size for each cluster (%):\t{0}\n".format(self.config.get('RAW_CLUSTERING', 'size_each_cluster')))
        input_file_to_write.write("//\n")
        input_file_to_write.write("###HUNTING###\n")
        input_file_to_write.write("Step to do [yes-no]:\t{0}\n".format(self.config.get('HUNTING', 'step_to_do')))
        input_file_to_write.write("Chosen clustering percentage of dissimilarity for the hunting step (%):\t{0}\n".format(self.config.get('HUNTING', 'clustering_percentage_dissimilarity_hunting')))
        input_file_to_write.write("//\n")
        input_file_to_write.write("###RECOVERING###\n")
        input_file_to_write.write("Step to do [yes-no]:\t{0}\n".format(self.config.get('RECOVERING', 'step_to_do')))
        input_file_to_write.write("Organism studied [bacteria, or fungi]:\t{0}\n".format(self.organism))
        input_file_to_write.write("Database used for the taxonomic assignment [RDP (bacteria only), or Silva (bacteria, fungi or algae)]:\t{0}\n".format(self.config.get('RECOVERING', 'database_taxonomic_assignment')))
        input_file_to_write.write("Database chosen Silva version for the taxonomic assignment of bacteria (R114 or R132, by default R132)]:\t{0}\n".format(self.config.get('RECOVERING', 'version_database_taxonomic_assignment')))
        input_file_to_write.write("Taxonomic level checked [domain, phylum, class, order, family or genus]:\t{0}\n".format(self.config.get('RECOVERING', 'taxonomic_level')))
        if self.organism == "fungi":
            input_file_to_write.write("Confidence estimates threshold [0-100]:\t{0}\n".format(self.config.get('RECOVERING', 'confidence_threshold_fungi')))
        else:
            input_file_to_write.write("Confidence estimates threshold [0-100]:\t{0}\n".format(self.config.get('RECOVERING', 'confidence_threshold')))
        input_file_to_write.write("Keep Taxonomy file for deleted reads [yes-no]:\t{0}\n".format(self.config.get('RECOVERING', 'keep_taxonomy_file_deleted_reads')))
        input_file_to_write.write("Number of cores or processors used for each sample to do the BLAST analysis [1-32]:\t{0}\n".format(self.config.get('RECOVERING', 'number_cores_processors_blast')))
        input_file_to_write.write("//\n")
        input_file_to_write.write("###RANDOM_CLEANED###\n")
        input_file_to_write.write("Step to do [yes-no]:\t{0}\n".format(self.config.get('RANDOM_CLEANED', 'step_to_do')))
        input_file_to_write.write("Number of reads in the subset:\t{0}\n".format(self.config.get('RANDOM_CLEANED', 'number_reads_subset')))
        input_file_to_write.write("//\n")
        input_file_to_write.write("###TAXONOMY###\n")
        input_file_to_write.write("Step to do [yes-no]:\t{0}\n".format(self.config.get('TAXONOMY', 'step_to_do')))
#        input_file_to_write.write("Organism studied [bacteria, or fungi]:\t{0}\n".format(self.config.get('TAXONOMY', 'organism_studied')))
        input_file_to_write.write("Organism studied [bacteria, or fungi]:\t{0}\n".format(self.organism))
        input_file_to_write.write("Database used for the taxonomic assignment [RDP (bacteria only), or Silva (bacteria and fungi)]:\t{0}\n".format(self.config.get('TAXONOMY', 'database_taxonomic_assignment')))
        input_file_to_write.write("Database chosen Silva version for the taxonomic assignment of bacteria (R114 or R132, by default R132)]:\t{0}\n".format(self.config.get('TAXONOMY', 'version_database_taxonomic_assignment')))
        input_file_to_write.write("Confidence estimates threshold [0-100]:\t{0}\n".format(self.config.get('TAXONOMY', 'confidence_threshold')))
        input_file_to_write.write("Number of cores or processors used for each sample to do the BLAST analysis [1-32]:\t{0}\n".format(self.config.get('TAXONOMY', 'number_cores_processors_blast')))
        input_file_to_write.write("//\n")
        input_file_to_write.write("###CLEAN_INFERNAL_ALIGNMENT###\n")
        input_file_to_write.write("Step to do [yes-no]:\t{0}\n".format(self.config.get('CLEAN_INFERNAL_ALIGNMENT', 'step_to_do')))
        input_file_to_write.write("Covariance model used [bacteria, archaea, algae or fungi]:\t{0}\n".format(self.organism))
        input_file_to_write.write("//\n")
        input_file_to_write.write("###CLEAN_CLUSTERING###\n")
        input_file_to_write.write("Step to do [yes-no]:\t{0}\n".format(self.config.get('CLEAN_CLUSTERING', 'step_to_do')))
        if sequencing_pf == "roche":
            input_file_to_write.write("Ignoring homopolymers differences [yes-no]:\t{0}\n".format(self.config.get('CLEAN_CLUSTERING', 'ignoring_homopolymers_differences_roche')))
        else:
            input_file_to_write.write("Ignoring homopolymers differences [yes-no]:\t{0}\n".format(self.config.get('CLEAN_CLUSTERING', 'ignoring_homopolymers_differences')))
        input_file_to_write.write("Ignoring distances at the beginning of sequences [yes-no]:\t{0}\n".format(self.config.get('CLEAN_CLUSTERING', 'ignoring_distances_beginning_seq')))
        input_file_to_write.write("Maximum percentage of dissimilarity for clustering (%):\t{0}\n".format(self.config.get('CLEAN_CLUSTERING', 'maximum_percentage_dissimilarity_clustering')))
        input_file_to_write.write("Step size for each cluster (%):\t{0}\n".format(self.config.get('CLEAN_CLUSTERING', 'size_each_cluster')))
        input_file_to_write.write("Rank-abundance curves computation [yes-no]:\t{0}\n".format(self.config.get('CLEAN_CLUSTERING', 'rank_abundance_curves')))
        input_file_to_write.write("//\n")
        input_file_to_write.write("###COMPUTATION###\n")
        input_file_to_write.write("Step to do [yes-no]:\t{0}\n".format(self.config.get('COMPUTATION', 'step_to_do')))
        input_file_to_write.write("Chosen clustering step for analysis [Raw, Clean, Both]:\t{0}\n".format(self.config.get('COMPUTATION', 'clustering_step_analysis')))
        input_file_to_write.write("Chosen percentage of dissimilarity for clustering (%):\t{0}\n".format(self.config.get('COMPUTATION', 'percentage_dissimilarity_clustering')))
        input_file_to_write.write("Determination of rarefaction curve(s) [yes-no]:\t{0}\n".format(self.config.get('COMPUTATION', 'rarefaction_curves')))
        input_file_to_write.write("Determination of rank-abundance curve(s) [yes-no]:\t{0}\n".format(self.config.get('COMPUTATION', 'rank_abundance_curves')))
        input_file_to_write.write("Computation of the full bias corrected Chao1 richness estimator [yes-no]:\t{0}\n".format(self.config.get('COMPUTATION', 'chao1_richness')))
        input_file_to_write.write("Computation of the ACE richness estimator [yes-no]:\t{0}\n".format(self.config.get('COMPUTATION', 'ace_richness')))
        input_file_to_write.write("Computation of bootstrap estimate, and shannon and simpson indexes [yes-no]:\t{0}\n".format(self.config.get('COMPUTATION', 'bootstrap_shannon_simpson')))
        input_file_to_write.write("//\n")
        input_file_to_write.write("###GLOBAL_ANALYSIS###\n")
        input_file_to_write.write("Step to do [yes-no]:\t{0}\n".format(self.config.get('GLOBAL_ANALYSIS', 'step_to_do')))
        input_file_to_write.write("Chosen clustering step for analysis [Raw, Clean, Both]:\t{0}\n".format(self.config.get('GLOBAL_ANALYSIS', 'clustering_step_analysis')))
        input_file_to_write.write("Chosen percentage of dissimilarity for the treated clustering (%):\t{0}\n".format(self.config.get('GLOBAL_ANALYSIS', 'percentage_dissimilarity_clustering')))
        input_file_to_write.write("Defining the taxonomic assignment of all OTUs in the global matrix [yes-no]:\t{0}\n".format(self.config.get('GLOBAL_ANALYSIS', 'taxonomic_assignment_all_otus_global_matrix')))
        input_file_to_write.write("Confidence estimates (RDP) or similarity percentage (SILVA) threshold [0-100]:\t{0}\n".format(self.config.get('GLOBAL_ANALYSIS', 'confidence_estimates_similarity_percentage_threshold')))
        input_file_to_write.write("Realization of a phylogenetic tree and an ID mapping file for UNIFRAC analysis (time-consuming step !!) [yes-no]:\t{0}\n".format(self.config.get('GLOBAL_ANALYSIS', 'phylogenetic_tree_unifrac_analysis')))
        input_file_to_write.write("Selection of the most abundant read to represent each OTU for UNIFRAC analysis (tree and mapping file) [yes-no]:\t{0}\n".format(self.config.get('GLOBAL_ANALYSIS', 'selection_most_abundant_read_represent_each_OTU_unifrac_analysis')))
        input_file_to_write.write("//\n")
        input_file_to_write.write("###RECLUSTOR###\n")
        input_file_to_write.write("Step to do [yes-no]:\t{0}\n".format(self.config.get('RECLUSTOR', 'step_to_do')))
        input_file_to_write.write("Do you want to define a new database to realize the clustering step [yes-no]:\t{0}\n".format(self.config.get('RECLUSTOR', 'define_new_db_clustering')))
        input_file_to_write.write("Do you want to use an existing database to realize the clustering step [yes-no]:\t{0}\n".format(self.config.get('RECLUSTOR', 'use_exist_db_clustering')))
        input_file_to_write.write("Do you want to enrich the used database with treated read [yes-no]:\t{0}\n".format(self.config.get('RECLUSTOR', 'enrich_db_clustering')))
        input_file_to_write.write("Database name (short name without spaces or special characters):\t{0}\n".format(self.config.get('RECLUSTOR', 'database_name_clustering')))
        input_file_to_write.write("Covariance model used [bacteria, archaea, algae or fungi]:\t{0}\n".format(self.organism))
        input_file_to_write.write("Chosen percentage of dissimilarity for the clustering (default 5%) [1-100]:\t{0}\n".format(self.config.get('RECLUSTOR', 'percentage_dissimilarity_clustering')))
        if sequencing_pf == "roche":
            input_file_to_write.write("Ignoring homopolymers differences during the clustering [yes-no]:\t{0}\n".format(self.config.get('RECLUSTOR', 'ignoring_homopolymers_differences_roche')))
        else:
            input_file_to_write.write("Ignoring homopolymers differences during the clustering [yes-no]:\t{0}\n".format(self.config.get('RECLUSTOR', 'ignoring_homopolymers_differences')))
        input_file_to_write.write("Ignoring differences at the beginning of sequences [yes-no]:\t{0}\n".format(self.config.get('RECLUSTOR', 'ignoring_differences_beginning_seq')))
        input_file_to_write.write("//\n")
        input_file_to_write.write("###UNIFRAC_ANALYSIS###\n")
        input_file_to_write.write("Step to do [yes-no]:\t{0}\n".format(self.config.get('UNIFRAC_ANALYSIS', 'step_to_do')))
        input_file_to_write.write("Calculate a UniFrac Distance Matrix and apply PCoA and UPGMA [yes-no]:\t{0}\n".format(self.config.get('UNIFRAC_ANALYSIS', 'uniFrac_distance_matrix_pcoa_upgma')))
        input_file_to_write.write("Compute the Phylogenetic Diversity (PD) of all samples [yes-no]:\t{0}\n".format(self.config.get('UNIFRAC_ANALYSIS', 'phylogenetic_diversity')))
        # input_file_to_write.write("Perform a single UniFrac significance test on the whole tree [yes-no]:\t{0}\n".format(self.config.get('UNIFRAC_ANALYSIS', 'single_unifrac_significance_test')))
        # input_file_to_write.write("Perform pairwise tests of whether samples are significantly different with UniFrac [yes-no]:\t{0}\n".format(self.config.get('UNIFRAC_ANALYSIS', 'pairwise_tests_unifrac')))
        # input_file_to_write.write("Perform tests for each sample individually to define if they are significantly different with UniFrac [yes-no]:\t{0}\n".format(self.config.get('UNIFRAC_ANALYSIS', 'test_significantly_different_unifrac')))
        # input_file_to_write.write("Perform a single P-test significance test on the whole tree [yes-no]:\t{0}\n".format(self.config.get('UNIFRAC_ANALYSIS', 'p_test_whole_tree')))
        # input_file_to_write.write("Perform pairwise tests of whether samples are significantly different with P-test [yes-no]:\t{0}\n".format(self.config.get('UNIFRAC_ANALYSIS', 'pairwise_tests_samples')))
        # input_file_to_write.write("Number of permutations [10-10000]:\t{0}\n".format(self.config.get('UNIFRAC_ANALYSIS', 'number_of_permutations')))
        input_file_to_write.write("Use abundance weigths (select whether the abundance of reads will be used or not for UniFrac tests) [yes-no]:\t{0}\n".format(self.config.get('UNIFRAC_ANALYSIS', 'abundance_weigths_unifrac')))
        # input_file_to_write.write("Use Normalized abundance weights (for UniFrac tests) [yes-no]:\t{0}\n".format(self.config.get('UNIFRAC_ANALYSIS', 'normalized_abundance_weights_unifrac')))
        input_file_to_write.write("//\n")
        input_file_to_write.close()
        print "\tThe '{0}' file has been successfully created.\n\t/!\ Please verify the resulting file '{0}' to see correct filenames and steps to do.".format(self.analysis_dir+self.input_file)

    def check_preprocess_mids(self):
        '''
            check if FLASH IS DONE OR NOT AND FILENAME FLASH_SAMPLENAME
        '''
        pass

    def create_figs_prinseq(self):
        '''
            Generate fig for PrinSeq.
        '''
        files_printseq = self.list_files_from_dir(self.analysis_dir+self.summary_dir,"log","PrinSeq")#PRINSEQ
        if files_printseq:
            print u" \u2713 PRINSEQ IS COMPUTED.".encode('utf8')
            list_sample_printseq = []
            prinseq_website_file = self.merge_path_filename_format(self.website_dir+self.website_dir_summary,"prinseq_data","js")
            prinseq_website_to_write = open(prinseq_website_file,"w+")
            for fp in files_printseq:
                sample_printseq = os.path.basename(fp).split(os.extsep,1)[0].lower().replace("-","_")
                # print sample_printseq
                list_sample_printseq.append(sample_printseq)
                with open(fp) as f:
                    content_prinseq = f.readlines()
                    command_line_prinseq = ""
                    input_data_prinseq = ""
                    list_file1 = []
                    list_file2 = []
                    list_other = []
                    filter_word = ["file","1","2","(",")","(file","1)","2)"]
                    for cp in content_prinseq:
                        cp = cp.rstrip("\n").replace('"',"").split(": ")
                        if len(cp) == 2:
                            cp_join = " ".join(cp)
                            text,value = cp
                            text_filter = [t for t in text.split()[3:] if t not in " ".join(filter_word)]
                            text_filter = ["{})".format(tf) if "(" in tf else tf for tf in text_filter]
                            if "Executing PRINSEQ with command" in cp_join:
                                command_line_prinseq = cp
                            elif "Parsing and processing input data" in cp_join:
                                input_data_prinseq = [cp_c.replace(" ","") for cp_c in value.split(":")[-1].split(" and ")]
                                input_data_prinseq = [os.path.basename(idp).split(os.extsep,1)[0].lower() for idp in input_data_prinseq]
                            #FILE 1
                            elif "file 1" in cp_join:
                                list_file1.append([text_filter,value])
                            #FILE 2
                            elif "file 2" in cp_join:
                                list_file2.append([text_filter,value])
                            else:
                                list_other.append(cp)
                    prinseq_file_size = self.get_file_size(fp)
                    prinseq_website_to_write.write("var table_{0} = {{\n\t'path_file': '{1}',\n\t'size_file': '{2}',\n\t'sample_name':'{0}',\n".format(sample_printseq,fp,prinseq_file_size))
                    # prinseq_website_to_write.write("var table_{0} = {{\n".format(sample_printseq))
                    # prinseq_website_to_write.write("\t'sample_name' : '{0}',\n".format(sample_printseq))
                    prinseq_website_to_write.write("\t'file': [\n\t{\n")
                    # prinseq_website_to_write.write("\t\t'Filename': '{0}',\n".format(input_data_prinseq[0]))
                    prinseq_website_to_write.write("\t\t'Filename': 'Read1',\n")
                    for lf1 in list_file1:
                        prinseq_website_to_write.write("\t\t'{0}': '{1}',\n".format(" ".join(lf1[0]),lf1[1]))
                    prinseq_website_to_write.write("\t},\n\t{\n")
                    # prinseq_website_to_write.write("\t\t'Filename': '{0}',\n".format(input_data_prinseq[1]))
                    prinseq_website_to_write.write("\t\t'Filename': 'Read2',\n")
                    for lf2 in list_file2:
                        prinseq_website_to_write.write("\t\t'{0}': '{1}',\n".format(" ".join(lf2[0]),lf2[1]))
                    prinseq_website_to_write.write("\t}\n\t]\n}\n")
            prinseq_website_to_write.write("var array_samples_name_prinseq = {0};\n".format(list_sample_printseq))
            #render for the first sample
            sample1 = list_sample_printseq.pop(0)
            prinseq_website_to_write.write("var template_prinseq_sample1 = $('#message-template-prinseq-sample1').html();\n")
            prinseq_website_to_write.write("var html_{0} = Mustache.render(template_prinseq_sample1, table_{0});\n".format(sample1))
            prinseq_website_to_write.write("$('.sample1-prinseq').append(html_{0});\n".format(sample1))
            #render for the others list_samples
            prinseq_website_to_write.write("var template_prinseq_others_samples = $('#message-template-prinseq-others').html();\n")
            for lsp in list_sample_printseq:
                prinseq_website_to_write.write("var html_{0} = Mustache.render(template_prinseq_others_samples, table_{0});\n".format(lsp))
                prinseq_website_to_write.write("$('.samples-prinseq').append(html_{0});\n".format(lsp))
            prinseq_website_to_write.close()
        else:
            print u" \u2717 PRINTSEQ IS NOT COMPUTED.".encode('utf8')
            prinseq_website_file = self.merge_path_filename_format(self.website_dir+self.website_dir_summary,"prinseq_data","js")
            prinseq_website_to_write = open(prinseq_website_file,"w+")
            prinseq_website_to_write.write("var array_samples_name_prinseq = [];\n")
            prinseq_website_to_write.close()

    def create_figs_flash(self):
        '''
            Generate fig for FLASH.
        '''
        files_flash = self.list_files_from_dir(self.analysis_dir+self.summary_dir,"log","FLASH")
        if files_flash:
            print u" \u2713 FLASH IS COMPUTED.".encode('utf8')
            list_sample_flash = []
            list_data_flash = []
            flash_website_file = self.merge_path_filename_format(self.website_dir+self.website_dir_summary,"flash_data","js")
            flash_website_to_write = open(flash_website_file,"w+")
            for ff in files_flash:
                # ARRGH : sample_flash = os.path.basename(ff).split(os.extsep,1)[0].lower()
                sample_flash = os.path.basename(ff).split(os.extsep,1)[0]
                tmp_list_data_flash = []
                list_sample_flash.append(sample_flash)
                # flash_website_to_write.write("\t'sample_name' : '{0}',\n".format(sample_flash))
                # sample_name = "\t'sample_name' : '{0}',\n".format(sample_flash)
                flash_file_size = self.get_file_size(ff)
                sample_name = "\t'path_file': '{0}',\n\t'size_file': '{1}',\n\t'sample_name':'{2}',\n".format(ff,flash_file_size,sample_flash)
                tmp_list_data_flash.append(sample_name)
                with open(ff) as f:
                    content_flash = f.readlines()
                    for cf in content_flash:
                        cf_filter = [cff.strip() for cff in cf.replace("[FLASH] ","").rstrip("\n").split(":")]
                        if "Total pairs" in cf:
                            # flash_website_to_write.write("\t\t'{0}': '{1}',\n".format(cf_filter[0],cf_filter[1]))
                            total_pairs = "\t'{0}': '{1}',\n".format(cf_filter[0],cf_filter[1])
                            tmp_list_data_flash.append(total_pairs)
                        if "Combined pair" in cf:
                            # flash_website_to_write.write("\t\t'{0}': '{1}',\n".format(cf_filter[0],cf_filter[1]))
                            combined_pair = "\t'{0}': '{1}',\n".format(cf_filter[0],cf_filter[1])
                            tmp_list_data_flash.append(combined_pair)
                        if "Uncombined pairs" in cf:
                            # flash_website_to_write.write("\t\t'{0}': '{1}',\n".format(cf_filter[0],cf_filter[1]))
                            uncombined_pair = "\t'{0}': '{1}',\n".format(cf_filter[0],cf_filter[1])
                            tmp_list_data_flash.append(uncombined_pair)
                        if "Percent combined" in cf:
                            # flash_website_to_write.write("\t\t'{0}': '{1}',\n".format(cf_filter[0],cf_filter[1]))
                            percent_combined = "\t'{0}': '{1}',\n".format(cf_filter[0],cf_filter[1])
                            tmp_list_data_flash.append(percent_combined)
                list_data_flash.append(tmp_list_data_flash)
            flash_website_to_write.write("var table_flash = {\n")
            flash_website_to_write.write("'content': [\n\t{\n")
            for ldf_e,ldf in enumerate(list_data_flash):
                for lfd_i in ldf:
                    flash_website_to_write.write(lfd_i)
                if ldf_e < len(list_data_flash)-1:
                    flash_website_to_write.write("\t},\n\t{\n")
                else:
                    flash_website_to_write.write("\t}\n\t]\n}\n")
            # flash_website_to_write.write("\t],\n")

            dereplication_file = self.analysis_dir+self.summary_dir+"Dereplication_summary_results.txt"
            lines_deriplication = open(dereplication_file).readlines()
            header_lines_deriplication = lines_deriplication.pop(0).rstrip("\n").split("\t")
            flash_website_to_write.write("var table_dereplication = {\n")
            dereplication_file_size = self.get_file_size(dereplication_file)
            flash_website_to_write.write("\t'path_file': '{0}',\n\t'size_file': '{1}',\n".format(dereplication_file,dereplication_file_size))
            flash_website_to_write.write("\t'dereplication': [\n\t{\n")
            for ld_e,ld in enumerate(lines_deriplication):
                ld = ld.rstrip("\n").split("\t")
                ld[0] = self.filter_samplenames(ld[0])
                for ld_ee, ld_i in enumerate(ld):
                    flash_website_to_write.write("\t'{0}': '{1}',\n".format(header_lines_deriplication[ld_ee],ld_i))
                if ld_e < len(lines_deriplication)-1:
                    flash_website_to_write.write("\t},\n\t{\n")
                else:
                    flash_website_to_write.write("\t}\n\t]\n}\n")
            #render for the list_samples
            flash_website_to_write.write("var template_flash = $('#message-template-flash').html();\n")
            flash_website_to_write.write("var html_flash = Mustache.render(template_flash, table_flash);\n")
            flash_website_to_write.write("$('.samples-flash').append(html_flash);\n")
            flash_website_to_write.write("var template_dereplication = $('#message-template-dereplication').html();\n")
            flash_website_to_write.write("var html_dereplication = Mustache.render(template_dereplication, table_dereplication);\n")
            flash_website_to_write.write("$('.dereplication').append(html_dereplication);\n")
            flash_website_to_write.close()
        else:
            print u" \u2717 FLASH IS NOT COMPUTED.".encode('utf8')
            flash_website_file = self.merge_path_filename_format(self.website_dir+self.website_dir_summary,"flash_data","js")
            flash_website_to_write = open(flash_website_file,"w+")
            flash_website_to_write.write("var table_flash = {};\n")
            flash_website_to_write.close()

    def create_figs_eval_qual_bars(self,cpt_sample_current,total_samples,sample_eval,content_eval_qual_lines):
        '''
            Generate fig for eval qual.
        '''
        names_eval_qual = ["Very good","Good","Reasonable","Poor"]
        nb_samples = len(total_samples)
        content_eval_qual_lines = [ceql.rstrip("\n").split("\t") for ceql in content_eval_qual_lines]
        self.categories_eval_qual = [row[0] for row in content_eval_qual_lines]
        very_good_quality = [int(row[6]) for row in content_eval_qual_lines]
        good_quality = [int(row[7]) for row in content_eval_qual_lines]
        reasonable_quality = [int(row[8]) for row in content_eval_qual_lines]
        very_poor_quality = [int(row[9]) for row in content_eval_qual_lines]
        all_quality = [very_good_quality,good_quality,reasonable_quality,very_poor_quality]
        box_plot_all = []
        self.bar_eval_qual_website_file_to_write.write("\t'{0}': [\n\t{{\n".format(sample_eval))
        self.box_plot_eval_qual_website_file_to_write.write("\t'{0}': [\n\t{{\n".format(sample_eval))
        for ceql in content_eval_qual_lines:
            box_plot_min = float(ceql[2])
            box_plot_q1 = float(ceql[4])
            box_plot_median = float(ceql[1])
            box_plot_q3 = float(ceql[5])
            box_plot_max = float(ceql[3])
            box_plot_all.append([box_plot_min,box_plot_q1,box_plot_median,box_plot_q3,box_plot_max])
        self.box_plot_eval_qual_website_file_to_write.write("\t\tname: 'Observations',\n\t\tdata: {0}\n\t\t}}],\n".format(box_plot_all))

        for neq_e,neq in enumerate(names_eval_qual):
            if neq_e < len(names_eval_qual)-1:
                self.bar_eval_qual_website_file_to_write.write("\t\tname: '{0}',\n\t\tdata: {1}\n\t\t}}, {{\n".format(neq,all_quality[neq_e]))
            else:
                self.bar_eval_qual_website_file_to_write.write("\t\tname: '{0}',\n\t\tdata: {1}\n\t\t}}],\n".format(neq,all_quality[neq_e]))
        if cpt_sample_current == nb_samples:
            self.bar_eval_qual_website_file_to_write.write("};\n")

    def create_figs_eval_qual(self):
        '''
            Generate fig for eval qual.
        '''
        files_eval_qual = self.list_files_from_dir(self.analysis_dir+self.summary_dir,"txt","Eval_qual")
        if files_eval_qual:
            print u" \u2713 EVAL_QUAL IS COMPUTED.".encode('utf8')
            list_sample_eval_qual = []
            bar_eval_qual_website_file = self.merge_path_filename_format(self.website_dir+self.website_dir_summary,"bar_eval_qual_data","js")
            self.bar_eval_qual_website_file_to_write = open(bar_eval_qual_website_file,"w+")
            box_plot_eval_qual_website_file = self.merge_path_filename_format(self.website_dir+self.website_dir_summary,"box_plot_eval_qual_data","js")
            self.box_plot_eval_qual_website_file_to_write = open(box_plot_eval_qual_website_file,"w+")
            self.bar_eval_qual_website_file_to_write.write("var dict_bar_eval_qual_data = {\n")
            self.box_plot_eval_qual_website_file_to_write.write("var dict_box_plot_eval_qual_data = {\n")
            eval_qual_website_file = self.merge_path_filename_format(self.website_dir+self.website_dir_summary,"eval_qual_data","js")
            eval_qual_website_to_write = open(eval_qual_website_file,"w+")
            cpt_sample = 0
            color_bar_eval_qual = ["#9fdf9f","#ffff4d","#ffd480","#ff8c66"]
            for feq in files_eval_qual:
                sample = os.path.basename(feq).split(os.extsep,1)[0].lower().replace("-","_")
                eval_qual_file_size = self.get_file_size(feq)
                eval_qual_website_to_write.write("var table_{0} = {{\n\t'path_file': '{1}',\n\t'size_file': '{2}',\n\t'sample_name':'{0}',\n".format(sample,feq,eval_qual_file_size))
                list_sample_eval_qual.append(sample)
                with open(feq) as f:
                    content_eval_qual = f.readlines()
                    header_1,length_threshold = content_eval_qual.pop(0).rstrip('\n').split('\t')
                    header_2,total_number_reads = content_eval_qual.pop(0).rstrip('\n').split('\t')
                    header_3 = content_eval_qual.pop(0).split()
                    cpt_sample = cpt_sample + 1
                    self.create_figs_eval_qual_bars(cpt_sample,files_eval_qual,sample,content_eval_qual)
                    # eval_qual_website_to_write.write("var table_{0} = {{\n".format(sample))
                    # eval_qual_website_to_write.write("\t'sample_name' : '{}',\n".format(sample))
                    eval_qual_website_to_write.write("\t'length_threshold' : '{}',\n".format(" ".join([header_1,length_threshold])))
                    eval_qual_website_to_write.write("\t'total_number_reads' : '{}',\n".format(" ".join([header_2,total_number_reads])))
                    eval_qual_website_to_write.write("\t'value': [\n\t\t\n")
                    for ceq in  content_eval_qual:
                        col_ceq = ceq.rstrip('\n').split('\t')
                        eval_qual_website_to_write.write("\t{\n")
                        for num_subitem,subitem in enumerate(col_ceq):
                            eval_qual_website_to_write.write("\t\t'{0}': '{1}',\n".format(header_3[num_subitem],subitem))
                        eval_qual_website_to_write.write("\t},\n")
                    eval_qual_website_to_write.write("\t]\n}\n")

            self.bar_eval_qual_website_file_to_write.write("var array_samples_eval_qual = {0};\n".format(list_sample_eval_qual))
            self.bar_eval_qual_website_file_to_write.write("var categories_eval_qual = {0};\n".format(self.categories_eval_qual))
            self.bar_eval_qual_website_file_to_write.write("var color_bar_eval_qual = {0};\n".format(color_bar_eval_qual))
            self.bar_eval_qual_website_file_to_write.close()
            self.box_plot_eval_qual_website_file_to_write.write("};\n")
            self.box_plot_eval_qual_website_file_to_write.write("var array_samples_eval_qual = {0};\n".format(list_sample_eval_qual))
            self.box_plot_eval_qual_website_file_to_write.write("var categories_eval_qual = {0};\n".format(self.categories_eval_qual))
            self.box_plot_eval_qual_website_file_to_write.close()
            eval_qual_website_to_write.write("var array_samples_name = {0};\n".format(list_sample_eval_qual))
            #render for the first sample
            sample1 = list_sample_eval_qual.pop(0)
            eval_qual_website_to_write.write("var template = $('#message-template-sample1').html();\n")
            eval_qual_website_to_write.write("var html_{0} = Mustache.render(template, table_{0});\n".format(sample1))
            eval_qual_website_to_write.write("$('.sample1').append(html_{0});\n".format(sample1))
            #render for the others list_samples
            eval_qual_website_to_write.write("var template = $('#message-template-other_samples').html();\n")
            for lseq in list_sample_eval_qual:
                eval_qual_website_to_write.write("var html_{0} = Mustache.render(template, table_{0});\n".format(lseq))
                eval_qual_website_to_write.write("$('.samples').append(html_{0});\n".format(lseq))
            eval_qual_website_to_write.close()
        else:
            print u" \u2717 EVAL_QUAL IS NOT COMPUTED.".encode('utf8')
            eval_qual_website_file = self.merge_path_filename_format(self.website_dir+self.website_dir_summary,"eval_qual_data","js")
            eval_qual_website_to_write = open(eval_qual_website_file,"w+")
            eval_qual_website_to_write.write("var array_samples_name = [];\n")
            eval_qual_website_to_write.close()
            bar_eval_qual_website_file = self.merge_path_filename_format(self.website_dir+self.website_dir_summary,"bar_eval_qual_data","js")
            self.bar_eval_qual_website_file_to_write = open(bar_eval_qual_website_file,"w+")
            self.bar_eval_qual_website_file_to_write.write("var array_samples_eval_qual = {};")
            box_plot_eval_qual_website_file = self.merge_path_filename_format(self.website_dir+self.website_dir_summary,"box_plot_eval_qual_data","js")
            self.box_plot_eval_qual_website_file_to_write = open(box_plot_eval_qual_website_file,"w+")
            self.box_plot_eval_qual_website_file_to_write.write("var array_samples_eval_qual = {};")

    def create_figs_preprocess_mids(self):
        '''
            Generate fig for Preprocess_MIDs.
        '''
        files_preprocess_mids = self.list_files_from_dir(self.analysis_dir+self.summary_dir,"txt","Preprocess_MIDs")
        if files_preprocess_mids:
            print u" \u2713 PREPROCESS_MIDS IS COMPUTED.".encode('utf8')
            list_sample_preprocess_mids = []
#            header_preprocess_mids = ['Chosen MID', 'MID Sequence', 'Sample name', 'Number of reads']
            header_preprocess_mids = ['Chosen MID Forward', 'MID Sequence Forward', 'Chosen MID Reverse', 'MID Sequence Reverse','Sample name', 'Number of reads']
            preprocess_mids_website_file = self.merge_path_filename_format(self.website_dir+self.website_dir_summary,"preprocess_mids_data","js")
            preprocess_mids_website_to_write = open(preprocess_mids_website_file,"w+")
            for fpm in files_preprocess_mids:
                sample = os.path.basename(fpm).split(os.extsep,1)[0].lower().replace("-","_")
                preprocess_mids_file_size = self.get_file_size(fpm)
                list_sample_preprocess_mids.append(sample)
                preprocess_mids_website_to_write.write("var table_{0} = {{\n\t'path_file': '{1}',\n\t'size_file': '{2}',\n\t'sample_name':'{0}',\n".format(sample,fpm,preprocess_mids_file_size))
                with open(fpm) as f:
                    content_preprocess_mids = f.readlines()
                    preprocess_mids_website_to_write.write("\t'value': [\n\t{\n")
                    tmp_content_preprocess_mids = []
                    for cpm_e,cpm in enumerate(content_preprocess_mids):
                        if ":" in cpm:
                            tmp_content_preprocess_mids.append(cpm.replace("\t","").rstrip("\n").split(":"))
                        else:
                            cpm = [cpm_v.rstrip("\n") for cpm_v in cpm.split("\t")]
                            if cpm != header_preprocess_mids and cpm != [""]:
                                cpm = [cpm_f.strip() for cpm_f in cpm]
                                for cpm_ee,cpm_c in enumerate(cpm):
                                    preprocess_mids_website_to_write.write("\t'{0}':'{1}',\n".format(header_preprocess_mids[cpm_ee],cpm_c))
                                if cpm_e <len(content_preprocess_mids)-1:
                                    preprocess_mids_website_to_write.write("\t},{\n")
                                else:
                                    preprocess_mids_website_to_write.write("\t}\n")
                    preprocess_mids_website_to_write.write("\t],\n")
                    for tcpm in tmp_content_preprocess_mids:
                        preprocess_mids_website_to_write.write("\t'{0}':'{1}',\n".format(*tcpm))
                    preprocess_mids_website_to_write.write("}\n")
            preprocess_mids_website_to_write.write("var array_samples_name_preprocess_mids = {0};\n".format(list_sample_preprocess_mids))
            #render for the first sample
            sample1 = list_sample_preprocess_mids.pop(0)
            preprocess_mids_website_to_write.write("var template_preprocess_mids_sample1 = $('#message-template-preprocess_mids-sample1').html();\n")
            preprocess_mids_website_to_write.write("var html_{0} = Mustache.render(template_preprocess_mids_sample1, table_{0});\n".format(sample1))
            preprocess_mids_website_to_write.write("$('.sample1-preprocess_mids').append(html_{0});\n".format(sample1))
            #render for the others list_samples
            preprocess_mids_website_to_write.write("var template_preprocess_mids_others_samples = $('#message-template-preprocess_mids-other_samples').html();\n")
            for lspm in list_sample_preprocess_mids:
                preprocess_mids_website_to_write.write("var html_{0} = Mustache.render(template_preprocess_mids_others_samples, table_{0});\n".format(lspm))
                preprocess_mids_website_to_write.write("$('.samples-preprocess_mids').append(html_{0});\n".format(lspm))
            preprocess_mids_website_to_write.close()
        else:
            print u" \u2717 PREPROCESS_MIDS IS NOT COMPUTED.".encode('utf8')
            preprocess_mids_website_file = self.merge_path_filename_format(self.website_dir+self.website_dir_summary,"preprocess_mids_data","js")
            preprocess_mids_website_to_write = open(preprocess_mids_website_file,"w+")
            preprocess_mids_website_to_write.write("var array_samples_name_preprocess_mids = {};\n")
            preprocess_mids_website_to_write.close()

    def create_figs_hunting(self):
        '''
            Generate fig for Hunting_ss_summary_results.
        '''
        if os.path.isfile(self.analysis_dir+self.hunting_file) :
            print u" \u2713 HUNTING IS COMPUTED.".encode('utf8')
            hunting_website_file = self.merge_path_filename_format(self.website_dir+self.website_dir_summary,"hunting_data","js")
            hunting_website_to_write = open(hunting_website_file,"w+")
            hunting_file_size = self.get_file_size(self.analysis_dir+self.hunting_file)
            hunting_website_to_write.write("var table_hunting = {{\n\t'path_file': '{0}',\n\t'size_file': '{1}',\n\t'sample': [\n\t{{".format(self.hunting_file,hunting_file_size))
            with open(self.analysis_dir+self.hunting_file) as f:
                content_hunting_file = f.readlines()
                header_hunting_file = content_hunting_file.pop(0).rstrip("\n").split("\t")
                for chf_e, chf in enumerate(content_hunting_file):
                    chf = chf.rstrip("\n").split("\t")
                    chf[0] = self.filter_samplenames(chf[0])
                    for chf_ee,chf_c in enumerate(chf):
                        hunting_website_to_write.write("\t'{0}': '{1}',\n".format(header_hunting_file[chf_ee],chf_c))
                    if chf_e < len(content_hunting_file)-1:
                        hunting_website_to_write.write("\t},\n\t{\n")
                    else:
                        hunting_website_to_write.write("\t}\n\t]\n}\n")
            #render for the list_samples
            hunting_website_to_write.write("var template_hunting = $('#message-template-hunting').html();\n")
            hunting_website_to_write.write("var html_hunting = Mustache.render(template_hunting, table_hunting);\n")
            hunting_website_to_write.write("$('.samples-hunting').append(html_hunting);\n")
            hunting_website_to_write.close()
        else:
            print u" \u2717 HUNTING IS NOT COMPUTED.".encode('utf8')
            hunting_website_file = self.merge_path_filename_format(self.website_dir+self.website_dir_summary,"hunting_data","js")
            hunting_website_to_write = open(hunting_website_file,"w+")
            hunting_website_to_write.write("var table_hunting = {};\n")
            hunting_website_to_write.close()

    def create_figs_recovering(self):
        '''
            Generate fig for recovering.
        '''
        if os.path.isfile(self.analysis_dir+self.recovering_file) :
            print u" \u2713 RECOVERING IS COMPUTED.".encode('utf8')
            recovering_website_file = self.merge_path_filename_format(self.website_dir+self.website_dir_summary,"recovering_data","js")
            recovering_website_to_write = open(recovering_website_file,"w+")
            recovering_file_size = self.get_file_size(self.analysis_dir+self.recovering_file)
            recovering_website_to_write.write("var table_recovering = {{\n\t'path_file': '{0}',\n\t'size_file': '{1}',\n".format(self.recovering_file,recovering_file_size))
            with open(self.analysis_dir+self.recovering_file) as f:
                content_recovering_file = [lines_crf.strip() for lines_crf in f.readlines() if lines_crf.strip()]
                mo_study_recovering_file = content_recovering_file.pop(0).replace("\t","").split(":")
                db_used_recovering_file = content_recovering_file.pop(0).replace("\t","").split(":")
                taxo_level_chose_recovering_file = content_recovering_file.pop(0).replace("\t","").split(":")
                confidence_threshold_recovering_file = content_recovering_file.pop(0).replace("\t","").split(":")
                header_recovering_file = content_recovering_file.pop(0).split("\t")
                recovering_website_to_write.write("\t'{0}': '{1}',\n".format(*mo_study_recovering_file))
                recovering_website_to_write.write("\t'{0}': '{1}',\n".format(*db_used_recovering_file))
                recovering_website_to_write.write("\t'{0}': '{1}',\n".format(*taxo_level_chose_recovering_file))
                recovering_website_to_write.write("\t'{0}': '{1}',\n\t'sample': [\n\t{{\n".format(*confidence_threshold_recovering_file))
                # ['File name', 'Total Number of Reads', 'Reads deleted by the HUNTING step', 'Reads recovered', 'Total of high-quality reads']
                for crf_e,crf in enumerate(content_recovering_file):
                    crf = crf.split("\t")
                    crf[0] = self.filter_samplenames(crf[0])
                    for crf_ee,crf_c in enumerate(crf):
                        recovering_website_to_write.write("\t\t'{0}': '{1}',\n".format(header_recovering_file[crf_ee],crf_c))
                    if crf_e < len(content_recovering_file)-1:
                        recovering_website_to_write.write("\t},\n\t{\n")
                    else:
                        recovering_website_to_write.write("\t}\n\t]\n}\n")
            #render for the list_samples
            recovering_website_to_write.write("var template_recovering = $('#message-template-recovering').html();\n")
            recovering_website_to_write.write("var html_recovering = Mustache.render(template_recovering, table_recovering);\n")
            recovering_website_to_write.write("$('.samples-recovering').append(html_recovering);\n")
            recovering_website_to_write.close()
        else:
            print u" \u2717 RECOVERING IS NOT COMPUTED.".encode('utf8')
            recovering_website_file = self.merge_path_filename_format(self.website_dir+self.website_dir_summary,"recovering_data","js")
            recovering_website_to_write = open(recovering_website_file,"w+")
            recovering_website_to_write.write("var table_recovering = {};\n")
            recovering_website_to_write.close()

    def create_figs_reclustor(self):
        '''
            Generate fig for reclustor.
        '''
        if os.path.isfile(self.analysis_dir+self.reclustor_file) :
            print u" \u2713 ReClustOR IS COMPUTED.".encode('utf8')
            reclustor_website_file = self.merge_path_filename_format(self.website_dir+self.website_dir_summary,"reclustor_data","js")
            reclustor_website_to_write = open(reclustor_website_file,"w+")
            reclustor_file_size = self.get_file_size(self.analysis_dir+self.reclustor_file)
            reclustor_website_to_write.write("var table_reclustor = {{\n\t'path_file': '{0}',\n\t'size_file': '{1}',\n".format(self.reclustor_file,reclustor_file_size))
            with open(self.analysis_dir+self.reclustor_file) as f:
                content_reclustor_file = [lines_cfr.strip() for lines_cfr in f.readlines() if lines_cfr.strip()]
                confidence_threshold_reclustor_file = content_reclustor_file.pop(0).replace("\t","").split(":")
                db_used_reclustor_file = content_reclustor_file.pop(0).replace("\t","").split(":")
                header_reclustor_file = content_reclustor_file.pop(0).split("\t")
                reclustor_website_to_write.write("\t'{0}': '{1}',\n".format(*confidence_threshold_reclustor_file))
                reclustor_website_to_write.write("\t'{0}': '{1}',\n\t'sample': [\n\t{{\n".format(*db_used_reclustor_file))
                for cpf_e,cpf in enumerate(content_reclustor_file):
                    cpf = cpf.split("\t")
                    cpf[0] = self.filter_samplenames(cpf[0])
                    for cpf_ee,cpf_c in enumerate(cpf):
                        reclustor_website_to_write.write("\t\t'{0}': '{1}',\n".format(header_reclustor_file[cpf_ee],cpf_c))
                    if cpf_e < len(content_reclustor_file)-1:
                        reclustor_website_to_write.write("\t},\n\t{\n")
                    else:
                        reclustor_website_to_write.write("\t}\n\t]\n}\n")
            #render for the list_samples
            reclustor_website_to_write.write("var template_reclustor = $('#message-template-reclustor').html();\n")
            reclustor_website_to_write.write("var html_reclustor = Mustache.render(template_reclustor, table_reclustor);\n")
            reclustor_website_to_write.write("$('.samples-reclustor').append(html_reclustor);\n")
            reclustor_website_to_write.close()
        else:
            print u" \u2717 ReClustOR IS NOT COMPUTED.".encode('utf8')
            reclustor_website_file = self.merge_path_filename_format(self.website_dir+self.website_dir_summary,"reclustor_data","js")
            reclustor_website_to_write = open(reclustor_website_file,"w+")
            reclustor_website_to_write.write("var table_reclustor = {};\n")
            reclustor_website_to_write.close()

    def read_releases_details_file(self):
        '''
            Read Releases_details.txt file to create js to integrate to the webpage.
        '''
        if os.path.isfile(self.biocom_pipe_dir+self.releases_details_file) :
            print u" \u2713 RELEASES_DETAILS PARAMETERS IS COMPUTED.".encode('utf8')
            lines_releases_details_file = open(self.biocom_pipe_dir+self.releases_details_file).readlines()
            lines_releases_details_file = [lrdf.rstrip("\n") for lrdf in lines_releases_details_file]
            lines_releases_details_file = [lrdf_p.replace("'",'"') for lrdf_p in lines_releases_details_file if lrdf_p]
            dico_releases = {}
            new_dico_releases = {}
            list_releases_render = []

            for num_lrdfp_l,lrdfp_l in enumerate(lines_releases_details_file):
                lrdfp_l
                if lrdfp_l.startswith('- BIOCOM-PIPE') or lrdfp_l.startswith('- GnS-PIPE'):
                    lrdfp_l = lrdfp_l.split()
                    pipeline_name = lrdfp_l.pop(1)
                    version_name = "_".join([lrdfp_l_e.lower() for lrdfp_l_e in lrdfp_l if lrdfp_l_e != "-"]+[pipeline_name])
                    dico_releases[version_name] = []
                    tmp_string = []
                else:
                    if re.match("^[1-9]\/", lrdfp_l):
                        current_point = int(lrdfp_l.split("/")[0])
                        lrdfp_l = " ".join(lrdfp_l.split()[1:])
                        if not tmp_string:
                            tmp_string = []
                            tmp_string.append(lrdfp_l)
                        else:
                            dico_releases[version_name].append(tmp_string)
                            tmp_string = []
                            tmp_string.append(lrdfp_l)
                    else:
                        if current_point==1 and len(lines_releases_details_file) -1 !=num_lrdfp_l and lines_releases_details_file[num_lrdfp_l+1].startswith('- '):
                            tmp_string.append(lrdfp_l)
                            dico_releases[version_name].append(tmp_string)
                        else:
                            tmp_string.append(lrdfp_l)
            for dr_k,dr_v in dico_releases.items():
                new_dico_releases[dr_k] = []
                for drv in dr_v:
                    new_dico_releases[dr_k].append("".join(drv))
            #write data
            releases_website_file = self.merge_path_filename_format(self.website_dir+self.website_dir_about,"releases_website_file","js")
            releases_website_to_write = open(releases_website_file,"w+")
            dico_releases_order = sorted(new_dico_releases, key=lambda x: map(int, x.split('_')[1].split('.')),reverse=True)
            for item in dico_releases_order:
                version_name_order = "_".join(item.split("_")[:2]).replace(".","_")
                version_date_order = item.split("_")[-1]
                list_releases_render.append(version_name_order)
                releases_website_to_write.write("var {0} = {{\n".format(version_name_order))
                releases_website_to_write.write("\tversion: '{0}',\n\tname_pipe: '{1}',\n\tversion_wo: '{2}',\n\tdate: '{3}',\n\tcontent: {4},\n}}\n".format("".join(item.split("_")[:2]),item.split("_")[-1],"".join(item.split("_")[:2]).replace(".",""),item.split("_")[2],new_dico_releases[item]))
            # releases_website_to_write.write("var last_releases = '{}';\n".format(list_releases_render[0]))
            releases_website_to_write.write("var template = $('#releases-template').html();\n")
            for vdo in list_releases_render:
                releases_website_to_write.write("var html_{0} = Mustache.render(template, {0});\n".format(vdo))
                releases_website_to_write.write("$('#releases').append(html_{0});\n".format(vdo))

            releases_website_to_write.write("var idArray = [];\n$('.biocom_pipe_version').each(function () {\n\tidArray.push(this.id);\n});\n")
            releases_website_to_write.write("$('#1').prev('div').find('#'+idArray[0]).prepend('<span class=\"tag is-success\">Last release</span>');\n")
            releases_website_to_write.close()
        else:
            print u" \u2717 RELEASES_DETAILS PARAMETERS IS NOT COMPUTED.".encode('utf8')
            releases_website_file = self.merge_path_filename_format(self.website_dir+self.website_dir_about,"releases_website_file","js")
            releases_website_to_write = open(releases_website_file,"w+")
            releases_website_to_write.write("var v_1_18 = {}\n")
            releases_website_to_write.close()

    def read_input_file(self):
        '''
            Read Input.txt file to create js to integrate to the webpage.
        '''
        if os.path.isfile(self.analysis_dir+self.input_file) :
            print u" \u2713 INPUT FILE PARAMETERS IS COMPUTED.".encode('utf8')
            lines_input_file = open(self.analysis_dir+self.input_file).readlines()
            message_parameters_pipe_file = self.merge_path_filename_format(self.website_dir+self.website_dir_parameters_pipe,"message_parameters_pipe","js")
            input_file_website_to_write = open(message_parameters_pipe_file,"w+")
            list_input_file = []
            num_title_intput = 0
            list_render = []
            for lif in lines_input_file:
                lif = lif.rstrip("\n").split(":")
                if "//" not in lif:
                    if lif[0].startswith('###'):
                        title_intput = lif[0].replace("###","").lower()
                        num_title_intput = num_title_intput + 1
                        var_render = "var html_{0} = Mustache.render(template, data_{0});".format(title_intput)
                        var_html = "$('#pane-1').append(html_{0});".format(title_intput)
                        list_input_file.append([title_intput])
                        list_render.append([var_render,var_html])
                    else:
                        list_input_file[num_title_intput-1].append([lif[0],lif[1].replace("\t","")])

            for llif in list_input_file:
                content_input_file = [" = ".join(data_content_input_file) for data_content_input_file in llif[1:]]
                #get info for softwares
                section_input_config = llif[0].upper()
                info_software = []
                info_software_url = ""
                if self.config.has_option(section_input_config, "info_general"):
                    info_software.append(self.config.get(section_input_config,"info_general").replace('"',''))
                    info_software.append(self.config.get(section_input_config,"info_version").replace('"',''))
                    info_software.append(self.config.get(section_input_config,"info_citation").replace('"',''))
                if self.config.has_option(section_input_config,"info_website"):
                    info_software_url = self.config.get(section_input_config,"info_website").replace('"','')
                    input_file_website_to_write.write("var data_{0} = {{\ntitle: '{1}',\ncontent: {2},\n id: 'data_{0}',\n info: {3},\n info_url: '{4}'\n}}\n".format(llif[0],llif[0],content_input_file,info_software[:3],info_software_url))
                else:
                    info_software_url = "false"
                    input_file_website_to_write.write("var data_{0} = {{\ntitle: '{1}',\ncontent: {2},\n id: 'data_{0}',\n info: {3},\n info_url: {4}\n}}\n".format(llif[0],llif[0],content_input_file,info_software[:3],info_software_url))
            input_file_website_to_write.write("var template = $('#message-template').html();\n")
            for lr in list_render:
                input_file_website_to_write.write("{0}\n".format(lr[0]))
                input_file_website_to_write.write("{0}\n".format(lr[1]))
            # input_file_website_to_write.write("$('.button').on('click', function() {\n\talert($('input#message-header-info').val());\n});\n")
            input_file_website_to_write.write("$(function() {\n\t$('[data-popup-open]').on('click', function(e) {\n\tvar targeted_popup_class = jQuery(this).attr('data-popup-open');\n\t$('[data-popup=\"' + targeted_popup_class + '\"]').fadeIn(350);\n\t\te.preventDefault();\n\t});\n\t$('[data-popup-close]').on('click', function(e) {\n\tvar targeted_popup_class = jQuery(this).attr('data-popup-close');\n\t$('[data-popup=\"' + targeted_popup_class + '\"]').fadeOut(350);\n\t\te.preventDefault();\n\t});\n});")
            input_file_website_to_write.close()
        else:
            print u" \u2717 INPUT FILE PARAMETER IS NOT COMPUTED.".encode('utf8')
            message_parameters_pipe_file = self.merge_path_filename_format(self.website_dir+self.website_dir_parameters_pipe,"message_parameters_pipe","js")
            input_file_website_to_write = open(message_parameters_pipe_file,"w+")
            input_file_website_to_write.write("var data_prinseq = {};\n")
            input_file_website_to_write.close()

    def create_figs_taxonomy(self):
        '''
            Generate fig for taxonomy.
        '''
        files_taxonomy = self.list_files_from_dir(self.analysis_dir+self.summary_dir,"txt","Taxonomy_results")
        if files_taxonomy:
            print u" \u2713 TAXONOMY IS COMPUTED.".encode('utf8')
            rank_to_kept = ["phylum","class","order","family","genus"]
            files_taxonomy = [fttk for fttk in files_taxonomy if fttk.split("_")[-1].replace(".txt","") in rank_to_kept]
            dico_count = {}
            dico_taxonomy = {}
            dico_count_seqs_by_samples = {}
            list_samples = []
            len_max_sample_per_graph = 25
            threshold_percent_min = 0.01
            for ft in files_taxonomy:
                confidence_t, tax_rank = os.path.splitext(os.path.basename(ft))[0].replace("Taxonomy_results_","").split("_")
                dico_count[tax_rank] = []
                with open(ft) as f:
                    content_tax = f.readlines()
                    # if content_tax[1].startswith("Taxonomy_intermediate"):
                    #     header_taxonomy_highest = [hth for hth in content_tax.pop(0).rstrip('\n').split('\t') if hth][1:]
                    #     header_taxonomy_intermediate = [hth for hth in content_tax.pop(0).rstrip('\n').split('\t') if hth][1:]
                    #     header_taxonomy_rank = [htr for htr in content_tax.pop(0).rstrip('\n').split('\t') if htr][1:]
                    # else:
                    #     header_taxonomy_highest = [hth for hth in content_tax.pop(0).rstrip('\n').split('\t') if hth][1:]
                    #     header_taxonomy_rank = [htr for htr in content_tax.pop(0).rstrip('\n').split('\t') if htr][1:]
                    # dico_taxonomy[tax_rank] = header_taxonomy_rank
                    for ct in content_tax:
                        if ct.startswith("Taxonomy_highest"):
                            header_taxonomy_highest = [hth for hth in ct.rstrip('\n').split('\t') if hth][1:]
                        elif ct.startswith("Taxonomy_intermediate"):
                            pass
                        elif ct.startswith(("Taxonomy_phylum", "Taxonomy_order", "Taxonomy_genus", "Taxonomy_family", "Taxonomy_class")):
                                header_taxonomy_rank = [htr for htr in ct.rstrip('\n').split('\t') if htr][1:]
                                dico_taxonomy[tax_rank] = header_taxonomy_rank
                        else:
                            lines_ct = [lct for lct in ct.rstrip('\n').split("\t") if lct]
                            if lines_ct:
                                sple_name_tax = lines_ct[0]
                                # print ft
                                sple_count_tax = [int(lct) for lct in lines_ct[1:]]
                                dico_count[tax_rank].append(sple_count_tax)
                                if tax_rank not in dico_count_seqs_by_samples:
                                    dico_count_seqs_by_samples[tax_rank] = {}
                                dico_count_seqs_by_samples[tax_rank].update({sple_name_tax:sum(sple_count_tax)})
                                if sple_name_tax not in list_samples:
                                    list_samples.append(sple_name_tax)
            #write highcharts_stack_bar_taxonomy
            stack_bar_taxonomy_file = self.merge_path_filename_format(self.website_dir+self.website_dir_taxonomy,"stack_bar_taxonomy","js")
            highcharts_stack_bar_taxonomy_to_write = open(stack_bar_taxonomy_file,"w+")
            for dc_k,dc_v in dico_count.items():
                order_matrix = self.transpose_matrix(dc_v)
                dico_part = {}
                #list of taxon inferior to the threshold_percent_min
                list_others_taxon_name = []
                list_others_taxon_count = []
                list_keep_taxon_name = []
                list_keep_taxon_count = []
                final_list_keep_taxon_name = []
                final_list_keep_taxon_count = []
                for i , om in enumerate(order_matrix):
                    list_samples_count_total = [dico_count_seqs_by_samples[dc_k][ls] for ls in list_samples]
                    count_div_total = [ai/bi for ai,bi in zip(om,list_samples_count_total)]
                    if all(cdt <= threshold_percent_min for cdt in count_div_total) == False:
                        list_keep_taxon_count.append(om)
                        list_keep_taxon_name.append(dico_taxonomy[dc_k][i])
                    elif dico_taxonomy[dc_k][i] == "Environmental":
                        list_keep_taxon_count.append(om)
                        list_keep_taxon_name.append(dico_taxonomy[dc_k][i])
                    elif dico_taxonomy[dc_k][i] == "Unclassified":
                        list_keep_taxon_count.append(om)
                        list_keep_taxon_name.append(dico_taxonomy[dc_k][i])
                    elif dico_taxonomy[dc_k][i] == "Unknown":
                        list_keep_taxon_count.append(om)
                        list_keep_taxon_name.append(dico_taxonomy[dc_k][i])
                    else:
                        list_others_taxon_count.append(om)
                        list_others_taxon_name.append(dico_taxonomy[dc_k][i])
                list_keep_taxon_count.append([sum(lotc) for lotc in zip(*list_others_taxon_count)])
                list_keep_taxon_name.append("Others")
                final_list_keep_taxon_count = list_keep_taxon_count
                final_list_keep_taxon_name = list_keep_taxon_name
                len_dc_k = len(final_list_keep_taxon_name)
                for flktc in final_list_keep_taxon_count:
                    if len(flktc) > len_max_sample_per_graph:
                        flktc_chunk = self.chunks_list(flktc,len_max_sample_per_graph)
                        for n,c in enumerate(flktc_chunk):
                            n = n +1
                            if n in dico_part:
                                dico_part[n].append(c)
                            else:
                                dico_part[n] = []
                                dico_part[n].append(c)
                    else:
                        if 1 in dico_part:
                            dico_part[1].append(flktc)
                        else:
                            dico_part[1] = []
                            dico_part[1].append(flktc)
                highcharts_stack_bar_taxonomy_to_write.write("var dict_{0} = {{\n".format(dc_k))
                for dp_k,dp_v in dico_part.items():
                    highcharts_stack_bar_taxonomy_to_write.write("\t'part_{0}' : [\n\t\t{{\n".format(dp_k))
                    for z,count_dp_v in enumerate(dp_v):
                        if z < len_dc_k-1:
                            if self.config.has_option("COLORS_TAXO", list_keep_taxon_name[z]):
                                color_tax = self.config.get('COLORS_TAXO', list_keep_taxon_name[z])
                                highcharts_stack_bar_taxonomy_to_write.write("\t\tname: '{0}',\n\t\tcolor: '{1}',\n\t\tdata: {2}\n\t\t}}, {{\n".format(list_keep_taxon_name[z],color_tax,count_dp_v))
                            else:
                                highcharts_stack_bar_taxonomy_to_write.write("\t\tname: '{0}',\n\t\tdata: {1}\n\t\t}}, {{\n".format(list_keep_taxon_name[z],count_dp_v))
                        else:
                            if self.config.has_option("COLORS_TAXO", list_keep_taxon_name[z]):
                                color_tax = self.config.get('COLORS_TAXO', list_keep_taxon_name[z])
                                highcharts_stack_bar_taxonomy_to_write.write("\t\tname: '{0}',\n\t\tcolor: '{1}',\n\t\tdata: {2}\n\t\t}}],\n".format(list_keep_taxon_name[z],color_tax,count_dp_v))
                            else:
                                highcharts_stack_bar_taxonomy_to_write.write("\t\tname: '{0}',\n\t\tdata: {1}\n\t\t}}],\n".format(list_keep_taxon_name[z],count_dp_v))
                highcharts_stack_bar_taxonomy_to_write.write("}};\n".format())
            list_samples_chunks = self.chunks_list(list_samples,len_max_sample_per_graph)
            list_samples_chunks = self.filter_samplenames(list_samples_chunks)
            highcharts_stack_bar_taxonomy_to_write.write("var array_part = {0};\n".format(["part_"+str(dpk) for dpk in xrange(1,len(list_samples_chunks)+1,1)]))
            highcharts_stack_bar_taxonomy_to_write.write("var categories_tax = {{".format())
            for num_lsc,lsc in enumerate(list_samples_chunks):
                highcharts_stack_bar_taxonomy_to_write.write("\tpart_{0} : {1},".format(num_lsc+1,lsc))
            highcharts_stack_bar_taxonomy_to_write.write("};")
            highcharts_stack_bar_taxonomy_to_write.close()
        else:
            print u" \u2717 TAXONOMY IS NOT COMPUTED.".encode('utf8')
            stack_bar_taxonomy_file = self.merge_path_filename_format(self.website_dir+self.website_dir_taxonomy,"stack_bar_taxonomy","js")
            highcharts_stack_bar_taxonomy_to_write = open(stack_bar_taxonomy_file,"w+")
            highcharts_stack_bar_taxonomy_to_write.write("var array_part = {};")
            highcharts_stack_bar_taxonomy_to_write.close()

    def create_figs_krona(self):
        '''
            Create krona files from matrix and taxonomy.
            ###todo if there is multiple matrix and taxo files
        '''
        files_clean_otu_matrix = self.list_files_from_dir(self.global_analysis_dir,"txt","Clean_OTU_matrix")[0]
        files_clean_taxo = self.list_files_from_dir(self.global_analysis_dir,"txt","Clean_OTU_taxo_assignment")[0]
        lines_clean_otu_matrix = open(files_clean_otu_matrix).readlines()
        lines_clean_taxo = open(files_clean_taxo).readlines()
        header_clean_otu_matrix = lines_clean_otu_matrix.pop(0).split()
        header_clean_taxo = lines_clean_taxo.pop(0)
        dico_clean_otu_matrix = {}
        dico_clean_taxo = {}
        for lct in lines_clean_taxo:
            lct = lct.split()
            otu_name = lct[0]
            otu_tax = "\t".join(lct[1:])
            regex_remove_parenthesis_content = re.compile('\(.+?\)')
            otu_tax_clean = regex_remove_parenthesis_content.sub('', otu_tax)
            dico_clean_taxo[otu_name] = otu_tax_clean
        for lcom in lines_clean_otu_matrix:
            lcom = lcom.split()
            samples_name = os.path.basename(lcom.pop(0)).split(os.extsep,1)[0].lower()
            ###write krona taxonomy files
            krona_taxonomy_file = self.merge_path_filename_format(self.krona_outputdir,"krona_taxonomy_"+samples_name,"csv")
            if not os.path.exists(self.krona_outputdir):
                os.makedirs(self.krona_outputdir)
            krona_taxonomy_to_write = open(krona_taxonomy_file,"w+")
            for lcom_e,lcom_otu in enumerate(lcom):
                krona_taxonomy_to_write.write("{0}\t{1}\n".format(lcom_otu,dico_clean_taxo[header_clean_otu_matrix[lcom_e]]))
            krona_taxonomy_to_write.close()

    def create_figs_index(self):
        '''
            Create index figs.
        '''
        file_index_independent = self.list_files_from_dir(self.analysis_dir+self.summary_dir,"txt","Indices_Independent_Clean")
        file_index_global = self.list_files_from_dir(self.analysis_dir+self.global_analysis_dir,"txt","Indices_Global_Clean")
        files_index = file_index_independent+file_index_global
        if files_index:
            print u" \u2713 ALPHA-DIVERSITY METRICS ARE COMPUTED.".encode('utf8')
            list_dissimilarity_index = []
            list_dissimilarity_index_wo_comma = []
            index_website_file = self.merge_path_filename_format(self.website_dir+self.website_dir_alpha_diversity,"index_data","js")
            index_website_to_write = open(index_website_file,"w+")
            lines_files_index_to_write = []
            dico_dissimilarity_index = {}
            for fi in files_index:
                dissimilarity_index = os.path.splitext(os.path.basename(fi))[0].split("_")[1]
                dissimilarity_index_wo_comma = dissimilarity_index.replace(".","_")
                list_dissimilarity_index.append(dissimilarity_index)
                list_dissimilarity_index_wo_comma.append(dissimilarity_index_wo_comma)
                dico_dissimilarity_index[dissimilarity_index_wo_comma] = []
                with open(fi) as f:
                    content_index = f.readlines()
                    header_content_index = content_index.pop(0).rstrip("\n").split("\t")
                    for ci_e,ci in enumerate(content_index):
                        ci = ci.split()
                        sample_index = ci[0]
                        sample_index = self.filter_samplenames(sample_index)
                        filename_index = ci[1]
                        data_index = [float(ci_di) for ci_di in ci[2:]]
                        ci_filter = [sample_index]+[filename_index]+data_index
                        tmp_list_data_dissimilarity_index= []
                        dico_dissimilarity_index[dissimilarity_index_wo_comma].append(ci_filter)

            for ldiwc_i, ldiwc in enumerate(list_dissimilarity_index_wo_comma):
                index_file_size = self.get_file_size(files_index[ldiwc_i])
                index_website_to_write.write("var table_{0} = {{\n\t'path_file': '{1}',\n\t'size_file': '{2}',\n".format(ldiwc,files_index[ldiwc_i],index_file_size))
                # index_website_to_write.write("var table_{0} = {{\n".format(ldiwc))
                index_website_to_write.write("\t'dissimilarity_index' : '{0}',\n".format(ldiwc))
                index_website_to_write.write("\t'data': [\n\t{\n")
                for ldiwc_e,ldiwc_c in enumerate(dico_dissimilarity_index[ldiwc]):
                    for ldiwc_e_e, ldiwc_c_c in enumerate(ldiwc_c):
                        index_website_to_write.write("\t\t'{0}': '{1}',\n".format(header_content_index[ldiwc_e_e],ldiwc_c_c))
                    if ldiwc_e < len(dico_dissimilarity_index[ldiwc])-1:
                        index_website_to_write.write("\t},\n\t{\n")
                    else:
                        index_website_to_write.write("\t}\n\t]\n}\n")

            #render for the first sample
            index_website_to_write.write("var array_dissimilarity_index = {0};\n".format(list_dissimilarity_index_wo_comma))
            dissimilarity_index1 = list_dissimilarity_index_wo_comma.pop(0)
            index_website_to_write.write("var template_dissimilarity_index1 = $('#message-template-dissimilarity_index1').html();\n")
            index_website_to_write.write("var html_{0} = Mustache.render(template_dissimilarity_index1, table_{0});\n".format(dissimilarity_index1))
            index_website_to_write.write("$('.indexes-dissimilarity_index1').append(html_{0});\n".format(dissimilarity_index1))
            if len(list_dissimilarity_index) >1:
                #render for the others list_samples
                index_website_to_write.write("var template_dissimilarity_index_others = $('#message-template-dissimilarity_index-others').html();\n")
                for ldiwc in list_dissimilarity_index_wo_comma:
                    index_website_to_write.write("var html_{0} = Mustache.render(template_dissimilarity_index_others, table_{0});\n".format(ldiwc))
                    index_website_to_write.write("$('.indexes-dissimilarity_index').append(html_{0});\n".format(ldiwc))
            index_website_to_write.close()
        else:
            print u" \u2717 ALPHA-DIVERSITY METRICS ARE NOT COMPUTED.".encode('utf8')
            index_website_file = self.merge_path_filename_format(self.website_dir+self.website_dir_alpha_diversity,"index_data","js")
            index_website_to_write = open(index_website_file,"w+")
            index_website_to_write.write("var table_Independent = {};")
            index_website_to_write.close()

    def create_figs_rank_abundance(self):
        '''
            Create rank_abundance figs.
        '''
        files_rank_abundance = self.list_files_from_dir(self.analysis_dir+self.curves_data_dir,"clust","Rank-abundance_curve")
        if files_rank_abundance:
            print u" \u2713 RANK ABUNDANCES CURVES ARE COMPUTED.".encode('utf8')
            len_max_sample_per_graph = 10
            files_rank_abundance_chunck = self.chunks_list(files_rank_abundance,len_max_sample_per_graph)
            files_rank_abundance_name = [os.path.splitext(os.path.basename(fra_n))[0].replace("Rank-abundance_curve_Clean_0.05_Clust_Align_Rdm_Derep_Rdm_Derep_Clean_Hunt_Derep_Hpreprocess_","") for fra_n in files_rank_abundance]
            files_rank_abundance_name_chuncks = self.chunks_list(files_rank_abundance_name,len_max_sample_per_graph)
            rank_abundance_website_file = self.merge_path_filename_format(self.website_dir+self.website_dir_alpha_diversity,"rank_abundance_data","js")
            rank_abundance_website_to_write = open(rank_abundance_website_file,"w+")
            rank_abundance_website_to_write.write("var dict_rank_abundance = {\n")
            for frac_e, frac in enumerate(files_rank_abundance_chunck):
                rank_abundance_website_to_write.write("\t'part_{0}': [\n\t{{\n".format(frac_e+1))
                for sub_frac_e,sub_frac in enumerate(frac):
                    with open(sub_frac) as f:
                        content_rank_abundance = f.readlines()
                        sample_name = os.path.splitext(os.path.basename(sub_frac))[0].replace("Rank-abundance_curve_Clean_0.05_Clust_Align_Rdm_Derep_Rdm_Derep_Clean_Hunt_Derep_Hpreprocess_","")
                        sample_name = self.filter_samplenames(sample_name)
                        tmp_content_rank_abundance = []
                        for cra in content_rank_abundance:
                            y = float(cra.rstrip("\n").split("\t")[1])
                            tmp_content_rank_abundance.append(y)
                    rank_abundance_website_to_write.write("\t\tname: '{0}',\n\t\tdata: {1}\n".format(sample_name, tmp_content_rank_abundance))
                    if sub_frac_e < len(frac)-1:
                        rank_abundance_website_to_write.write("\t\t},{\n")
                    else:
                        rank_abundance_website_to_write.write("\t\t}],\n")
            rank_abundance_website_to_write.write("};\n")
            rank_abundance_website_to_write.write("var array_part_rank_abundance = {0};\n".format(["part_"+str(frac_n) for frac_n in xrange(1,len(files_rank_abundance_chunck),1)]))
            rank_abundance_website_to_write.write("var categories_rank_abundance = {{".format())
            for franc_e,franc in enumerate(files_rank_abundance_name_chuncks):
                rank_abundance_website_to_write.write("\tpart_{0} : {1},".format(franc_e+1,franc))
            rank_abundance_website_to_write.write("};")
            rank_abundance_website_to_write.close()
        else:
            print u" \u2717 RANK ABUNDANCES CURVES ARE NOT COMPUTED.".encode('utf8')
            rank_abundance_website_file = self.merge_path_filename_format(self.website_dir+self.website_dir_alpha_diversity,"rank_abundance_data","js")
            rank_abundance_website_to_write = open(rank_abundance_website_file,"w+")
            rank_abundance_website_to_write.write("var dict_rank_abundance = {};")
            rank_abundance_website_to_write.close()

    def create_figs_rarefaction_curve(self):
        '''
            Create rarefaction_curve figs.
        '''
        files_rarefaction_curve = self.list_files_from_dir(self.analysis_dir+self.curves_data_dir,"clust","Rarefactcurve")
        if files_rarefaction_curve:
            print u" \u2713 RAREFACTION CURVES ARE COMPUTED.".encode('utf8')
            len_max_sample_per_graph = 10
            files_rarefaction_curve_chunck = self.chunks_list(files_rarefaction_curve,len_max_sample_per_graph)
            files_rarefaction_curve_name = [os.path.splitext(os.path.basename(fra_n))[0].replace("Rarefactcurve_Clean_0.05_Clust_Align_Rdm_Derep_Rdm_Derep_Clean_Hunt_Derep_Hpreprocess_","") for fra_n in files_rarefaction_curve]
            files_rarefaction_curve_name_chuncks = self.chunks_list(files_rarefaction_curve_name,len_max_sample_per_graph)
            rarefaction_curve_website_file = self.merge_path_filename_format(self.website_dir+self.website_dir_alpha_diversity,"rarefaction_curve_data","js")
            rarefaction_curve_website_to_write = open(rarefaction_curve_website_file,"w+")
            rarefaction_curve_website_to_write.write("var dict_rarefaction_curve = {\n")
            for frac_e, frac in enumerate(files_rarefaction_curve_chunck):
                rarefaction_curve_website_to_write.write("\t'part_{0}': [\n\t{{\n".format(frac_e+1))
                for sub_frac_e,sub_frac in enumerate(frac):
                    with open(sub_frac) as f:
                        content_rarefaction_curve = f.readlines()
                        sample_name = os.path.splitext(os.path.basename(sub_frac))[0].replace("Rarefactcurve_Clean_0.05_Clust_Align_Rdm_Derep_Rdm_Derep_Clean_Hunt_Derep_Hpreprocess_","")
                        sample_name = self.filter_samplenames(sample_name)
                        tmp_content_rarefaction_curve = []
                        list_x = []
                        for cra in content_rarefaction_curve:
                            x = float(cra.rstrip("\n").split("\t")[0])
                            y = float(cra.rstrip("\n").split("\t")[1])
                            list_x.append(x)
                            tmp_content_rarefaction_curve.append(y)
                        list_x = sorted(list_x)
                        tmp_content_rarefaction_curve = sorted(tmp_content_rarefaction_curve)
                    rarefaction_curve_website_to_write.write("\t\tname: '{0}',\n\t\tdata: {1}\n".format(sample_name, tmp_content_rarefaction_curve))
                    if sub_frac_e < len(frac)-1:
                        rarefaction_curve_website_to_write.write("\t\t},{\n")
                    else:
                        rarefaction_curve_website_to_write.write("\t\t}],\n")
            rarefaction_curve_website_to_write.write("};\n")
            rarefaction_curve_website_to_write.write("var x_rarefaction_curve = {0};\n".format(list_x))
            rarefaction_curve_website_to_write.write("var array_part_rarefaction_curve = {0};\n".format(["part_"+str(frac_n) for frac_n in xrange(1,len(files_rarefaction_curve_chunck),1)]))
            rarefaction_curve_website_to_write.write("var categories_rarefaction_curve = {{".format())
            for franc_e,franc in enumerate(files_rarefaction_curve_name_chuncks):
                rarefaction_curve_website_to_write.write("\tpart_{0} : {1},".format(franc_e+1,franc))
            rarefaction_curve_website_to_write.write("};")
            rarefaction_curve_website_to_write.close()
        else:
            print u" \u2717 RAREFACTION CURVES ARE NOT COMPUTED.".encode('utf8')
            rarefaction_curve_website_file = self.merge_path_filename_format(self.website_dir+self.website_dir_alpha_diversity,"rarefaction_curve_data","js")
            rarefaction_curve_website_to_write = open(rarefaction_curve_website_file,"w+")
            rarefaction_curve_website_to_write.write("var dict_rank_abundance = {};")
            rarefaction_curve_website_to_write.close()

    def create_figs_unifrac_tree(self):
        '''
            Create unifrac_tree figs.
        '''
        if os.path.isfile(self.analysis_dir+self.unifrac_tree_data_file) :
            print u" \u2713 UNIFRAC_TREE IS COMPUTED.".encode('utf8')
            unifrac_tree_website_file = self.merge_path_filename_format(self.website_dir+self.website_dir_beta_diversity,"unifrac_tree_data","js")
            unifrac_tree_website_to_write = open(unifrac_tree_website_file,"w+")
            # hunting_website_to_write.write("var table_hunting = {{\n\t'path_file': '{0}',\n\t'size_file': '{1}',\n\t'sample': [\n\t{{".format(self.hunting_file,hunting_file_size))
            unifrac_tree_website_to_write.write('window.onload = function(){\n')
            with open(self.analysis_dir+self.unifrac_tree_data_file) as f:
                content_unifrac_tree = f.readlines()[0]
                rx = r'[(),]+([^;:]+)\b'
                res = re.findall(rx, content_unifrac_tree)
                list_before = []
                list_after = []
                for val in res:
                    list_before.append(val)
                    list_after.append(val.split("_FLASH_PrinSeq")[0].replace("Filter",""))
                for e_la, la in enumerate(list_after):
                    content_unifrac_tree = content_unifrac_tree.replace(list_before[e_la],la)
                unifrac_tree_website_to_write.write("var dataObject = {{ newick:'{0}' }};".format(content_unifrac_tree))
            unifrac_tree_website_to_write.write("phylocanvas = new Smits.PhyloCanvas(\ndataObject,\n'svgCanvas',\n500, 500\n);\n};\n")
            unifrac_tree_website_to_write.close()
        else:
            print u" \u2717 UNIFRAC_TREE IS NOT COMPUTED.".encode('utf8')
            unifrac_tree_website_file = self.merge_path_filename_format(self.website_dir+self.website_dir_beta_diversity,"unifrac_tree_data","js")
            unifrac_tree_website_to_write = open(reclustor_website_file,"w+")
            unifrac_tree_website_to_write.write('window.onload = function(){\n')
            unifrac_tree_website_to_write.write("var dataObject = {};\n};\n")
            unifrac_tree_website_to_write.close()

    def create_figs_unifrac_pd(self):
        '''
            Create unifrac PD metric.
        '''
        if os.path.isfile(self.analysis_dir+self.unifrac_dir+"Phylogenetic_Diversity_distance.txt"):
            print u" \u2713 PHYLOGENETIC DIVERSITY METRIC IS COMPUTED.".encode('utf8')
            unifrac_pd_website_file = self.merge_path_filename_format(self.website_dir+self.website_dir_beta_diversity,"unifrac_pd_data","js")
            unifrac_pd_website_to_write = open(unifrac_pd_website_file,"w+")
            unifrac_pd_file_size = self.get_file_size(self.analysis_dir+self.unifrac_dir+"Phylogenetic_Diversity_distance.txt")
            unifrac_pd_website_to_write.write("var table_unifrac_pd = {{\n\t'path_file': '{0}',\n\t'size_file': '{1}',\n\t'data': [\n\t{{".format(self.analysis_dir+self.unifrac_dir+"Phylogenetic_Diversity_distance.tsv",unifrac_pd_file_size))
            with open(self.analysis_dir+self.unifrac_dir+"Phylogenetic_Diversity_distance.txt") as f:
                content_pd = f.readlines()
                content_pd_samples = content_pd.pop(0).rstrip("\n").split("\t")[1:]
                for cpd in content_pd:
                    pd_value = map(str,cpd.rstrip("\n").split("\t")[1:])
                    for pdv_e, pdv in enumerate(pd_value):
                        samplePD = self.filter_samplenames(content_pd_samples[pdv_e])
                        unifrac_pd_website_to_write.write("\t'Sample': '{0}',\n\t'Value': '{1}'".format(samplePD,pdv))
                        if pdv_e < len(content_pd_samples)-1:
                            unifrac_pd_website_to_write.write("\t},\n\t{\n")
                        else:
                            unifrac_pd_website_to_write.write("\t}\n\t]\n}\n")
            #render for the list_samples
            unifrac_pd_website_to_write.write("var template_unifrac_pd = $('#message-template-unifrac_pd').html();\n")
            unifrac_pd_website_to_write.write("var html_unifrac_pd = Mustache.render(template_unifrac_pd, table_unifrac_pd);\n")
            unifrac_pd_website_to_write.write("$('.samples-unifrac_pd').append(html_unifrac_pd);\n")
            unifrac_pd_website_to_write.close()
        else:
            print u" \u2717 PHYLOGENETIC DIVERSITY METRIC IS NOT COMPUTED.".encode('utf8')

    def get_databases_tax(self):
        db_to_keep = "{0}{1}/{2}/Structures/".format(self.databases_dir,self.config.get('RECOVERING', 'organism_studied').upper(),self.config.get('RECOVERING', 'database_taxonomic_assignment').upper())
        db_files = self.list_files_from_dir(db_to_keep,"txt","")
        dico_db = {}
        for dbf in db_files:
            dbf_rank = os.path.splitext(os.path.basename(dbf))[0]
            dico_db[dbf_rank] = {}
            with open(dbf) as f:
                content_db = f.readlines()
                for cdb in content_db:
                    parent_tax,child_tax = cdb.split()
                    dico_db[dbf_rank].update({child_tax:parent_tax})
        child_tax_to_parent_tax = dico_db["order"]["Thermococcales"]
        print child_tax_to_parent_tax

    def create_figs_folders_tree(self):
        '''
            Create jstree figs.
        '''
        print u" \u2713 FOLDERS_TREE IS COMPUTED.".encode('utf8')
        jstree_file_website = self.merge_path_filename_format(self.website_dir+self.website_dir_parameters_pipe,"jstree_data","js")
        jstree_file_website_to_write = open(jstree_file_website,"w+")
        dico_treejs = self.create_JSTree(self.analysis_dir)
        dico_treejs["text"] = "BIOCOM-PIPE/ ({0})".format(self.get_file_size(self.analysis_dir))
        dico_treejs["state"] = {'opened' : 'true', 'selected' : 'true' }
        jstree_file_website_to_write.write("var jsonData = [\n{0}\n];".format(pprint.pformat(dico_treejs)))
        jstree_file_website_to_write.close()

    def compress_files(self):
        '''
            Compress files from Results directory.
        '''
        for rf in glob.glob(self.analysis_dir+self.results_dir+'*/*.*'):
            if self.format_compress_results_files not in rf:
                cmd_compress_files = ""
                if self.format_compress_results_files == "gz":
                    cmd_compress_files = "gzip {0}".format(rf)
                if self.format_compress_results_files == "tar.gz":
                    cmd_compress_files = "tar czvf {0}".format(rf)
                os.system(cmd_compress_files)
                #print cmd_compress_files
                print u" \u2713 The '{0}' has been compressed into '{0}.{1}'.".format(rf,self.format_compress_results_files).encode('utf8')

# if __name__ == '__main__':
#     run_utils = BiocomPipeUtils()
#     run_utils.extract_compress_files()
#     run_utils.get_sample_names()
#     run_utils.create_preprocess_mids()
#     run_utils.create_input_file()
    # if run_utils.compress_results_files == "yes":
    #     run_utils.compress_files()
