# -*- coding: utf-8 -*-
import sys
import biocomPipe_utils

python_script, path_biocom_pipe, path_analysis = sys.argv
# print 'Number of arguments:', len(sys.argv), 'arguments.'
# print 'Argument List:', str(sys.argv)

DIR_BIOCOM_PIPE = path_biocom_pipe
DIR_BIOCOM_PIPE_ANALYSIS = path_analysis


if __name__ == '__main__':
    run_prep_files = biocomPipe_utils.BiocomPipeUtils(DIR_BIOCOM_PIPE,DIR_BIOCOM_PIPE_ANALYSIS)
    print "===Step 1: Preparation of files===".format()
    if run_prep_files.check_raw_data_files == "yes":
        print "#Check the Raw_data folder."
        run_prep_files.check_raw_data_dir()
    if run_prep_files.clean_project_files == "yes":
        print "#Check and clean the Project_files folder."
        run_prep_files.clean_project_files_dir()
    # if run_prep_files.decompress_raw_data_files == "yes":
    #     print "#Try to extract rawfiles:"
    #     run_prep_files.decompress_files()
    # #si PRINSEQ ou FLASH is to do
    if run_prep_files.check_project_file_exist()==True and run_prep_files.check_project_file_data("TARGET")==True:
        print "#Creation of the Input.txt file containing the different steps of the pipeline."
        run_prep_files.set_input_file()
        run_prep_files.create_input_file()
