#!/usr/bin/ python2.7

#================================================================================================================================================
#         FILE:  Unifrac_computation_1.1.py
#        USAGE:  python2.7 Unifrac_computation_1.1.py
#  DESCRIPTION:  Dedicated program to import the Newick Tree and the ID Mapping File from the previous step (GLOBAL_ANALYSIS) to compute several
#                matematical tests (Unifrac significance, P-Tests, PD computations, PCoA, dendrogram, etc...) on them, and give back to the user
#                these results formatted for another use.
#
#                1.1 Modifs (23/07/2018): All steps specific to the ptests or statistical tests integrated by the Pycogent library were removed
#                to simplify the program and the Input.txt file. The lines and functions have been kept, as the user can realize it if needed.
#
# REQUIREMENTS:  Python language (version 2.5 or later)
#       AUTHOR:  Sebastien TERRAT <sebastien.terrat@inra.fr>
#      COMPANY:  UMR 1347 Agroecologie
#      VERSION:  1.1
#    REALEASED:  17/07/2013
#     REVISION:  23/07/2018
#================================================================================================================================================
#     This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
#================================================================================================================================================


#Needed libraries
import sys
import Library
import types
from cogent.parse.tree import DndParser
from cogent.maths.unifrac.fast_unifrac import fast_unifrac, PD_whole_tree, fast_unifrac_permutations_file, fast_p_test_file
from cogent.maths.unifrac.fast_tree import UniFracTreeNode, count_envs

#================================================================================================================================================
# DEFINED FUNCTIONS
#================================================================================================================================================

def define_user_choices():
    """Treat and dispatch all input arguments given by the main program.

    Arguments: None.

    Note: Simple function to treat and dispatch all user choices given by the main program for all other functions. Indeed,
    this function returns all formatted choices for further use in different functions, like fast_unifrac, or PD_whole_tree."""

    #Catch and store in user_choices all input arguments
    user_choices = sys.argv
    #We format all user choices to avoid misleadings
    user_choices = [user_choice.upper() for user_choice in user_choices]
    #We then store all choices in dedicated variables
    unifrac_choice = user_choices[1]
    PD_choice = user_choices[2]
    #unifrac_test_wtree_choice = user_choices[3]
    #unifrac_test_pairwise_choice = user_choices[4]
    #unifrac_test_individuals_choice = user_choices[5]
    #ptest_test_wtree_choice = user_choices[6]
    #ptest_test_pairwise_choice = user_choices[7]
    #nb_permutations = int(user_choices[8])
    if user_choices[3] == "YES":
        user_weight_choice = True
    else:
        user_weight_choice = False
    #All choices are then returned to the main program
    return (unifrac_choice, PD_choice, user_weight_choice)

def catch_needed_input_files():
    """Returns the tree and ID mapping data found in the dedicated files.

    Arguments: None.

    Note: Simple function to automatically open and store data from the two needed files: the Newick tree file and
    the ID mapping file."""

    #We first store the paths and the names of needed files (tree and ID mapping file)
    input_tree_file_name = 'Summary_files/Global_Analysis/Unifrac_data/Clean_Tree_Concatenated_reads.fasta.fasta'
    input_mapping_file_name = 'Summary_files/Global_Analysis/Unifrac_data/Clean_ID_mapping_file.txt'
    #We then catch the tree itself
    input_tree_file = open(input_tree_file_name, 'r')
    tree_str = input_tree_file.readline()
    input_tree_file.close()
    #We also catch the elements corresponding to the ID mapping file
    input_mapping_file = open(input_mapping_file_name, 'r')
    mapping_lines = input_mapping_file.readlines()
    input_mapping_file.close()
    #We finally return the two catched elements
    return (tree_str, mapping_lines)

def compute_print_PD(tree, envs):
    """Compute and give formatted results.

    Arguments:
    - tree: A tree element from a Newick tree after the Dndparser function formatting,
    - envs: the ID mapping file formatted by the count_envs function.

    Notes: This function was developed to easily compute PD values for all treated samples and print
    them in a dedicated file."""

    #We launch the test
    PD_envs, PD_values = PD_whole_tree(tree, envs)
    #We format the results
    PD_envs = [PD_env.replace('#','_') for PD_env in PD_envs]
    #We store them in the dedicated result file
    PD_file = open('Summary_files/Global_Analysis/Unifrac_data/Phylogenetic_Diversity_distance.txt', 'w')
    PD_file.write('Samples\t'+'\t'.join(PD_envs)+'\n')
    PD_file.write('Phylogenetic Diversity\t'+'\t'.join(map(str,PD_values))+'\n')
    PD_file.close()

def print_dist_matrix(dist_matrix):
    """Simply create a result file formatted as needed using the distance_matrix element from the fast_unifrac function.

    Arguments:
    - 'dist_matrix': a tuple with a numpy array of pairwise distances between samples and a list of names describing
    the order of samples in the array.

    Note: Simple function to generate easily a result file with the given distance_matrix previously computed by the
    fast_unifrac function."""

    #We catch and format the sample names
    sample_names = dist_matrix[1]
    sample_names = [sample_name.replace('#','_') for sample_name in sample_names]
    #We also create the result file
    dist_matrix_file = open('Summary_files/Global_Analysis/Unifrac_data/Unifrac_distance_matrix.txt', 'w')
    #We first write the needed headers (sample_names)
    dist_matrix_file.write('\t'+'\t'.join(sample_names)+'\n')
    i = 0
    #We then print all formatted results
    for values in dist_matrix[0]:
        dist_matrix_file.write(sample_names[i]+'\t')
        dist_matrix_file.write('\t'.join(map(str,values))+'\n')
        i = i + 1
    #We finally close the result file
    dist_matrix_file.close()

def print_dendrogram(dendrogram):
    """Simply create a result file formatted as needed using the dendrogram element from the fast_unifrac function.

    Arguments:
    - 'cluster_envs': a string from the cogent.core.PhyloNode object containing results of running UPGMA on the
    distance matrix."""

    #After formatting the results, we print it in the dedicated result file
    dendrogram = dendrogram.replace('#', '_')
    dendro_samples_file = open('Summary_files/Global_Analysis/Unifrac_data/Unifrac_Cluster_samples.tre', 'w')
    dendro_samples_file.write(dendrogram)
    dendro_samples_file.close()

def print_pcoa(pcoa):
    """Simply create a result file formatted as needed using the pcoa element from the fast_unifrac function.

    Arguments:
    - 'pcoa': a cogent.util.Table object with the results of running Principal Coordines Analysis on the distance
    matrix.

    Notes: the cogent.util.Table given as input is first transformed as a string to print easily formatted results."""

    #After formatting the results, we print it in the dedicated result file
    pcoa_results = pcoa.tostring(sep='\t', format='')
    pcoa_results = pcoa_results.replace(' ','')
    pcoa_results = pcoa_results.replace('#', '_')
    pcoa_samples_file = open('Summary_files/Global_Analysis/Unifrac_data/Unifrac_PCoA.txt', 'w')
    pcoa_samples_file.write(pcoa_results)
    pcoa_samples_file.close()

def print_whole_tree_test(test_result, type_of_test):
    """Create and format the result file given for the user for the significance test on the whole tree.

    Arguments:
    - test_result: tuple containing results obtained after significance test,
    - type_of_test: string defining the realized test (Unifrac or P-Test)."""

    #We first define the filename, using the type_of_test
    filename = 'Summary_files/Global_Analysis/Unifrac_data/'+type_of_test+'_wholetree.txt'
    #We create the result file
    output_file = open(filename, 'w')
    #Based on the type_of_test, we write the neededheaders
    if "Unifrac" in type_of_test:
        output_file.write('Choice\tUncorrected p-value\tCorrected p-value\n')
    else:
        output_file.write('Choice\tP-Test value\n')
    #We then write results and close the file
    output_file.write('\t'.join(map(str, test_result[0]))+'\n')
    output_file.close()

def print_pairwise_tests(test_result, type_of_test):
    """Create and format the result file given for the user for the significance test on pairwise samples.

    Arguments:
    - test_result: tuple containing results obtained after significance test,
    - type_of_test: string defining the realized test (Unifrac or P-Test)."""

    #Needed defined variables
    line = str()
    samples = list()
    #We first define the filenames, using the type_of_test
    output_filename_raw = 'Summary_files/Global_Analysis/Unifrac_data/'+type_of_test+'_pairwise_raw.txt'
    output_filename_corr = 'Summary_files/Global_Analysis/Unifrac_data/'+type_of_test+'_pairwise_corrected.txt'
    #We create the result files
    output_file_raw = open(output_filename_raw, 'w')
    output_file_corr = open(output_filename_corr, 'w')
    #For each tuple found in the array given as input, we will analyze, sort and format all headers
    for single_result in test_result:
        if single_result[0] not in line:
            line = line+'\t'+single_result[0]
            samples.append(single_result[0])
        if single_result[1] not in line:
            line = line+'\t'+single_result[1]
            samples.append(single_result[1])
    #We format all sample names when we store them
    output_file_raw.write(line.replace("#","_")+'\n')
    output_file_corr.write(line.replace("#","_")+'\n')
    needed_tab = str()
    #For each tuple found in the array given as input, we will analyze, sort and format all results
    for sample in samples:
        line_raw = '0.0\t'
        line_corr = '0.0\t'
        needed_tab = needed_tab + '\t'
        for single_result in test_result:
            if single_result[0] in sample:
                if type(single_result[2]) is types.StringType:
                    line_raw = line_raw + single_result[2] + '\t'
                else:
                    line_raw = line_raw + '%1.4f' % single_result[2] + '\t'
                if type(single_result[3]) is types.StringType:
                    line_corr = line_corr + single_result[3] + '\t'
                else:
                    line_corr = line_corr + '%1.4f' % single_result[3] +'\t'
        #We print all results in the dedicated files
        output_file_raw.write(sample.replace("#","_")+needed_tab+line_raw+'\n')
        output_file_corr.write(sample.replace("#","_")+needed_tab+line_corr+'\n')
    #We finally close the two result files
    output_file_raw.close()
    output_file_corr.close()

def print_unifrac_test_envs(unifrac_test_result):
    """Create and format the result files given for the user for the Unifrac significance test on individual samples.

    Arguments:
    - unifrac_test_result: tuple containing results obtained after Unifrac significance test."""

    #We first create the result file and add the needed headers
    unifrac_significance_file = open('Summary_files/Global_Analysis/Unifrac_data/Unifrac_Significance_individuals.txt', 'w')
    unifrac_significance_file.write('Samples\tUncorrected p-value\tCorrected p-value\n')
    #For all results found in the array given as input, we format and print them in the result file
    for single_result in unifrac_test_result:
        single_result = str(single_result)
        single_result = single_result.replace('#', '_')
        single_result = single_result.replace('\'', '')
        single_result = single_result.replace("(", '')
        single_result = single_result.replace(")", '\n')
        single_result = single_result.replace(', ', '\t')
        unifrac_significance_file.write(single_result)
    #We finally close the result file
    unifrac_significance_file.close()

def compute_unifrac_significance(test_on, nb_permutations, user_weight_choice):
    """Return the result given by the unifrac significance test based on chosen parameters.

    Arguments:
    - test_on: string defining if the test is realized on the whole tree (Tree), for all couple of samples
    (Pairwise), or for each sample against all other samples (Envs),
    - nb_permutations: Number of iterations defined by the user,
    - user_weight_choice: string defining if the computation is done with weighted and normalized values (correct),
    weighted but not normalized values (True), or not weighted values (False)."""

    input_tree_file_name = open('Summary_files/Global_Analysis/Unifrac_data/Clean_Tree_Concatenated_reads.fasta.fasta')
    input_mapping_file_name = open('Summary_files/Global_Analysis/Unifrac_data/Clean_ID_mapping_file.txt')
    unifrac_test_result = fast_unifrac_permutations_file(input_tree_file_name, input_mapping_file_name, weighted=user_weight_choice,
    num_iters = nb_permutations, test_on=test_on)
    input_tree_file_name.close()
    input_mapping_file_name.close()
    return unifrac_test_result

def compute_ptest_significance(test_on, nb_permutations):
    """Return the result given by the P-Test (Martin 2002) test based on chosen parameters.

    Arguments:
    - test_on: string defining if the test is realized on the whole tree (Tree), for all couple of samples
    (Pairwise), or for each sample against all other samples (Envs),
    - nb_permutations: Number of iterations defined by the user."""

    input_tree_file_name = open('Summary_files/Global_Analysis/Unifrac_data/Clean_Tree_Concatenated_reads.fasta.fasta')
    input_mapping_file_name = open('Summary_files/Global_Analysis/Unifrac_data/Clean_ID_mapping_file.txt')
    ptest_result = fast_p_test_file(input_tree_file_name, input_mapping_file_name, num_iters=nb_permutations, test_on=test_on)
    input_tree_file_name.close()
    input_mapping_file_name.close()
    return ptest_result

#================================================================================================================================================
# MAIN PROGRAM
#================================================================================================================================================

#First, we catch and store all chosen parameters given by the main program
(unifrac_choice, PD_choice, user_weight_choice) = define_user_choices()

#Catch needed information using the dedicated function and put in format the input
#files using pycogent functions (DndParser and count_envs)
(tree_str, mapping_lines) = catch_needed_input_files()
tree = DndParser(tree_str, UniFracTreeNode)
envs = count_envs(mapping_lines)

#We first compute and store the Phylogenetic diversities of all samples
if PD_choice == "YES":
    compute_print_PD(tree, envs)

#Compute all unifrac analysis using the fast_unifrac function (weighted AND/OR normalized, OR not weighted)
if unifrac_choice == "YES":
    if user_weight_choice == 'correct' or user_weight_choice == True:
        unifrac_result = fast_unifrac(tree, envs, metric = 'weighted_unifrac', weighted = user_weight_choice)
    else:
        unifrac_result = fast_unifrac(tree, envs, weighted = user_weight_choice)
    #Using dedicated functions, we print results for the user into several summary_files
    print_dist_matrix(unifrac_result['distance_matrix'])
    print_dendrogram(str(unifrac_result['cluster_envs']))
    print_pcoa(unifrac_result['pcoa'])

#We quit properly the program
sys.exit(0)

#Realization of the Unifrac significance test on the whole tree and creation of the result file
#if unifrac_test_wtree_choice == "YES":
#    unifrac_test_result = compute_unifrac_significance("Tree", nb_permutations, user_weight_choice)
#    print_whole_tree_test(unifrac_test_result, "Unifrac_Significance")
#Realization of the Unifrac significance test on pairwise samples and creation of the result file
#if unifrac_test_pairwise_choice == "YES":
#    unifrac_test_result = compute_unifrac_significance("Pairwise", nb_permutations, user_weight_choice)
#    print_pairwise_tests(unifrac_test_result, "Unifrac_Significance")
#Realization of the Unifrac significance test on the individual samples and creation of the result file
#if unifrac_test_individuals_choice == "YES":
#    unifrac_test_result = compute_unifrac_significance("Envs", nb_permutations, user_weight_choice)
#    print_unifrac_test_envs(unifrac_test_result)
#Realization of the P-Test significance test on the whole tree and creation of the result file
#if ptest_test_wtree_choice == "YES":
#    ptest_result = compute_ptest_significance("Tree", nb_permutations)
#    print_whole_tree_test(ptest_result, "P_Test")
#Realization of the P-Test significance test on pairwise samples and creation of the result file
#if ptest_test_pairwise_choice == "YES":
#    ptest_result = compute_ptest_significance("Pairwise", nb_permutations)
#    print_pairwise_tests(ptest_result, "P_Test")
